﻿using HoloBase.Texts.Urr;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UnitTests.Texts.Urr
{
    [TestClass]
    public class UndoRedoManagerTest
    {
        [TestMethod]
        public void TestUndoRedo()
        {
            var manager = new UndoRedoManager();
            // manager.AutoCombine = false;

            var opSuc = false;
            var content = "";

            content = "ab";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "abcdef";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "abcde";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "abcdi";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "abcxi";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "abcxy";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "zbcxy";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "zb1234567890hiy";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "zabaxchiy";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "zabaxchiuvw";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "zaxchiuvw";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = "123uvw";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);



            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.CurrentContent;
            Debug.WriteLine("CurrentContent: content = {0}", content);



            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.CurrentContent;
            Debug.WriteLine("CurrentContent: content = {0}", content);

        }


        [TestMethod]
        public void TestUndoRedoWithAutoCombine()
        {
            var manager = new UndoRedoManager();
            manager.AutoCombine = true;

            var opSuc = false;
            var content = "";

            content = "ab";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abc";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd ";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd e";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd e";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd ef";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ij";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ijk";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ijk ";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ijk m";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ijk mn ";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ijk mn pq";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ijk m";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ijk ";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ijk";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ij";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg i";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg ";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd efg";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd ef";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd e";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd ";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abcd";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);

            content = "abc";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}||", opSuc, content);



            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}||", content);

            content = manager.CurrentContent;
            Debug.WriteLine("CurrentContent: content = {0}||", content);



            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}||", content);

            content = manager.CurrentContent;
            Debug.WriteLine("CurrentContent: content = {0}||", content);

        }


        [TestMethod]
        public void TestUndoRedoLargeText()
        {
            var manager = new UndoRedoManager();
            // manager.AutoCombine = false;

            var opSuc = false;
            var content = "";

            //var str1 = "abc\r\n";
            //Debug.WriteLine("abc\r\n = {0}.", str1.Length);
            //var str2 = "abc\n";
            //Debug.WriteLine("abc\n = {0}.", str2.Length);

            
            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;

4 And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;

5 And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;

4 And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;

5 And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;

6 And Jesse begat David the king; and David the king begat Solomon of her that had been the wife of Urias;

7 And Solomon begat Roboam; and Roboam begat Abia; and Abia begat Asa;

8 And Asa begat Josaphat; and Josaphat begat Joram; and Joram begat Ozias;

9 And Ozias begat Joatham; and Joatham begat Achaz; and Achaz begat Ezekias;
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;

4 And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;

5 And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;

6 And Jesse begat David the king; and David the king begat Solomon of her that had been the wife of Urias;

7 And Solomon begat Roboam; and Roboam begat Abia; and Abia begat Asa;

8 And Asa begat Josaphat; and Josaphat begat Joram; and Joram begat Ozias;

9 And Ozias begat Joatham; and Joatham begat Achaz; and Achaz begat Ezekias;

10 And Ezekias begat Manasses; and Manasses begat Amon; and Amon begat Josias;

11 And Josias begat Jechonias and his brethren, about the time they were carried away to Babylon:

12 And after they were brought to Babylon, Jechonias begat Salathiel; and Salathiel begat Zorobabel;

13 And Zorobabel begat Abiud; and Abiud begat Eliakim; and Eliakim begat Azor;

14 And Azor begat Sadoc; and Sadoc begat Achim; and Achim begat Eliud;

15 And Eliud begat Eleazar; and Eleazar begat Matthan; and Matthan begat Jacob;
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;

4 And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;

5 And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;

6 And Jesse begat David the king; and David the king begat Solomon of her that had been the wife of Urias;

7 And Solomon begat Roboam; and Roboam begat Abia; and Abia begat Asa;

8 And Asa begat Josaphat; and Josaphat begat Joram; and Joram begat Ozias;

9 And Ozias begat Joatham; and Joatham begat Achaz; and Achaz begat Ezekias;

10 And Ezekias begat Manasses; and Manasses begat Amon; and Amon begat Josias;

11 And Josias begat Jechonias and his brethren, about the time they were carried away to Babylon:

12 And after they were brought to Babylon, Jechonias begat Salathiel; and Salathiel begat Zorobabel;

13 And Zorobabel begat Abiud; and Abiud begat Eliakim; and Eliakim begat Azor;

14 And Azor begat Sadoc; and Sadoc begat Achim; and Achim begat Eliud;

15 And Eliud begat Eleazar; and Eleazar begat Matthan; and Matthan begat Jacob;

16 And Jacob begat Joseph the husband of Mary, of whom was born Jesus, who is called Christ.

17 So all the generations from Abraham to David are fourteen generations; and from David until the carrying away into Babylon are fourteen generations; and from the carrying away into Babylon unto Christ are fourteen generations.

18 Now the birth of Jesus Christ was on this wise: When as his mother Mary was espoused to Joseph, before they came together, she was found with child of the Holy Ghost.

19 Then Joseph her husband, being a just man, and not willing to make her a publick example, was minded to put her away privily.

20 But while he thought on these things, behold, the angel of the Lord appeared unto him in a dream, saying, Joseph, thou son of David, fear not to take unto thee Mary thy wife: for that which is conceived in her is of the Holy Ghost.

21 And she shall bring forth a son, and thou shalt call his name JESUS: for he shall save his people from their sins.

22 Now all this was done, that it might be fulfilled which was spoken of the Lord by the prophet, saying,

23 Behold, a virgin shall be with child, and shall bring forth a son, and they shall call his name Emmanuel, which being interpreted is, God with us.

24 Then Joseph being raised from sleep did as the angel of the Lord had bidden him, and took unto him his wife:

25 And knew her not till she had brought forth her firstborn son: and he called his name JESUS.
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);


            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;

4 And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;

5 And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;

6 And Jesse begat David the king; and David the king begat Solomon of her that had been the wife of Urias;

7 And Solomon begat Roboam; and Roboam begat Abia; and Abia begat Asa;

8 And Asa begat Josaphat; and Josaphat begat Joram; and Joram begat Ozias;

9 And Ozias begat Joatham; and Joatham begat Achaz; and Achaz begat Ezekias;

10 And Ezekias begat Manasses; and Manasses begat Amon; and Amon begat Josias;

20 But while he thought on these things, behold, the angel of the Lord appeared unto him in a dream, saying, Joseph, thou son of David, fear not to take unto thee Mary thy wife: for that which is conceived in her is of the Holy Ghost.

21 And she shall bring forth a son, and thou shalt call his name JESUS: for he shall save his people from their sins.

22 Now all this was done, that it might be fulfilled which was spoken of the Lord by the prophet, saying,

23 Behold, a virgin shall be with child, and shall bring forth a son, and they shall call his name Emmanuel, which being interpreted is, God with us.

24 Then Joseph being raised from sleep did as the angel of the Lord had bidden him, and took unto him his wife:

25 And knew her not till she had brought forth her firstborn son: and he called his name JESUS.
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;

4 And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;

5 And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;

6 And Jesse begat David the king; and David the king begat Solomon of her that had been the wife of Urias;

7 And Solomon begat Roboam; and Roboam begat Abia; and Abia begat Asa;

17 So all the generations from Abraham to David are fourteen generations; and from David until the carrying away into Babylon are fourteen generations; and from the carrying away into Babylon unto Christ are fourteen generations.

18 Now the birth of Jesus Christ was on this wise: When as his mother Mary was espoused to Joseph, before they came together, she was found with child of the Holy Ghost.

19 Then Joseph her husband, being a just man, and not willing to make her a publick example, was minded to put her away privily.

20 But while he thought on these things, behold, the angel of the Lord appeared unto him in a dream, saying, Joseph, thou son of David, fear not to take unto thee Mary thy wife: for that which is conceived in her is of the Holy Ghost.

21 And she shall bring forth a son, and thou shalt call his name JESUS: for he shall save his people from their sins.

22 Now all this was done, that it might be fulfilled which was spoken of the Lord by the prophet, saying,

23 Behold, a virgin shall be with child, and shall bring forth a son, and they shall call his name Emmanuel, which being interpreted is, God with us.

24 Then Joseph being raised from sleep did as the angel of the Lord had bidden him, and took unto him his wife:

25 And knew her not till she had brought forth her firstborn son: and he called his name JESUS.
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;

4 And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;

5 And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;

6 And Jesse begat David the king; and David the king begat Solomon of her that had been the wife of Urias;

7 And Solomon begat Roboam; and Roboam begat Abia; and Abia begat Asa;

8 And Asa begat Josaphat; and Josaphat begat Joram; and Joram begat Ozias;

9 And Ozias begat Joatham; and Joatham begat Achaz; and Achaz begat Ezekias;

10 And Ezekias begat Manasses; and Manasses begat Amon; and Amon begat Josias;

11 And Josias begat Jechonias and his brethren, about the time they were carried away to Babylon:

12 And after they were brought to Babylon, Jechonias begat Salathiel; and Salathiel begat Zorobabel;
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;

4 And Aram begat Aminadab; and Aminadab begat Naasson; and Naasson begat Salmon;

5 And Salmon begat Booz of Rachab; and Booz begat Obed of Ruth; and Obed begat Jesse;

6 And Jesse begat David the king; and David the king begat Solomon of her that had been the wife of Urias;

7 And Solomon begat Roboam; and Roboam begat Abia; and Abia begat Asa;

8 And Asa begat Josaphat; and Josaphat begat Joram; and Joram begat Ozias;
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);

            content = @"
1 The book of the generation of Jesus Christ, the son of David, the son of Abraham.

2 Abraham begat Isaac; and Isaac begat Jacob; and Jacob begat Judas and his brethren;

3 And Judas begat Phares and Zara of Thamar; and Phares begat Esrom; and Esrom begat Aram;
";
            opSuc = manager.UpdateContent(content);
            Debug.WriteLine("opSuc: {0}, content = {1}", opSuc, content);


            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.Undo();
            Debug.WriteLine("Undo: content = {0}", content);

            content = manager.CurrentContent;
            Debug.WriteLine("CurrentContent: content = {0}", content);



            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.Redo();
            Debug.WriteLine("Redo: content = {0}", content);

            content = manager.CurrentContent;
            Debug.WriteLine("CurrentContent: content = {0}", content);

        }

    }
}
