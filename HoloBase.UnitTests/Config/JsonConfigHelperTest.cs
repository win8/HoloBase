﻿using HoloBase.Config;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UnitTests.Config
{
    [TestClass]
    public class JsonConfigHelperTest
    {
        [TestMethod]
        public async Task TestJsonConfigDictionary()
        {
            // var dict = JsonConfigHelper.Instance.Dictionary;
            var configHelper = await JsonConfigHelper.GetInstanceAsync();
            var dict = configHelper.Dictionary;
            if (dict != null && dict.Count > 0) {
                foreach (var k in dict.Keys) {
                    System.Diagnostics.Debug.WriteLine("Config: {0} = {1}", k, dict[k]);
                }
                Assert.AreEqual(4, dict.Count);
            } else {
                Assert.Fail("Failed to read JSON config file.");
            }
        }

        [TestMethod]
        public async Task TestJsonConfigGetValue()
        {
            var key1 = "key1";
            //var val1 = JsonConfigHelper.Instance.GetConfigValue(key1);
            var configHelper = await JsonConfigHelper.GetInstanceAsync();
            var val1 = configHelper.GetConfigValue(key1);
            System.Diagnostics.Debug.WriteLine("Key-value: {0} = {1}", key1, val1);

            // tbd
            var expectedVal1 = 111;
            Assert.AreEqual(expectedVal1, val1);

        }

    }
}
