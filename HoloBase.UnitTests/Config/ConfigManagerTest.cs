﻿using HoloBase.Config;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UnitTests.Config
{
    [TestClass]
    public class ConfigManagerTest
    {
        [TestMethod]
        public async Task TestConfigManagerConfgMap()
        {
            // ConfigManager.Instance.ConfigType = ConfigTypes.PROPS | ConfigTypes.JSON;
            var configManager = await ConfigManager.GetInstanceAsync();
            await configManager.SetConfigTypeAsync(ConfigTypes.PROPS | ConfigTypes.JSON);
            // var map = ConfigManager.Instance.ConfigMap;
            var map = configManager.ConfigMap;
            if (map != null && map.Count > 0) {
                foreach (var k in map.Keys) {
                    System.Diagnostics.Debug.WriteLine("Config: {0} = {1}", k, map[k]);
                }
                Assert.AreEqual(7, map.Count);
            } else {
                Assert.Fail("Failed to read config Map.");
            }
        }

        [TestMethod]
        public async Task TestConfigManagerGetValue()
        {
            // ConfigManager.Instance.ConfigType = ConfigTypes.PROPS | ConfigTypes.JSON;
            var configManager = await ConfigManager.GetInstanceAsync();
            await configManager.SetConfigTypeAsync(ConfigTypes.PROPS | ConfigTypes.JSON);

            var key1 = "key1";
            // var val1 = ConfigManager.Instance.GetConfigValue(key1);
            var val1 = configManager.GetConfigValue(key1);
            System.Diagnostics.Debug.WriteLine("Key-value: {0} = {1}", key1, val1);

            // tbd
            //var expectedVal1 = 111;
            //Assert.AreEqual(expectedVal1, val1);

            var key2 = "k1";
            // var val2 = ConfigManager.Instance.GetConfigValue(key2);
            var val2 = configManager.GetConfigValue(key2);
            System.Diagnostics.Debug.WriteLine("Key-value: {0} = {1}", key2, val2);

            // tbd
            //var expectedVal2 = 123;
            //Assert.AreEqual(expectedVal2, val2);

        }

    }
}
