﻿using HoloBase.Config;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UnitTests.Config
{
    [TestClass]
    public class PropertyConfigHelperTest
    {
        [TestMethod]
        public async Task TestPropsConfigDictionary()
        {
            // var dict = PropertyConfigHelper.Instance.Dictionary;
            var configHelper = await PropertyConfigHelper.GetInstanceAsync();
            var dict = configHelper.Dictionary;
            if (dict != null && dict.Count > 0) {
                foreach (var k in dict.Keys) {
                    System.Diagnostics.Debug.WriteLine("Config: {0} = {1}", k, dict[k]);
                }
                Assert.AreEqual(4, dict.Count);
            } else {
                Assert.Fail("Failed to read properties config file.");
            }
        }

        [TestMethod]
        public async Task TestPropsConfigGetValue()
        {
            var key1 = "k1";
            // var val1 = PropertyConfigHelper.Instance.GetConfigValue(key1);
            var configHelper = await PropertyConfigHelper.GetInstanceAsync();
            var val1 = configHelper.GetConfigValue(key1);
            System.Diagnostics.Debug.WriteLine("Key-value: {0} = {1}", key1, val1);

            // tbd.
            var expectedVal1 = 123;
            Assert.AreEqual(expectedVal1, val1);

        }



    }
}
