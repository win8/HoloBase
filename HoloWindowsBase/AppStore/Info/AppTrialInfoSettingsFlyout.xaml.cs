﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace HoloWindowsBase.AppStore.Info
{
    public sealed partial class AppTrialInfoSettingsFlyout : SettingsFlyout
    {
        public AppTrialInfoSettingsFlyout()
        {
            this.InitializeComponent();

            // temporary
            UserControlAppTrialInfo.ParentSettingsFlyout = this;
            UserControlAppTrialInfo.ShowCancelButton();
        }


        public string DefaultEmailSubject
        {
            get
            {
                return UserControlAppTrialInfo.DefaultEmailSubject;
            }
            set
            {
                UserControlAppTrialInfo.DefaultEmailSubject = value;
            }
        }

        public string DefaultEmailBody
        {
            get
            {
                return UserControlAppTrialInfo.DefaultEmailBody;
            }
            set
            {
                UserControlAppTrialInfo.DefaultEmailBody = value;
            }
        }


    }
}
