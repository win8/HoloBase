﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HoloBase.Settings.UI;
using HoloBase.Settings.ViewModels;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace HoloWindowsBase.Settings.UI
{
    public sealed partial class GlobalAppSettingsFlyout : SettingsFlyout
    {
        // private GlobalAppSettingsControl globalAppSettingsControl;

        public GlobalAppSettingsFlyout()
        {
            this.InitializeComponent();

            // temporary
            UserControlGlobalAppSettings.ParentSettingsFlyout = this;
        }


        public GlobalAppSettingsControl GlobalAppSettingsControl
        {
            get
            {
                return UserControlGlobalAppSettings;
                // return globalAppSettingsControl;
            }
            //private set
            //{
            //    globalAppSettingsControl = value;
            //}
        }

        public GlobalAppSettingsViewModel GlobalAppSettingsViewModel
        {
            get
            {
                return (GlobalAppSettingsViewModel) UserControlGlobalAppSettings.ViewModel;
            }
        }

        
    }
}
