﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;


namespace HoloWindowsBase.Texts.Common
{
    public class SystemClipboardProxy : ISystemClipboardProxy
    {
        public SystemClipboardProxy()
        {
        }

        public async Task<string> CopyFromClipboardAsync()
        {
            string text = null;
            DataPackageView dataPackageView = Clipboard.GetContent();
            if (dataPackageView != null) {
                text = await dataPackageView.GetTextAsync();
            }
            return text;
        }

        public bool CopyToClipboard(string text)
        {
            var suc = false;
            if (text != null) {
                DataPackage dataPackage = new DataPackage();
                dataPackage.SetText(text);
                Clipboard.SetContent(dataPackage);
            }
            return suc;
        }

    }
}
