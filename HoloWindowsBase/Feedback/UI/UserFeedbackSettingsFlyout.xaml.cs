﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace HoloWindowsBase.Feedback.UI
{
    public sealed partial class UserFeedbackSettingsFlyout : SettingsFlyout
    {
        public UserFeedbackSettingsFlyout()
        {
            this.InitializeComponent();

            // temporary
            UserControlUserFeedbackSettings.ParentSettingsFlyout = this;
            UserControlUserFeedbackSettings.ShowCancelButton();
        }


        public string DefaultMessageSubject
        {
            get
            {
                return UserControlUserFeedbackSettings.DefaultMessageSubject;
            }
            set
            {
                UserControlUserFeedbackSettings.DefaultMessageSubject = value;
            }
        }

        public string DefaultMessageBody
        {
            get
            {
                return UserControlUserFeedbackSettings.DefaultMessageBody;
            }
            set
            {
                UserControlUserFeedbackSettings.DefaultMessageBody = value;
            }
        }

    }
}
