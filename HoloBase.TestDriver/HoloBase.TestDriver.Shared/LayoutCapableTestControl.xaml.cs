﻿using HoloBase.Layout;
using HoloBase.Layout.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.TestDriver
{

    public sealed partial class LayoutCapableTestControl : UserControl, ILayoutCapable
    {
        private static LayoutSizeSet DefaultLayoutSizeSet;
        static LayoutCapableTestControl()
        {
            // TBD: LayoutSizeSet should be different for Windows and Windows Phone, in general.
#if WINDOWS_PHONE_APP
            // var ls1 = new DecoratedLayoutSize(LayoutSize.Narrow, new DecoratorPadding(90));   // 320 + 2*90 = 500 
            var ls1 = new DecoratedLayoutSize(LayoutSize.Narrow);
            var ls2 = new DecoratedLayoutSize(LayoutSize.Median);
            var lsList = new List<DecoratedLayoutSize>() {
                ls1,
                ls2
            };

            var vs1 = new LayoutVisualState("VisualStateNormal");
            var vs2 = new LayoutVisualState("VisualStateSizeMedium");
            var vsMap = new Dictionary<LayoutSize, LayoutVisualState>() {
                {LayoutSize.Narrow, vs1},
                {LayoutSize.Median, vs2}
            };
// #elif WINDOWS_APP
#else
            // var ls1 = new DecoratedLayoutSize(LayoutSize.Narrow, new DecoratorPadding(90));   // 320 + 2*90 = 500 
            var ls1 = new DecoratedLayoutSize(LayoutSize.Narrow, new DecoratorPadding(40));
            var ls2 = new DecoratedLayoutSize(LayoutSize.Median, new DecoratorPadding(15));
            var ls3 = new DecoratedLayoutSize(LayoutSize.Wide);
            var lsList = new List<DecoratedLayoutSize>() {
                ls1,
                ls2,
                ls3
            };

            var vs1 = new LayoutVisualState("VisualStateNormal");
            var vs2 = new LayoutVisualState("VisualStateSizeMedium");
            var vs3 = new LayoutVisualState("VisualStateSizeLarge");
            var vsMap = new Dictionary<LayoutSize, LayoutVisualState>() {
                {LayoutSize.Narrow, vs1},
                {LayoutSize.Median, vs2},
                {LayoutSize.Wide, vs3}
            };
#endif

            DefaultLayoutSizeSet = new LayoutSizeSet(lsList, vsMap, LayoutPickOrder.WidthFirst);
        }

        public LayoutCapableTestControl()
        {
            this.InitializeComponent();

            // GridLayoutRoot.SizeChanged += GridLayoutRoot_SizeChanged;
            this.SizeChanged += LayoutCapableTestControl_SizeChanged;

        }

        // Required by ILayoutCapable interface.
        private LayoutSizeSet GetLayoutSizeSet()
        {
            return DefaultLayoutSizeSet;
        }

        void GridLayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("GridLayoutRoot_SizeChanged() NewSize = {0}", e.NewSize);
        }
        void LayoutCapableTestControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("LayoutCapableTestControl_SizeChanged() NewSize = {0}", e.NewSize);


            var vs = VisualStateLayoutManager.Instance.GetLayoutVisualState(e.NewSize, GetLayoutSizeSet());

            var vsName = vs.Name;
            System.Diagnostics.Debug.WriteLine("vsName = {0}", vsName);
            // tbd: check if vsName is valid ???
            // ...

            var suc = VisualStateManager.GoToState(this, vs.Name, false);
            System.Diagnostics.Debug.WriteLine("GoToState(): {0}. Succeeded = {1}", vs.Name, suc);
        }
        
    
    }

}
