﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace HoloBase.TestDriver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class IconPickerTestPage : Page
    {
        // temporary
        private Size currentPageSize = Size.Empty;

        public IconPickerTestPage()
        {
            this.InitializeComponent();

            this.SizeChanged += ColorPickerTestPage_SizeChanged;
        }

        void ColorPickerTestPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            currentPageSize = e.NewSize;
        }


        private void ButtonShowFontFamilyPicker_Click(object sender, RoutedEventArgs e)
        {
            if (!PopupFontFamilyPicker.IsOpen) {
                if (currentPageSize != Size.Empty) {
                    PopupFontFamilyPicker.HorizontalOffset = (currentPageSize.Width - 320) / 2.0;
                    PopupFontFamilyPicker.VerticalOffset = 200;
                }
                PopupFontFamilyPicker.IsOpen = true;
            }
        }

        private void ButtonShowSymbolBlockPicker_Click(object sender, RoutedEventArgs e)
        {
            if (!PopupSymbolBlockPicker.IsOpen) {
                if (currentPageSize != Size.Empty) {
                    PopupSymbolBlockPicker.HorizontalOffset = (currentPageSize.Width - 320) / 2.0;
                    PopupSymbolBlockPicker.VerticalOffset = 200;
                }
                PopupSymbolBlockPicker.IsOpen = true;
            }
        }

        private void ButtonShowSymbolIconPicker_Click(object sender, RoutedEventArgs e)
        {
            if (!PopupSymbolIconPicker.IsOpen) {
                if (currentPageSize != Size.Empty) {
                    PopupSymbolIconPicker.HorizontalOffset = (currentPageSize.Width - 320) / 2.0;
                    PopupSymbolIconPicker.VerticalOffset = 200;
                }
                PopupSymbolIconPicker.IsOpen = true;
            }
        }

        private void ButtonShowFontIconPicker_Click(object sender, RoutedEventArgs e)
        {
            if (!PopupFontIconPicker.IsOpen) {
                if (currentPageSize != Size.Empty) {
                    PopupFontIconPicker.HorizontalOffset = (currentPageSize.Width - 320) / 2.0;
                    PopupFontIconPicker.VerticalOffset = 200;
                }
                PopupFontIconPicker.IsOpen = true;
            }
        }


    }
}
