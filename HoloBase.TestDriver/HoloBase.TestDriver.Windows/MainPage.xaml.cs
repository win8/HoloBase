﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace HoloBase.TestDriver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public MainPage()
        {
            this.InitializeComponent();

            // this.SizeChanged += MainPage_SizeChanged;

        }

        void MainPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("MainPage_SizeChanged() NewSize = {0}", e.NewSize);



        }

        private void ButtonNavAboutPopupTestPage_Click(object sender, RoutedEventArgs e)
        {

            Frame rootFrame = Window.Current.Content as Frame;
            // TBD: How to set the properties???? (Use parameter?)
            if (rootFrame != null) {
                rootFrame.Navigate(typeof(AboutPopupTestPage));
            }

        }

        private void ButtonNavColorPickerTestPage_Click(object sender, RoutedEventArgs e)
        {

            Frame rootFrame = Window.Current.Content as Frame;
            // TBD: How to set the properties???? (Use parameter?)
            if (rootFrame != null) {
                rootFrame.Navigate(typeof(ColorPickerTestPage));
            }

        }

        private void ButtonNavIconPickerTestPage_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            // TBD: How to set the properties???? (Use parameter?)
            if (rootFrame != null) {
                rootFrame.Navigate(typeof(IconPickerTestPage));
            }
        }

        private void ButtonNavSettingsShortcutTestPage_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            // TBD: How to set the properties???? (Use parameter?)
            if (rootFrame != null) {
                rootFrame.Navigate(typeof(SettingsShortcutTestPage));
            }
        }

        private void ButtonNavCredentialLockerTestPage_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            // TBD: How to set the properties???? (Use parameter?)
            if (rootFrame != null) {
                rootFrame.Navigate(typeof(CredentialLockerTestPage));
            }
        }
    
    
    }
}
