﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HoloBase.TestDriver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ColorPickerTestPage : Page
    {
        // temporary
        private Size currentPageSize = Size.Empty;

        public ColorPickerTestPage()
        {
            this.InitializeComponent();

            this.SizeChanged += ColorPickerTestPage_SizeChanged;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void ColorPickerTestPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            currentPageSize = e.NewSize;
        }

        private void ButtonShowSimpleColorPicker_Click(object sender, RoutedEventArgs e)
        {
            if (!PopupSimpleNamedColorPicker.IsOpen) {
                if (currentPageSize != Size.Empty) {
                    PopupSimpleNamedColorPicker.HorizontalOffset = (currentPageSize.Width - 320) / 2.0;
                    PopupSimpleNamedColorPicker.VerticalOffset = 50;
                }
                PopupSimpleNamedColorPicker.IsOpen = true;
            }
        }

        private void ButtonShowRichColorPicker_Click(object sender, RoutedEventArgs e)
        {
            if (!PopupRichNamedColorPicker.IsOpen) {
                if (currentPageSize != Size.Empty) {
                    PopupRichNamedColorPicker.HorizontalOffset = (currentPageSize.Width - 320) / 2.0;
                    PopupRichNamedColorPicker.VerticalOffset = 50;
                }
                PopupRichNamedColorPicker.IsOpen = true;
            }
        }

    }
}
