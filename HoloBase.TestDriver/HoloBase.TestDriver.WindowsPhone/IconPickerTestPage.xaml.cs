﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HoloBase.TestDriver
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class IconPickerTestPage : Page
    {
        public IconPickerTestPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        private void AppBarButtonPopupSimbolIconPickerTestPage_Click(object sender, RoutedEventArgs e)
        {
            if (!PopupSymbolIconPicker.IsOpen) {
                PopupSymbolIconPicker.HorizontalOffset = 10;
                PopupSymbolIconPicker.VerticalOffset = 50;
                PopupSymbolIconPicker.IsOpen = true;
            }
        }

        private void AppBarButtonPopupFontIconPickerTestPage_Click(object sender, RoutedEventArgs e)
        {
            if (!PopupFontIconPicker.IsOpen) {
                PopupFontIconPicker.HorizontalOffset = 10;
                PopupFontIconPicker.VerticalOffset = 50;
                PopupFontIconPicker.IsOpen = true;
            }
        }
    }
}
