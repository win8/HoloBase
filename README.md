# HoloBase

Base class + helper library on Windows 8 (C#).
It depends on HoloCore.

_Note: Despite the name, this library has little to do with HoloLens (which had not been released when I was working on this project)._




## References

* Guidelines for Segoe UI Symbol icons    
https://msdn.microsoft.com/en-us/library/windows/apps/jj841126.aspx


* Segoe MDL2 Assets - Cheatsheet    
http://modernicons.io/segoe-mdl2/cheatsheet/


