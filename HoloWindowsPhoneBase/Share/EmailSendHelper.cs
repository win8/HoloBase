﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Email;
using Windows.Storage;
using Windows.Storage.Streams;


namespace HoloBase.Share
{
    public sealed class EmailSendHelper
    {

        // temporary
        public async Task TriggerSendEmailAsync(string recipientEmail, string recipientName, string subject, string body)
        {
            await TriggerSendEmailAsync(recipientEmail, recipientName, subject, body, null);
        }
        public async Task TriggerSendEmailAsync(string recipientEmail, string recipientName, string subject, string body, IStorageFile fileToAttach)
        {
            var email = new EmailMessage {
                Subject = subject,
                Body = body,
            };
            email.To.Add(new EmailRecipient(recipientEmail, recipientName));

            if (fileToAttach != null) {
                var randomAccessStreamReference = RandomAccessStreamReference.CreateFromFile(fileToAttach);
                var attachment = new EmailAttachment(fileToAttach.Name, randomAccessStreamReference);
                email.Attachments.Add(attachment);
            }

            await EmailManager.ShowComposeNewEmailAsync(email);
        }



    }
}
