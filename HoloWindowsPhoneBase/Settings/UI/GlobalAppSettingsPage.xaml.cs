﻿using HoloBase.Settings.UI;
using HoloBase.Settings.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HoloWindowsPhoneBase.Settings.UI
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GlobalAppSettingsPage : Page
    {
        // private GlobalAppSettingsControl globalAppSettingsControl;

        public GlobalAppSettingsPage()
        {
            this.InitializeComponent();

            // temporary
            UserControlGlobalAppSettings.ParentSettingsPage = this;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        public GlobalAppSettingsControl GlobalAppSettingsControl
        {
            get
            {
                return UserControlGlobalAppSettings;
                // return globalAppSettingsControl;
            }
            //private set
            //{
            //    globalAppSettingsControl = value;
            //}
        }

        public GlobalAppSettingsViewModel GlobalAppSettingsViewModel
        {
            get
            {
                return (GlobalAppSettingsViewModel) UserControlGlobalAppSettings.ViewModel;
            }
        }


        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame != null) {
                if (rootFrame.CanGoBack) {
                    rootFrame.GoBack();
                }
            }
        }

    }
}
