﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HoloWindowsPhoneBase.Feedback.UI
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserFeedbackSettingsPage : Page
    {
        public UserFeedbackSettingsPage()
        {
            this.InitializeComponent();

            // temporary
            UserControlUserFeedbackSettings.ParentSettingsPage = this;
            UserControlUserFeedbackSettings.HideCancelButton();

            // HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        private void DoGoBack()
        {
            // TBD:
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame != null && rootFrame.CanGoBack) {
                rootFrame.GoBack();
            }
        }
        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (e.Handled == false) {
                e.Handled = true;
                DoGoBack();
            }
        }


        public string DefaultMessageSubject
        {
            get
            {
                return UserControlUserFeedbackSettings.DefaultMessageSubject;
            }
            set
            {
                UserControlUserFeedbackSettings.DefaultMessageSubject = value;
            }
        }

        public string DefaultMessageBody
        {
            get
            {
                return UserControlUserFeedbackSettings.DefaultMessageBody;
            }
            set
            {
                UserControlUserFeedbackSettings.DefaultMessageBody = value;
            }
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            DoGoBack();
        }
    }
}