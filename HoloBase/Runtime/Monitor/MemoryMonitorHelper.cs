﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Runtime.Monitor
{
    public class MemoryMonitorHelper
    {
        private static MemoryMonitorHelper instance = null;
        public static MemoryMonitorHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new MemoryMonitorHelper();
                }
                return instance;
            }
        }

        private ulong memoryUsageLimit = 0UL;
        private ulong currentMemoryUsage = 0UL;

        private MemoryMonitorHelper()
        {
            // TBD:
            // Add memory manager event handlers....
            // ...
        }


        public ulong MemoryUsageLimit
        {
            get
            {
                // Lazy initialized.
                if (memoryUsageLimit == 0UL) {
                    // ????
                    // memoryUsageLimit = Windows.System.MemoryManager.AppMemoryUsageLimit;
                    // ...
                }

                return memoryUsageLimit;
            }
            //private set
            //{
            //    memoryUsageLimit = value;
            //}
        }

        public ulong CurrentMemoryUsage
        {
            get
            {
                if (currentMemoryUsage == 0UL) {
                    // ???
                    // currentMemoryUsage = Windows.System.MemoryManager.AppMemoryUsage;
                    // ....
                }
                return currentMemoryUsage;
            }
            internal set
            {
                currentMemoryUsage = value;
            }
        }
   
    
    
    }

}
