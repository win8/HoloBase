﻿using HoloBase.Help.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Help.Protocol
{
    public static class HelpUriSchemeUtil
    {
        // Examples:
        //help-abc:///list
        //help-abc:///list/topic1
        //help-abc:///view/topic2/1234
        // ...


        // temporary
        private static string HELP_URI_PREFIX
        {
            get
            {
                // tbd: "cache" this.
                return HelpSchemeManager.Instance.SchemeName + ":///";   // Note the three slashes.
            }
        }
        private const string URI_PATH_SEPARATOR = "/";
        // temporary

        // Note:
        // HelpContentFormat is currently ignored.
        // ...

        public static Uri BuildUri(string verb)
        {
            return BuildUri(verb, null);
        }
        public static Uri BuildUri(string verb, string topic)
        {
            return BuildUri(verb, topic, 0UL);
        }
        public static Uri BuildUri(string verb, string topic, ulong docId)
        {
            return BuildUri(verb, topic, docId, HelpContentFormat.Default);  // ???
        }
        public static Uri BuildUri(string verb, string topic, ulong docId, HelpContentFormat contentFormat)
        {
            var strUri = BuildUriString(verb, topic, docId, contentFormat);
            var uri = new Uri(strUri);
            return uri;
        }

        public static string BuildUriString(string verb)
        {
            return BuildUriString(verb, null);
        }
        public static string BuildUriString(string verb, string topic)
        {
            return BuildUriString(verb, topic, 0UL);
        }
        public static string BuildUriString(string verb, string topic, ulong docId)
        {
            return BuildUriString(verb, topic, docId, HelpContentFormat.Default);  // ???
        }
        public static string BuildUriString(string verb, string topic, ulong docId, HelpContentFormat contentFormat)
        {
            // Note: this simple incremental build algo may not guarantee it generates an overall valid URL
            // ....

            // TBD: Validate input args???
            var sb = new StringBuilder();
            sb.Append(HELP_URI_PREFIX);
            sb.Append(verb);

            if (!String.IsNullOrEmpty(topic)) {
                sb.Append(URI_PATH_SEPARATOR);
                sb.Append(topic);
                if (docId > 0UL) {
                    sb.Append(URI_PATH_SEPARATOR);
                    sb.Append(docId);

                    // tbd
                    if (contentFormat.IsValid()) {
                        // add contentFormat as a query string???
                        // ...
                    }
                }
            }

            return sb.ToString();
        }



        public static HelpUri ParseUri(string strUri)
        {
            var uri = new Uri(strUri);
            return ParseUri(uri);
        }
        public static HelpUri ParseUri(Uri uri)
        {
            var path = uri.AbsolutePath;
            //// StringSplitOptions.RemoveEmptyEntries removes the leading slash..
            //if (path.StartsWith("/")) {
            //    path = path.Substring(1);
            //}
            var parts = path.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);   // this option may possibly remove a trailing "/", if any.
            // --> Note: This option possibly parse the uri incorrectly.
            // e.g., In this (incorrectly formatted) uri, help-xxx:///verb//1234, --> 1234 becomes topic not doc id in our algorithm.
            // tbd: better validation????

            HelpUri helpUri = HelpUri.NONE;
            if (parts != null) {
                var len = parts.Length;
                if (len > 0) {
                    var verb = parts[0];
                    if (HelpVerb.IsValid(verb)) {
                        helpUri.Verb = verb;
                        if (len > 1) {
                            var topic = parts[1];
                            if (!String.IsNullOrEmpty(topic)) {
                                helpUri.Topic = topic;
                                if (len > 2) {
                                    var strHelpId = parts[2];
                                    try {
                                        var docId = Convert.ToUInt64(strHelpId);
                                        helpUri.DocId = docId;
                                        if (len > 3) {
                                            // ????
                                        }
                                    } catch (Exception ex) {
                                        // ????
                                        System.Diagnostics.Debug.WriteLine("Invalid help doc Id: {0}. Error: {1}", strHelpId, ex.Message);
                                        // what to do???
                                    }
                                }
                            }
                        }
                    } else {
                        // ????
                        System.Diagnostics.Debug.WriteLine("Invalid verb: {0}", verb);
                    }
                }
            }


            //// TBD: 
            //// parse query string???
            //// ...
            //var queryStr = uri.Query;
            //// ...
            //var strHelpContentFormat = ...;
            //try {
            //    var contentFormat = (HelpContentFormat) Enum.Parse(typeof(HelpContentFormat), , true);   // Note: uri may contain all lowercase, whereas the enum is defined with capitalized strings.
            //    // System.Diagnostics.Debug.WriteLine(">>>> contentFormat = {0}", contentFormat);
            //    helpUri.HelpContentFormat = contentFormat;
            //} catch (Exception ex) {
            //    // ????
            //    System.Diagnostics.Debug.WriteLine("Invalid contentFormat: {0}. Error: {1}", strHelpContentFormat, ex.Message);
            //    // what to do???
            //}


            return helpUri;
        }

        //// tbd:
        //public static string FindVerb(string strUri)
        //{
        //    return null;
        //}
    }
}
