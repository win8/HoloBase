﻿using HoloBase.Help.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Help.Protocol
{
    public struct HelpUri
    {
        // ???
        public static readonly HelpUri NONE = new HelpUri(HelpVerb.NULL);

        // private string scheme;   // always "help", or "help-<app>"
        // private string subject;  // Not used for now. (We can put this as a "hostname" in a Uri.)
        private string verb;
        private string topic;
        private ulong docId;
        private HelpContentFormat contentFormat;

        public HelpUri(string verb)
            : this(verb, null)  // ???
        {
        }
        public HelpUri(string verb, string topic)
            : this(verb, topic, 0UL)
        {
        }
        public HelpUri(string verb, string topic, ulong docId)
            : this(verb, topic, docId, HelpContentFormat.Default)  // ???
        {
        }
        public HelpUri(string verb, string topic, ulong docId, HelpContentFormat contentFormat)
        {
            this.verb = verb;
            this.topic = topic;
            this.docId = docId;
            this.contentFormat = contentFormat;
        }

        public string Verb
        {
            get
            {
                return verb;
            }
            set
            {
                verb = value;
            }
        }
        public string Topic
        {
            get
            {
                return topic;
            }
            set
            {
                topic = value;
            }
        }
        public ulong DocId
        {
            get
            {
                return docId;
            }
            set
            {
                docId = value;
            }
        }
        public HelpContentFormat HelpContentFormat
        {
            get
            {
                return contentFormat;
            }
            set
            {
                contentFormat = value;
            }
        }
        
    }
}
