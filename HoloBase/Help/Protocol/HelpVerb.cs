﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Help.Protocol
{
    public static class HelpVerb
    {
        public const string NULL = "null";   // ???

        // public const string NEW = "new";
        // public const string EDIT = "edit";
        public const string VIEW = "view";
        public const string LIST = "list";

        private static readonly ISet<string> sVerbSet;
        static HelpVerb()
        {
            sVerbSet = new HashSet<string>();
            // sVerbSet.Add(NEW);
            // sVerbSet.Add(EDIT);
            sVerbSet.Add(VIEW);
            sVerbSet.Add(LIST);
        }

        public static bool IsValid(string verb)
        {
            // verb.ToLower() ????
            return sVerbSet.Contains(verb);
        }

    }
}
