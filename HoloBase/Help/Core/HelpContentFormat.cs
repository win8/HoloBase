﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Help.Core
{
    // temporary
    public enum HelpContentFormat
    {
        Default = 0,   // ???
        Text,
        Html,
        Markdown
        // ...
    }

    public static class HelpContentFormatExtensions
    {
        public static bool IsValid(this HelpContentFormat me)
        {
            // temporary
            if (me == HelpContentFormat.Default) {
                return false;
            } else {
                return true;
            }
        }
    }
}
