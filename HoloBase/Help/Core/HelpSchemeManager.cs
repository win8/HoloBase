﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Help.Core
{
    public sealed class HelpSchemeManager
    {
        // Note;
        // Each app (using HoloBase library) should use a different scheme, in general.
        // (e.g., help-myapp:///topic1/view/1234, etc.)
        // The best way to do is to use a config file.
        // (The scheme name should match the one specified in appxpackage manifest file.)
        // For now, we use DI.
        // The app, upon startup, should set the help scheme via this "manager" class.
        private const string DEFAULT_SCHEME = "help";

        private static HelpSchemeManager instance = null;
        public static HelpSchemeManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new HelpSchemeManager();
                }
                return instance;
            }
        }

        private string schemeName;
        private HelpSchemeManager()
        {
            // TBD: Read this from config.
            schemeName = DEFAULT_SCHEME;
        }

        // TBD:
        // This need to be called with a proper scheme name upon app startup.
        //  (This should be done until we implement config.)
        public string SchemeName
        {
            get
            {
                return schemeName;
            }
            set
            {
                schemeName = value;
            }
        }



    }
}
