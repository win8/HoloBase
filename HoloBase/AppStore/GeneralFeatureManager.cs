﻿using HoloBase.AppStore.Core;
using HoloBase.AppStore.Trial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.AppStore
{
    public sealed class GeneralFeatureManager
    {
        private static GeneralFeatureManager instance = null;
        public static GeneralFeatureManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new GeneralFeatureManager();
                }
                return instance;
            }
        }
        private GeneralFeatureManager()
        {
        }



        // tbd

        public bool IsFeatureAvailable(IAppFeature feature)
        {
            var trialStatus = TrialLicensingHelper.Instance.TrialStatus;
            if (trialStatus == TrialStatus.NotInTrial) {
                return true;
            }

            // tbd:
            // ...

            return false;
        }


        //public bool IsToShowInterstitial(IInterstitial intterstitial)
        //{
        //    return false;
        //}



    }
}
