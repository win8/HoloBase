﻿using HoloBase.AppStore.Trial;
using HoloBase.Share;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.AppStore.Info
{
    public sealed partial class AppTrialInfoControl : UserControl
    {
        private SettingsFlyout parentSettingsFlyout = null;   // For Windows
        private Page parentSettingsPage = null;               // For Windows Phone.

        private bool isShowingEmailSection = true;
        private string defaultEmailSubject;
        private string defaultEmailBody;

        public AppTrialInfoControl()
        {
            this.InitializeComponent();
        }

        public SettingsFlyout ParentSettingsFlyout
        {
            get
            {
                return parentSettingsFlyout;
            }
            set
            {
                parentSettingsFlyout = value;
            }
        }
        public Page ParentSettingsPage
        {
            get
            {
                return parentSettingsPage;
            }
            set
            {
                parentSettingsPage = value;
            }
        }


        public string DefaultEmailSubject
        {
            get
            {
                return defaultEmailSubject;
            }
            set
            {
                defaultEmailSubject = value;
                TextBoxEmailSubject.Text = defaultEmailSubject;  // ???
            }
        }

        public string DefaultEmailBody
        {
            get
            {
                return defaultEmailBody;
            }
            set
            {
                defaultEmailBody = value;
                TextBoxEmailBody.Text = defaultEmailBody;  // ???
            }
        }

        // tbd:
        public bool IsShowingEmailSection
        {
            get
            {
                return isShowingEmailSection;
            }
            set
            {
                isShowingEmailSection = value;
                if (isShowingEmailSection) {
                    StackPanelEmailSection.Visibility = Visibility.Visible;
                } else {
                    StackPanelEmailSection.Visibility = Visibility.Collapsed;
                }
            }
        }


        // temporary
        public void ShowCancelButton()
        {
            if (isShowingEmailSection) {
                ButtonSend.Width = 160;
                ButtonCancel.Width = 90;
                ButtonSend.Visibility = Visibility.Visible;
                ButtonCancel.Visibility = Visibility.Visible;
            }
        }
        public void HideCancelButton()
        {
            if (isShowingEmailSection) {
                ButtonSend.Width = 255;
                ButtonSend.Visibility = Visibility.Visible;
                ButtonCancel.Visibility = Visibility.Collapsed;
            }
        }
        // temporary


        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            var subject = TextBoxEmailSubject.Text;
            var msgBody = TextBoxEmailBody.Text;
            if (!String.IsNullOrEmpty(subject) && !String.IsNullOrEmpty(msgBody)) {
                var emailShareHandler = new EmailShareHandler(subject, msgBody);
                emailShareHandler.InitiateShare(EmailShareContentFormat.Text);

                // Explicit
                Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
            } else {
                // ????
            }

            // ???
            // DismissSettingsFlyout();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            // ???
            DismissSettingsFlyout();
        }

        private void DismissSettingsFlyout()
        {
            if (parentSettingsFlyout != null) {
                parentSettingsFlyout.Hide();
            } else if (parentSettingsPage != null) {
                // ????
                // Go "back" ???
            }
        }

        private async void ButtonUpgradeToNonTrial_Click(object sender, RoutedEventArgs e)
        {
            await TrialLicensingHelper.Instance.RequestAppPurchaseAsync();
        }

    }
}
