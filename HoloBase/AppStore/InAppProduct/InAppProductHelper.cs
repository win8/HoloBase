﻿using HoloBase.AppStore.Trial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;


namespace HoloBase.AppStore.InAppProduct
{
    public sealed class InAppProductHelper
    {
        private static InAppProductHelper instance = null;
        public static InAppProductHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new InAppProductHelper();
                }
                return instance;
            }
        }
        private InAppProductHelper()
        {
        }


        public ProductLicense GetProductLicense(string iapToken)
        {
            return TrialLicensingHelper.Instance.LicenseInformation.ProductLicenses[iapToken];
        }
        public bool IsProductLicenseActive(string iapToken)
        {
            var productLicense = GetProductLicense(iapToken);
            return (productLicense != null && productLicense.IsActive);
        }


        public async Task RequestIapPurchaseAsync(string iapToken, bool includeReceipt = false)
        {
            // Note that this call initializes the license information in TrialLicensingHelper
            // --> IsSimulated returns the correct value, which is not automatically initialzied.
            if (IsProductLicenseActive(iapToken)) {
                return;
            }
            if (TrialLicensingHelper.Instance.IsSimulated) {
                await CurrentAppSimulator.RequestProductPurchaseAsync(iapToken, includeReceipt);
            } else {
                await CurrentApp.RequestProductPurchaseAsync(iapToken, includeReceipt);
            }
        }

    
    }
}
