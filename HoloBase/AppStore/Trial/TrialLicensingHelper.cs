﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;


namespace HoloBase.AppStore.Trial
{
    // Note:
    // As a general rule,
    // We will (almost) never use time-limited trials.
    // Our trials (even for games) will not have expiration dates.
    // They will be just feature-limited.


    public enum TrialStatus
    {
        Unknown = 0,
        TrialExpired,   // Not used in our apps.
        TrialActive,
        NotInTrial      // Full version.
    }

    public sealed class TrialLicensingHelper
    {
        private static TrialLicensingHelper instance = null;
        public static TrialLicensingHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new TrialLicensingHelper();
                }
                return instance;
            }
        }

        private bool simulated = false;
        private LicenseInformation licenseInformation = null;
        private TrialStatus trialStatus = TrialStatus.Unknown;
        // tbd: cache the last known trialStatus (and, timestamp) ????

        private TrialLicensingHelper()
        {
        }

        // Note that this call may return different values
        //    even without using setter.
        public bool IsSimulated
        {
            get
            {
                return simulated;
            }
        }

        public LicenseInformation GetLicenseInformation(bool simulated = false)
        {
            // ????
            if (licenseInformation == null) {
                InitializeLicense(simulated);
            }
            return licenseInformation;
        }
        public LicenseInformation LicenseInformation
        {
            get
            {
                return GetLicenseInformation();
            }
        }

        public TrialStatus GetTrialStatus(bool simulated = false)
        {
            // ????
            if (licenseInformation == null) {
                InitializeLicense(simulated);
            }
            return trialStatus;
        }

        public TrialStatus TrialStatus
        {
            get
            {
                return GetTrialStatus();
            }
        }

        // Returns true if the user is using/entitled to using "full" version of the app.
        public bool IsNotInTrial()
        {
            return (this.TrialStatus == TrialStatus.NotInTrial);
        }

        // InitializeLicense(.) should be called at the start/launch/resume of the app.
        public void InitializeLicense(bool simulated = false)
        {
            this.simulated = simulated;
            if (simulated) {
                licenseInformation = CurrentAppSimulator.LicenseInformation;
            } else {
                licenseInformation = CurrentApp.LicenseInformation;
            }
            ReloadLicense();

            // Is it safe to call this multiple times????
            licenseInformation.LicenseChanged += new LicenseChangedEventHandler(licenseInformation_LicenseChanged);
        }

        private void licenseInformation_LicenseChanged()
        {
            ReloadLicense(); 
        }

        private void ReloadLicense()
        {
            if (licenseInformation.IsActive) {
                if (licenseInformation.IsTrial) {
                    trialStatus = TrialStatus.TrialActive;
                } else {
                    trialStatus = TrialStatus.NotInTrial;
                }
            } else {
                // trialStatus = TrialStatus.Unknown;         // ???
                trialStatus = TrialStatus.TrialExpired;    // ????
            }
        }


        public async Task RequestAppPurchaseAsync(bool includeReceipt = false)
        {
            // Note that this call initializes the license information.
            // --> IsSimulated returns the correct value, which is not automatically initialzied otherwise.
            if (IsNotInTrial()) {
                return;
            }
            if (simulated) {
                await CurrentAppSimulator.RequestAppPurchaseAsync(includeReceipt);
            } else {
                await CurrentApp.RequestAppPurchaseAsync(includeReceipt);
            }
        }


    }
}
