﻿using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.SystemSettings.ViewModels
{
    public class SettingsShortcutListViewModel : IViewModel, INotifyPropertyChanged
    {
        public SettingsShortcutListViewModel()
        {
        }




        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
