﻿using HoloBase.SystemSettings.ViewModels;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.SystemSettings
{
    public sealed partial class SettingsShortcutCubbyControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private SettingsShortcutListViewModel viewModel;
        public SettingsShortcutCubbyControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new SettingsShortcutListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SettingsShortcutListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SettingsShortcutListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

    
    }
}