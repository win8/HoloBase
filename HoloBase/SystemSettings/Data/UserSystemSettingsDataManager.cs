﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.SystemSettings.Data
{
    public sealed class UserSystemSettingsDataManager
    {
        private static UserSystemSettingsDataManager instance = null;
        public static UserSystemSettingsDataManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new UserSystemSettingsDataManager();
                }
                return instance;
            }
        }

    
    
    }
}
