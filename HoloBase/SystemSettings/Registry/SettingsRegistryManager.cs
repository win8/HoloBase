﻿using HoloBase.SystemSettings.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.SystemSettings.Registry
{
    public class SettingsRegistryManager
    {
        private static SettingsRegistryManager instance = null;
        public static SettingsRegistryManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new SettingsRegistryManager();
                }
                return instance;
            }
        }

        private SettingsPageUriRegistry settingsPageUriRegistry = null;
        private SettingsRegistryManager()
        {
            // temporary
            var schemes = new SystemSettingsSchemesForWindows8();
            settingsPageUriRegistry = new SettingsPageUriRegistry(schemes);
        }

        public IDictionary<string, SystemSettingsUriStruct> UriSchemes
        {
            get
            {
                return settingsPageUriRegistry.UriSchemes;
            }
        }
        public IList<string> ValidSchemes
        {
            get
            {
                return settingsPageUriRegistry.ValidSchemes;
            }
        }

        public SystemSettingsUriStruct GetSettingsUriScheme(string key)
        {
            return settingsPageUriRegistry.GetSettingsUriScheme(key);
        }
        public string GetSettingsUri(string key)
        {
            return settingsPageUriRegistry.GetSettingsUri(key);
        }

    }

}
