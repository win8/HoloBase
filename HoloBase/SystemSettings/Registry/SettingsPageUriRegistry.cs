﻿using HoloBase.SystemSettings.Common;
using HoloBase.SystemSettings.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.SystemSettings.Registry
{
    // How to display built-in settings pages by using the ms-settings protocol (XAML)
    // https://msdn.microsoft.com/en-us/library/windows/apps/xaml/dn741261.aspx

    public static class SettingsPageUris
    {
        public const string KEY_SETTINGS_HOME = "SettingsHome";
        public const string KEY_SETTINGS_DISPLAY = "SettingsDisplay";
        // public const string KEY_SETTINGS_SCREEN = "SettingsScreen";
        public const string KEY_SETTINGS_NOTIFICATIONS = "SettingsNotifications";
        public const string KEY_SETTINGS_STORAGE_SENSE = "SettingsStorageSense";
        public const string KEY_SETTINGS_BATTERY_SAVER = "SettingsBatterySaver";
        public const string KEY_SETTINGS_MAPS = "SettingsMaps";
        public const string KEY_SETTINGS_BLUETOOTH = "SettingsBluetooth";
        public const string KEY_SETTINGS_WIFI = "SettingsWiFi";
        public const string KEY_SETTINGS_AIRPLANE_MODE = "SettingsAirplaneMode";
        public const string KEY_SETTINGS_CELLULAR = "SettingsCellular";
        public const string KEY_SETTINGS_DATA_SENSE = "SettingsDataSense";
        public const string KEY_SETTINGS_NFC_PROXIMITY = "SettingsNFCProximity";
        public const string KEY_SETTINGS_NFC_TRANSACTIONS = "SettingsNFCTransactions";
        public const string KEY_SETTINGS_PROXY = "SettingsProxy";
        public const string KEY_SETTINGS_LOCK_SCREEN = "SettingsLockScreen";
        public const string KEY_SETTINGS_ACCOUNTS = "SettingsAccounts";
        public const string KEY_SETTINGS_WORKPLACE = "SettingsWorkplace";
        public const string KEY_SETTINGS_REGION_LANGUAGE = "SettingsRegionLanguage";
        public const string KEY_SETTINGS_LOCATION = "SettingsLocation";
        public const string KEY_SETTINGS_WEBCAM = "SettingsWebCam";
        public const string KEY_SETTINGS_MICROPHONE = "SettingsMicrophone";
        public const string KEY_SETTINGS_CUSTOM_DEVICES = "SettingsCustomDevices";


        // temporary
        // Bit mask.
        private const int WINDOWS = 1;
        private const int WINDOWS_PHONE = 2;
        // BOTH == 3.
        private static readonly IDictionary<string, int> availabilityMap;
        static SettingsPageUris()
        {
            // temporary
            availabilityMap[KEY_SETTINGS_HOME] = 3;
            availabilityMap[KEY_SETTINGS_DISPLAY] = 3;
            // availabilityMap[KEY_SETTINGS_SCREEN] = 3;
            availabilityMap[KEY_SETTINGS_NOTIFICATIONS] = 3;
            availabilityMap[KEY_SETTINGS_STORAGE_SENSE] = 3;
            availabilityMap[KEY_SETTINGS_BATTERY_SAVER] = 3;
            availabilityMap[KEY_SETTINGS_MAPS] = 3;
            availabilityMap[KEY_SETTINGS_BLUETOOTH] = 3;
            availabilityMap[KEY_SETTINGS_WIFI] = 3;
            availabilityMap[KEY_SETTINGS_AIRPLANE_MODE] = 3;
            availabilityMap[KEY_SETTINGS_CELLULAR] = 3;
            availabilityMap[KEY_SETTINGS_DATA_SENSE] = 3;
            availabilityMap[KEY_SETTINGS_NFC_PROXIMITY] = 2;
            availabilityMap[KEY_SETTINGS_NFC_TRANSACTIONS] = 2;
            availabilityMap[KEY_SETTINGS_PROXY] = 1;
            availabilityMap[KEY_SETTINGS_LOCK_SCREEN] = 3;
            availabilityMap[KEY_SETTINGS_ACCOUNTS] = 3;
            availabilityMap[KEY_SETTINGS_WORKPLACE] = 3;
            availabilityMap[KEY_SETTINGS_REGION_LANGUAGE] = 3;
            availabilityMap[KEY_SETTINGS_LOCATION] = 3;
            availabilityMap[KEY_SETTINGS_WEBCAM] = 3;
            availabilityMap[KEY_SETTINGS_MICROPHONE] = 3;
            availabilityMap[KEY_SETTINGS_CUSTOM_DEVICES] = 3;
        }

        public static bool IsAvailableOnWindows(string settingsKey)
        {
            return (availabilityMap.ContainsKey(settingsKey) && ((availabilityMap[settingsKey] & WINDOWS) != 0));
        }
        public static bool IsAvailableOnWindowsPhone(string settingsKey)
        {
            return (availabilityMap.ContainsKey(settingsKey) && ((availabilityMap[settingsKey] & WINDOWS_PHONE) != 0));
        }
    }


    internal class SettingsPageUriRegistry
    {
        private SystemSettingsSchemes schemes;
        internal SettingsPageUriRegistry(SystemSettingsSchemes schemes)
        {
            this.schemes = schemes;
        }

        internal IDictionary<string, SystemSettingsUriStruct> UriSchemes
        {
            get
            {
                return schemes.UriSchemes;
            }
        }
        internal IList<string> ValidSchemes
        {
            get
            {
                return schemes.ValidSchemes;
            }
        }
        
        internal SystemSettingsUriStruct GetSettingsUriScheme(string key)
        {
            return schemes.GetSettingsUriScheme(key);
        }
        internal string GetSettingsUri(string key)
        {
            // return GetSettingsUriScheme(key).Uri;
            return schemes.GetSettingsUri(key);
        }
    }


    internal abstract class SystemSettingsSchemes
    {
        internal protected static readonly IDictionary<string, SystemSettingsUriStruct> uriSchemes = new Dictionary<string, SystemSettingsUriStruct>();
        internal protected static readonly IList<string> validSchemes = new List<string>();

        internal IDictionary<string, SystemSettingsUriStruct> UriSchemes
        {
            get
            {
                return uriSchemes;
            }
        }
        internal IList<string> ValidSchemes
        {
            get
            {
                return validSchemes;
            }
        }

        internal SystemSettingsUriStruct GetSettingsUriScheme(string key)
        {
            // tbd:
            if (UriSchemes.ContainsKey(key)) {
                return UriSchemes[key];
            }
            return SystemSettingsUriStruct.Null;
        }
        internal string GetSettingsUri(string key)
        {
            return GetSettingsUriScheme(key).Uri;
        }
    }

    internal sealed class SystemSettingsSchemesForWindows8 : SystemSettingsSchemes
    {
        // Null value means the scheme is not being used.
        private const string URI_SETTINGS_HOME = "ms-settings://";
        private const string URI_SETTINGS_DISPLAY = "ms-settings-screenrotation://";
        // private const string URI_SETTINGS_SCREEN = "";
        private const string URI_SETTINGS_NOTIFICATIONS = "ms-settings-notifications://";
        private const string URI_SETTINGS_STORAGE_SENSE = null;
        private const string URI_SETTINGS_BATTERY_SAVER = "ms-settings-power://";
        private const string URI_SETTINGS_MAPS = null;
        private const string URI_SETTINGS_BLUETOOTH = "ms-settings-bluetooth://";
        private const string URI_SETTINGS_WIFI = "ms-settings-wifi://";
        private const string URI_SETTINGS_AIRPLANE_MODE = "ms-settings-airplanemode://";
        private const string URI_SETTINGS_CELLULAR = "ms-settings-cellular://";
        private const string URI_SETTINGS_DATA_SENSE = null;
        private const string URI_SETTINGS_NFC_PROXIMITY = "ms-settings-proximity://";
        private const string URI_SETTINGS_NFC_TRANSACTIONS = "ms-settings-nfctransactions://";
        private const string URI_SETTINGS_PROXY = null;
        private const string URI_SETTINGS_LOCK_SCREEN = "ms-settings-lock://";
        private const string URI_SETTINGS_ACCOUNTS = "ms-settings-emailandaccounts://";
        private const string URI_SETTINGS_WORKPLACE = "ms-settings-workplace://";
        private const string URI_SETTINGS_REGION_LANGUAGE = null;
        private const string URI_SETTINGS_LOCATION = "ms-settings-location://";
        private const string URI_SETTINGS_WEBCAM = null;
        private const string URI_SETTINGS_MICROPHONE = null;
        private const string URI_SETTINGS_CUSTOM_DEVICES = null;

        static SystemSettingsSchemesForWindows8()
        {
            uriSchemes[SettingsPageUris.KEY_SETTINGS_HOME] = new SystemSettingsUriStruct(URI_SETTINGS_HOME, SystemSettingsCategory.Home, "Home", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_DISPLAY] = new SystemSettingsUriStruct(URI_SETTINGS_DISPLAY, SystemSettingsCategory.System, "Display", "");
            // uriSchemes[SettingsPageUris.KEY_SETTINGS_SCREEN] = new SystemSettingsUriStruct(URI_SETTINGS_SCREEN, SystemSettingsCategory.System, "", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_NOTIFICATIONS] = new SystemSettingsUriStruct(URI_SETTINGS_NOTIFICATIONS, SystemSettingsCategory.System, "Notifications", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_STORAGE_SENSE] = new SystemSettingsUriStruct(URI_SETTINGS_STORAGE_SENSE, SystemSettingsCategory.System, "Storage Sense", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_BATTERY_SAVER] = new SystemSettingsUriStruct(URI_SETTINGS_BATTERY_SAVER, SystemSettingsCategory.System, "Batter Saver", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_MAPS] = new SystemSettingsUriStruct(URI_SETTINGS_MAPS, SystemSettingsCategory.System, "Maps", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_BLUETOOTH] = new SystemSettingsUriStruct(URI_SETTINGS_BLUETOOTH, SystemSettingsCategory.Devices, "Bluetooth", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_WIFI] = new SystemSettingsUriStruct(URI_SETTINGS_WIFI, SystemSettingsCategory.Network, "Wi-Fi", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_AIRPLANE_MODE] = new SystemSettingsUriStruct(URI_SETTINGS_AIRPLANE_MODE, SystemSettingsCategory.Network, "Airplane mode", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_CELLULAR] = new SystemSettingsUriStruct(URI_SETTINGS_CELLULAR, SystemSettingsCategory.Network, "Cellular", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_DATA_SENSE] = new SystemSettingsUriStruct(URI_SETTINGS_DATA_SENSE, SystemSettingsCategory.Network, "Data Sense", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_NFC_PROXIMITY] = new SystemSettingsUriStruct(URI_SETTINGS_NFC_PROXIMITY, SystemSettingsCategory.Network, "NFC - Proximity", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_NFC_TRANSACTIONS] = new SystemSettingsUriStruct(URI_SETTINGS_NFC_TRANSACTIONS, SystemSettingsCategory.Network, "NFC - Transactions", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_PROXY] = new SystemSettingsUriStruct(URI_SETTINGS_PROXY, SystemSettingsCategory.Network, "Network Proxy", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_LOCK_SCREEN] = new SystemSettingsUriStruct(URI_SETTINGS_LOCK_SCREEN, SystemSettingsCategory.Personalization, "Lock Screen", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_ACCOUNTS] = new SystemSettingsUriStruct(URI_SETTINGS_ACCOUNTS, SystemSettingsCategory.Accounts, "Your Account", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_WORKPLACE] = new SystemSettingsUriStruct(URI_SETTINGS_WORKPLACE, SystemSettingsCategory.Accounts, "Your Workplace", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_REGION_LANGUAGE] = new SystemSettingsUriStruct(URI_SETTINGS_REGION_LANGUAGE, SystemSettingsCategory.Locale, "Region & Language", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_LOCATION] = new SystemSettingsUriStruct(URI_SETTINGS_LOCATION, SystemSettingsCategory.Privacy, "Privacy - Location", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_WEBCAM] = new SystemSettingsUriStruct(URI_SETTINGS_WEBCAM, SystemSettingsCategory.Privacy, "Privacy - Webcam", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_MICROPHONE] = new SystemSettingsUriStruct(URI_SETTINGS_MICROPHONE, SystemSettingsCategory.Privacy, "Privacy - Microphone", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_CUSTOM_DEVICES] = new SystemSettingsUriStruct(URI_SETTINGS_CUSTOM_DEVICES, SystemSettingsCategory.Privacy, "Privacy - Other", "");

            foreach (var s in uriSchemes) {
                if (s.Value.Uri != null) {
                    validSchemes.Add(s.Key);
                }
            }
        }

    }

    internal sealed class SystemSettingsSchemesForWindows10 : SystemSettingsSchemes
    {
        private const string URI_SETTINGS_HOME = "";
        private const string URI_SETTINGS_DISPLAY = "ms-settings://screenrotation";
        // private const string URI_SETTINGS_SCREEN = "";
        private const string URI_SETTINGS_NOTIFICATIONS = "ms-settings://notifications";
        private const string URI_SETTINGS_STORAGE_SENSE = "ms-settings://storagesense";
        private const string URI_SETTINGS_BATTERY_SAVER = "ms-settings://batterysaver";
        private const string URI_SETTINGS_MAPS = "ms-settings://maps";
        private const string URI_SETTINGS_BLUETOOTH = "ms-settings://bluetooth";
        private const string URI_SETTINGS_WIFI = "ms-settings://network/wifi";
        private const string URI_SETTINGS_AIRPLANE_MODE = "ms-settings://network/airplanemode";
        private const string URI_SETTINGS_CELLULAR = "ms-settings://network/cellular";
        private const string URI_SETTINGS_DATA_SENSE = "ms-settings://datasense";
        private const string URI_SETTINGS_NFC_PROXIMITY = "ms-settings://proximity";
        private const string URI_SETTINGS_NFC_TRANSACTIONS = "ms-settings://nfctransactions";
        private const string URI_SETTINGS_PROXY = "ms-settings://network/proxy";
        private const string URI_SETTINGS_LOCK_SCREEN = "ms-settings://lockscreen";
        private const string URI_SETTINGS_ACCOUNTS = "ms-settings://emailandaccounts";
        private const string URI_SETTINGS_WORKPLACE = "ms-settings://workplace";
        private const string URI_SETTINGS_REGION_LANGUAGE = "ms-settings://regionlanguage";
        private const string URI_SETTINGS_LOCATION = "ms-settings://privacy/location";
        private const string URI_SETTINGS_WEBCAM = "ms-settings://privacy/webcam";
        private const string URI_SETTINGS_MICROPHONE = "ms-settings://privacy/microphone";
        private const string URI_SETTINGS_CUSTOM_DEVICES = "ms-settings://privacy/customdevices";

        static SystemSettingsSchemesForWindows10()
        {
            uriSchemes[SettingsPageUris.KEY_SETTINGS_HOME] = new SystemSettingsUriStruct(URI_SETTINGS_HOME, SystemSettingsCategory.Home, "Home", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_DISPLAY] = new SystemSettingsUriStruct(URI_SETTINGS_DISPLAY, SystemSettingsCategory.System, "Display", "");
            // uriSchemes[SettingsPageUris.KEY_SETTINGS_SCREEN] = new SystemSettingsUriStruct(URI_SETTINGS_SCREEN, SystemSettingsCategory.System, "", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_NOTIFICATIONS] = new SystemSettingsUriStruct(URI_SETTINGS_NOTIFICATIONS, SystemSettingsCategory.System, "Notifications", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_STORAGE_SENSE] = new SystemSettingsUriStruct(URI_SETTINGS_STORAGE_SENSE, SystemSettingsCategory.System, "Storage Sense", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_BATTERY_SAVER] = new SystemSettingsUriStruct(URI_SETTINGS_BATTERY_SAVER, SystemSettingsCategory.System, "Batter Saver", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_MAPS] = new SystemSettingsUriStruct(URI_SETTINGS_MAPS, SystemSettingsCategory.System, "Maps", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_BLUETOOTH] = new SystemSettingsUriStruct(URI_SETTINGS_BLUETOOTH, SystemSettingsCategory.Devices, "Bluetooth", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_WIFI] = new SystemSettingsUriStruct(URI_SETTINGS_WIFI, SystemSettingsCategory.Network, "Wi-Fi", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_AIRPLANE_MODE] = new SystemSettingsUriStruct(URI_SETTINGS_AIRPLANE_MODE, SystemSettingsCategory.Network, "Airplane mode", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_CELLULAR] = new SystemSettingsUriStruct(URI_SETTINGS_CELLULAR, SystemSettingsCategory.Network, "Cellular", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_DATA_SENSE] = new SystemSettingsUriStruct(URI_SETTINGS_DATA_SENSE, SystemSettingsCategory.Network, "Data Sense", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_NFC_PROXIMITY] = new SystemSettingsUriStruct(URI_SETTINGS_NFC_PROXIMITY, SystemSettingsCategory.Network, "NFC - Proximity", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_NFC_TRANSACTIONS] = new SystemSettingsUriStruct(URI_SETTINGS_NFC_TRANSACTIONS, SystemSettingsCategory.Network, "NFC - Transactions", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_PROXY] = new SystemSettingsUriStruct(URI_SETTINGS_PROXY, SystemSettingsCategory.Network, "Network Proxy", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_LOCK_SCREEN] = new SystemSettingsUriStruct(URI_SETTINGS_LOCK_SCREEN, SystemSettingsCategory.Personalization, "Lock Screen", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_ACCOUNTS] = new SystemSettingsUriStruct(URI_SETTINGS_ACCOUNTS, SystemSettingsCategory.Accounts, "Your Account", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_WORKPLACE] = new SystemSettingsUriStruct(URI_SETTINGS_WORKPLACE, SystemSettingsCategory.Accounts, "Your Workplace", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_REGION_LANGUAGE] = new SystemSettingsUriStruct(URI_SETTINGS_REGION_LANGUAGE, SystemSettingsCategory.Locale, "Region & Language", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_LOCATION] = new SystemSettingsUriStruct(URI_SETTINGS_LOCATION, SystemSettingsCategory.Privacy, "Privacy - Location", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_WEBCAM] = new SystemSettingsUriStruct(URI_SETTINGS_WEBCAM, SystemSettingsCategory.Privacy, "Privacy - Webcam", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_MICROPHONE] = new SystemSettingsUriStruct(URI_SETTINGS_MICROPHONE, SystemSettingsCategory.Privacy, "Privacy - Microphone", "");
            uriSchemes[SettingsPageUris.KEY_SETTINGS_CUSTOM_DEVICES] = new SystemSettingsUriStruct(URI_SETTINGS_CUSTOM_DEVICES, SystemSettingsCategory.Privacy, "Privacy - Other", "");

            foreach (var s in uriSchemes) {
                if (s.Value.Uri != null) {
                    validSchemes.Add(s.Key);
                }
            }
        }
    }

}

