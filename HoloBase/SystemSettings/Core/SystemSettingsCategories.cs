﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.SystemSettings.Core
{
    public enum SystemSettingsCategory
    {
        Unknown = 0,
        Home,    // Settings landing page
        System,
        Devices,
        Network,
        Personalization,
        Accounts,
        Locale,    // time and language
        Privacy,
        // ...
    }

    public static class SystemSettingsCategories
    {

    }

}

