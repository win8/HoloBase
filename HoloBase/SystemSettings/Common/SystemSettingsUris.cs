﻿using HoloBase.SystemSettings.Core;
using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.SystemSettings.Common
{
    public struct SystemSettingsUriStruct
    {
        public static readonly SystemSettingsUriStruct Null = new SystemSettingsUriStruct(null);

        private string uri;
        private SystemSettingsCategory category;
        private string name;
        private string description;

        public SystemSettingsUriStruct(string uri, SystemSettingsCategory category = SystemSettingsCategory.Unknown, string name = null, string description = null)
        {
            this.uri = uri;
            this.category = category;
            this.name = name;
            this.description = description;
        }

        public string Uri
        {
            get
            {
                return uri;
            }
            set
            {
                uri = value;
            }
        }
        public SystemSettingsCategory Category
        {
            get
            {
                return category;
            }
            set
            {
                category = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Uri:").Append(Uri).Append("; ");
            sb.Append("Category:").Append(Category).Append("; ");
            sb.Append("Name:").Append(Name).Append("; ");
            sb.Append("Description:").Append(Description);
            return sb.ToString();
        }
    }


    public class SystemSettingsUriWrap
    {
        private SystemSettingsUriStruct systemSettingsUriStruct;
        private Symbol iconSymbol;
        private ColorStruct backgroundColor;

        public SystemSettingsUriWrap(SystemSettingsUriStruct systemSettingsUriStruct)
            : this(systemSettingsUriStruct, Symbol.Clear)  // ????
        {
        }
        public SystemSettingsUriWrap(SystemSettingsUriStruct systemSettingsUriStruct, Symbol iconSymbol)
            : this(systemSettingsUriStruct, iconSymbol, ColorStruct.Transparent)  // ????
        {
        }
        public SystemSettingsUriWrap(SystemSettingsUriStruct systemSettingsUriStruct, Symbol iconSymbol, ColorStruct backgroundColor)
        {
            this.systemSettingsUriStruct = systemSettingsUriStruct;
            this.iconSymbol = iconSymbol;
            this.backgroundColor = backgroundColor;
        }


        public string Uri
        {
            get
            {
                return systemSettingsUriStruct.Uri;
            }
            set
            {
                systemSettingsUriStruct.Uri = value;
            }
        }
        public SystemSettingsCategory Category
        {
            get
            {
                return systemSettingsUriStruct.Category;
            }
            set
            {
                systemSettingsUriStruct.Category = value;
            }
        }
        public string Name
        {
            get
            {
                return systemSettingsUriStruct.Name;
            }
            set
            {
                systemSettingsUriStruct.Name = value;
            }
        }
        public string Description
        {
            get
            {
                return systemSettingsUriStruct.Description;
            }
            set
            {
                systemSettingsUriStruct.Description = value;
            }
        }

        public Symbol IconSymbol
        {
            get
            {
                return iconSymbol;
            }
            set
            {
                iconSymbol = value;
            }
        }
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
            }
        }


        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(base.ToString()).Append("; ");
            sb.Append("IconSymbol:").Append(IconSymbol).Append("; ");
            sb.Append("BackgroundColor:").Append(BackgroundColor);
            return sb.ToString();
        }

    }

}
