﻿using HoloBase.SystemSettings.ViewModels;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.SystemSettings
{
    public sealed partial class SettingsShortcutViewControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private SettingsShortcutItemViewModel viewModel;
        public SettingsShortcutViewControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new SettingsShortcutItemViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SettingsShortcutItemViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SettingsShortcutItemViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

    
    }
}