﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;


namespace HoloBase.Share
{
    public sealed class TextShareHandler
    {
        private string title;
        private string description;
        private string text;

        // Title is mandatory.
        public TextShareHandler(string title)
            : this(title, null)
        {
        }
        public TextShareHandler(string title, string description)
            : this(title, description, null)
        {
        }
        public TextShareHandler(string title, string description, string text)
        {
            this.title = title;
            this.description = description;
            this.text = text;
        }

        public void InitiateShare()
        {
            RegisterForShare();
        }
        public void InitiateShare(string text)
        {
            this.text = text;
            RegisterForShare();
        }

        private void RegisterForShare()
        {
            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager,
                DataRequestedEventArgs>(this.ShareTextHandler);
        }

        private void ShareTextHandler(DataTransferManager sender, DataRequestedEventArgs e)
        {
            DataRequest request = e.Request;
            if (!String.IsNullOrEmpty(title)) {
                request.Data.Properties.Title = title;
            } else {
                // ????
                request.Data.Properties.Title = " ";
            }
            if (!String.IsNullOrEmpty(description)) {
                request.Data.Properties.Description = description;
            }
            if (!String.IsNullOrEmpty(text)) {
                request.Data.SetText(text);
            }
        }

    }
}
