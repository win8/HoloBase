﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;


namespace HoloBase.Version
{
    public static class VersionUtil
    {
        // Note that the following version string <--> id conversion relies on
        //   the assumption that each component of the version string is no bigger than 4 digits.

        // temporary
        private static readonly ulong[] FACTOR = new ulong[] { 1000000000000UL, 1000000000UL, 10000UL, 1UL };

        // versionString: a.b.c.d
        // versionCode: a * 100000000000 + b * 100000000 + c * 10000 + d;
        public static ulong GetVersionCode(string versionStr)
        {
            var code = 0UL;
            var arr = ParseVersionString(versionStr);
            for (var i = 0; i < 4; i++) {
                code += (ulong) arr[i] * FACTOR[i];
            }
            return code;
        }

        // array size: 4
        public static int[] ParseVersionString(string versionStr)
        {
            var parts = versionStr.Split('.');
            var arr = new int[4];
            for (var i = 0; i < 4; i++) {
                arr[i] = 0;
                if(parts.Length > i) {
                    try {
                        arr[i] = Convert.ToInt32(parts[i]);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
            }
            return arr;
        }

        public static string GetVersionString(ulong versionCode)
        {
            var sb = new StringBuilder();
            var rem = versionCode;
            for (var i = 0; i < 4; i++) {
                var part = rem / FACTOR[i];
                sb.Append(part);
                if (i != 3) {
                    sb.Append(".");
                }
                rem = rem % FACTOR[i];
            }
            return sb.ToString();
        }


        public static string GetCurrentVersionString()
        {
            var versionString = (string) null;
            var packageVersion = Package.Current.Id.Version;
            // if (packageVersion != null) {
                // versionString = packageVersion.ToString();   // ???
                versionString = String.Format("{0}.{1}.{2}.{3}", packageVersion.Major, packageVersion.Minor, packageVersion.Revision, packageVersion.Build);
            // }
            return versionString;
        }
        public static ulong GetCurrentVersionCode()
        {
            return GetVersionCode(GetCurrentVersionString());
        }

    }
}
