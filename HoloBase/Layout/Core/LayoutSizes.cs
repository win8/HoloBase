﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;


namespace HoloBase.Layout.Core
{

    public static class LayoutVisualStates
    {
        // ??? To be used in a XAML ????
        public static readonly string LAYOUT_VISUAL_STATE_GROUP = "LayoutVisualStateGroup";

    }

    // Cf: Layout rules ????
    public enum LayoutPickOrder
    {
        Indeterminate = 0,    // ???
        AreaWise,             // Pick the bigger area first.
        // WidthOnly,
        // HeightOnly,
        WidthFirst,           // Bigger width first, then bigger height for a given width...
        HeightFirst,          // Bigger height first, then bigger width for a given height...
        // aspect ratio - based ????
        // ....
    }


    // Predefined layout sizes.
    // Note that these are "minimum" sizes for layouts.
    // Layouts (of a given layout size) can expand, but cannot shrink below the minimum size.
    // ...
    public enum LayoutSize
    {
        // tbd:
        // Adding this affects the length of the enum.....
        // ....
        // Indeterminate = 0,
        // Normal = 1,      // "Default". Normal can be any one of the VS states (specific to a particular widget)

        // This should be removed from the total count.
        Collapsed = 0,
        // ...


        ExtraNarrow,     // 150 x H
        Narrow,          // 310 x H
        Median,          // 470 x H   // Use 500 for winrt default min size ????
        Regular,         // 630 x H
        Wide,            // 950 x H
        ExtraWide,       // 1270 x H
        SuperWide,       // 1590 x H

        Dwarf,           // W x 70
        Short,           // W x 150
        Medium,          // W x 310
        Tall,            // W x 470
        Double,          // W x 630
        Triple,          // W x 950
        Quadruple,       // W x 1270


        // "Square" does not mean the control size is square.
        // These are just minimum sizes.
        TinySquare,            // 70 x 70      // ????
        SmallSquare,           // 100 x 100    // ????
        SmallBlockLandscape,   // 120 x 80     // ????
        SmallBlockPortrait,    // 80 x 120     // ????
        // --> ExtraNarrowShort
        //SmallSquare,      // 150 x 150


        // [For Microsoft band]
        ScrollableTileWithPeek,      // 235 x H  <- 245 x H   (35: Peek)
        ScrollableTileWithoutPeek,   // 270 x H  <- 280 x H   (280 = 320 - 40/back button)
        FixedHeightTileWithPeek,     // 235 x 100  <- 245 x 106
        FixedHeightTileWithoutPeek,  // 270 x 100  <- 280 x 106
        // [For Microsoft band]


        // [Full Screen Layouts]
        // These are just size names,
        // not necessarily specific to phones/tablets,
        // or even to device orientations
        // Suffix, 240, 140, etc. are not scale factors.
        // They represent ppi.
        SmallPhoneLandscape240,    // 500 x 300   (resolution: 1280x768 ?)
        SmallPhonePortrait240,     // 300 x 500
        MediumPhoneLandscape240,   // 580 x 320   (resolution: ??)
        MediumPhonePortrait240,    // 320 x 580

        SmallTabletLandscape180,   // 680 x 400   (resolution: 1280x800)
        SmallTabletPortrait180,    // 400 x 680
        SmallTabletLandscape140,   // 860 x 520
        SmallTabletPortrait140,    // 520 x 860
        // SmallTabletLandscape100,   // 1200 x 740
        // SmallTabletPortrait100,    // 740 x 1200
        MediumTabletLandscape180,   // 760 x 560   (resolution: 1440x1080)
        MediumTabletPortrait180,    // 560 x 760
        MediumTabletLandscape140,   // 960 x 700
        MediumTabletPortrait140,    // 700 x 960

        HD720pScreenLandscape240,    // 480 x 260   (resolution: 1280x720)
        HD720pScreenPortrait240,     // 260 x 480
        HD720pScreenLandscape140,    // 840 x 460
        HD720pScreenPortrait140,     // 460 x 840
        HD720pScreenLandscape100,    // 1220 x 680  
        HD720pScreenPortrait100,     // 680 x 1220

        SmallScreenLandscape140,    // 920 x 500  (resolution: 1366x768)
        SmallScreenPortrait140,     // 500 x 920
        SmallScreenLandscape100,    // 1300 x 720
        SmallScreenPortrait100,     // 720 x 1300
        HD1080pScreenLandscape100,   // 1840 x 1000  (resolution: 1920x1080)
        HD1080pScreenPortrait100,    // 1000 x 1840
        // HD1080pScreenLandscape140,   // 1300 x 720  --> SmallScreenLandscape100
        // HD1080pScreenPortrait140,    // 720 x 1300
        // [Full Screen Layouts]


        ////////////////////////////////////////////
        // "Widgets"
        // (tbd: Decorators????)

        ExtraNarrowDwarf,     // 150 x 70
        ExtraNarrowShort,     // 150 x 150
        ExtraNarrowMedium,    // 150 x 310
        ExtraNarrowTall,      // 150 x 470
        ExtraNarrowDouble,    // 150 x 630

        NarrowDwarf,     // 310 x 70
        NarrowShort,     // 310 x 150
        NarrowMedium,    // 310 x 310
        NarrowTall,      // 310 x 470
        NarrowDouble,    // 310 x 630
        NarrowTriple,    // 310 x 950

        MedianShort,     // 470 x 150
        MedianMedium,    // 470 x 310
        MedianTall,      // 470 x 470
        MedianDouble,    // 470 x 630
        MedianTriple,    // 470 x 950

        RegularShort,    // 630 x 150
        RegularMedium,   // 630 x 310
        RegularTall,     // 630 x 470
        RegularDouble,   // 630 x 630
        RegularTriple,   // 630 x 950

        WideMedium,      // 950 x 310
        WideTall,        // 950 x 470
        WideDouble,      // 950 x 630
        WideTriple,      // 950 x 950
        WideQuadruple,   // 950 x 1270

        ExtraWideMedium,      // 1270 x 310
        ExtraWideTall,        // 1270 x 470
        ExtraWideDouble,      // 1270 x 630
        ExtraWideTriple,      // 1270 x 950
        ExtraWideQuadruple,   // 1270 x 1270

        SuperWideTall,        // 1590 x 470
        SuperWideDouble,      // 1590 x 630
        SuperWideTriple,      // 1590 x 950
        SuperWideQuadruple,   // 1590 x 1270

    }


    public struct DecoratorPadding
    {
        public static readonly DecoratorPadding Empty = new DecoratorPadding(0);

        // Allow negative paddings???
        //private uint paddingLeft;
        //private uint paddingTop;
        //private uint paddingRight;
        //private uint paddingBottom;
        private int paddingLeft;
        private int paddingTop;
        private int paddingRight;
        private int paddingBottom;

        public DecoratorPadding(int padding)
            : this(padding, padding)
        {
        }
        public DecoratorPadding(int paddingLeftRight, int paddingTopBottom)
            : this(paddingLeftRight, paddingTopBottom, paddingLeftRight, paddingTopBottom)
        {
        }
        public DecoratorPadding(int paddingLeft, int paddingTop, int paddingRight, int paddingBottom)
        {
            this.paddingLeft = paddingLeft;
            this.paddingTop = paddingTop;
            this.paddingRight = paddingRight;
            this.paddingBottom = paddingBottom;
        }

        public int PaddingLeft
        {
            get
            {
                return paddingLeft;
            }
            set
            {
                paddingLeft = value;
            }
        }
        public int PaddingTop
        {
            get
            {
                return paddingTop;
            }
            set
            {
                paddingTop = value;
            }
        }
        public int PaddingRight
        {
            get
            {
                return paddingRight;
            }
            set
            {
                paddingRight = value;
            }
        }
        public int PaddingBottom
        {
            get
            {
                return paddingBottom;
            }
            set
            {
                paddingBottom = value;
            }
        }

    }



    public static class LayoutSizes
    {
        public static string GetLayoutSizeName(LayoutSize layoutSize)
        {
            var name = Enum.GetName(typeof(LayoutSize), layoutSize);
            return name;
        }

        private static IList<LayoutSize> sAllLayoutSizes = null;
        public static IList<LayoutSize> All
        {
            get
            {
                if (sAllLayoutSizes == null) {
                    sAllLayoutSizes = new List<LayoutSize>();
                    foreach (LayoutSize ls in Enum.GetValues(typeof(LayoutSize))) {
                        if (ls != LayoutSize.Collapsed) {    // Collapsed is not really a layout size.
                            sAllLayoutSizes.Add(ls);
                        }
                    }
                }
                return sAllLayoutSizes;
            }
        }

        public static Size GetMinimumSize(LayoutSize layoutSize)
        {
            return GetMinimumSize(layoutSize, DecoratorPadding.Empty);
        }
        public static Size GetMinimumSize(LayoutSize layoutSize, DecoratorPadding decoratorPadding)
        {
            var size = Size.Empty;

            // 0 means not-fixed.
            // Or, it means unbound????
            // Or, it means the smallest dimension along that direction?????
            // --> Which is better???
            // (Below, in the Compare methods,
            //  we adopt the view unbound == infinite.
            //  --> Need to check if this is sensible....)
            // ...
            // Actually,
            // 0 should be interpreted as infinity (or, the biggest possible dimension) in the "containing" context.
            // and, it should be interpreted as 0 (or, the smallest possible dimension) in the "contained" context.
            // ...
            switch (layoutSize) {
                // We cannot really support real 0 ....
                //case LayoutSize.Collapsed:
                //    size = new Size(0, 0);   // 0 here means real zero...
                //    break;

                case LayoutSize.ExtraNarrow:
                    size = new Size(150, 0);
                    break;
                case LayoutSize.Narrow:
                    size = new Size(310, 0);
                    break;
                case LayoutSize.Median:
                    size = new Size(470, 0);
                    break;
                case LayoutSize.Regular:
                    size = new Size(630, 0);
                    break;
                case LayoutSize.Wide:
                    size = new Size(950, 0);
                    break;
                case LayoutSize.ExtraWide:
                    size = new Size(1270, 0);
                    break;
                case LayoutSize.SuperWide:
                    size = new Size(1590, 0);
                    break;

                case LayoutSize.Dwarf:
                    size = new Size(0, 70);
                    break;
                case LayoutSize.Short:
                    size = new Size(0, 150);
                    break;
                case LayoutSize.Medium:
                    size = new Size(0, 310);
                    break;
                case LayoutSize.Tall:
                    size = new Size(0, 470);
                    break;
                case LayoutSize.Double:
                    size = new Size(0, 630);
                    break;
                case LayoutSize.Triple:
                    size = new Size(0, 950);
                    break;
                case LayoutSize.Quadruple:
                    size = new Size(0, 1270);
                    break;



                case LayoutSize.TinySquare:
                    size = new Size(70, 70);
                    break;
                case LayoutSize.SmallSquare:
                    size = new Size(100, 100);
                    break;
                case LayoutSize.SmallBlockLandscape:
                    size = new Size(120, 80);
                    break;
                case LayoutSize.SmallBlockPortrait:
                    size = new Size(80, 120);
                    break;

                // --> ExtraNarrowShort
                //case LayoutSize.SmallSquare:
                //    size = new Size(150, 150);
                //    break;




                case LayoutSize.ScrollableTileWithPeek:
                    size = new Size(235, 0);
                    break;
                case LayoutSize.ScrollableTileWithoutPeek:
                    size = new Size(270, 0);
                    break;
                case LayoutSize.FixedHeightTileWithPeek:
                    size = new Size(235, 100);
                    break;
                case LayoutSize.FixedHeightTileWithoutPeek:
                    size = new Size(270, 100);
                    break;




                case LayoutSize.SmallPhoneLandscape240:
                    size = new Size(500, 300);
                    break;
                case LayoutSize.SmallPhonePortrait240:
                    size = new Size(300, 500);
                    break;
                case LayoutSize.MediumPhoneLandscape240:
                    size = new Size(580, 320);
                    break;
                case LayoutSize.MediumPhonePortrait240:
                    size = new Size(320, 580);
                    break;

                case LayoutSize.SmallTabletLandscape180:
                    size = new Size(680, 400);
                    break;
                case LayoutSize.SmallTabletPortrait180:
                    size = new Size(400, 680);
                    break;
                case LayoutSize.SmallTabletLandscape140:
                    size = new Size(860, 520);
                    break;
                case LayoutSize.SmallTabletPortrait140:
                    size = new Size(520, 860);
                    break;
                case LayoutSize.MediumTabletLandscape180:
                    size = new Size(760, 650);
                    break;
                case LayoutSize.MediumTabletPortrait180:
                    size = new Size(560, 760);
                    break;
                case LayoutSize.MediumTabletLandscape140:
                    size = new Size(960, 700);
                    break;
                case LayoutSize.MediumTabletPortrait140:
                    size = new Size(700, 960);
                    break;

                case LayoutSize.HD720pScreenLandscape240:
                    size = new Size(480, 260);
                    break;
                case LayoutSize.HD720pScreenPortrait240:
                    size = new Size(260, 480);
                    break;
                case LayoutSize.HD720pScreenLandscape140:
                    size = new Size(840, 460);
                    break;
                case LayoutSize.HD720pScreenPortrait140:
                    size = new Size(460, 840);
                    break;
                case LayoutSize.HD720pScreenLandscape100:
                    size = new Size(1220, 680);
                    break;
                case LayoutSize.HD720pScreenPortrait100:
                    size = new Size(680, 1220);
                    break;

                case LayoutSize.SmallScreenLandscape140:
                    size = new Size(920, 500);
                    break;
                case LayoutSize.SmallScreenPortrait140:
                    size = new Size(500, 920);
                    break;
                case LayoutSize.SmallScreenLandscape100:
                    size = new Size(1300, 720);
                    break;
                case LayoutSize.SmallScreenPortrait100:
                    size = new Size(720, 1300);
                    break;
                case LayoutSize.HD1080pScreenLandscape100:
                    size = new Size(1840, 1000);
                    break;
                case LayoutSize.HD1080pScreenPortrait100:
                    size = new Size(1000, 1840);
                    break;




                case LayoutSize.ExtraNarrowDwarf:
                    size = new Size(150, 70);
                    break;
                case LayoutSize.ExtraNarrowShort:
                    size = new Size(150, 150);
                    break;
                case LayoutSize.ExtraNarrowMedium:
                    size = new Size(150, 310);
                    break;
                case LayoutSize.ExtraNarrowTall:
                    size = new Size(150, 470);
                    break;
                case LayoutSize.ExtraNarrowDouble:
                    size = new Size(150, 630);
                    break;


                case LayoutSize.NarrowDwarf:
                    size = new Size(310, 70);
                    break;
                case LayoutSize.NarrowShort:
                    size = new Size(310, 150);
                    break;
                case LayoutSize.NarrowMedium:
                    size = new Size(310, 310);
                    break;
                case LayoutSize.NarrowTall:
                    size = new Size(310, 470);
                    break;
                case LayoutSize.NarrowDouble:
                    size = new Size(310, 630);
                    break;
                case LayoutSize.NarrowTriple:
                    size = new Size(310, 950);
                    break;

                case LayoutSize.MedianShort:
                    size = new Size(470, 150);
                    break;
                case LayoutSize.MedianMedium:
                    size = new Size(470, 310);
                    break;
                case LayoutSize.MedianTall:
                    size = new Size(470, 470);
                    break;
                case LayoutSize.MedianDouble:
                    size = new Size(470, 630);
                    break;
                case LayoutSize.MedianTriple:
                    size = new Size(470, 950);
                    break;

                case LayoutSize.RegularShort:
                    size = new Size(630, 150);
                    break;
                case LayoutSize.RegularMedium:
                    size = new Size(630, 310);
                    break;
                case LayoutSize.RegularTall:
                    size = new Size(630, 470);
                    break;
                case LayoutSize.RegularDouble:
                    size = new Size(630, 630);
                    break;
                case LayoutSize.RegularTriple:
                    size = new Size(630, 950);
                    break;

                case LayoutSize.WideMedium:
                    size = new Size(950, 310);
                    break;
                case LayoutSize.WideTall:
                    size = new Size(950, 470);
                    break;
                case LayoutSize.WideDouble:
                    size = new Size(950, 630);
                    break;
                case LayoutSize.WideTriple:
                    size = new Size(950, 950);
                    break;
                case LayoutSize.WideQuadruple:
                    size = new Size(950, 150);
                    break;

                case LayoutSize.ExtraWideMedium:
                    size = new Size(1270, 310);
                    break;
                case LayoutSize.ExtraWideTall:
                    size = new Size(1270, 470);
                    break;
                case LayoutSize.ExtraWideDouble:
                    size = new Size(1270, 630);
                    break;
                case LayoutSize.ExtraWideTriple:
                    size = new Size(1270, 950);
                    break;
                case LayoutSize.ExtraWideQuadruple:
                    size = new Size(1270, 150);
                    break;

                case LayoutSize.SuperWideTall:
                    size = new Size(1590, 470);
                    break;
                case LayoutSize.SuperWideDouble:
                    size = new Size(1590, 630);
                    break;
                case LayoutSize.SuperWideTriple:
                    size = new Size(1590, 950);
                    break;
                case LayoutSize.SuperWideQuadruple:
                    size = new Size(1590, 150);
                    break;


                // default:
                //     // ???

            }

            // ????
            size = size.AddDecoratorPadding(decoratorPadding);

            return size;
        }

        internal static Size AddDecoratorPadding(this Size size, DecoratorPadding decoratorPadding)
        {
            if (decoratorPadding.Equals(DecoratorPadding.Empty)) {
                return size;
            } else {
                // Note 1: We do not change the "0" value.
                // Note 2: We can potentially use negative paddings...
                var paddedSize = new Size(
                        (size.Width == 0) ? 0 : size.Width + decoratorPadding.PaddingLeft + decoratorPadding.PaddingRight,
                        (size.Height == 0) ? 0 : size.Height + decoratorPadding.PaddingTop + decoratorPadding.PaddingBottom
                    );
                return paddedSize;
            }
        }


        public static bool IsWidthOpenEnded(LayoutSize layoutSize)
        {
            var size = GetMinimumSize(layoutSize);
            return (size.Width == 0);
        }
        public static bool IsHeightOpenEnded(LayoutSize layoutSize)
        {
            var size = GetMinimumSize(layoutSize);
            return (size.Height == 0);
        }



        // tbd:
        // The comparisons are based on the "minimum sizes" of the layoutsizes.
        // ....
        // In the case of the "0" value,
        // We assume it's infinity in the following compare methods
        // This may not always be what we want, however.
        // In some cases, "0" should be treated as zero...
        // ...


        // returns -1 if layoutSize1 < layoutSize2, etc.
        // ...
        public static int CompareWidth(LayoutSize layoutSize1, LayoutSize layoutSize2)
        {
            var size1 = GetMinimumSize(layoutSize1);
            var size2 = GetMinimumSize(layoutSize2);
            // return (size1.Width < size2.Width) ? -1 : ((size1.Width > size2.Width) ? 1 : 0);

            return CompareSizeWidth(size1, size2);
        }
        internal static int CompareSizeWidth(Size size1, Size size2)
        {
            if (size1.Width == 0 && size2.Width == 0) {
                return 0;
            } else if (size1.Width == 0) {
                return 1;
            } else if (size2.Width == 0) {
                return -1;
            } else {
                if (size1.Width < size2.Width) {
                    return -1;
                } else if (size1.Width > size2.Width) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }

        public static int CompareHeight(LayoutSize layoutSize1, LayoutSize layoutSize2)
        {
            var size1 = GetMinimumSize(layoutSize1);
            var size2 = GetMinimumSize(layoutSize2);
            // return (size1.Height < size2.Height) ? -1 : ((size1.Height > size2.Height) ? 1 : 0);

            return CompareSizeHeight(size1, size2);
        }
        internal static int CompareSizeHeight(Size size1, Size size2)
        {
            if (size1.Height == 0 && size2.Height == 0) {
                return 0;
            } else if (size1.Height == 0) {
                return 1;
            } else if (size2.Height == 0) {
                return -1;
            } else {
                if (size1.Height < size2.Height) {
                    return -1;
                } else if (size1.Height > size2.Height) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }

        public static int CompareWidthAndHeight(LayoutSize layoutSize1, LayoutSize layoutSize2)
        {
            var cmp1 = CompareWidth(layoutSize1, layoutSize2);
            if (cmp1 == 0) {   // this case includes both widths are "0".
                var cmp2 = CompareHeight(layoutSize1, layoutSize2);
                return cmp2;
            } else {
                return cmp1;
            }
        }
        public static int CompareHeightAndWidth(LayoutSize layoutSize1, LayoutSize layoutSize2)
        {
            var cmp1 = CompareHeight(layoutSize1, layoutSize2);
            if (cmp1 == 0) {   // this case includes both heights are "0".
                var cmp2 = CompareWidth(layoutSize1, layoutSize2);
                return cmp2;
            } else {
                return cmp1;
            }
        }

        // Returning 0 does not mean the two are the same shape.
        // Note: "Open ended" area is always considered bigger than a finite area. 
        public static int CompareArea(LayoutSize layoutSize1, LayoutSize layoutSize2)
        {
            var size1 = GetMinimumSize(layoutSize1);
            var size2 = GetMinimumSize(layoutSize2);
            return CompareSizeArea(size1, size2);
        }
        internal static int CompareSizeArea(Size size1, Size size2)
        {
            var area1 = size1.Width * size1.Height;
            var area2 = size2.Width * size2.Height;
            if (area1 == 0 && area2 == 0) {
                // tbd:
                // compare width,height if one side is open-ended????

                // Note that we assume both width and height cannot be "0".
                var girth1 = (size1.Width == 0) ? size1.Height : size1.Width;
                var girth2 = (size2.Width == 0) ? size2.Height : size2.Width;

                if (girth1 < girth2) {
                    return -1;
                } else if (girth2 > girth1) {
                    return 1;
                } else {
                    return 0;
                }
            } else if (area1 == 0) {
                return 1;
            } else if (area2 == 0) {
                return -1;
            } else {
                if (area1 < area2) {
                    return -1;
                } else if (area1 > area2) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }

        
        // Not being used...
        // Note: This method behavior is not consistent with "compare" functions.
        // returns 1 if layoutSize1 "contains" layoutSize2, including layoutSize1 == layoutSize2.
        // returns -1 if layoutSize1 is "contained" in layoutSize2.
        // returns 0 if there is no containment relationship between the two.
        // ...
        // TBD:
        // Need to double check this logic.
        // Note that
        // "0" can contain anything, and "0" can be containted in any size.
        // (Hence layoutSize1 can contain layoutSize2 and layoutSize2 can contain layoutSize1 even when they are not exactly the same.
        //  although the value 1 is returned if that happens.)
        private static int ComputeContainment(LayoutSize layoutSize1, LayoutSize layoutSize2)
        {
            var size1 = GetMinimumSize(layoutSize1);
            var size2 = GetMinimumSize(layoutSize2);

            // Note that a layoutsize cannot have both 0 width and 0 height.

            if (size1.Width == 0 && size2.Width == 0) {
                return (size1.Height >= size2.Height) ? 1 : -1;
            } else {
                if (size1.Height == 0 && size2.Height == 0) {
                    return (size1.Width >= size2.Width) ? 1 : -1;
                } else {
                    if (size1.Width == 0 && size2.Height == 0) {
                        return 0;
                    } else if (size2.Width == 0 && size1.Height == 0) {
                        return 0;
                    } else {
                        if (size1.Width == 0) {
                            if (size1.Height >= size2.Height) {
                                return 1;
                            } else {
                                return 0;
                            }
                        } else if (size2.Width == 0) {
                            if (size2.Height >= size1.Height) {
                                return -1;
                            } else {
                                return 0;
                            }
                        } else if (size1.Height == 0) {
                            if (size1.Width >= size2.Width) {
                                return 1;
                            } else {
                                return 0;
                            }
                        } else if (size2.Height == 0) {
                            if (size2.Width >= size1.Width) {
                                return -1;
                            } else {
                                return 0;
                            }
                        } else {
                            // No open-ended dimension, at this point.

                            if (size1.Width >= size2.Width && size1.Height >= size2.Height) {
                                return 1;
                            } else if (size1.Width <= size2.Width && size1.Height <= size2.Height) {
                                // Note the equal signs in both condtions
                                // Because we already checked the equality in both conditionals (in the first if())
                                // this works.
                                return -1;
                            } else {
                                // No specific relationship between the two.
                                return 0;
                            }
                        }
                    }
                }
            }
        }


    }



    public class DefaultLayoutSizeComparerByWidthAscending : IComparer<LayoutSize>
    {
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            return LayoutSizes.CompareWidth(lhs, rhs);
        }
    }
    public class DefaultLayoutSizeComparerByWidthDescending : IComparer<LayoutSize>
    {
        // private IComparer<LayoutSize> ascendingComparer = new DefaultLayoutSizeComparerByWidthAscending();
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * LayoutSizes.CompareWidth(lhs, rhs);
        }
    }

    public class DefaultLayoutSizeComparerByHeightAscending : IComparer<LayoutSize>
    {
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            return LayoutSizes.CompareHeight(lhs, rhs);
        }
    }
    public class DefaultLayoutSizeComparerByHeightDescending : IComparer<LayoutSize>
    {
        // private IComparer<LayoutSize> ascendingComparer = new DefaultLayoutSizeComparerByHeightAscending();
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * LayoutSizes.CompareHeight(lhs, rhs);
        }
    }

    public class DefaultLayoutSizeComparerByWidthAndHeightAscending : IComparer<LayoutSize>
    {
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            return LayoutSizes.CompareWidthAndHeight(lhs, rhs);
        }
    }
    public class DefaultLayoutSizeComparerByWidthAndHeightDescending : IComparer<LayoutSize>
    {
        // private IComparer<LayoutSize> ascendingComparer = new DefaultLayoutSizeComparerByWidthAndHeightAscending();
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * LayoutSizes.CompareWidthAndHeight(lhs, rhs);
        }
    }

    public class DefaultLayoutSizeComparerByHeightAndWidthAscending : IComparer<LayoutSize>
    {
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            return LayoutSizes.CompareHeightAndWidth(lhs, rhs);
        }
    }
    public class DefaultLayoutSizeComparerByHeightAndWidthDescending : IComparer<LayoutSize>
    {
        // private IComparer<LayoutSize> ascendingComparer = new DefaultLayoutSizeComparerByHeightAndWidthAscending();
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * LayoutSizes.CompareHeightAndWidth(lhs, rhs);
        }
    }

    public class DefaultLayoutSizeComparerByAreaAscending : IComparer<LayoutSize>
    {
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            return LayoutSizes.CompareArea(lhs, rhs);
        }
    }
    public class DefaultLayoutSizeComparerByAreaDescending : IComparer<LayoutSize>
    {
        // private IComparer<LayoutSize> ascendingComparer = new DefaultLayoutSizeComparerByAreaAscending();
        public int Compare(LayoutSize lhs, LayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * LayoutSizes.CompareArea(lhs, rhs);
        }
    }



}
