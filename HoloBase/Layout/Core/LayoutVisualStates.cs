﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;


namespace HoloBase.Layout.Core
{

    // TBD: Just use string???
    public struct LayoutVisualState
    {
        // ???
        public static readonly LayoutVisualState Undefined = new LayoutVisualState("Undefined");

        // private bool isNormal;
        private string name;
        // private LayoutSize preferredLayoutSize;   // ????

        // tbd...
        private bool isDisabled;


        public LayoutVisualState(string name)
        {
            // this.isNormal = false;
            this.name = name;
            this.isDisabled = false;
        }

        //public bool IsNormal
        //{
        //    get
        //    {
        //        return isNormal;
        //    }
        //    set
        //    {
        //        isNormal = value;
        //    }
        //}

        public string Name
        {
            get
            {
                return name;
            }
            //private set
            //{
            //    name = value;
            //}
        }

        public bool IsDisabled
        {
            get
            {
                return isDisabled;
            }
            set
            {
                isDisabled = value;
            }
        }

    }
}
