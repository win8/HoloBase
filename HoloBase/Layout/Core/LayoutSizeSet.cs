﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Layout.Core
{
    // To be used by a ILayoutCapable controls/pages....
    public class LayoutSizeSet
    {
        // Ordered ???
        // private IList<LayoutSize> layoutSizes;
        // Add decorator padding ????
        private IList<DecoratedLayoutSize> layoutSizes;  // Not ordered.
        // private IDictionary<LayoutSize, DecoratorPadding> layoutPaddingMap;
        // Note: we use LayoutSize as key rather than DecoratedLayoutSize,
        //       which makes it a bit easier, with some limitations.
        private IDictionary<LayoutSize, LayoutVisualState> visualStateMap;
        // private IDictionary<DecoratedLayoutSize, LayoutVisualState> visualStateMap;
        // This (indirectly) determines ordering.
        private LayoutPickOrder pickOrder;

        // tbd
        // what's the best way to initialize the member vars???
        // ....

        public LayoutSizeSet()
            : this(new List<DecoratedLayoutSize>(), new Dictionary<LayoutSize, LayoutVisualState>())
        {
        }
        public LayoutSizeSet(IList<DecoratedLayoutSize> layoutSizes, IDictionary<LayoutSize, LayoutVisualState> visualStateMap)
            // : this(layoutSizes, visualStateMap, LayoutPickOrder.AreaWise)
            : this(layoutSizes, visualStateMap, LayoutPickOrder.Indeterminate)   // ????
        {
        }
        public LayoutSizeSet(IList<DecoratedLayoutSize> layoutSizes, IDictionary<LayoutSize, LayoutVisualState> visualStateMap, LayoutPickOrder pickOrder)
        {
            this.layoutSizes = layoutSizes;
            this.visualStateMap = visualStateMap;
            this.pickOrder = pickOrder;
            // ....
        }



        public IList<DecoratedLayoutSize> LayoutSizes
        {
            get
            {
                return layoutSizes;
            }
            set
            {
                layoutSizes = value;
            }
        }

        public IDictionary<LayoutSize, LayoutVisualState> VisualStateMap
        {
            get
            {
                return visualStateMap;
            }
            set
            {
                visualStateMap = value;
            }
        }

        public LayoutPickOrder LayoutPickOrder
        {
            get
            {
                return pickOrder;
            }
            set
            {
                pickOrder = value;
            }
        }




        //// string GetNormalLayoutVisualState();
        //LayoutVisualState GetNormalLayoutVisualState();

        //List<LayoutSize> GetAllLayoutSizes();
        //List<LayoutVisualState> GetAllLayoutVisualStates();

        //LayoutVisualState GetLayoutVisualState(LayoutSize layoutSize);
        //List<LayoutSize> GetLayoutSizes(LayoutVisualState layoutVisualState);




    }
}
