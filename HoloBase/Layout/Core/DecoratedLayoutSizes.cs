﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;


namespace HoloBase.Layout.Core
{

    public struct DecoratedLayoutSize
    {
        public static readonly DecoratedLayoutSize Collapsed = new DecoratedLayoutSize(LayoutSize.Collapsed);

        private LayoutSize layoutSize;
        private DecoratorPadding decoratorPadding;

        public DecoratedLayoutSize(LayoutSize layoutSize)
            : this(layoutSize, DecoratorPadding.Empty)
        {
        }
        public DecoratedLayoutSize(LayoutSize layoutSize, DecoratorPadding decoratorPadding)
        {
            this.layoutSize = layoutSize;
            this.decoratorPadding = decoratorPadding;
        }

        public LayoutSize LayoutSize
        {
            get
            {
                return layoutSize;
            }
            set
            {
                layoutSize = value;
            }
        }
        public Size MinimunSize
        {
            get
            {
                return LayoutSizes.GetMinimumSize(layoutSize);
            }
        }

        public DecoratorPadding DecoratorPadding
        {
            get
            {
                return decoratorPadding;
            }
            set
            {
                decoratorPadding = value;
            }
        }

    }

    public static class DecoratedLayoutSizes
    {

        public static bool IsWidthOpenEnded(DecoratedLayoutSize layoutSize)
        {
            var size = LayoutSizes.GetMinimumSize(layoutSize.LayoutSize, layoutSize.DecoratorPadding);
            return (size.Width == 0);
        }
        public static bool IsHeightOpenEnded(DecoratedLayoutSize layoutSize)
        {
            var size = LayoutSizes.GetMinimumSize(layoutSize.LayoutSize, layoutSize.DecoratorPadding);
            return (size.Height == 0);
        }


        public static int CompareWidth(DecoratedLayoutSize layoutSize1, DecoratedLayoutSize layoutSize2)
        {
            var size1 = LayoutSizes.GetMinimumSize(layoutSize1.LayoutSize, layoutSize1.DecoratorPadding);
            var size2 = LayoutSizes.GetMinimumSize(layoutSize2.LayoutSize, layoutSize2.DecoratorPadding);
            return LayoutSizes.CompareSizeWidth(size1, size2);
        }

        public static int CompareHeight(DecoratedLayoutSize layoutSize1, DecoratedLayoutSize layoutSize2)
        {
            var size1 = LayoutSizes.GetMinimumSize(layoutSize1.LayoutSize, layoutSize1.DecoratorPadding);
            var size2 = LayoutSizes.GetMinimumSize(layoutSize2.LayoutSize, layoutSize2.DecoratorPadding);
            return LayoutSizes.CompareSizeHeight(size1, size2);
        }

        public static int CompareWidthAndHeight(DecoratedLayoutSize layoutSize1, DecoratedLayoutSize layoutSize2)
        {
            var cmp1 = CompareWidth(layoutSize1, layoutSize2);
            if (cmp1 == 0) {
                var cmp2 = CompareHeight(layoutSize1, layoutSize2);
                return cmp2;
            } else {
                return cmp1;
            }
        }
        public static int CompareHeightAndWidth(DecoratedLayoutSize layoutSize1, DecoratedLayoutSize layoutSize2)
        {
            var cmp1 = CompareHeight(layoutSize1, layoutSize2);
            if (cmp1 == 0) {
                var cmp2 = CompareWidth(layoutSize1, layoutSize2);
                return cmp2;
            } else {
                return cmp1;
            }
        }

        public static int CompareArea(DecoratedLayoutSize layoutSize1, DecoratedLayoutSize layoutSize2)
        {
            var size1 = LayoutSizes.GetMinimumSize(layoutSize1.LayoutSize, layoutSize1.DecoratorPadding);
            var size2 = LayoutSizes.GetMinimumSize(layoutSize2.LayoutSize, layoutSize2.DecoratorPadding);
            return LayoutSizes.CompareSizeArea(size1, size2);
        }


    }




    public class DefaultDecoratedLayoutSizeComparerByWidthAscending : IComparer<DecoratedLayoutSize>
    {
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            return DecoratedLayoutSizes.CompareWidth(lhs, rhs);
        }
    }
    public class DefaultDecoratedLayoutSizeComparerByWidthDescending : IComparer<DecoratedLayoutSize>
    {
        // private IComparer<DecoratedLayoutSize> ascendingComparer = new DefaultDecoratedLayoutSizeComparerByWidthAscending();
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * DecoratedLayoutSizes.CompareWidth(lhs, rhs);
        }
    }

    public class DefaultDecoratedLayoutSizeComparerByHeightAscending : IComparer<DecoratedLayoutSize>
    {
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            return DecoratedLayoutSizes.CompareHeight(lhs, rhs);
        }
    }
    public class DefaultDecoratedLayoutSizeComparerByHeightDescending : IComparer<DecoratedLayoutSize>
    {
        // private IComparer<DecoratedLayoutSize> ascendingComparer = new DefaultDecoratedLayoutSizeComparerByHeightAscending();
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * DecoratedLayoutSizes.CompareHeight(lhs, rhs);
        }
    }

    public class DefaultDecoratedLayoutSizeComparerByWidthAndHeightAscending : IComparer<DecoratedLayoutSize>
    {
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            return DecoratedLayoutSizes.CompareWidthAndHeight(lhs, rhs);
        }
    }
    public class DefaultDecoratedLayoutSizeComparerByWidthAndHeightDescending : IComparer<DecoratedLayoutSize>
    {
        // private IComparer<LayoutSize> ascendingComparer = new DefaultDecoratedLayoutSizeComparerByWidthAndHeightAscending();
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * DecoratedLayoutSizes.CompareWidthAndHeight(lhs, rhs);
        }
    }

    public class DefaultDecoratedLayoutSizeComparerByHeightAndWidthAscending : IComparer<DecoratedLayoutSize>
    {
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            return DecoratedLayoutSizes.CompareHeightAndWidth(lhs, rhs);
        }
    }
    public class DefaultDecoratedLayoutSizeComparerByHeightAndWidthDescending : IComparer<DecoratedLayoutSize>
    {
        // private IComparer<LayoutSize> ascendingComparer = new DefaultDecoratedLayoutSizeComparerByHeightAndWidthAscending();
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * DecoratedLayoutSizes.CompareHeightAndWidth(lhs, rhs);
        }
    }

    public class DefaultDecoratedLayoutSizeComparerByAreaAscending : IComparer<DecoratedLayoutSize>
    {
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            return DecoratedLayoutSizes.CompareArea(lhs, rhs);
        }
    }
    public class DefaultDecoratedLayoutSizeComparerByAreaDescending : IComparer<DecoratedLayoutSize>
    {
        // private IComparer<LayoutSize> ascendingComparer = new DefaultDecoratedLayoutSizeComparerByAreaAscending();
        public int Compare(DecoratedLayoutSize lhs, DecoratedLayoutSize rhs)
        {
            // return ascendingComparer.Compare(rhs, lhs);
            return -1 * DecoratedLayoutSizes.CompareArea(lhs, rhs);
        }
    }


}
