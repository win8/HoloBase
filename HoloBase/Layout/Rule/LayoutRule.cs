﻿using HoloBase.Layout.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;


namespace HoloBase.Layout.Rule
{
    // Not being used.
    public interface ILayoutRule
    {
        // Size -> LayoutSize
        // LayoutSize -> LayoutVisualState
        // ...

        // TBD:
        ISet<LayoutSize> GetAllowedLayoutSizes(Size availableSize);
        LayoutVisualState GetLayoutVisualState(Size availableSize, LayoutSizeSet layoutSizeSet);


        // Minimum aspect ratio
        // Maximum aspect ratio

        // Preferred layout sizes
        // Excluded layout sizes
        // ...

        // Preferred dimension ????
        //   width, height, area
        // --> Just use the ordering of the layout sizes from the given control/page???




    }
}
