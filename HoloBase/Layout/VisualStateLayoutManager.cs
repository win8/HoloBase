﻿using HoloBase.Layout.Core;
using HoloBase.Layout.Rule;
using HoloBase.Layout.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;


namespace HoloBase.Layout
{
    public sealed class VisualStateLayoutManager
    {
               
        private static VisualStateLayoutManager instance = null;
        public static VisualStateLayoutManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new VisualStateLayoutManager();
                }
                return instance;
            }
        }
        private VisualStateLayoutManager()
        {
        }


        // TBD:
        // Use
        // 1) layoutSet from a control
        // 2) layoutRule from a page (or, parent of the control)
        //

        public LayoutVisualState GetLayoutVisualState(Size availableSize, LayoutSizeSet layoutSizeSet)
        {
            return GetLayoutVisualState(availableSize, layoutSizeSet, null);
        }
        // ILayoutRule is currently not being used.
        public LayoutVisualState GetLayoutVisualState(Size availableSize, LayoutSizeSet layoutSizeSet, ILayoutRule layoutRule)
        {
            // TBD:
            var state = LayoutVisualState.Undefined;   // ???


            var layoutSizes = SizeComparisonHelper.Instance.GetAllowedLayoutSizes(availableSize, layoutSizeSet.LayoutSizes, layoutSizeSet.LayoutPickOrder);
            foreach (var s in layoutSizes) {
                if (! layoutSizeSet.VisualStateMap.ContainsKey(s.LayoutSize)) {
                    continue;
                }

                var vs = layoutSizeSet.VisualStateMap[s.LayoutSize];
                if (vs.IsDisabled) {
                    continue;
                }

                state = vs;
                break;
            }

            return state;
        }



    }

}
