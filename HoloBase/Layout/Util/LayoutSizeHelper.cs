﻿using HoloBase.Layout.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Layout.Util
{
    public sealed class LayoutSizeHelper
    {
               
        private static LayoutSizeHelper instance = null;
        public static LayoutSizeHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new LayoutSizeHelper();
                }
                return instance;
            }
        }

        //private bool listsInitialized = false;
        private LayoutSizeHelper()
        {
            // ???
            InitAllLists();
        }


        ////////////////////////////////////////////////////////
        // "Cache"
        // These values never change.
        // These are realy pre-computed values (based on then-defined list of LayoutSizes)
        // (Although we may add/remove LayoutSizes in the future, 
        //   once the code is compiled, it does not (cannot) change.)

        // "Current" LayoutSize enum length.
        // private static readonly int LayoutSizeCount = Enum.GetNames(typeof(LayoutSize)).Length - 1;  // Exclude LayoutSize.Collapsed.
        private static readonly int LayoutSizeCount = LayoutSizes.All.Count;
        // ....


        // Sets for "open-ended" sizes:
        private readonly ISet<LayoutSize> layoutSizesWithOpenEndedWidth = new HashSet<LayoutSize>();
        private readonly ISet<LayoutSize> layoutSizesWithOpenEndedHeight = new HashSet<LayoutSize>();
        // ...


        // Ordered lists of LayoutSizes

        //private readonly IList<LayoutSize> layoutSizesAscendingByWidth = new List<LayoutSize>();
        //private readonly IList<LayoutSize> layoutSizesDescendingByWidth = new List<LayoutSize>();
        //private readonly IList<LayoutSize> layoutSizesAscendingByHeight = new List<LayoutSize>();
        //private readonly IList<LayoutSize> layoutSizesDescendingByHeight = new List<LayoutSize>();
        //private readonly IList<LayoutSize> layoutSizesAscendingByArea = new List<LayoutSize>();
        //private readonly IList<LayoutSize> layoutSizesDescendingByArea = new List<LayoutSize>();

        //private readonly LayoutSize[] layoutSizesAscendingByWidth = new LayoutSize[LayoutSizeCount];
        //private readonly LayoutSize[] layoutSizesDescendingByWidth = new LayoutSize[LayoutSizeCount];
        //private readonly LayoutSize[] layoutSizesAscendingByHeight = new LayoutSize[LayoutSizeCount];
        //private readonly LayoutSize[] layoutSizesDescendingByHeight = new LayoutSize[LayoutSizeCount];
        //private readonly LayoutSize[] layoutSizesAscendingByArea = new LayoutSize[LayoutSizeCount];
        //private readonly LayoutSize[] layoutSizesDescendingByArea = new LayoutSize[LayoutSizeCount];

        // private LayoutSize[] layoutSizesAscendingByWidthAndHeight = null;
        private LayoutSize[] layoutSizesDescendingByWidthAndHeight = null;
        // private LayoutSize[] layoutSizesAscendingByHeightAndWidth = null;
        private LayoutSize[] layoutSizesDescendingByHeightAndWidth = null;
        // private LayoutSize[] layoutSizesAscendingByArea = null;
        private LayoutSize[] layoutSizesDescendingByArea = null;


        // TBD:
        // We also need pre-computed lists/arrays for "width only" sizes, "height only" sizes, "widgets", "band tiles", etc.
        // ....


        private void InitAllLists()
        {
            foreach (var s in LayoutSizes.All) {
                if (LayoutSizes.IsWidthOpenEnded(s)) {
                    layoutSizesWithOpenEndedWidth.Add(s);
                }
                if (LayoutSizes.IsHeightOpenEnded(s)) {
                    layoutSizesWithOpenEndedHeight.Add(s);
                }
            }


            ////var orderedListAscendingByWidthAndHeight = LayoutSizes.GetAllLayoutSizes().OrderBy(o => LayoutSizes.GetMinimumSizeForLayoutSize(o).Width);
            ////layoutSizesAscendingByWidthAndHeight = orderedListAscendingByWidthAndHeight.ToArray();

            //var orderedListAscendingByWidthAndHeight = new List<LayoutSize>(LayoutSizes.GetAllLayoutSizes());
            //orderedListAscendingByWidthAndHeight.Sort(new DefaultLayoutSizeComparerByWidthAndHeightAscending());
            //layoutSizesAscendingByWidthAndHeight = orderedListAscendingByWidthAndHeight.ToArray();

            var orderedListDescendingByWidthAndHeight = new List<LayoutSize>(LayoutSizes.All);
            orderedListDescendingByWidthAndHeight.Sort(new DefaultLayoutSizeComparerByWidthAndHeightDescending());
            layoutSizesDescendingByWidthAndHeight = orderedListDescendingByWidthAndHeight.ToArray();


            //var orderedListAscendingByHeightAndWidth = new List<LayoutSize>(LayoutSizes.GetAllLayoutSizes());
            //orderedListAscendingByHeightAndWidth.Sort(new DefaultLayoutSizeComparerByHeightAndWidthAscending());
            //layoutSizesAscendingByHeightAndWidth = orderedListAscendingByHeightAndWidth.ToArray();

            var orderedListDescendingByHeightAndWidth = new List<LayoutSize>(LayoutSizes.All);
            orderedListDescendingByHeightAndWidth.Sort(new DefaultLayoutSizeComparerByHeightAndWidthDescending());
            layoutSizesDescendingByHeightAndWidth = orderedListDescendingByHeightAndWidth.ToArray();


            //var orderedListAscendingByArea = new List<LayoutSize>(LayoutSizes.GetAllLayoutSizes());
            //orderedListAscendingByArea.Sort(new DefaultLayoutSizeComparerByAreaAscending());
            //layoutSizesAscendingByArea = orderedListAscendingByArea.ToArray();

            var orderedListDescendingByArea = new List<LayoutSize>(LayoutSizes.All);
            orderedListDescendingByArea.Sort(new DefaultLayoutSizeComparerByAreaDescending());
            layoutSizesDescendingByArea = orderedListDescendingByArea.ToArray();



            //// hack.
            //await Task.Delay(1);
            //listsInitialized = true;
        }

        //// This will most likely return false in the first call.
        //// then true in subsequent calls.
        //public bool IsInitialized
        //{
        //    get
        //    {
        //        return listsInitialized;
        //    }
        //    //private set
        //    //{
        //    //    listsInitialized = value;
        //    //}
        //}


        public ISet<LayoutSize> LayoutSizesWithOpenEndedWidth
        {
            get
            {
                return layoutSizesWithOpenEndedWidth;
            }
        }

        public ISet<LayoutSize> LayoutSizesWithOpenEndedHeight
        {
            get
            {
                return layoutSizesWithOpenEndedHeight;
            }
        }


        public LayoutSize[] DescendingByWidthAndHeight
        {
            get
            {
                // if(listsInitialized) ???
                return layoutSizesDescendingByWidthAndHeight;
            }
        }

        public LayoutSize[] DescendingByHeightAndWidth
        {
            get
            {
                // if(listsInitialized) ???
                return layoutSizesDescendingByHeightAndWidth;
            }
        }

        public LayoutSize[] DescendingByArea
        {
            get
            {
                // if(listsInitialized) ???
                return layoutSizesDescendingByArea;
            }
        }


    }

}
