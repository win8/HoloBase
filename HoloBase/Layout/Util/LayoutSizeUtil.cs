﻿using HoloBase.Layout.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;


namespace HoloBase.Layout.Util
{

    // TBD:
    // Just use SizeComparisonHelper ????
    // ...

    public static class LayoutSizeUtil
    {

        public static IList<LayoutSize> GetLayoutSizesWithSmallerWidth(double width)
        {
            //if (width == 0.0) {
            //    return new List<LayoutSize>(LayoutSizes.All);   // ???
            //}

            var layoutSizes = new List<LayoutSize>();

            for (var i = 0; i < LayoutSizeHelper.Instance.DescendingByWidthAndHeight.Length; i++ ) {
                var ls = LayoutSizeHelper.Instance.DescendingByWidthAndHeight[i];
                if (LayoutSizes.GetMinimumSize(ls).Width > width) {   // LayoutSizes.GetMinimumSize(ls).Width == 0 ||  ???
                    continue;
                } else {
                    for (var j = i; j < LayoutSizeHelper.Instance.DescendingByWidthAndHeight.Length; j++) {
                        layoutSizes.Add(LayoutSizeHelper.Instance.DescendingByWidthAndHeight[j]);
                    }
                    break;
                }
            }

            return layoutSizes;
        }
        public static IList<LayoutSize> GetLayoutSizesWithSmallerHeight(double height)
        {
            //if (height == 0.0) {
            //    return new List<LayoutSize>(LayoutSizes.All);   // ???
            //}

            var layoutSizes = new List<LayoutSize>();

            for (var i = 0; i < LayoutSizeHelper.Instance.DescendingByHeightAndWidth.Length; i++) {
                var ls = LayoutSizeHelper.Instance.DescendingByHeightAndWidth[i];
                if (LayoutSizes.GetMinimumSize(ls).Height > height) {   // LayoutSizes.GetMinimumSize(ls).Height == 0 ||   ?????
                    continue;
                } else {
                    for (var j = i; j < LayoutSizeHelper.Instance.DescendingByHeightAndWidth.Length; j++) {
                        layoutSizes.Add(LayoutSizeHelper.Instance.DescendingByHeightAndWidth[j]);
                    }
                    break;
                }
            }

            return layoutSizes;
        }

        public static IList<LayoutSize> GetLayoutSizesWithSmallerWidthAndHeight(double width, double height)
        {
            var layoutSizes = new List<LayoutSize>();
            for (var i = 0; i < LayoutSizeHelper.Instance.DescendingByWidthAndHeight.Length; i++) {
                var ls = LayoutSizeHelper.Instance.DescendingByWidthAndHeight[i];
                if (LayoutSizes.GetMinimumSize(ls).Width > width || LayoutSizes.GetMinimumSize(ls).Height > height) { 
                    continue;
                } else {
                    layoutSizes.Add(LayoutSizeHelper.Instance.DescendingByWidthAndHeight[i]);
                }
            }
            return layoutSizes;
        }
        public static IList<LayoutSize> GetLayoutSizesWithSmallerHeightAndWidth(double width, double height)
        {
            var layoutSizes = new List<LayoutSize>();
            for (var i = 0; i < LayoutSizeHelper.Instance.DescendingByHeightAndWidth.Length; i++) {
                var ls = LayoutSizeHelper.Instance.DescendingByHeightAndWidth[i];
                if (LayoutSizes.GetMinimumSize(ls).Width > width || LayoutSizes.GetMinimumSize(ls).Height > height) {
                    continue;
                } else {
                    layoutSizes.Add(LayoutSizeHelper.Instance.DescendingByHeightAndWidth[i]);
                }
            }
            return layoutSizes;
        }

        // The method name is misleading.
        // the layouts should fit into the given width x height (not just areas)
        // the return list is sorted according to the area.
        public static IList<LayoutSize> GetLayoutSizesWithSmallerArea(double width, double height)
        {
            var layoutSizes = new List<LayoutSize>();
            for (var i = 0; i < LayoutSizeHelper.Instance.DescendingByArea.Length; i++) {
                var ls = LayoutSizeHelper.Instance.DescendingByArea[i];
                if (LayoutSizes.GetMinimumSize(ls).Width > width || LayoutSizes.GetMinimumSize(ls).Height > height) {
                    continue;
                } else {
                    layoutSizes.Add(LayoutSizeHelper.Instance.DescendingByArea[i]);
                }
            }
            return layoutSizes;
        }



        public static IList<LayoutSize> GetAllowedLayoutSizes(Size avaliableSize, LayoutPickOrder pickOrder)
        {
            IList<LayoutSize> layoutSizes = null;
            switch (pickOrder) {
                // On Windows 8.1
                // Isn't it more reasonable to use WidthFirst as default????
                case LayoutPickOrder.AreaWise:
                default:  // ???
                    layoutSizes = GetLayoutSizesWithSmallerArea(avaliableSize.Width, avaliableSize.Height);
                    break;
                case LayoutPickOrder.WidthFirst:
                    layoutSizes = GetLayoutSizesWithSmallerWidthAndHeight(avaliableSize.Width, avaliableSize.Height);
                    break;
                case LayoutPickOrder.HeightFirst:
                    layoutSizes = GetLayoutSizesWithSmallerHeightAndWidth(avaliableSize.Width, avaliableSize.Height);
                    break;
            }
            return layoutSizes;
        }



        public static LayoutSize GetMaximumAllowedLayoutSize(Size availableSize)
        {
            return GetMaximumAllowedLayoutSize(availableSize, 0, 0);
        }
        public static LayoutSize GetMaximumAllowedLayoutSize(Size availableSize, int paddingWidth, int paddingHeight)
        {
            // var size = Size.Empty;  // ???
            var layoutSize = LayoutSize.Collapsed;   // ???

            // bb = bounding box
            var bbWidth = availableSize.Width;
            var bbHeight = availableSize.Height;

            var width = 0;
            var height = 0;





            return layoutSize;
        }

    }
}
