﻿using HoloBase.Layout.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;


namespace HoloBase.Layout.Util
{
    public sealed class SizeComparisonHelper
    {
        private static SizeComparisonHelper instance = null;
        public static SizeComparisonHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new SizeComparisonHelper();
                }
                return instance;
            }
        }
        private SizeComparisonHelper()
        {
        }



        public IList<LayoutSize> GetAllowedLayoutSizes(Size containerSize, LayoutPickOrder pickOrder)
        {
            return GetAllowedLayoutSizes(containerSize, pickOrder);
        }
        public IList<LayoutSize> GetAllowedLayoutSizes(Size containerSize, LayoutPickOrder pickOrder, bool excludeOpenEnded)
        {
            LayoutSize[] sortedLayoutSizes;
            switch (pickOrder) {
                // On Windows 8.1
                // Isn't it more reasonable to use WidthFirst as default????
                case LayoutPickOrder.AreaWise:
                default:  // ???
                    sortedLayoutSizes = LayoutSizeHelper.Instance.DescendingByArea;
                    break;
                case LayoutPickOrder.WidthFirst:
                    sortedLayoutSizes = LayoutSizeHelper.Instance.DescendingByWidthAndHeight;
                    break;
                case LayoutPickOrder.HeightFirst:
                    sortedLayoutSizes = LayoutSizeHelper.Instance.DescendingByHeightAndWidth;
                    break;
            }
            var decoratedLayoutSize = GetAllowedLayoutSizes(containerSize, sortedLayoutSizes, excludeOpenEnded);
            return decoratedLayoutSize;
        }

        public IList<LayoutSize> GetAllowedLayoutSizes(Size containerSize, LayoutSize[] sortedLayoutSizes)
        {
            return GetAllowedLayoutSizes(containerSize, sortedLayoutSizes, false);
        }
        public IList<LayoutSize> GetAllowedLayoutSizes(Size containerSize, LayoutSize[] sortedLayoutSizes, bool excludeOpenEnded)
        {
            var decoratedLayoutSizes = new List<LayoutSize>();

            foreach (var s in sortedLayoutSizes) {
                if (LayoutSizes.GetMinimumSize(s).Width > containerSize.Width || LayoutSizes.GetMinimumSize(s).Height > containerSize.Height) {
                    continue;
                } else {
                    if (excludeOpenEnded && (LayoutSizes.GetMinimumSize(s).Width == 0 || LayoutSizes.GetMinimumSize(s).Height == 0)) {
                        continue;
                    } else {
                        decoratedLayoutSizes.Add(s);
                    }
                }
            }

            return decoratedLayoutSizes;
        }

        public LayoutSize GetMaximumAllowedLayoutSize(Size containerSize, LayoutPickOrder pickOrder)
        {
            return GetMaximumAllowedLayoutSize(containerSize, pickOrder, false);
        }
        public LayoutSize GetMaximumAllowedLayoutSize(Size containerSize, LayoutPickOrder pickOrder, bool excludeOpenEnded)
        {
            var decoratedLayoutSize = LayoutSize.Collapsed;
            var list = GetAllowedLayoutSizes(containerSize, pickOrder, excludeOpenEnded);
            if (list != null && list.Count > 0) {
                decoratedLayoutSize = list[0];     // list is already sorted.
            }
            return decoratedLayoutSize;
        }

        public LayoutSize GetMaximumAllowedLayoutSize(Size containerSize, LayoutSize[] sortedLayoutSizes)
        {
            return GetMaximumAllowedLayoutSize(containerSize, sortedLayoutSizes, false);
        }
        public LayoutSize GetMaximumAllowedLayoutSize(Size containerSize, LayoutSize[] sortedLayoutSizes, bool excludeOpenEnded)
        {
            var decoratedLayoutSize = LayoutSize.Collapsed;
            var list = GetAllowedLayoutSizes(containerSize, sortedLayoutSizes, excludeOpenEnded);
            if (list != null && list.Count > 0) {
                decoratedLayoutSize = list[0];     // list is already sorted.
            }
            return decoratedLayoutSize;
        }



        public IList<DecoratedLayoutSize> GetAllowedLayoutSizes(Size containerSize, IList<DecoratedLayoutSize> layoutSizes, LayoutPickOrder pickOrder)
        {
            return GetAllowedLayoutSizes(containerSize, layoutSizes, pickOrder, false);
        }
        public IList<DecoratedLayoutSize> GetAllowedLayoutSizes(Size containerSize, IList<DecoratedLayoutSize> layoutSizes, LayoutPickOrder pickOrder, bool excludeOpenEnded)
        {
            DecoratedLayoutSize[] sortedDecoratedLayoutSizes;
            switch (pickOrder) {
                // On Windows 8.1
                // Isn't it more reasonable to use WidthFirst as default????
                case LayoutPickOrder.AreaWise:
                default:  // ???
                    sortedDecoratedLayoutSizes = DecoratedLayoutSizeHelper.Instance.SortDescendingByArea(layoutSizes);
                    break;
                case LayoutPickOrder.WidthFirst:
                    sortedDecoratedLayoutSizes = DecoratedLayoutSizeHelper.Instance.SortDescendingByWidthAndHeight(layoutSizes);
                    break;
                case LayoutPickOrder.HeightFirst:
                    sortedDecoratedLayoutSizes = DecoratedLayoutSizeHelper.Instance.SortDescendingByHeightAndWidth(layoutSizes);
                    break;
            }
            var decoratedLayoutSize = GetAllowedLayoutSizes(containerSize, sortedDecoratedLayoutSizes, excludeOpenEnded);
            return decoratedLayoutSize;
        }

        public IList<DecoratedLayoutSize> GetAllowedLayoutSizes(Size containerSize, DecoratedLayoutSize[] sortedDecoratedLayoutSizes)
        {
            return GetAllowedLayoutSizes(containerSize, sortedDecoratedLayoutSizes, false);
        }
        public IList<DecoratedLayoutSize> GetAllowedLayoutSizes(Size containerSize, DecoratedLayoutSize[] sortedDecoratedLayoutSizes, bool excludeOpenEnded)
        {
            var decoratedLayoutSizes = new List<DecoratedLayoutSize>();

            foreach (var s in sortedDecoratedLayoutSizes) {
                if (s.MinimunSize.Width > containerSize.Width || s.MinimunSize.Height > containerSize.Height) {
                    continue;
                } else {
                    if (excludeOpenEnded && (s.MinimunSize.Width == 0 || s.MinimunSize.Height == 0)) {
                        continue;
                    } else {
                        decoratedLayoutSizes.Add(s);
                    }
                }
            }

            return decoratedLayoutSizes;
        }

        public DecoratedLayoutSize GetMaximumAllowedLayoutSize(Size containerSize, IList<DecoratedLayoutSize> layoutSizes, LayoutPickOrder pickOrder)
        {
            return GetMaximumAllowedLayoutSize(containerSize, layoutSizes, pickOrder, false);
        }
        public DecoratedLayoutSize GetMaximumAllowedLayoutSize(Size containerSize, IList<DecoratedLayoutSize> layoutSizes, LayoutPickOrder pickOrder, bool excludeOpenEnded)
        {
            var decoratedLayoutSize = DecoratedLayoutSize.Collapsed;
            var list = GetAllowedLayoutSizes(containerSize, layoutSizes, pickOrder, excludeOpenEnded);
            if (list != null && list.Count > 0) {
                decoratedLayoutSize = list[0];     // list is already sorted.
            }
            return decoratedLayoutSize;
        }

        public DecoratedLayoutSize GetMaximumAllowedLayoutSize(Size containerSize, DecoratedLayoutSize[] sortedDecoratedLayoutSizes)
        {
            return GetMaximumAllowedLayoutSize(containerSize, sortedDecoratedLayoutSizes, false);
        }
        public DecoratedLayoutSize GetMaximumAllowedLayoutSize(Size containerSize, DecoratedLayoutSize[] sortedDecoratedLayoutSizes, bool excludeOpenEnded)
        {
            var decoratedLayoutSize = DecoratedLayoutSize.Collapsed;
            var list = GetAllowedLayoutSizes(containerSize, sortedDecoratedLayoutSizes, excludeOpenEnded);
            if (list != null && list.Count > 0) {
                decoratedLayoutSize = list[0];     // list is already sorted.
            }
            return decoratedLayoutSize;
        }


    }
}
