﻿using HoloBase.Layout.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Layout.Util
{
    public sealed class DecoratedLayoutSizeHelper
    {
        private static DecoratedLayoutSizeHelper instance = null;
        public static DecoratedLayoutSizeHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new DecoratedLayoutSizeHelper();
                }
                return instance;
            }
        }
        private DecoratedLayoutSizeHelper()
        {
        }


        // Note:
        // See the comment below wrt the Sort functions.
        // These methods can be potentially optimized...

        public ISet<DecoratedLayoutSize> GetDecoratedLayoutSizesWithOpenEndedWidth(IList<DecoratedLayoutSize> layoutSizeList)
        {
            var set = new HashSet<DecoratedLayoutSize>();
            foreach (var s in layoutSizeList) {
                if (DecoratedLayoutSizes.IsWidthOpenEnded(s)) {
                    set.Add(s);
                }
            }
            return set;
        }

        public ISet<DecoratedLayoutSize> GetDecoratedLayoutSizesWithOpenEndedHeight(IList<DecoratedLayoutSize> layoutSizeList)
        {
            var set = new HashSet<DecoratedLayoutSize>();
            foreach (var s in layoutSizeList) {
                if (DecoratedLayoutSizes.IsHeightOpenEnded(s)) {
                    set.Add(s);
                }
            }
            return set;
        }
 

        // Note:
        // Since DecoratorPadding can be arbitrary size,
        // We cannot precompute/cache these lists/arrays (as in LayoutSizeHelper).
        // (In the case of "widget framework" with the fixed size header, etc.
        // we may again be able to optimize this....)

        public DecoratedLayoutSize[] SortDescendingByWidthAndHeight(IList<DecoratedLayoutSize> layoutSizeList)
        {
            var list = new List<DecoratedLayoutSize>(layoutSizeList);
            list.Sort(new DefaultDecoratedLayoutSizeComparerByWidthAndHeightDescending());
            return list.ToArray();
        }

        public DecoratedLayoutSize[] SortDescendingByHeightAndWidth(IList<DecoratedLayoutSize> layoutSizeList)
        {
            var list = new List<DecoratedLayoutSize>(layoutSizeList);
            list.Sort(new DefaultDecoratedLayoutSizeComparerByHeightAndWidthDescending());
            return list.ToArray();
        }

        public DecoratedLayoutSize[] SortDescendingByArea(IList<DecoratedLayoutSize> layoutSizeList)
        {
            var list = new List<DecoratedLayoutSize>(layoutSizeList);
            list.Sort(new DefaultDecoratedLayoutSizeComparerByAreaDescending());
            return list.ToArray();
        }


    }
}
