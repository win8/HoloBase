﻿using HoloBase.Icons.ViewModels;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class SymbolIconCubbyControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private SymbolIconCubbyViewModel viewModel;

        public SymbolIconCubbyControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new SymbolIconCubbyViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SymbolIconCubbyViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SymbolIconCubbyViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

        private void GridViewSymbolList_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

    }
}
