﻿using HoloBase.Icons.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Util
{
    public static class SymbolBlockRanges
    {
        private static readonly IDictionary<UnicodeSymbolBlock, int[]> rangeMap = new Dictionary<UnicodeSymbolBlock, int[]>();

        static SymbolBlockRanges()
        {
            rangeMap.Add(UnicodeSymbolBlock.GeneralPunctuation, new int[] { 0x2000, 0x206F });
            rangeMap.Add(UnicodeSymbolBlock.SuperscriptsAndSubscripts, new int[] { 0x2070, 0x209F });
            rangeMap.Add(UnicodeSymbolBlock.CurrencySymbols, new int[] { 0x20A0, 0x20CF });
            rangeMap.Add(UnicodeSymbolBlock.CombiningDiacriticalMarks, new int[] { 0x20D0, 0x20FF });
            rangeMap.Add(UnicodeSymbolBlock.LetterlikeSymbols, new int[] { 0x2100, 0x214F });
            rangeMap.Add(UnicodeSymbolBlock.NumberForms, new int[] { 0x2150, 0x218F });
            rangeMap.Add(UnicodeSymbolBlock.Arrows, new int[] { 0x2190, 0x21FF });
            rangeMap.Add(UnicodeSymbolBlock.MathematicalOperators, new int[] { 0x2200, 0x22FF });
            rangeMap.Add(UnicodeSymbolBlock.MiscellaneousTechnical, new int[] { 0x2300, 0x23FF });
            rangeMap.Add(UnicodeSymbolBlock.ControlPictures, new int[] { 0x2400, 0x243F });
            rangeMap.Add(UnicodeSymbolBlock.OpticalCharacterRecognition, new int[] { 0x2440, 0x245F });
            rangeMap.Add(UnicodeSymbolBlock.EnclosedAlphanumerics, new int[] { 0x2460, 0x24FF });
            rangeMap.Add(UnicodeSymbolBlock.BoxDrawing, new int[] { 0x2500, 0x257F });
            rangeMap.Add(UnicodeSymbolBlock.BlockElements, new int[] { 0x2580, 0x259F });
            rangeMap.Add(UnicodeSymbolBlock.GeometricShapes, new int[] { 0x25A0, 0x25FF });
            rangeMap.Add(UnicodeSymbolBlock.MiscellaneousSymbols, new int[] { 0x2600, 0x26FF });
            rangeMap.Add(UnicodeSymbolBlock.Dingbats, new int[] { 0x2700, 0x27BF });
            rangeMap.Add(UnicodeSymbolBlock.MiscellaneousMathematicalSymbolsA, new int[] { 0x27C0, 0x27EF });
            rangeMap.Add(UnicodeSymbolBlock.SupplementalArrowsA, new int[] { 0x27F0, 0x27FF });
            rangeMap.Add(UnicodeSymbolBlock.BraillePatterns, new int[] { 0x2800, 0x28FF });
            rangeMap.Add(UnicodeSymbolBlock.SupplementalArrowsB, new int[] { 0x2900, 0x297F });
            rangeMap.Add(UnicodeSymbolBlock.MiscellaneousMathematicalSymbolsB, new int[] { 0x2980, 0x29FF });
            rangeMap.Add(UnicodeSymbolBlock.SupplementalMathematicalOperators, new int[] { 0x2A00, 0x2AFF });
            rangeMap.Add(UnicodeSymbolBlock.MiscellaneousSymbolsAndArrows, new int[] { 0x2B00, 0x2BFF });

            // tbd:
            rangeMap.Add(UnicodeSymbolBlock.AllSymbols, new int[] { 0x2000, 0x2BFF });
            // ...
        }

        public static IDictionary<UnicodeSymbolBlock, int[]> Map
        {
            get
            {
                return rangeMap;
            }
        }

        public static int[] CodeRange(UnicodeSymbolBlock symbolBlock)
        {
            if (rangeMap.ContainsKey(symbolBlock)) {
                return rangeMap[symbolBlock];
            } else {
                return null;
            }
        }


    }
}
