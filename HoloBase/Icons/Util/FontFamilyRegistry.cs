﻿using HoloBase.Icons.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Util
{
    // Guidelines for fonts
    // https://msdn.microsoft.com/en-us/library/windows/apps/hh700394.aspx

    public static class FontFamilyRegistry
    {
        public static readonly FontFamilyStruct FF_Arial = new FontFamilyStruct("Arial", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic, FontStyle.Black });
        public static readonly FontFamilyStruct FF_Calibri = new FontFamilyStruct("Calibri", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic, FontStyle.Light, FontStyle.LightItalic });
        public static readonly FontFamilyStruct FF_Cambria = new FontFamilyStruct("Cambria", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_CambriaMath = new FontFamilyStruct("Cambria Math", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_ComicSansMS = new FontFamilyStruct("Comic Sans MS", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic });
        public static readonly FontFamilyStruct FF_CourierNew = new FontFamilyStruct("Courier New", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic });
        public static readonly FontFamilyStruct FF_Ebrima = new FontFamilyStruct("Ebrima", new FontStyle[] {	FontStyle.Regular, FontStyle.Bold });
        public static readonly FontFamilyStruct FF_Gadugi = new FontFamilyStruct("Gadugi", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_Georgia = new FontFamilyStruct("Georgia", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic });
        public static readonly FontFamilyStruct FF_JapaneseText = new FontFamilyStruct("Japanese Text", new FontStyle[] { FontStyle.Regular });
        public static readonly FontFamilyStruct FF_LeelawadeeUI = new FontFamilyStruct("Leelawadee UI", new FontStyle[] {	FontStyle.Regular, FontStyle.Semilight, FontStyle.Bold });
        public static readonly FontFamilyStruct FF_LucidaConsole = new FontFamilyStruct("Lucida Console", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MalgunGothic = new FontFamilyStruct("Malgun Gothic", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftHimalaya = new FontFamilyStruct("Microsoft Himalaya", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftJhengHei = new FontFamilyStruct("Microsoft JhengHei", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftJhengHeiUI = new FontFamilyStruct("Microsoft JhengHei UI", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftNewTaiLue = new FontFamilyStruct("Microsoft New Tai Lue", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftPhagsPa = new FontFamilyStruct("Microsoft PhagsPa", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftTaiLe = new FontFamilyStruct("Microsoft Tai Le", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftYaHei = new FontFamilyStruct("Microsoft YaHei", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftYaHeiUI = new FontFamilyStruct("Microsoft YaHei UI", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MicrosoftYiBaiti = new FontFamilyStruct("Microsoft Yi Baiti", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MongolianBaiti = new FontFamilyStruct("Mongolian Baiti", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MVBoli = new FontFamilyStruct("MV Boli", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_MyanmarText = new FontFamilyStruct("Myanmar Text", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_NirmalaUI = new FontFamilyStruct("Nirmala UI", new FontStyle[] {	FontStyle.Regular, FontStyle.Semilight, FontStyle.Bold });
        public static readonly FontFamilyStruct FF_SegoeMDL2Assets = new FontFamilyStruct("Segoe MDL2 Assets", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_SegoePrint = new FontFamilyStruct("Segoe Print", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_SegoeUI = new FontFamilyStruct("Segoe UI", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic, FontStyle.Light, FontStyle.Semilight, FontStyle.Semibold, FontStyle.Black });
        public static readonly FontFamilyStruct FF_SegoeUIEmoji = new FontFamilyStruct("Segoe UI Emoji", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_SegoeUIHistoric = new FontFamilyStruct("Segoe UI Historic", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_SegoeUISymbol = new FontFamilyStruct("Segoe UI Symbol", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_SimSun = new FontFamilyStruct("SimSun", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_TimesNewRoman = new FontFamilyStruct("Times New Roman", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic });
        public static readonly FontFamilyStruct FF_TrebuchetMS = new FontFamilyStruct("Trebuchet MS", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic });
        public static readonly FontFamilyStruct FF_Verdana = new FontFamilyStruct("Verdana", new FontStyle[] {	FontStyle.Regular, FontStyle.Italic, FontStyle.Bold, FontStyle.BoldItalic });
        public static readonly FontFamilyStruct FF_Webdings = new FontFamilyStruct("Webdings", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_Wingdings = new FontFamilyStruct("Wingdings", new FontStyle[] {	FontStyle.Regular });
        public static readonly FontFamilyStruct FF_YuGothic = new FontFamilyStruct("Yu Gothic", new FontStyle[] {	FontStyle.Medium });
        public static readonly FontFamilyStruct FF_YuGothicUI = new FontFamilyStruct("Yu Gothic UI", new FontStyle[] {	FontStyle.Regular });

        private static readonly IDictionary<string, FontFamilyWrap> fontFamilyMap = new Dictionary<string, FontFamilyWrap>();
        private static readonly IList<FontFamilyStruct> fontFamilyList = new List<FontFamilyStruct>();
        static FontFamilyRegistry()
        {
            fontFamilyMap.Add(FF_Arial.Name, new FontFamilyWrap(FF_Arial));
            fontFamilyMap.Add(FF_Calibri.Name, new FontFamilyWrap(FF_Calibri));
            fontFamilyMap.Add(FF_Cambria.Name, new FontFamilyWrap(FF_Cambria));
            fontFamilyMap.Add(FF_CambriaMath.Name, new FontFamilyWrap(FF_CambriaMath));
            fontFamilyMap.Add(FF_ComicSansMS.Name, new FontFamilyWrap(FF_ComicSansMS));
            fontFamilyMap.Add(FF_CourierNew.Name, new FontFamilyWrap(FF_CourierNew));
            fontFamilyMap.Add(FF_Ebrima.Name, new FontFamilyWrap(FF_Ebrima));
            fontFamilyMap.Add(FF_Gadugi.Name, new FontFamilyWrap(FF_Gadugi));
            fontFamilyMap.Add(FF_Georgia.Name, new FontFamilyWrap(FF_Georgia));
            fontFamilyMap.Add(FF_JapaneseText.Name, new FontFamilyWrap(FF_JapaneseText));
            fontFamilyMap.Add(FF_LeelawadeeUI.Name, new FontFamilyWrap(FF_LeelawadeeUI));
            fontFamilyMap.Add(FF_LucidaConsole.Name, new FontFamilyWrap(FF_LucidaConsole));
            fontFamilyMap.Add(FF_MalgunGothic.Name, new FontFamilyWrap(FF_MalgunGothic));
            fontFamilyMap.Add(FF_MicrosoftHimalaya.Name, new FontFamilyWrap(FF_MicrosoftHimalaya));
            fontFamilyMap.Add(FF_MicrosoftJhengHei.Name, new FontFamilyWrap(FF_MicrosoftJhengHei));
            fontFamilyMap.Add(FF_MicrosoftJhengHeiUI.Name, new FontFamilyWrap(FF_MicrosoftJhengHeiUI));
            fontFamilyMap.Add(FF_MicrosoftNewTaiLue.Name, new FontFamilyWrap(FF_MicrosoftNewTaiLue));
            fontFamilyMap.Add(FF_MicrosoftPhagsPa.Name, new FontFamilyWrap(FF_MicrosoftPhagsPa));
            fontFamilyMap.Add(FF_MicrosoftTaiLe.Name, new FontFamilyWrap(FF_MicrosoftTaiLe));
            fontFamilyMap.Add(FF_MicrosoftYaHei.Name, new FontFamilyWrap(FF_MicrosoftYaHei));
            fontFamilyMap.Add(FF_MicrosoftYaHeiUI.Name, new FontFamilyWrap(FF_MicrosoftYaHeiUI));
            fontFamilyMap.Add(FF_MicrosoftYiBaiti.Name, new FontFamilyWrap(FF_MicrosoftYiBaiti));
            fontFamilyMap.Add(FF_MongolianBaiti.Name, new FontFamilyWrap(FF_MongolianBaiti));
            fontFamilyMap.Add(FF_MVBoli.Name, new FontFamilyWrap(FF_MVBoli));
            fontFamilyMap.Add(FF_MyanmarText.Name, new FontFamilyWrap(FF_MyanmarText));
            fontFamilyMap.Add(FF_NirmalaUI.Name, new FontFamilyWrap(FF_NirmalaUI));
            fontFamilyMap.Add(FF_SegoeMDL2Assets.Name, new FontFamilyWrap(FF_SegoeMDL2Assets));
            fontFamilyMap.Add(FF_SegoePrint.Name, new FontFamilyWrap(FF_SegoePrint));
            fontFamilyMap.Add(FF_SegoeUI.Name, new FontFamilyWrap(FF_SegoeUI));
            fontFamilyMap.Add(FF_SegoeUIEmoji.Name, new FontFamilyWrap(FF_SegoeUIEmoji));
            fontFamilyMap.Add(FF_SegoeUIHistoric.Name, new FontFamilyWrap(FF_SegoeUIHistoric));
            fontFamilyMap.Add(FF_SegoeUISymbol.Name, new FontFamilyWrap(FF_SegoeUISymbol));
            fontFamilyMap.Add(FF_SimSun.Name, new FontFamilyWrap(FF_SimSun));
            fontFamilyMap.Add(FF_TimesNewRoman.Name, new FontFamilyWrap(FF_TimesNewRoman));
            fontFamilyMap.Add(FF_TrebuchetMS.Name, new FontFamilyWrap(FF_TrebuchetMS));
            fontFamilyMap.Add(FF_Verdana.Name, new FontFamilyWrap(FF_Verdana));
            fontFamilyMap.Add(FF_Webdings.Name, new FontFamilyWrap(FF_Webdings));
            fontFamilyMap.Add(FF_Wingdings.Name, new FontFamilyWrap(FF_Wingdings));
            fontFamilyMap.Add(FF_YuGothic.Name, new FontFamilyWrap(FF_YuGothic));
            fontFamilyMap.Add(FF_YuGothicUI.Name, new FontFamilyWrap(FF_YuGothicUI));

            // ???
            // Why do we need this additional list??
            foreach (var entry in fontFamilyMap) {
                fontFamilyList.Add(entry.Value.FontFamilyStruct);
            }
        }

        public static IDictionary<string, FontFamilyWrap> Map
        {
            get
            {
                return fontFamilyMap;
            }
        }

        public static IList<FontFamilyWrap> AllFontFamilies
        {
            get
            {
                return new List<FontFamilyWrap>(fontFamilyMap.Values);
            }
        }

        public static IList<FontFamilyStruct> List
        {
            get
            {
                return fontFamilyList;
            }
        }

        public static IList<string> Names
        {
            get
            {
                return new List<string>(fontFamilyMap.Keys);
            }
        }

        public static bool IsValid(string ffName)
        {
            return fontFamilyMap.ContainsKey(ffName);
        }

        public static FontStyle[] GetStyles(string ffName)
        {
            if (fontFamilyMap.ContainsKey(ffName)) {
                return fontFamilyMap[ffName].Styles;
            } else {
                return null;
            }
        }


    }
}
