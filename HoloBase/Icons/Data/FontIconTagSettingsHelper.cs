﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Data
{
    public sealed class FontIconTagSettingsHelper
    {
        private static FontIconTagSettingsHelper instance = null;
        public static FontIconTagSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new FontIconTagSettingsHelper();
                }
                return instance;
            }
        }
        private FontIconTagSettingsHelper()
        {
        }

    
    
    }
}
