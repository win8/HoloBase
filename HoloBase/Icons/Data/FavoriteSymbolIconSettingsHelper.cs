﻿using HoloBase.Settings.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.Data
{
    public sealed class FavoriteSymbolIconSettingsHelper
    {
        private const string KEY_FAVORITE_SYMBOL_ICONS = "HB-FavSymbolIcons";

        private static FavoriteSymbolIconSettingsHelper instance = null;
        public static FavoriteSymbolIconSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new FavoriteSymbolIconSettingsHelper();
                }
                return instance;
            }
        }
        private FavoriteSymbolIconSettingsHelper()
        {
        }


        // cache the list?
        private IList<Symbol> favoriteIconList = null;
        private ISet<Symbol> favoriteIconSet = null;

        // Empty list is valid, but null is not.
        public void StoreFavoriteSymbolIcons(IList<Symbol> favoriteIcons)
        {
            if (favoriteIcons != null) {
                // Remove duplicates.
                favoriteIcons = favoriteIcons.Distinct().ToList();

                var strList = String.Join(",", favoriteIcons);  // TBD: Are we using int value of the enum? or string name?  --> string name.
                System.Diagnostics.Debug.WriteLine("StoreFavoriteSymbolIcons(): Input favoriteIcons = {0}", strList);
                favoriteIconList = favoriteIcons;
                favoriteIconSet = new HashSet<Symbol>(favoriteIconList);

                // var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
                var appSettings = AppDataOptionHelper.Instance.GetRoamingAppSettings();
                // Note that we store strList even if it's empty.
                //   (it can unset the existing value...)
                appSettings.Values[KEY_FAVORITE_SYMBOL_ICONS] = strList;
            } else {
                // Null input just resets the cache.
                favoriteIconList = null;
                favoriteIconSet = null;
            }
        }
        public IList<Symbol> FetchFavoriteSymbolIcons()
        {
            // var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
            var appSettings = AppDataOptionHelper.Instance.GetRoamingAppSettings();

            IList<Symbol> favoriteIcons = null;
            if (appSettings.Values.ContainsKey(KEY_FAVORITE_SYMBOL_ICONS) && appSettings.Values[KEY_FAVORITE_SYMBOL_ICONS] != null) {
                var strFavoriteIcons = appSettings.Values[KEY_FAVORITE_SYMBOL_ICONS].ToString();
                var parts = strFavoriteIcons.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                favoriteIcons = new List<Symbol>();
                foreach (var s in parts) {
                    try {
                        // Is s string name or int value?  --> String name.
                        var symbol = (Symbol) Enum.Parse(typeof(Symbol), s);
                        // var symbol = (Symbol) Convert.ToInt32(s);
                        favoriteIcons.Add(symbol);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
            }
            return favoriteIcons;
        }
        public IList<Symbol> FavoriteSymbolIconList
        {
            get
            {
                if (favoriteIconList == null) {
                    RefreshFavoriteSymbolIconCollection();
                }
                return favoriteIconList;
            }
        }
        public ISet<Symbol> FavoriteSymbolIconSet
        {
            get
            {
                if (favoriteIconSet == null) {
                    RefreshFavoriteSymbolIconCollection();
                }
                return favoriteIconSet;
            }
        }
        private void RefreshFavoriteSymbolIconCollection()
        {
            IList<Symbol> favoriteIcons = FetchFavoriteSymbolIcons();
            if (favoriteIcons != null) {    // Null means no data found in the db. Need to reset the cache... --> But, at this point, favoriteIconList is already null.
                // Remove duplicates.
                favoriteIcons = favoriteIcons.Distinct().ToList();
                favoriteIconList = favoriteIcons;
                favoriteIconSet = new HashSet<Symbol>(favoriteIconList);
            }
        }

        public bool ContainsFavoriteSymbolIcon(Symbol symbol)
        {
            return (FavoriteSymbolIconSet != null) && FavoriteSymbolIconSet.Contains(symbol);
        }

    }
}
