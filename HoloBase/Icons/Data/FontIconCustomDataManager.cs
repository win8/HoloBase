﻿using HoloBase.Data.Core;
using HoloBase.Icons.Common;
using HoloCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Data
{
    public sealed class FontIconCustomDataManager
    {
        private static FontIconCustomDataManager instance = null;
        public static FontIconCustomDataManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new FontIconCustomDataManager();
                }
                return instance;
            }
        }
        private FontIconCustomDataManager()
        {
        }


        // TBD:
        public IList<GlyphName> GetAllGlyphNames(int size = -1)
        {
            return GetAllGlyphNames(SortFieldAndOrder.Null, size);
        }
        public IList<GlyphName> GetAllGlyphNames(SortFieldAndOrder sfo, int size = -1)
        {
            IList<GlyphName> glyphnames = new List<GlyphName>();
            var profs = GlyphNameSettingsHelper.Instance.GetAllGlyphNames(sfo);
            if (profs != null) {
                if (size > 0) {
                    var len = profs.Count;
                    var limit = Math.Min(len, size);
                    glyphnames = ((List<GlyphName>) profs).GetRange(0, limit);
                } else {
                    glyphnames = profs;
                }
            }
            return glyphnames;
        }


        // Create or Update.
        public GlyphName StoreGlyphName(GlyphName glyphname)
        {
            var id = glyphname.Glyph;
            if (id == 0) {
                // Error !!
                // what to do ???
                id = (int) IdUtil.GetNextUniqueId05();
                ((GlyphName) glyphname).Glyph = id;
            }

            // TBD:
            // Validate the names?
            // ...
            // If name == null/empty
            // delete the record ???
            // ....


            var now = DateTimeUtil.CurrentUnixEpochMillis();
            if (glyphname.Created == 0L) {
                ((GlyphName) glyphname).Created = now;
            }
            ((GlyphName) glyphname).Updated = now;


            GlyphNameSettingsHelper.Instance.StoreGlyphName(glyphname);

            return glyphname;
        }


        public GlyphName GetGlyphName(int glyph)
        {
            return GlyphNameSettingsHelper.Instance.GetGlyphName(glyph);
        }

        public bool IsGlyphNamePresent(int glyph)
        {
            return GlyphNameSettingsHelper.Instance.IsGlyphNamePresent(glyph);
        }
        public bool ContainsGlyphName(int glyph)
        {
            return GlyphNameSettingsHelper.Instance.ContainsGlyphName(glyph);
        }

        public GlyphName FetchGlyphName(int glyph)
        {
            var glyphname = GlyphNameSettingsHelper.Instance.FetchGlyphName(glyph);
            System.Diagnostics.Debug.WriteLine("FetchGlyphName() glyph = {0}, glyphname = {1}", glyph, glyphname);
            return glyphname;
        }

        // TBD:
        public bool DeleteGlyphName(int glyph)
        {
            var suc = GlyphNameSettingsHelper.Instance.DeleteGlyphName(glyph);
            if (suc) {
                // ..
            }
            return suc;
        }

    }
}
