﻿using HoloBase.Settings.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Data
{
    // Note that we ignore fontfamily.
    // We only store glyphs.....
    public sealed class FavoriteFontIconSettingsHelper
    {
        private const string KEY_FAVORITE_FONTICON_GLYPHS = "HB-FavFontIcons";

        private static FavoriteFontIconSettingsHelper instance = null;
        public static FavoriteFontIconSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new FavoriteFontIconSettingsHelper();
                }
                return instance;
            }
        }
        private FavoriteFontIconSettingsHelper()
        {
        }

        // cache the list?
        private IList<int> favoriteGlyphList = null;
        private ISet<int> favoriteGlyphSet = null;

        // Empty list is valid, but null is not.
        public void StoreFavoriteFontIconGlyphs(IList<int> fontIconGlyphs)
        {
            if (fontIconGlyphs != null) {
                // Remove duplicates.
                fontIconGlyphs = fontIconGlyphs.Distinct().ToList();

                var strList = String.Join(",", fontIconGlyphs);
                System.Diagnostics.Debug.WriteLine("StoreFavoriteFontIconGlyphs(): Input fontIconGlyphs = {0}", strList);
                favoriteGlyphList = fontIconGlyphs;
                favoriteGlyphSet = new HashSet<int>(favoriteGlyphList);

                // var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
                var appSettings = AppDataOptionHelper.Instance.GetRoamingAppSettings();
                // Note that we store strList even if it's empty.
                //   (it can unset the existing value...)
                appSettings.Values[KEY_FAVORITE_FONTICON_GLYPHS] = strList;
            } else {
                // Null input just resets the cache.
                favoriteGlyphList = null;
                favoriteGlyphSet = null;
            }
        }
        public IList<int> FetchFavoriteFontIconGlyphs()
        {
            // var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
            var appSettings = AppDataOptionHelper.Instance.GetRoamingAppSettings();

            IList<int> fontIconGlyphs = null;
            if (appSettings.Values.ContainsKey(KEY_FAVORITE_FONTICON_GLYPHS) && appSettings.Values[KEY_FAVORITE_FONTICON_GLYPHS] != null) {
                var strFavoriteIcons = appSettings.Values[KEY_FAVORITE_FONTICON_GLYPHS].ToString();
                var parts = strFavoriteIcons.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                fontIconGlyphs = new List<int>();
                foreach (var s in parts) {
                    try {
                        var glyph = Convert.ToInt32(s);
                        fontIconGlyphs.Add(glyph);
                    } catch (Exception) {
                        // ignore...
                    }
                }
            }
            return fontIconGlyphs;
        }
        public IList<int> FavoriteFontIconGlyphList
        {
            get
            {
                if (favoriteGlyphList == null) {
                    RefreshFavoriteFontIconGlyphCollection();
                }
                return favoriteGlyphList;
            }
        }
        public ISet<int> FavoriteFontIconGlyphSet
        {
            get
            {
                if (favoriteGlyphSet == null) {
                    RefreshFavoriteFontIconGlyphCollection();
                }
                return favoriteGlyphSet;
            }
        }
        private void RefreshFavoriteFontIconGlyphCollection()
        {
            IList<int> fontIconGlyphs = FetchFavoriteFontIconGlyphs();
            if (fontIconGlyphs != null) {     // Null means no data found in the db. Need to reset the cache...  --> But, at this point, favoriteGlyphList is already null.
                // Remove duplicates.
                fontIconGlyphs = fontIconGlyphs.Distinct().ToList();
                favoriteGlyphList = fontIconGlyphs;
                favoriteGlyphSet = new HashSet<int>(favoriteGlyphList);
            }
        }

        public bool ContainsFontIconGlyph(int glyph)
        {
            return (FavoriteFontIconGlyphSet != null) && FavoriteFontIconGlyphSet.Contains(glyph);
        }

    }
}
