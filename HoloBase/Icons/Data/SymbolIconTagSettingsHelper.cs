﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Data
{
    public sealed class SymbolIconTagSettingsHelper
    {
        private static SymbolIconTagSettingsHelper instance = null;
        public static SymbolIconTagSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new SymbolIconTagSettingsHelper();
                }
                return instance;
            }
        }
        private SymbolIconTagSettingsHelper()
        {
        }

    
    
    
    }
}
