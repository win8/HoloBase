﻿using HoloBase.Data.Core;
using HoloBase.Icons.Common;
using HoloBase.Settings.App;
using HoloCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Icons.Data
{
    public sealed class GlyphNameSettingsHelper
    {
        private const string KEY_GLYPHNAME_CONTAINER_PREFIX = "HBIC-GN-";
        private const string ITEM_FIELD_GLYPH = "G";
        private const string ITEM_FIELD_NAME = "N";
        private const string ITEM_FIELD_COMMENT = "CM";
        private const string ITEM_FIELD_CREATED_TIME = "C";
        private const string ITEM_FIELD_UPDATED_TIME = "U";

        private static GlyphNameSettingsHelper instance = null;
        public static GlyphNameSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new GlyphNameSettingsHelper();
                }
                return instance;
            }
        }
        private GlyphNameSettingsHelper()
        {
        }

        // "lock"
        private object lockObject = new object();

        // "Cache"
        private IDictionary<int, GlyphName> allGlyphNameMap = null;
        private IDictionary<int, GlyphName> AllGlyphNameMap
        {
            get
            {
                if (allGlyphNameMap == null) {
                    InitAllGlyphNameMap();
                }
                return allGlyphNameMap;
            }
        }
        private void InitAllGlyphNameMap()
        {
            var containers = AppDataOptionHelper.Instance.GetAppSettings().Containers;
            lock (lockObject) {
                allGlyphNameMap = new Dictionary<int, GlyphName>();
                foreach (var key in containers.Keys) {
                    var glyph = ParseGlyphFromGlyphNameContainerKey(key);
                    var dm = FetchGlyphName(glyph);
                    if (dm != null) {
                        allGlyphNameMap.Add(dm.Glyph, dm);
                    }
                }
            }
        }



        private static string GenerateGlyphNameContainerKey(int glyph)
        {
            var key = KEY_GLYPHNAME_CONTAINER_PREFIX + glyph;
            return key;
        }
        private static int ParseGlyphFromGlyphNameContainerKey(string key)
        {
            var glyph = 0;
            try {
                var strId = key.Substring(KEY_GLYPHNAME_CONTAINER_PREFIX.Length);
                glyph = Convert.ToInt32(strId);
            } catch (Exception) {
                // Ignore.
            }
            return glyph;
        }


        // Sorts a given list and returns the sorted list.
        private IList<GlyphName> SortGlyphNameList(IList<GlyphName> glyphnames, SortFieldAndOrder sfo)
        {
            IOrderedEnumerable<GlyphName> ordered = null;
            if (sfo.SortField != null) {
                if (sfo.SortField.Equals("Created")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = glyphnames.OrderByDescending(o => o.Created);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = glyphnames.OrderBy(o => o.Created);
                            break;
                    }
                } else if (sfo.SortField.Equals("Updated")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = glyphnames.OrderByDescending(o => o.Updated);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = glyphnames.OrderBy(o => o.Updated);
                            break;
                    }
                } else if (sfo.SortField.Equals("Glyph")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = glyphnames.OrderByDescending(o => o.Glyph);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = glyphnames.OrderBy(o => o.Glyph);
                            break;
                    }
                } else if (sfo.SortField.Equals("Name")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = glyphnames.OrderByDescending(o => o.Name);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = glyphnames.OrderBy(o => o.Name);
                            break;
                    }
                } else if (sfo.SortField.Equals("Comment")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = glyphnames.OrderByDescending(o => o.Comment);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = glyphnames.OrderBy(o => o.Comment);
                            break;
                    }
                } else {
                    // Default.
                    // Note the sort order.
                    ordered = glyphnames.OrderByDescending(o => o.Updated);
                }
            }
            var sortedList = (ordered != null) ? ordered.ToList() : null;   // ???
            return sortedList;
        }


        // Note that we return List not IList, for convenience.
        private List<GlyphName> GetGlyphNameList()
        {
            return new List<GlyphName>(AllGlyphNameMap.Values);
        }

        public IList<GlyphName> GetAllGlyphNames(SortFieldAndOrder sfo)
        {
            var glyphnames = GetGlyphNameList();
            var sortedList = SortGlyphNameList(glyphnames, sfo);
            return sortedList;
        }



        public void StoreGlyphName(GlyphName glyphname)
        {
            var id = glyphname.Glyph;
            if (id == 0) {
                id = (int) IdUtil.GetNextUniqueId05();
                ((GlyphName) glyphname).Glyph = id;
            }
            var key = GenerateGlyphNameContainerKey(id);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();

            var container = appSettings.CreateContainer(key, ApplicationDataCreateDisposition.Always);
            appSettings.Containers[key].Values[ITEM_FIELD_GLYPH] = glyphname.Glyph;
            appSettings.Containers[key].Values[ITEM_FIELD_NAME] = glyphname.Name;
            appSettings.Containers[key].Values[ITEM_FIELD_COMMENT] = glyphname.Comment;
            appSettings.Containers[key].Values[ITEM_FIELD_CREATED_TIME] = glyphname.Created;
            appSettings.Containers[key].Values[ITEM_FIELD_UPDATED_TIME] = glyphname.Updated;

            // Update the cache.
            // (Note: if allGlyphNameMap has not been initialized, 
            //     this statement will try fetching all glyphnames from the settings DB,
            //     which actually include this just saved glyphname....
            //  --> is that a problem????)
            AllGlyphNameMap[id] = glyphname;
        }

        public GlyphName GetGlyphName(int glyph)
        {
            if (AllGlyphNameMap.ContainsKey(glyph) && AllGlyphNameMap[glyph] != null) {
                return AllGlyphNameMap[glyph];
            } else {
                return null;
            }
        }

        public bool IsGlyphNamePresent(int glyph)
        {
            var key = GenerateGlyphNameContainerKey(glyph);
            return AppDataOptionHelper.Instance.GetAppSettings().Containers.ContainsKey(key);
        }
        public bool ContainsGlyphName(int glyph)
        {
            return (AllGlyphNameMap.ContainsKey(glyph) && AllGlyphNameMap[glyph] != null);
        }

        // Does not use the allGlyphNameMap cache.
        // It does not update the cache. (Cf. GetGlyphName())
        public GlyphName FetchGlyphName(int glyph)
        {
            GlyphName glyphname = null;
            var key = GenerateGlyphNameContainerKey(glyph);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
            if (appSettings.Containers.ContainsKey(key)) {
                // var strId = appSettings.Containers[key].Values[ITEM_FIELD_GLYPH].ToString();

                GlyphName dm = new GlyphName(glyph);

                var name = "";
                if (appSettings.Containers[key].Values.ContainsKey(ITEM_FIELD_NAME) && appSettings.Containers[key].Values[ITEM_FIELD_NAME] != null) {
                    name = appSettings.Containers[key].Values[ITEM_FIELD_NAME].ToString();
                }
                dm.Name = name;

                var comment = "";
                if (appSettings.Containers[key].Values.ContainsKey(ITEM_FIELD_COMMENT) && appSettings.Containers[key].Values[ITEM_FIELD_COMMENT] != null) {
                    comment = appSettings.Containers[key].Values[ITEM_FIELD_COMMENT].ToString();
                }
                dm.Comment = comment;

                var created = 0L;
                if (appSettings.Containers[key].Values.ContainsKey(ITEM_FIELD_CREATED_TIME) && appSettings.Containers[key].Values[ITEM_FIELD_CREATED_TIME] != null) {
                    try {
                        var strCreated = appSettings.Containers[key].Values[ITEM_FIELD_CREATED_TIME].ToString();
                        created = Convert.ToInt64(strCreated);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                dm.Created = created;

                var updated = 0L;
                if (appSettings.Containers[key].Values.ContainsKey(ITEM_FIELD_UPDATED_TIME) && appSettings.Containers[key].Values[ITEM_FIELD_UPDATED_TIME] != null) {
                    try {
                        var strUpdated = appSettings.Containers[key].Values[ITEM_FIELD_UPDATED_TIME].ToString();
                        updated = Convert.ToInt64(strUpdated);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                dm.Updated = updated;

                glyphname = dm;
            }
            return glyphname;
        }

        // TBD: What about child glyphnames of this glyphname???
        public bool DeleteGlyphName(int glyph)
        {
            var suc = false;
            var key = GenerateGlyphNameContainerKey(glyph);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
            if (appSettings.Containers.ContainsKey(key)) {
                appSettings.DeleteContainer(key);
                suc = true;
            }
            AllGlyphNameMap.Remove(glyph);  // Note that we call this even if suc == false.
            return suc;
        }


    }
}
