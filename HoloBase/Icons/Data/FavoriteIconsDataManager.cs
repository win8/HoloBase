﻿using HoloBase.Icons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.Data
{
    public sealed class FavoriteIconsDataManager
    {
        private static FavoriteIconsDataManager instance = null;
        public static FavoriteIconsDataManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new FavoriteIconsDataManager();
                }
                return instance;
            }
        }
        private FavoriteIconsDataManager()
        {
        }


        public IList<Symbol> GetFavoriteSymbolIcons()
        {
            return FavoriteSymbolIconSettingsHelper.Instance.FavoriteSymbolIconList;
        }
        public IList<SymbolIconSymbol> GetFavoriteSymbolIconSymbolList()
        {
            var list = FavoriteSymbolIconSettingsHelper.Instance.FavoriteSymbolIconList;
            if (list == null) {
                return null;
            }
            var symbolIconSymbols = new List<SymbolIconSymbol>();
            foreach (var s in list) {
                symbolIconSymbols.Add(new SymbolIconSymbol(s, null, true));
            }
            return symbolIconSymbols;
        }

        public void SetFavoriteSymbolIcons(IList<Symbol> symbols)
        {
            FavoriteSymbolIconSettingsHelper.Instance.StoreFavoriteSymbolIcons(symbols);
        }
        public void ClearFavoriteSymbolIcons()
        {
            var emptyList = new List<Symbol>();
            FavoriteSymbolIconSettingsHelper.Instance.StoreFavoriteSymbolIcons(emptyList);
        }

        public bool IsFavorteSymbolIcon(Symbol symbol)
        {
            //var isFavorite = false;
            //var list = FavoriteSymbolIconSettingsHelper.Instance.FavoriteSymbolIconList;
            //if (list != null) {
            //    // TBD: We should have uses a set in the first place???
            //    foreach (var s in list) {
            //        if (symbol == s) {
            //            isFavorite = true;
            //            break;
            //        }
            //    }
            //}
            //return isFavorite;
            return FavoriteSymbolIconSettingsHelper.Instance.ContainsFavoriteSymbolIcon(symbol);
        }

        public void UpdateFavorteSymbolIconStatus(Symbol symbol, bool isFavorite)
        {
            System.Diagnostics.Debug.WriteLine("UpdateFavorteSymbolIconStatus() symbol = {0}, isFavorite = {1}", symbol, isFavorite);
            // if (symbol is valid) {   // ????
                if (isFavorite) {
                    AddFavorteSymbolIcon(symbol);
                } else {
                    RemoveFavorteSymbolIcon(symbol);
                }
            // }
        }

        public void AddFavorteSymbolIcon(Symbol symbol)
        {
            var list = FavoriteSymbolIconSettingsHelper.Instance.FavoriteSymbolIconList;
            if (list == null) {
                list = new List<Symbol>();
            }
            list.Add(symbol);
            FavoriteSymbolIconSettingsHelper.Instance.StoreFavoriteSymbolIcons(list);
        }
        public void AddFavorteSymbolIcons(IList<Symbol> symbols)
        {
            var list = FavoriteSymbolIconSettingsHelper.Instance.FavoriteSymbolIconList;
            if (list == null) {
                list = new List<Symbol>();
            }
            ((List<Symbol>) list).AddRange(symbols);
            FavoriteSymbolIconSettingsHelper.Instance.StoreFavoriteSymbolIcons(list);
        }

        public void RemoveFavorteSymbolIcon(Symbol symbol)
        {
            var list = FavoriteSymbolIconSettingsHelper.Instance.FavoriteSymbolIconList;
            if (list == null) {
                return;
            }
            list.Remove(symbol);  // this works since there are no duplicates..
            FavoriteSymbolIconSettingsHelper.Instance.StoreFavoriteSymbolIcons(list);
        }
        public void RemoveFavorteSymbolIcons(IList<Symbol> symbols)
        {
            var list = FavoriteSymbolIconSettingsHelper.Instance.FavoriteSymbolIconList;
            if (list == null) {
                return;
            }
            foreach (var s in symbols) {
                list.Remove(s);
            }
            FavoriteSymbolIconSettingsHelper.Instance.StoreFavoriteSymbolIcons(list);
        }



        public IList<int> GetFavoriteFontIconGlyphs()
        {
            return FavoriteFontIconSettingsHelper.Instance.FavoriteFontIconGlyphList;
        }
        public IDictionary<int, FontIconSymbol> GetFavoriteFontIconSymbolMap(string fontFamilyName)
        {
            var list = FavoriteFontIconSettingsHelper.Instance.FavoriteFontIconGlyphList;
            if (list == null) {
                return null;
            }
            var fontIconSymbolMap = new Dictionary<int, FontIconSymbol>();
            foreach (var g in list) {
                string name = null;
                if (FontIconCustomDataManager.Instance.ContainsGlyphName(g)) {
                    name = FontIconCustomDataManager.Instance.GetGlyphName(g).Name;
                }
                fontIconSymbolMap.Add(g, new FontIconSymbol(fontFamilyName, g, name, true));
            }
            return fontIconSymbolMap;
        }
        public IList<FontIconSymbol> GetFavoriteFontIconSymbolList(string fontFamilyName)
        {
            var map = GetFavoriteFontIconSymbolMap(fontFamilyName);
            if (map == null) {
                return null;
            } else {
                return new List<FontIconSymbol>(map.Values);
            }
        }

        public void SetFavoriteFontIconGlyphs(IList<int> fontGlyphs)
        {
            FavoriteFontIconSettingsHelper.Instance.StoreFavoriteFontIconGlyphs(fontGlyphs);
        }
        public void ClearFavoriteFontIconGlyphs()
        {
            var emptyList = new List<int>();
            FavoriteFontIconSettingsHelper.Instance.StoreFavoriteFontIconGlyphs(emptyList);
        }

        public bool IsFavorteFontIconGlyph(int fontGlyph)
        {
            //var isFavorite = false;
            //var list = FavoriteFontIconSettingsHelper.Instance.FavoriteFontIconGlyphList;
            //if (list != null) {
            //    // TBD: We should have uses a set in the first place???
            //    foreach (var g in list) {
            //        if (fontGlyph == g) {
            //            isFavorite = true;
            //            break;
            //        }
            //    }
            //}
            //return isFavorite;
            return FavoriteFontIconSettingsHelper.Instance.ContainsFontIconGlyph(fontGlyph);
        }

        public void UpdateFavorteFontIconGlyphStatus(int fontGlyph, bool isFavorite)
        {
            System.Diagnostics.Debug.WriteLine("UpdateFavorteFontIconGlyphStatus() fontGlyph = {0}, isFavorite = {1}", fontGlyph, isFavorite);
            if (fontGlyph > 0) {   // ???
                if (isFavorite) {
                    AddFavorteFontIconGlyph(fontGlyph);
                } else {
                    RemoveFavorteFontIconGlyph(fontGlyph);
                }
            }
        }

        public void AddFavorteFontIconGlyph(int fontGlyph)
        {
            var list = FavoriteFontIconSettingsHelper.Instance.FavoriteFontIconGlyphList;
            if (list == null) {
                list = new List<int>();
            }
            list.Add(fontGlyph);
            FavoriteFontIconSettingsHelper.Instance.StoreFavoriteFontIconGlyphs(list);
        }
        public void AddFavorteFontIconGlyphs(IList<int> fontGlyphs)
        {
            var list = FavoriteFontIconSettingsHelper.Instance.FavoriteFontIconGlyphList;
            if (list == null) {
                list = new List<int>();
            }
            ((List<int>) list).AddRange(fontGlyphs);
            FavoriteFontIconSettingsHelper.Instance.StoreFavoriteFontIconGlyphs(list);
        }

        public void RemoveFavorteFontIconGlyph(int fontGlyph)
        {
            var list = FavoriteFontIconSettingsHelper.Instance.FavoriteFontIconGlyphList;
            if (list == null) {
                return;
            }
            list.Remove(fontGlyph);  // this works since there are no duplicates..
            FavoriteFontIconSettingsHelper.Instance.StoreFavoriteFontIconGlyphs(list);
        }
        public void RemoveFavorteFontIconGlyphs(IList<int> fontGlyphs)
        {
            var list = FavoriteFontIconSettingsHelper.Instance.FavoriteFontIconGlyphList;
            if (list == null) {
                return;
            }
            foreach (var s in fontGlyphs) {
                list.Remove(s);
            }
            FavoriteFontIconSettingsHelper.Instance.StoreFavoriteFontIconGlyphs(list);
        }


    }
}
