﻿using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.FontIcons
{
    public sealed class FontIconListManager
    {
        private static FontIconListManager instance = null;
        public static FontIconListManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new FontIconListManager();
                }
                return instance;
            }
        }
        private FontIconListManager()
        {
        }

        public static IList<FontIconSymbol> BuildFontIconSymbolList(IEnumerable<FontIconCode> codes)
        {
            var list = new List<FontIconSymbol>();
            foreach (var s in codes) {
                string name = null;
                var glyphName = FontIconCustomDataManager.Instance.GetGlyphName(s.Glyph);
                if (glyphName != null) {
                    name = glyphName.Name;
                }
                var isFavorite = FavoriteIconsDataManager.Instance.IsFavorteFontIconGlyph(s.Glyph);
                var symbol = new FontIconSymbol(s.FontFamily, s.Glyph, name, isFavorite);
                list.Add(symbol);
            }
            return list;
        }


        // TBD:
        // These are pretty inefficent methods
        // Can these be cached in some way?????
        // Need better algorithms....
        // ...


        // These are really static methods...

        // Symbol list across all "symbol blocks" in BMP.
        public IDictionary<int, FontIconSymbol> BuildFontIconSymbolMap(FontFamilyStruct fontFamily)
        {
            var blockList = BmpSymbolBlocks.List;
            return BuildFontIconSymbolMap(fontFamily, blockList.ToArray());
        }
        public IDictionary<int, FontIconSymbol> BuildFontIconSymbolMap(FontFamilyStruct fontFamily, SymbolBlockStruct[] blockList)
        {
            // TBD:
            // Is this the best way to merge dictionaries????
            var symbolMap = new Dictionary<int, FontIconSymbol>();
            if (blockList != null && blockList.Any()) {
                foreach (var b in blockList) {
                    var map = BuildFontIconSymbolMap(fontFamily, b);
                    if (map != null && map.Any()) {
                        symbolMap = symbolMap.Union(map).ToDictionary(pair => pair.Key, pair => pair.Value);
                    }
                }
            }
            return symbolMap;
        }
        public IDictionary<int, FontIconSymbol> BuildFontIconSymbolMap(FontFamilyStruct fontFamily, SymbolBlockStruct symbolBlock)
        {
            var symbolMap = new Dictionary<int, FontIconSymbol>();
            int minCode = symbolBlock.MinCode;
            int maxCode = symbolBlock.MaxCode;
            if (minCode >= 0 && maxCode >= 0) {
                for (var i = minCode; i <= maxCode; i++) {
                    string name = null;
                    if (FontIconCustomDataManager.Instance.ContainsGlyphName(i)) {
                        name = FontIconCustomDataManager.Instance.GetGlyphName(i).Name;
                    }
                    var isFavorite = FavoriteIconsDataManager.Instance.IsFavorteFontIconGlyph(i);
                    symbolMap.Add(i, new FontIconSymbol(fontFamily.Name, i, name, isFavorite));
                }
            }
            return symbolMap;
        }

        public IList<FontIconSymbol> BuildFontIconSymbolList(FontFamilyStruct fontFamily)
        {
            return new List<FontIconSymbol>(BuildFontIconSymbolMap(fontFamily).Values);
        }
        public IList<FontIconSymbol> BuildFontIconSymbolList(FontFamilyStruct fontFamily, SymbolBlockStruct[] symbolBlocks)
        {
            return new List<FontIconSymbol>(BuildFontIconSymbolMap(fontFamily, symbolBlocks).Values);
        }
        public IList<FontIconSymbol> BuildFontIconSymbolList(FontFamilyStruct fontFamily, SymbolBlockStruct symbolBlock)
        {
            return new List<FontIconSymbol>(BuildFontIconSymbolMap(fontFamily, symbolBlock).Values);
        }


    
    }
}
