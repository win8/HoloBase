﻿using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.FontIcons
{
    public sealed class FontIconSearchManager
    {
        //private static FontIconSearchManager instance = null;
        //public static FontIconSearchManager Instance
        //{
        //    get
        //    {
        //        if (instance == null) {
        //            instance = new FontIconSearchManager();
        //        }
        //        return instance;
        //    }
        //}
        //private FontIconSearchManager()
        //{
        //}


        // TBD:
        // Just use Linq.findXXX methods ???
        // ...

        // Note:
        //    The search is based on name and glyph, independent of font family.
        //    But, for consistency, we use font family in this class.

        // temporary
        private static readonly FontFamilyStruct sDefaultFontFamily = FontFamilyRegistry.FF_SegoeUISymbol;

        // Default instance.
        private static FontIconSearchManager defaultInstance = null;
        public static FontIconSearchManager DefaultInstance
        {
            get
            {
                if (defaultInstance == null) {
                    defaultInstance = new FontIconSearchManager();
                }
                return defaultInstance;
            }
        }

        // Not used. Could be just "null".
        private FontFamilyStruct fontFamily;

        // targetList cannot be null.
        private IList<FontIconSymbol> targetList;

        public FontIconSearchManager()
            : this(sDefaultFontFamily)
        {
        }
        public FontIconSearchManager(FontFamilyStruct fontFamily)
            : this(fontFamily, null)
        {
        }
        public FontIconSearchManager(FontFamilyStruct fontFamily, IList<FontIconSymbol> targetList)
        {
            this.fontFamily = fontFamily;
            if (targetList != null) {
                this.targetList = targetList;
            } else {
                // Note that defaultInstance uses this "default list".
                // (The list spans all symbol blocks in BMP.)
                this.targetList = new List<FontIconSymbol>(FontIconListManager.Instance.BuildFontIconSymbolList(fontFamily));
            }
        }
        internal IList<FontIconSymbol> TargetList
        {
            get
            {
                return this.targetList;
            }
            set
            {
                if (value != null) {
                    this.targetList = value;
                    this.glyphMap = null;
                    this.nameMap = null;
                } else {
                    // ????
                }
            }
        }


        //////////////////////////////////
        // Search "Indices"

        // This is not really necessary
        // since we build font icon list/map this way. (cf. FontIcontListManager)
        private Object lockObject1 = new Object();
        private IDictionary<int, FontIconCode> glyphMap = null;
        private IDictionary<int, FontIconCode> IconGlyphMap
        {
            get
            {
                if (glyphMap == null) {
                    lock (lockObject1) {
                        if (glyphMap == null) {
                            glyphMap = new Dictionary<int, FontIconCode>();
                            foreach (var s in TargetList) {
                                if (s.Glyph > 0) {
                                    glyphMap[s.Glyph] = new FontIconCode(s.Name, s.Glyph);
                                }
                            }
                        }
                    }
                }
                return glyphMap;
            }
        }

        // Note the the user may use the same name for more than one glyphs...
        private Object lockObject2 = new Object();
        private IDictionary<string, IList<FontIconCode>> nameMap = null;
        private IDictionary<string, IList<FontIconCode>> IconNameMap
        {
            get
            {
                if (nameMap == null) {
                    lock (lockObject2) {
                        if (nameMap == null) {
                            nameMap = new Dictionary<string, IList<FontIconCode>>();
                            foreach (var s in TargetList) {
                                if (!String.IsNullOrEmpty(s.Name)) {
                                    var key = s.Name.ToLower();
                                    IList<FontIconCode> list = null;
                                    if (nameMap.ContainsKey(key) && nameMap[key] != null) {
                                        list = nameMap[key];
                                    } else {
                                        list = new List<FontIconCode>();
                                        nameMap[key] = list;
                                    }
                                    list.Add(new FontIconCode(s.Name, s.Glyph));
                                }
                            }
                        }
                    }
                }
                return nameMap;
            }
        }


        //// Use other FindIconCodeForGlyph().
        //private FontIconCode FindIconCodeForGlyph(int glyph)
        //{
        //    if (glyph > 0 && IconGlyphMap.ContainsKey(glyph)) {
        //        return IconGlyphMap[glyph];
        //    } else {
        //        // ????
        //        return FontIconCode.Null;
        //    }
        //}

        public bool FindIconCodeForGlyph(string hexCode, out FontIconCode code)
        {
            var glyph = FontIconSymbols.GlyphFromHexCode(hexCode);
            // if glyph == 0 ??? just return false?
            return FindIconCodeForGlyph(glyph, out code);
        }
        public bool FindIconCodeForGlyph(int glyph, out FontIconCode code)
        {
            var found = false;
            try {
                code = IconGlyphMap[glyph];
                found = true;
            } catch (Exception) {
                // Just use an arbitrary code. Not to be used anyway since found == false.
                code = FontIconCode.Null;
            }
            return found;
        }


        //// Use other FindIconCodeByName().
        //private IList<FontIconCode> FindIconCodesByName(string name)
        //{
        //    if (name != null && IconNameMap.ContainsKey(name)) {
        //        return IconNameMap[name];
        //    } else {
        //        // ????
        //        //return new FontIconCode[] { FontIconCode.Null }.ToList();
        //        return null;
        //    }
        //}

        public bool FindIconCodesForName(string name, out IList<FontIconCode> codes)
        {
            IList<FontIconCode> codeList;
            var suc = FindIconCodesByName(name, out codeList, true);
            if (suc) {
                // Note that this could be an empty list.
                codes = codeList;
                return true;
            } else {
                codes = null;
                return false;
            }
        }

        // It returns whether the opeartion was successful (not whether any codes are found).
        // It may return an empty/null list even when the find operation was "successful".
        public bool FindIconCodesByName(string name, out IList<FontIconCode> codes, bool exactMatch = false)
        {
            try {
                if (exactMatch) {
                    codes = IconNameMap[name.ToLower()];
                    return true;
                } else {
                    // Note that we use CustomName for name search.
                    // (User can use glyph search as well)
                    var list = ((List<FontIconSymbol>) targetList).FindAll(o => (o.CustomName != null && o.CustomName.ToLower().Contains(name.ToLower())));
                    if (list != null) {
                        codes = new List<FontIconCode>();
                        foreach (var f in list) {
                            codes.Add(new FontIconCode(f.Name, f.Glyph));
                        }
                        return true;
                    }
                }
            } catch (Exception ex) {
                // Ignore..
                System.Diagnostics.Debug.WriteLine("FindIconCodesByName() failed. name = {0}; error = {1}", name, ex.Message);
            }
            codes = null;
            return false;
        }


    }
}
