﻿using HoloBase.Collection.Util;
using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.Data;
using HoloBase.Icons.Util;
using HoloJson.Mini.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.FontIcons
{
    public sealed class RandomFontIconListBuilder
    {
        // temporary
        private const long TTL = 12 * 3600 * 1000L;   // 1/2 day
        private const int DEFAULT_MAX_COUNT = 200;    // arbitrary.
        private static readonly FontFamilyStruct DEFAULT_FONT_FAMILY = FontFamilyRegistry.FF_SegoeUISymbol;

        // Unfortunately,
        // the routines here are rather inefficient...
        // the default should use a smaller list....
        private static readonly SymbolBlockStruct[] DEFAULT_SYMBOL_BLOCKS = new SymbolBlockStruct[]{ 
            BmpSymbolBlocks.SB_Arrows,
            BmpSymbolBlocks.SB_BraillePatterns,
            BmpSymbolBlocks.SB_CurrencySymbols,
            BmpSymbolBlocks.SB_Dingbats,
            BmpSymbolBlocks.SB_GeometricShapes,
            BmpSymbolBlocks.SB_MiscellaneousSymbols,
            BmpSymbolBlocks.SB_MiscellaneousTechnical,
            BmpSymbolBlocks.SB_NumberForms
        };

        private static readonly Random Rand = new Random((int) (DateTimeUtil.CurrentUnixEpochMillis() / 1000L));

        private static RandomFontIconListBuilder instance = null;
        public static RandomFontIconListBuilder Instance
        {
            get
            {
                if (instance == null) {
                    instance = new RandomFontIconListBuilder();
                }
                return instance;
            }
        }

        // In order to be able to cache the list/map,
        // we need to fix the following properties:
        // TBd: maxCount cannot be bigger than the allowed value for the user....
        private int maxCount = DEFAULT_MAX_COUNT;
        private FontFamilyStruct fontFamily = DEFAULT_FONT_FAMILY;
        private SymbolBlockStruct[] symbolBlocks = DEFAULT_SYMBOL_BLOCKS;

        //// "cache"
        //// private IDictionary<int, FontIconSymbol> symbolMap = null;
        //private IDictionary<int, FontIconSymbolWrap> symbolMap = null;
        //private long lastBuiltTime = 0L;
        //private bool lastUsedUseFavoriteListFirst = false;

        // New cache
        // (count, useFavoriteListFirst) => (timestamp, symbolList)
        private readonly IDictionary<Tuple<int, bool>, Tuple<long, IDictionary<int, FontIconSymbolWrap>>> masterCache = new Dictionary<Tuple<int, bool>, Tuple<long, IDictionary<int, FontIconSymbolWrap>>>();

        private RandomFontIconListBuilder()
        {
        }

        public int MaxCount
        {
            get
            {
                return maxCount;
            }
            set
            {
                if (maxCount != value) {
                    maxCount = value;
                    //symbolMap = null;
                    //lastBuiltTime = 0L;

                    // No need for this.
                    // masterCache[Tuple.Create(maxCount, lastUsedUseFavoriteListFirst)] = null;
                }
            }
        }
        public FontFamilyStruct FontFamily
        {
            get
            {
                return fontFamily;
            }
            set
            {
                if (value.Name != null && !value.Name.Equals(fontFamily.Name)) {
                    fontFamily = value;
                    //symbolMap = null;
                    //lastBuiltTime = 0L;

                    // Unfortunately, changing FontFamily, SymbolBlocks makes the cache invalidated..
                    // these values should be fixed first, and the build() methods should be called without chaning these values.
                    masterCache.Clear();
                }
            }
        }
        public SymbolBlockStruct[] SymbolBlocks
        {
            get
            {
                return symbolBlocks;
            }
            set
            {
                if (value != null && (symbolBlocks == null || (!AreTwoSymbolBlockArraysEssentiallyEqual(value, symbolBlocks)))) {
                    symbolBlocks = value;
                    //symbolMap = null;
                    //lastBuiltTime = 0L;

                    // Unfortunately, changing FontFamily, SymbolBlocks makes the cache invalidated..
                    // these values should be fixed first, and the build() methods should be called without chaning these values.
                    masterCache.Clear();
                }
            }
        }

        // tbd.
        // temporary implementation.
        // note that lhs and rhs are not null.
        internal static bool AreTwoSymbolBlockArraysEssentiallyEqual(SymbolBlockStruct[] lhs, SymbolBlockStruct[] rhs)
        {
            if (lhs.Length != rhs.Length) {
                return false;
            }

            // tbd:
            // the two arrays need to be ordered first.
            //// otherwise, the following comparison does not make sense.
            //for (var i = 0; i < lhs.Length; i++) {
            //    if (lhs[i].Block != rhs[i].Block) {
            //        return false;
            //    }
            //}
            //return true;

            var lhsSet = new HashSet<UnicodeSymbolBlock>(lhs.Select(o => o.Block));
            var rhsSet = new HashSet<UnicodeSymbolBlock>(rhs.Select(o => o.Block));
            if (lhsSet.Count != rhsSet.Count) {
                return false;
            }
            foreach (var s in lhsSet) {
                if (!rhsSet.Contains(s)) {
                    return false;
                }
            }
            //foreach (var s in rhsSet) {
            //    if (!lhsSet.Contains(s)) {
            //        return false;
            //    }
            //}
            return true;
        }



        // public IList<FontIconSymbol> GetRandomFontIconSymbolList()
        public IList<FontIconSymbolWrap> GetRandomFontIconSymbolList(bool useFavoriteListFirst = false)
        {
           //  return new List<FontIconSymbol>(GetRandomFontIconSymbolMap().Values);
            var symbolList = new List<FontIconSymbolWrap>(GetRandomFontIconSymbolMap(useFavoriteListFirst).Values);
            // Now shuffling is done in the map routine.
            //if (useFavoriteListFirst == true) {
            //    // Now we are randomizing the list.
            //    // Note that we cannot really randomize the map since it's not ordered...
            //    symbolList.Shuffle();
            //}
            return symbolList;
        }
        // public IDictionary<int, FontIconSymbol> GetRandomFontIconSymbolMap()
        public IDictionary<int, FontIconSymbolWrap> GetRandomFontIconSymbolMap(bool useFavoriteListFirst = false)
        {
            // Setting FontFamily, SymbolBlocks invalidates the cache...
            return GetRandomFontIconSymbolMap(MaxCount, FontFamily, SymbolBlocks, useFavoriteListFirst);
        }
        // private IDictionary<int, FontIconSymbol> GetRandomFontIconSymbolMap(int count, FontFamilyStruct fontFamily, SymbolBlockStruct[] symbolBlocks)
        private IDictionary<int, FontIconSymbolWrap> GetRandomFontIconSymbolMap(int count, FontFamilyStruct fontFamily, SymbolBlockStruct[] symbolBlocks, bool useFavoriteListFirst = false)
        {
            //// Note that the cache is ignored if tbe last used useFavoriteListFirst is different
            //var now = DateTimeUtil.CurrentUnixEpochMillis();
            //if (symbolMap != null && lastUsedUseFavoriteListFirst == useFavoriteListFirst && lastBuiltTime + TTL > now) {
            //    return symbolMap;
            //}

            var key = Tuple.Create(maxCount, useFavoriteListFirst);
            // Note that the cache is ignored if tbe last used useFavoriteListFirst is different
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            if (masterCache.ContainsKey(key) && masterCache[key] != null && masterCache[key].Item1 + TTL > now) {
                var symbolMap = masterCache[key].Item2;
                if (symbolMap != null) {   // this cannot be null, obviously.
                    System.Diagnostics.Debug.WriteLine("Cached FontIconSymbolWrap dictionary is returned for maxCount = {0}, useFavoriteListFirst = {1}", maxCount, useFavoriteListFirst);
                    return symbolMap;
                }
            }
            System.Diagnostics.Debug.WriteLine("No cache hit for FontIconSymbolWrap dictionary: maxCount = {0}, useFavoriteListFirst = {1}", maxCount, useFavoriteListFirst);

            var allSymbols = FontIconListManager.Instance.BuildFontIconSymbolList(fontFamily, symbolBlocks);
            var symbolCount = allSymbols.Count;

            // Note:
            // Instead of insisting on adding exactly the count number of symbols,
            // we just try to add up to count.
            // var randomMap = new Dictionary<int, FontIconSymbol>();
            var randomMap = new Dictionary<int, FontIconSymbolWrap>();
            if (useFavoriteListFirst) {
                var favoriteList = FavoriteIconsDataManager.Instance.GetFavoriteFontIconSymbolList(fontFamily.Name);
                if (favoriteList != null && favoriteList.Any()) {
                    var favCount = favoriteList.Count;
                    var maxFavs = (int) (count * 0.5);   // 50% max favs.
                    for (var i = 0; i < maxFavs; i++) {
                        var r = Rand.Next(favCount);
                        randomMap[favoriteList[r].Glyph] = new FontIconSymbolWrap(favoriteList[r]);
                    }
                }
            } else {
                for (var i = 0; i < count; i++) {
                    var r = Rand.Next(symbolCount);
                    // randomMap[allSymbols[r].Glyph] = allSymbols[r];
                    randomMap[allSymbols[r].Glyph] = new FontIconSymbolWrap(allSymbols[r]);
                }
            }
            // Try one more time,
            // but from allSymbols list regardless of useFavoriteListFirst flag.
            var randomSetCount = randomMap.Count;
            if (randomSetCount < count) {
                var delta = count - randomSetCount;
                for (var i = 0; i < delta; i++) {
                    var r = Rand.Next(symbolCount);
                    // randomMap[allSymbols[r].Glyph] = allSymbols[r];
                    randomMap[allSymbols[r].Glyph] = new FontIconSymbolWrap(allSymbols[r]);
                }
            }
            // We may still be a bit short at this point, but that's ok.
            // We do not need to return the exact count in our use cases.

            //// Update the "cache".
            //symbolMap = randomMap;
            //// Now shuffling to mix the favorites....
            //if (useFavoriteListFirst == true) {
            //    symbolMap = symbolMap.Shuffle();
            //}
            //lastBuiltTime = now;
            //lastUsedUseFavoriteListFirst = useFavoriteListFirst;

            // Update the "cache".
            IDictionary<int, FontIconSymbolWrap> symbolWrapMap = randomMap;
            // Now shuffling to mix the favorites....
            if (useFavoriteListFirst == true) {
                symbolWrapMap = symbolWrapMap.Shuffle();
            }

            // Cache it.
            var newValue = Tuple.Create(now, symbolWrapMap);
            masterCache[key] = newValue;

            // debuggings
            foreach (var g in symbolWrapMap.Keys) {
                System.Diagnostics.Debug.WriteLine("Random glyph: {0}", g);
            }
            // debuggings

            return symbolWrapMap;
        }


    }
}
