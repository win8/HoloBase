﻿using HoloBase.Controls.Core;
using HoloBase.Icons.Common;
using HoloBase.Icons.UI;
using HoloBase.Icons.ViewModels;
using HoloBase.Toast;
using HoloBase.UI.Colors;
using HoloBase.UI.Util;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class SymbolIconListControl : UserControl, IRefreshableElement, IViewModelHolder, ISymbolIconPickerControl, ISymbolIconListingControl  // , ITextClipSourceControl
    {
        // Public event for the symbol picker clients.
        public event EventHandler<SymbolIconPickerEventArgs> SymbolIconSymbolSelectionChanged;
        // for relaying xaml snippet to its container/parent.
        public event EventHandler<TextClipEventArgs> TextClipAvailable;

        // temporary
        // private bool useFavoriteList = false;
        private Size currentControlSize = Size.Empty;

        private SymbolIconListViewModel viewModel;

        private bool isSymbolInfoPopupEnabed;
        private Popup symbolInfoPopup;
        // private static readonly Popup symbolInfoPopup = new Popup();

        private string visualStateName;    // tbd: use enum???

        public SymbolIconListControl()
        {
            this.InitializeComponent();

            ResetViewModel();

            isSymbolInfoPopupEnabed = false;
            symbolInfoPopup = BuildSymbolInfoPopup();
            // UpdateSymbolInfoPopup();

            isGridFindForSymbolVisible = false;
            isGridFindByNameVisible = false;
            isSortButtonsVisible = false;
            // No need for this when in singleInput mode...
            AppBarButtonHideAll.Visibility = Visibility.Collapsed;
            // ...
            RefreshElementVisibility();

            visualStateName = "VisualStateNormal";
            this.SizeChanged += SymbolIconListControl_SizeChanged;
        }
        void SymbolIconListControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            currentControlSize = e.NewSize;
            System.Diagnostics.Debug.WriteLine("SymbolIconViewGridControl_SizeChanged() currentControlSize = {0}", currentControlSize);

            if (currentControlSize.Width > 560) {
                IsCompactForAppBarButtons = false;
            } else {
                IsCompactForAppBarButtons = true;
            }

            if (currentControlSize.Width > 480) {               // 480 Arbitrary
                visualStateName = "VisualStateMediumAndWide";   // the rest.
            } else {
                visualStateName = "VisualStateNormal";          // Phone/Portrait
            }
            // System.Diagnostics.Debug.WriteLine("visualStateName = {0}", visualStateName);

            var suc = VisualStateManager.GoToState(this, visualStateName, false);
            System.Diagnostics.Debug.WriteLine("GoToState(): {0}. Succeeded = {1}", visualStateName, suc);
        }
        private void ResetViewModel()
        {
            var vm = new SymbolIconListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SymbolIconListViewModel viewModel)
        {
            this.viewModel = viewModel;
            // this.viewModel.UseFavoriteList = useFavoriteList;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        private Popup BuildSymbolInfoPopup()
        {
            // tbd...
            var popup = new Popup();
            var infoControl = new SymbolIconSymbolInfoControl();
            infoControl.FavoriteStatusChanged += infoControl_FavoriteStatusChanged;
            infoControl.TextClipAvailable += infoControl_TextClipAvailable;
            popup.Child = infoControl;
            return popup;
        }
        //private void UpdateSymbolInfoPopup()
        //{
        //    var infoControl = new SymbolIconSymbolInfoControl();
        //    infoControl.FavoriteStatusChanged += infoControl_FavoriteStatusChanged;
        //    symbolInfoPopup.Child = infoControl;
        //}
        private void infoControl_FavoriteStatusChanged(object sender, FavoriteStatusChangeEventArgs e)
        {
            var iconSymbol = e.IconSymbol;
            viewModel.UpdateFavoriteStatus((SymbolIconSymbol) iconSymbol);
        }
        void infoControl_TextClipAvailable(object sender, TextClipEventArgs e)
        {
            // Just "relay"
            var text = e.TextClip;
            NotifyTextClipAvailable(text);
        }


        public bool IsSymbolInfoPopupEnabed
        {
            get
            {
                return isSymbolInfoPopupEnabed;
            }
            set
            {
                isSymbolInfoPopupEnabed = value;
            }
        }
        public bool IsSymbolInfoPopupOpen
        {
            get
            {
                // System.Diagnostics.Debug.WriteLine("symbolInfoPopup = {0}", symbolInfoPopup);
                var isOpen = (symbolInfoPopup != null && symbolInfoPopup.IsOpen);
                // System.Diagnostics.Debug.WriteLine("IsSymbolInfoPopupOpen = {0}", isOpen);
                return isOpen;
            }
            set
            {
                if (symbolInfoPopup != null) {
                    symbolInfoPopup.IsOpen = value;
                }
            }
        }
        public SymbolIconSymbolWrap CurrentSymbolInfo
        {
            get
            {
                if (symbolInfoPopup != null && symbolInfoPopup.IsOpen) {
                    var infoControl = symbolInfoPopup.Child as SymbolIconSymbolInfoControl;
                    if (infoControl != null) {
                        return infoControl.SymbolIconSymbolInfo;
                    }
                }
                return null;
            }
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SymbolIconListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }


        public bool UseFavoriteList
        {
            get
            {
                return viewModel.UseFavoriteList;
            }
            set
            {
                viewModel.UseFavoriteList = value;
            }
        }
      

        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }
        public string PopupBackgroundColor
        {
            get
            {
                var infoControl = symbolInfoPopup.Child as SymbolIconSymbolInfoControl;
                if (infoControl != null) {
                    return infoControl.BackgroundGridColor;
                } else {
                    // ????
                    return "Navy";
                }
            }
            set
            {
                var infoControl = symbolInfoPopup.Child as SymbolIconSymbolInfoControl;
                if (infoControl != null) {
                    infoControl.BackgroundGridColor = value;
                } else {
                    // ???
                }
            }
        }

        public string VisibilityForAppBarButtons
        {
            get
            {
                return viewModel.VisibilityForAppBarButtons;
            }
            set
            {
                viewModel.VisibilityForAppBarButtons = value;
            }
        }
        public bool IsCompactForAppBarButtons
        {
            get
            {
                return (viewModel != null) ? viewModel.IsCompactForAppBarButtons : false;
            }
            set
            {
                if (viewModel != null) {
                    viewModel.IsCompactForAppBarButtons = value;
                }
            }
        }


        // tbd:
        private bool isGridFindForSymbolVisible;
        public bool IsGridFindForSymbolVisible
        {
            get
            {
                return isGridFindForSymbolVisible;
            }
            set
            {
                isGridFindForSymbolVisible = value;
                RefreshElementVisibility();
            }
        }
        private bool isGridFindByNameVisible;
        public bool IsGridFindByNameVisible
        {
            get
            {
                return isGridFindByNameVisible;
            }
            set
            {
                isGridFindByNameVisible = value;
                RefreshElementVisibility();
            }
        }
        private bool isSortButtonsVisible;
        public bool IsSortButtonsVisible
        {
            get
            {
                return isSortButtonsVisible;
            }
            set
            {
                isSortButtonsVisible = value;
                RefreshElementVisibility();
            }
        }

        private void RefreshElementVisibility()
        {
            if (isGridFindForSymbolVisible) {
                GridFindForSymbol.Visibility = Visibility.Visible;
            } else {
                GridFindForSymbol.Visibility = Visibility.Collapsed;
            }
            AppBarToggleButtonView.IsChecked = isGridFindForSymbolVisible;

            if (isGridFindByNameVisible) {
                GridFindByName.Visibility = Visibility.Visible;
            } else {
                GridFindByName.Visibility = Visibility.Collapsed;
            }
            AppBarToggleButtonFind.IsChecked = isGridFindByNameVisible;

            if (isSortButtonsVisible) {
                GridSortButtons.Visibility = Visibility.Visible;
            } else {
                GridSortButtons.Visibility = Visibility.Collapsed;
            }
        }


        private void HideAllInputGrids()
        {
            isGridFindForSymbolVisible = false;
            isGridFindByNameVisible = false;
            RefreshElementVisibility();
        }
        private void ShowGridFindForSymbolOnly()
        {
            isGridFindForSymbolVisible = true;
            isGridFindByNameVisible = false;
            RefreshElementVisibility();
        }
        private void ShowGridFindByNameOnly()
        {
            isGridFindForSymbolVisible = false;
            isGridFindByNameVisible = true;
            RefreshElementVisibility();
        }



        private void NotifySymbolSelectionChange(SymbolIconSymbol symbol)
        {
            // temporary
            if (SymbolIconSymbolSelectionChanged != null) {
                var e = new SymbolIconPickerEventArgs(this, symbol);
                // SymbolIconSymbolSelectionChanged(this, e);
                SymbolIconSymbolSelectionChanged.Invoke(this, e);
            }
        }



        private void ButtonSortByName_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByName();
        }

        private void ButtonSortBySymbol_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortBySymbol();
        }


        //private void DismissPopup()
        //{
        //    // tbd:
        //    var popup = this.GetParentPopup();
        //    if (popup != null) {
        //        popup.IsOpen = false;
        //    }
        //}

        private void ListViewSymbolList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = (SymbolIconSymbol) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("Symbol selected: {0}", clickedItem);
            NotifySymbolSelectionChange(clickedItem);

            if (isSymbolInfoPopupEnabed) {
                var infoControl = symbolInfoPopup.Child as SymbolIconSymbolInfoControl;
                if (infoControl != null) {
                    infoControl.SymbolIconSymbol = clickedItem;
                    if (!symbolInfoPopup.IsOpen) {
                        if (currentControlSize != Size.Empty) {
                            symbolInfoPopup.HorizontalOffset = (currentControlSize.Width - 340) < 0 ? 0 : (currentControlSize.Width - 340) / 2.0;
                            symbolInfoPopup.VerticalOffset = (currentControlSize.Height - 380) > 400 ? 200 : ((currentControlSize.Height - 380) < 0 ? 0 : (currentControlSize.Height - 380) / 2.0);
                        }
                        symbolInfoPopup.IsOpen = true;
                    }
                }
            }

            //DismissPopup();
        }


        // Global flag
        private bool isSingleInputModeVisible = true;
        public bool IsSingleInputModeVisible
        {
            get
            {
                return isSingleInputModeVisible;
            }
            set
            {
                // The effect of changing this flag will not be manifested 
                //   until visibility of any of the input grids changes.
                isSingleInputModeVisible = value;
            }
        }

        private void AppBarToggleButtonView_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as AppBarToggleButton;
            if (button != null && button.IsChecked != null) {
                if (isSingleInputModeVisible) {
                    if (button.IsChecked.Value == true) {
                        ShowGridFindForSymbolOnly();
                        //AppBarToggleButtonFind.IsChecked = false;
                    } else {
                        HideAllInputGrids();
                    }
                } else {
                    isGridFindForSymbolVisible = button.IsChecked.Value;
                    RefreshElementVisibility();
                }
                if (button.IsChecked.Value == false) {
                    TextBoxSymbolForSearch.Text = String.Empty;
                    TextBoxSymbolForSearch2.Text = String.Empty;
                } else {
                    viewModel.EndSearch();
                }
            }
        }

        private void AppBarToggleButtonFind_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as AppBarToggleButton;
            if (button != null && button.IsChecked != null) {
                if (isSingleInputModeVisible) {
                    if (button.IsChecked.Value == true) {
                        ShowGridFindByNameOnly();
                        //AppBarToggleButtonView.IsChecked = false;
                    } else {
                        HideAllInputGrids();
                    }
                } else {
                    isGridFindByNameVisible = button.IsChecked.Value;
                    RefreshElementVisibility();
                }
                if (button.IsChecked.Value == false) {
                    TextBoxKeywordForSearch.Text = String.Empty;
                    TextBoxKeywordForSearch2.Text = String.Empty;
                    viewModel.EndSearch();
                }
            }
        }

        private void AppBarButtonHideAll_Click(object sender, RoutedEventArgs e)
        {
            HideAllInputGrids();
            //AppBarToggleButtonView.IsChecked = false;
            //AppBarToggleButtonFind.IsChecked = false;
            TextBoxSymbolForSearch.Text = String.Empty;
            TextBoxKeywordForSearch.Text = String.Empty;
            TextBoxSymbolForSearch2.Text = String.Empty;
            TextBoxKeywordForSearch2.Text = String.Empty;
            viewModel.EndSearch();
        }


        private void ButtonFindForSymbol_Click(object sender, RoutedEventArgs e)
        {
            string strSymbol = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                strSymbol = TextBoxSymbolForSearch2.Text;
            } else {
                strSymbol = TextBoxSymbolForSearch.Text;
            }
            if(!String.IsNullOrEmpty(strSymbol)) {
                var wrap = viewModel.GetSymbolWrapForName(strSymbol);
                System.Diagnostics.Debug.WriteLine("GetSymbolWrapForName(): strSymbol = {0}; wrap = {1}", strSymbol, wrap);
                if(wrap != null) {
                    // Note that we do this even if isSymbolInfoPopupEnabed == false.
                    var infoControl = symbolInfoPopup.Child as SymbolIconSymbolInfoControl;
                    if (infoControl != null) {
                        infoControl.SymbolIconSymbol = wrap.SymbolIconSymbol;
                        if (!symbolInfoPopup.IsOpen) {
                            if (currentControlSize != Size.Empty) {
                                symbolInfoPopup.HorizontalOffset = (currentControlSize.Width - 320) < 0 ? 0 : (currentControlSize.Width - 320) / 2.0;
                                symbolInfoPopup.VerticalOffset = (currentControlSize.Height - 300) > 400 ? 200 : (currentControlSize.Height - 300) / 2.0;
                            }
                            symbolInfoPopup.IsOpen = true;
                        }
                    }
                } else {
                    TextBoxSymbolForSearch.Text = String.Empty;   // ???
                    TextBoxSymbolForSearch2.Text = String.Empty;   // ???
                    InstantToastHelper.Instance.ShowToast("No symbols found for name, " + strSymbol);
                }
            }
        }

        private void ButtonFindByKeyword_Click(object sender, RoutedEventArgs e)
        {
            string strName = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                strName = TextBoxKeywordForSearch2.Text;
            } else {
                strName = TextBoxKeywordForSearch.Text;
            }
            if (!String.IsNullOrEmpty(strName)) {
                var suc = viewModel.DoSearchByName(strName);
                System.Diagnostics.Debug.WriteLine("DoSearchByName(): strName = {0}; suc = {1}", strName, suc);
                if(suc) {
                    // ...
                } else {
                    TextBoxKeywordForSearch.Text = String.Empty;   // ???
                    TextBoxKeywordForSearch2.Text = String.Empty;   // ???
                    InstantToastHelper.Instance.ShowToast("No symbols found for the keyword: " + strName);
                }
            }
        }

        private void TextBoxSymbolForSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                TextBoxSymbolForSearch.Text = TextBoxSymbolForSearch2.Text;
            } else {
                TextBoxSymbolForSearch2.Text = TextBoxSymbolForSearch.Text;
            }
        }

        private void TextBoxKeywordForSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strName = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                strName = TextBoxKeywordForSearch2.Text;
                TextBoxKeywordForSearch.Text = strName;
            } else {
                strName = TextBoxKeywordForSearch.Text;
                TextBoxKeywordForSearch2.Text = strName;
            }
            if (String.IsNullOrEmpty(strName)) {
                viewModel.EndSearch();
            }
        }


        private void NotifyTextClipAvailable(string textClip)
        {
            // temporary
            if (TextClipAvailable != null) {
                var e = new TextClipEventArgs(this, textClip);
                // TextClipAvailable(this, e);
                TextClipAvailable.Invoke(this, e);
            }
        }

    }
}
