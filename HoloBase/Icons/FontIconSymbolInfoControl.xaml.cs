﻿using HoloBase.Controls.Core;
using HoloBase.Icons.Clipboard;
using HoloBase.Icons.Common;
using HoloBase.Icons.Data;
using HoloBase.Icons.UI;
using HoloBase.Toast;
using HoloBase.UI.Colors;
using HoloBase.UI.Util;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HoloBase.Icons
{
    public sealed partial class FontIconSymbolInfoControl : UserControl, IRefreshableElement, IViewModelHolder, IGlyphNameUpdaterControl, IFavoriteStatusUpdaterControl, ITextClipSourceControl
    {
        // To convey the name/favorite status change information to the "parent" (list/grid view)
        public event EventHandler<GlyphNameUpdateEventArgs> GlyphNameChanged;
        public event EventHandler<FavoriteStatusChangeEventArgs> FavoriteStatusChanged;
        // for copying xaml snippets...
        public event EventHandler<TextClipEventArgs> TextClipAvailable;

        private FontIconSymbolWrap viewModel;

        public FontIconSymbolInfoControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new FontIconSymbolWrap();

            ResetViewModel(vm);
        }
        private void ResetViewModel(FontIconSymbolWrap viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
            // ???
            SymbolIconAddToClipboard.Visibility = Visibility.Visible;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (FontIconSymbolWrap) value;
                // ????

                ResetViewModel(vm);
            }
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Navy);
        public string BackgroundGridColor
        {
            get
            {
                return backgroundColor.ARGB;
            }
            set
            {
                backgroundColor = new ColorStruct(value);
                GridFontIconSymbolInfoControl.Background = new SolidColorBrush(backgroundColor.Color);
            }
        }


        public FontIconSymbolWrap FontIconSymbolInfo
        {
            get
            {
                return (FontIconSymbolWrap) viewModel;
            }
        }
        public FontIconSymbol FontIconSymbol
        {
            get
            {
                return ((FontIconSymbolWrap) viewModel).FontIconSymbol;
            }
            set
            {
                ((FontIconSymbolWrap) viewModel).FontIconSymbol = value;
                // ???
                SymbolIconAddToClipboard.Visibility = Visibility.Visible;
            }
        }


        private void DismissPopup()
        {
            // tbd:
            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }
        }
        private void ButtonDone_Click(object sender, RoutedEventArgs e)
        {
            SymbolIconAddToClipboard.Visibility = Visibility.Visible;
            DismissPopup();
        }

        private void ButtonCopyXaml_Click(object sender, RoutedEventArgs e)
        {
            var xamlSnippets = viewModel.XamlSnippetsAsString;
            if (!String.IsNullOrEmpty(xamlSnippets)) {
                NotifyTextClipAvailable(xamlSnippets);
            }
        }


        // TBD:
        // The name validate logic should be moved to more central place...
        // ....
        // temporary
        private const int MAX_NAME_LENGTH = 24;
        // temporary
        // tbd: punctuaiton marks seem to cause problems during search....
        private readonly static ISet<char> sAllowedPunctutions = new HashSet<char>{
            '-',
            '_',
            ':',
            '#',
            '$',
            '&',
            '@',
            '*',
            '/'
            ////// '%',    // Name with % in it crashes search for some reason.  --> TBD: need to figure out why!!!!
            // '.',
            // '?',
            // '!',
            // ...
        };
        // temporary
        private void ButtonUpdateName_Click(object sender, RoutedEventArgs e)
        {
            var glyph = viewModel.Glyph;
            var name = TextBoxGlyphCustomName.Text;
            // tbd:
            // Validate name here???
            // (allow only alphanumeric chars???)
            name = name.Trim();
            // tbd
            // Length limit???
            // temporary
            if (name.Length > MAX_NAME_LENGTH) {
                name = name.Substring(0, MAX_NAME_LENGTH);
            }
            // temporary
            // ...
            // Empty string resets/removes the custom name.  --> Hence valid.
            //if (!String.IsNullOrEmpty(name)) {
                if (name.All(c => (Char.IsLetterOrDigit(c) || Char.IsWhiteSpace(c) || sAllowedPunctutions.Contains(c) ))) {
                    viewModel.Name = name;
                    NotifyGlyphNameChange(new GlyphName(glyph, name));
                } else {
                    // ????
                    System.Diagnostics.Debug.WriteLine("ButtonUpdateName_Click(): Invalid name = {0}.", name);
                    InstantToastHelper.Instance.ShowToast("Invalid name: " + name);
                }
            //}
        }


        private void GridFavoriteMarker_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var isFavorite = viewModel.IsFavorite;
            viewModel.IsFavorite = !isFavorite;
            NotifyFavoriteStatusChange(viewModel.FontIconSymbol);
        }

        private void SymbolIconAddToClipboard_Tapped(object sender, TappedRoutedEventArgs e)
        {
            IconClipboardManager.Instance.Clipboard.Push(viewModel.FontIconSymbol);
            // Temporarily hide it (as a feedback)...
            // (TBD: We need to make it visible again if a different symbol is selected without closing the popup....)
            SymbolIconAddToClipboard.Visibility = Visibility.Collapsed;
        }


        private void NotifyGlyphNameChange(GlyphName updatedGlyphName)
        {
            // temporary
            if (GlyphNameChanged != null) {
                var e = new GlyphNameUpdateEventArgs(this, updatedGlyphName);
                // GlyphNameChanged(this, e);
                GlyphNameChanged.Invoke(this, e);
            }
        }
        private void NotifyFavoriteStatusChange(FontIconSymbol iconSymbol)
        {
            // temporary
            if (FavoriteStatusChanged != null) {
                var e = new FavoriteStatusChangeEventArgs(this, iconSymbol);
                // FavoriteStatusChanged(this, e);
                FavoriteStatusChanged.Invoke(this, e);
            }
        }

        private void NotifyTextClipAvailable(string textClip)
        {
            // temporary
            if (TextClipAvailable != null) {
                var e = new TextClipEventArgs(this, textClip);
                // TextClipAvailable(this, e);
                TextClipAvailable.Invoke(this, e);
            }
        }
    }
}
