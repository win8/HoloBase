﻿using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;


namespace HoloBase.Icons.Converters
{
    public class SymbolNameValueConverter : IValueConverter
    {
        // value: ISymbol
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var name = "";
            if (value is IIconSymbol) {
                name = ((IIconSymbol) value).Name;
            } else if (value is SymbolClip || value is SymbolClipWrap) {
                SymbolClip symbolClip;
                if (value is SymbolClip) {
                    symbolClip = (SymbolClip) value;
                } else {
                    symbolClip = ((SymbolClipWrap) value).SymbolClipStruct;
                }
                var type = symbolClip.Type;
                if (type == IconType.SymbolIcon) {
                    Symbol s = (Symbol) ((ISymbol) value).Glyph;
                    name = Enum.GetName(typeof(Symbol), s); 
                } else if (type == IconType.FontIcon) {
                    var gn = FontIconCustomDataManager.Instance.GetGlyphName(((ISymbol) value).Glyph);
                    if (gn != null) {
                        name = gn.Name;
                    } else {
                        // ignore.
                    }
                }
            } else {
                name = value.ToString();
            }
            return name;
        }
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
