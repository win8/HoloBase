﻿using HoloBase.Icons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;


namespace HoloBase.Icons.Converters
{
    public class GlyphTextValueConverter : IValueConverter
    {
        // value: glyph
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var glyph = (int) value;
            var text = FontIconSymbols.GlyphText(glyph);
            return text;
            //return value.ToString();
        }
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
