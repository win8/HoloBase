﻿using HoloBase.Collection.Util;
using HoloBase.Icons.Common;
using HoloBase.Icons.SymbolIcons;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.ViewModels
{
    public class SymbolIconCubbyViewModel : IViewModel, INotifyPropertyChanged
    {
        private int maxCount;
        private bool useFavoriteListFirst;
        // private IList<SymbolIconSymbol> symbolList = null;
        private IList<SymbolIconSymbolWrap> symbolList = null;

        public SymbolIconCubbyViewModel()
        {
            // temporary
            maxCount = 50;
            useFavoriteListFirst = false;
            CreateSymbolList();
        }

        // Note that if UseFavoriteListFirst == true,
        //   then up to the first 50% of the list can be from the user's favorite icons.
        public bool UseFavoriteListFirst
        {
            get
            {
                return useFavoriteListFirst;
            }
            set
            {
                if (useFavoriteListFirst != value) {
                    useFavoriteListFirst = value;
                    CreateSymbolList();
                }
            }
        }

        private void CreateSymbolList()
        {
            // tbd:
            RandomSymbolIconListBuilder.Instance.MaxCount = maxCount;
            symbolList = RandomSymbolIconListBuilder.Instance.GetRandomSymbols(useFavoriteListFirst);
            // --> Do this in Random builder....
            //if (useFavoriteListFirst == true) {
            //    // Now we are randomizing the list.
            //    // --> Shuffling may look the list different to the user evey time the listview is loaded (which is undesirable)
            //    symbolList.Shuffle();
            //}
            RaisePropertyChanged("SymbolIconSymbolList");
            RaisePropertyChanged("SymbolIconSymbolList1");
            RaisePropertyChanged("SymbolIconSymbolList2");
            RaisePropertyChanged("SymbolIconSymbolListA");
            RaisePropertyChanged("SymbolIconSymbolListB");
            RaisePropertyChanged("SymbolIconSymbolListC");
            // RaisePropertyChanged("SymbolIconSymbolPairList");
        }

        public void UpdateFavoriteStatus(SymbolIconSymbol iconSymbol)
        {
            // ????
            for (var i = 0; i < symbolList.Count; i++) {
                var s = symbolList[i];
                if (s.Symbol == iconSymbol.Symbol) {
                    //// symbolList[i] = iconSymbol;
                    //var symbol = s;
                    //symbol.IsFavorite = iconSymbol.IsFavorite;
                    //symbolList[i] = symbol;
                    s.IsFavorite = iconSymbol.IsFavorite;
                    break;
                }
            }
            //// symbolList = new List<SymbolIconSymbol>(symbolList);   // ????
            //symbolList = new List<SymbolIconSymbolWrap>(symbolList);   // ????

            // ????
            //RaisePropertyChanged("SymbolIconSymbolList");
            //RaisePropertyChanged("SymbolIconSymbolList1");
            //RaisePropertyChanged("SymbolIconSymbolList2");
            // // RaisePropertyChanged("SymbolIconSymbolPairList");
        }


        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }
        private ColorStruct itemBackgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct ItemBackgroundColor
        {
            get
            {
                return itemBackgroundColor;
            }
            set
            {
                itemBackgroundColor = value;
                // ????
                // tbd: this cannot be efficient....
                if (symbolList != null) {
                    foreach (var s in symbolList) {
                        s.BackgroundColor = itemBackgroundColor;
                    }
                }
                // ????
                RaisePropertyChanged("ItemBackgroundColor");
            }
        }


        public int MaxCount
        {
            get
            {
                return maxCount;
            }
            set
            {
                if (maxCount != value) {
                    maxCount = value;
                    RandomSymbolIconListBuilder.Instance.MaxCount = maxCount;
                    CreateSymbolList();
                }
            }
        }


        // public IList<SymbolIconSymbol> SymbolIconSymbolList
        public IList<SymbolIconSymbolWrap> SymbolIconSymbolList
        {
            get
            {
                // ...
                return symbolList;
            }
            set
            {
                symbolList = value ?? new List<SymbolIconSymbolWrap>();   // ???
                RaisePropertyChanged("SymbolIconSymbolList");
                RaisePropertyChanged("SymbolIconSymbolList1");
                RaisePropertyChanged("SymbolIconSymbolList2");
                RaisePropertyChanged("SymbolIconSymbolListA");
                RaisePropertyChanged("SymbolIconSymbolListB");
                RaisePropertyChanged("SymbolIconSymbolListC");
                // RaisePropertyChanged("SymbolIconSymbolPairList");
            }
        }

        // TBD:
        // Dividing odd/even is better for UX...
        public IList<SymbolIconSymbolWrap> SymbolIconSymbolList1
        {
            get
            {
                if (symbolList != null) {
                    int mid = (symbolList.Count + 1) / 2;
                    int size = mid;
                    return ((List<SymbolIconSymbolWrap>) symbolList).GetRange(0, size);
                } else {
                    return null;
                }
            }
        }
        public IList<SymbolIconSymbolWrap> SymbolIconSymbolList2
        {
            get
            {
                if (symbolList != null) {
                    int mid = (symbolList.Count + 1) / 2;
                    int size = symbolList.Count - mid;
                    return ((List<SymbolIconSymbolWrap>) symbolList).GetRange(mid, size);
                } else {
                    return null;
                }
            }
        }

        public IList<SymbolIconSymbolWrap> SymbolIconSymbolListA
        {
            get
            {
                if (symbolList != null) {
                    int third1 = (symbolList.Count + 2) / 3;
                    int size = third1;
                    return ((List<SymbolIconSymbolWrap>) symbolList).GetRange(0, size);
                } else {
                    return null;
                }
            }
        }
        public IList<SymbolIconSymbolWrap> SymbolIconSymbolListB
        {
            get
            {
                if (symbolList != null) {
                    int third1 = (symbolList.Count + 2) / 3;
                    int third2 = (symbolList.Count + 1) / 3;
                    int size = third2;
                    return ((List<SymbolIconSymbolWrap>) symbolList).GetRange(third2, size);
                } else {
                    return null;
                }
            }
        }
        public IList<SymbolIconSymbolWrap> SymbolIconSymbolListC
        {
            get
            {
                if (symbolList != null) {
                    int third1 = (symbolList.Count + 2) / 3;
                    int third2 = (symbolList.Count + 1) / 3;
                    int size = symbolList.Count - (third1 + third2);
                    return ((List<SymbolIconSymbolWrap>) symbolList).GetRange((third1 + third2), size);
                } else {
                    return null;
                }
            }
        }

        // Not being used.
        public IList<Tuple<SymbolIconSymbolWrap, SymbolIconSymbolWrap>> SymbolIconSymbolPairList
        {
            get
            {
                IList<Tuple<SymbolIconSymbolWrap, SymbolIconSymbolWrap>> pairList = null;
                if (symbolList != null) {
                    pairList = new List<Tuple<SymbolIconSymbolWrap, SymbolIconSymbolWrap>>();
                    // Note that to simplify the logic (not just here, but on UI),
                    //    we truncate the last element if symbolList has an odd number of elements.
                    // This is fine as long as we use this "Cubby" viewmodel for displaying random elements, etc.,
                    //    but, when more precision is needed, we need to use a better algorithm.
                    //    (e.g., how to handle the last null element on the UI, etc.)
                    for (var i = 0; i < symbolList.Count - 1; i += 2) {
                        var pair = new Tuple<SymbolIconSymbolWrap, SymbolIconSymbolWrap>(symbolList[i], symbolList[i + 1]);
                        pairList.Add(pair);
                    }
                    // ...
                }
                return pairList;
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
