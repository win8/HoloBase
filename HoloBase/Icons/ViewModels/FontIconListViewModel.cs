﻿using HoloBase.Data.Core;
using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.Data;
using HoloBase.Icons.FontIcons;
using HoloBase.Icons.Util;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;


namespace HoloBase.Icons.ViewModels
{
    // Note:
    // In the current implementation, 
    // we cannot dynamically (e.g., in response to UI even) change the symbol list between generic vs favorite lists.
    // ...
    // Note also that if list/grid view is used for favorite list,
    //    then the font family / symbol block combobox should be hidden in the UI.
    public class FontIconListViewModel : IViewModel, INotifyPropertyChanged
    {
        // temporary.
        // We do not have to use this, if it's correctly implemented.
        private readonly SymbolBlockStruct DefaultCustomSymbolBlock = BmpSymbolBlocks.SB_DefaultCustomRange;

        private bool isInSearch = false;   // the initial value needs to be always false.
        //private FontIconSearchManager favoriteListSearchManager;
        //private FontIconSearchManager allListSearchManager;
        private FontIconSearchManager searchManager = null;
        private IList<FontIconSymbol> searchResultList = null;

        private bool useFavoriteList;
        private FontFamilyStruct fontFamily;
        private SymbolBlockStruct symbolBlock;
        private SymbolBlockStruct lastUsedCustomSymbolBlock;
        private SymbolSizeStruct symbolSize;
        
        // TBD:
        // It is always better to use reference type (FontIconSymbolWrap) in a collection than value type (FontIconSymbol).
        private IDictionary<int, FontIconSymbol> symbolMap = null;
        private IList<FontIconSymbol> symbolList = null;

        private SortFieldAndOrder currentSortFieldAndOrder = null;

        public FontIconListViewModel()
        {
            // tbd
            // fontFamily = FontFamilyRegistry.FF_SegoeUI;
            fontFamily = FontFamilyRegistry.FF_SegoeUISymbol;
            // fontFamily = FontFamilyRegistry.FF_CambriaMath;
            symbolBlock = BmpSymbolBlocks.SB_MiscellaneousSymbols;   // temporary
            // symbolBlock = BmpSymbolBlocks.SB_Arrows;
            // symbolBlock = new SymbolBlockStruct(UnicodeSymbolBlock.CustomRange, new int[] { 50, 2000 });

            // tbd: 
            lastUsedCustomSymbolBlock = DefaultCustomSymbolBlock;
            // ...

            symbolSize = SymbolSizes.SymbolSizeMedium;
            currentSortFieldAndOrder = new SortFieldAndOrder("Glyph", SortOrder.Ascending);

            useFavoriteList = false;
            InitializeSymbolMap();
            // ???
        }


        private void InitializeSymbolMap()
        {
            if (symbolList != null && isInSearch) {
                // Keep the current list.
            } else {
                IDictionary<int, FontIconSymbol> map = null;
                if (useFavoriteList) {
                    map = FavoriteIconsDataManager.Instance.GetFavoriteFontIconSymbolMap(fontFamily.Name);
                } else {
                    map = FontIconListManager.Instance.BuildFontIconSymbolMap(fontFamily, symbolBlock);
                }
                UpdateSymbolMap(map);
            }
        }
        private void RebuildSymbolMapForFontFamily()
        {
            // ????
            if (symbolList != null && useFavoriteList) {
                // Keep the current list.
                // symbolMap = FavoriteIconsDataManager.Instance.GetFavoriteFontIconSymbolMap(fontFamily.Name);
                // ...
            } else {
                var map = FontIconListManager.Instance.BuildFontIconSymbolMap(fontFamily, symbolBlock);
                UpdateSymbolMap(map);
            }
        }
        private void UpdateSymbolMap(IDictionary<int, FontIconSymbol> symbolMap)
        {
            this.symbolMap = symbolMap ?? new Dictionary<int, FontIconSymbol>();   // ????
            this.symbolList = new List<FontIconSymbol>(this.symbolMap.Values);
            searchManager = new FontIconSearchManager(this.fontFamily, this.symbolList);
            searchResultList = null;
            SortListItems();
        }
        //private void RebuildSymbolList()
        //{
        //    symbolList = new List<FontIconSymbol>(symbolMap.Values);
        //    searchManager = new FontIconSearchManager(this.fontFamily, this.symbolList);
        //    SortListItems();
        //}


        public bool IsInSearch
        {
            get
            {
                return isInSearch;
            }
            internal set
            {
                if (isInSearch != value) {
                    isInSearch = value;
                    //RaisePropertyChanged("FontIconSymbolList");
                    //RaisePropertyChanged("FontIconSymbolMap");
                    SortListItems();    // ????
                }
            }
        }

        public bool UseFavoriteList
        {
            get
            {
                return useFavoriteList;
            }
            set
            {
                if (symbolList == null
                    || (isInSearch == false    // ???
                    && useFavoriteList != value)) {
                    useFavoriteList = value;
                    InitializeSymbolMap();
                }
            }
        }


        public IList<FontIconSymbol> FontIconSymbolList
        {
            get
            {
                if (isInSearch) {
                    return searchResultList;
                } else {
                    return symbolList;
                }
            }
        }
        public IDictionary<int, FontIconSymbol> FontIconSymbolMap
        {
            get
            {
                return symbolMap;
            }
            set
            {
                if (isInSearch == false) {
                    //symbolMap = value;
                    //currentSortFieldAndOrder = new SortFieldAndOrder("Glyph", SortOrder.Ascending);   // Reset the sort order since it's a new map/list.
                    //RebuildSymbolList();

                    UpdateSymbolMap(value);
                }
            }
        }


        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }

        private Visibility visibilityForAppBarButtons = Visibility.Visible;
        public string VisibilityForAppBarButtons
        {
            get
            {
                return Enum.GetName(typeof(Visibility), visibilityForAppBarButtons);
            }
            set
            {
                visibilityForAppBarButtons = (Visibility) Enum.Parse(typeof(Visibility), value);
                RaisePropertyChanged("VisibilityForAppBarButtons");
            }
        }
        private bool isCompactForAppBarButtons = false;
        public bool IsCompactForAppBarButtons
        {
            get
            {
                return isCompactForAppBarButtons;
            }
            set
            {
                isCompactForAppBarButtons = value;
                RaisePropertyChanged("IsCompactForAppBarButtons");
            }
        }


        public IList<FontFamilyStruct> FontFamilyList
        {
            get
            {
                return FontFamilyRegistry.List;
            }
        }
        public static IList<SymbolBlockStruct> SymbolBlockList
        {
            get
            {
                // return BmpSymbolBlocks.List;
                var list = new List<SymbolBlockStruct>(BmpSymbolBlocks.List.OrderBy(o => o.Name));

                // tbd: Put AllSymbols at the top.
                //      then, custom range.
                // (Currently, because of the names, "(Custom Range)" comes first, then "All Symbols", then the rest follow.)
                return list;
            }
        }

        public FontFamilyStruct FontFamily
        {
            get
            {
                return fontFamily;
            }
            set
            {
                if (isInSearch == false) {
                    if (value.Name != null && !value.Name.Equals(fontFamily.Name)) {
                        fontFamily = value;
                        // ???
                        // Note: This reets the symbol map/list  even it it has been explicitly set via the Setter propery.
                        RebuildSymbolMapForFontFamily();
                    }
                }
            }
        }
        public string FontFamilyName
        {
            get
            {
                return fontFamily.Name;
            }
        }

        // TBD: Custom symbol block ???
        public SymbolBlockStruct SymbolBlock
        {
            get
            {
                // ...
                return symbolBlock;
            }
            set
            {
                if (isInSearch == false) {
                    if (SymbolBlocks.IsCustomRange(value) || value.Block != symbolBlock.Block) {
                        if (SymbolBlocks.IsCustomRange(value)) {
                            if (SymbolBlocks.IsRangeNull(value)) {
                                symbolBlock = lastUsedCustomSymbolBlock;
                            } else {
                                lastUsedCustomSymbolBlock = value;
                                symbolBlock = lastUsedCustomSymbolBlock;
                            }
                        } else {
                            symbolBlock = value;
                        }

                        // ???
                        // Note: This reets the symbol map/list  even it it has been explicitly set via the Setter propery.
                        RebuildSymbolMapForFontFamily();
                    }
                }
            }
        }
        public string SymbolBlockName
        {
            get
            {
                return symbolBlock.Name;
            }
        }


        // Setting FontFamily and SymbolBlock separately is ineffienct
        //    since it builds the list twice.
        // Use this method instead.
        // TBD: lazy-initialize symbol list???
        public void ResetFontIconSymbolList(FontFamilyStruct fontFamily, SymbolBlockStruct symbolBlock)
        {
            if (isInSearch == false) {
                this.fontFamily = fontFamily;
                this.symbolBlock = symbolBlock;
                // ???
                // Note: This reets the symbol map/list  even it it has been explicitly set via the Setter propery.
                RebuildSymbolMapForFontFamily();
            }
        }
        public void ResetFontIconSymbolList()
        {
            ResetFontIconSymbolList(fontFamily, symbolBlock);
        }

        // Is this really more efficient than
        // say, just regenerating the list.
        public void UpdateGlyphName(GlyphName glyphName)
        {
            // TBD: Does this work???
            if (symbolMap.ContainsKey(glyphName.Glyph)) {
                var symbol = symbolMap[glyphName.Glyph];
                symbol.Name = glyphName.Name;
                symbolMap[glyphName.Glyph] = symbol;


                // TBD: Can we update the element without resetting the list reference????
                // This assignment unfortnately invalidates the target list of searchManater....
                symbolList = new List<FontIconSymbol>(symbolMap.Values);
                if (searchManager != null) {
                    searchManager.TargetList = symbolList;
                    // searchResultList = null;    // ???
                }
                // Unfortunately, this has the effect of moving the currently updated item to a different location
                //      (if the list currently sorted by name)...
                // Without this, however, the list is completely jumbled...
                SortListItems();
                // ....
                RaisePropertyChanged("FontIconSymbolList");
                RaisePropertyChanged("FontIconSymbolMap");
            }
        }
        public void UpdateFavoriteStatus(FontIconSymbol iconSymbol)
        {
            // ????
            var glyph = iconSymbol.Glyph;
            if (symbolMap.ContainsKey(glyph)) {
                var symbol = symbolMap[glyph];
                // symbol = iconSymbol;
                symbol.IsFavorite = iconSymbol.IsFavorite;
                symbolMap[glyph] = symbol;
                symbolList = new List<FontIconSymbol>(symbolMap.Values);
                // This is a bit confusing.
                // We do not call SortListItems() for symbol icons,
                // but, we need to call this for font icon list....  Why????
                SortListItems();
                // ....
                RaisePropertyChanged("FontIconSymbolList");
                RaisePropertyChanged("FontIconSymbolMap");
            }
        }


        public SymbolSizeStruct SymbolSize
        {
            get
            {
                // ...
                return symbolSize;
            }
            set
            {
                if (symbolSize.Size != value.Size) {
                    symbolSize = value;
                    // ???
                    RaisePropertyChanged("SymbolSize");
                    // ????
                    RaisePropertyChanged("FontIconSymbolList");
                    RaisePropertyChanged("FontIconSymbolMap");
                    // ???
                }
            }
        }

        public static IList<SymbolSizeStruct> SymbolSizeList
        {
            get
            {
                return SymbolSizes.List;
            }
        }


        public void SortByFontFamily()
        {
            if (IsCurrentlySortedByFontFamily && CurrentSortOrder == SortOrder.Ascending) {
                currentSortFieldAndOrder = new SortFieldAndOrder("FontFamily", SortOrder.Descending);
            } else {
                currentSortFieldAndOrder = new SortFieldAndOrder("FontFamily", SortOrder.Ascending);
            }
            SortListItems();
        }
        public void SortByName()
        {
            if (IsCurrentlySortedByName && CurrentSortOrder == SortOrder.Ascending) {
                currentSortFieldAndOrder = new SortFieldAndOrder("Name", SortOrder.Descending);
            } else {
                currentSortFieldAndOrder = new SortFieldAndOrder("Name", SortOrder.Ascending);
            }
            SortListItems();
        }

        public void SortByGlyph()
        {
            if (IsCurrentlySortedByGlyph && CurrentSortOrder == SortOrder.Ascending) {
                currentSortFieldAndOrder = new SortFieldAndOrder("Glyph", SortOrder.Descending);
            } else {
                currentSortFieldAndOrder = new SortFieldAndOrder("Glyph", SortOrder.Ascending);
            }
            SortListItems();
        }


        private bool IsCurrentlySortedByFontFamily
        {
            get
            {
                return (currentSortFieldAndOrder != null && "FontFamily".Equals(currentSortFieldAndOrder.SortField));
            }
        }
        private bool IsCurrentlySortedByName
        {
            get
            {
                return (currentSortFieldAndOrder != null && "Name".Equals(currentSortFieldAndOrder.SortField));
            }
        }
        private bool IsCurrentlySortedByGlyph
        {
            get
            {
                return (currentSortFieldAndOrder != null && "Glyph".Equals(currentSortFieldAndOrder.SortField));
            }
        }
        private SortOrder CurrentSortOrder
        {
            get
            {
                if (currentSortFieldAndOrder == null) {
                    return SortOrder.None;
                } else {
                    return currentSortFieldAndOrder.SortOrder;
                }
            }
        }

        private void SortListItems()
        {
            // TBD:
            // var list = UniversalBookmarkManager.Instance.GetAllFontIconSymbols();
            if (IsCurrentlySortedByFontFamily) {
                switch (CurrentSortOrder) {
                    case SortOrder.Descending:
                        symbolList = new List<FontIconSymbol>(symbolList.OrderByDescending(o => o.FontFamily));
                        if (searchResultList != null) {
                            searchResultList = new List<FontIconSymbol>(searchResultList.OrderByDescending(o => o.FontFamily));
                        }
                        break;
                    case SortOrder.Ascending:
                    default:
                        symbolList = new List<FontIconSymbol>(symbolList.OrderBy(o => o.FontFamily));
                        if (searchResultList != null) {
                            searchResultList = new List<FontIconSymbol>(searchResultList.OrderBy(o => o.FontFamily));
                        }
                        break;
                }
            } else if (IsCurrentlySortedByName) {
                switch (CurrentSortOrder) {
                    case SortOrder.Descending:
                        symbolList = new List<FontIconSymbol>(symbolList.OrderByDescending(o => o.Name));
                        if (searchResultList != null) {
                            searchResultList = new List<FontIconSymbol>(searchResultList.OrderByDescending(o => o.Name));
                        }
                        break;
                    case SortOrder.Ascending:
                    default:
                        symbolList = new List<FontIconSymbol>(symbolList.OrderBy(o => o.Name));
                        if (searchResultList != null) {
                            searchResultList = new List<FontIconSymbol>(searchResultList.OrderBy(o => o.Name));
                        }
                        break;
                }
            } else if (IsCurrentlySortedByGlyph) {
                switch (CurrentSortOrder) {
                    case SortOrder.Descending:
                        symbolList = new List<FontIconSymbol>(symbolList.OrderByDescending(o => o.Glyph));
                        if (searchResultList != null) {
                            searchResultList = new List<FontIconSymbol>(searchResultList.OrderByDescending(o => o.Glyph));
                        }
                        break;
                    case SortOrder.Ascending:
                    default:
                        symbolList = new List<FontIconSymbol>(symbolList.OrderBy(o => o.Glyph));
                        if (searchResultList != null) {
                            searchResultList = new List<FontIconSymbol>(searchResultList.OrderBy(o => o.Glyph));
                        }
                        break;
                }
            } else {
                // ignore.
            }
            RaisePropertyChanged("FontIconSymbolList");
            RaisePropertyChanged("FontIconSymbolMap");
        }


        // This is NOT considered "search".
        public FontIconSymbolWrap GetSymbolWrapForGlyph(int glyph)
        {
            // Note that we use the global list for this...
            //  (which includes all symbol blocks in BMP).
            FontIconCode code;
            // var found = searchManager.FindIconCodeForGlyph(glyph, out code);
            var found = FontIconSearchManager.DefaultInstance.FindIconCodeForGlyph(glyph, out code);
            // temporary
            if (!found) {
                // Does not exist in our BMP glyph list
                // But, still return the character corresponding to the glyph????
                // TBD:
                // --> Use a user option ????

                // ???
                code = new FontIconCode(fontFamily.Name, glyph);   // Use the current fontFamily name...
                System.Diagnostics.Debug.WriteLine("Character outside the given BMP blocks: glyph = {0}; code = {1}", glyph, code);
                found = true;
                // ???
            }
            // temporary
            if (found) {
                string name = null;
                if (FontIconCustomDataManager.Instance.ContainsGlyphName(code.Glyph)) {
                    name = FontIconCustomDataManager.Instance.GetGlyphName(code.Glyph).Name;
                }
                var isFavorite = FavoriteIconsDataManager.Instance.IsFavorteFontIconGlyph(code.Glyph);
                var fontIconSymbolWrap = new FontIconSymbolWrap(new FontIconSymbol(code.FontFamily, code.Glyph, name, isFavorite));
                System.Diagnostics.Debug.WriteLine("GetSymbolWrapForGlyph(). glyph = {0}; fontIconSymbolWrap = {1}", glyph, fontIconSymbolWrap);
                return fontIconSymbolWrap;
            } else {
                return null;
            }
        }

        // Note: DoSearchByName() can be called multiple times before EndSearch() is called.
        public bool DoSearchByName(string keyword)
        {
            IList<FontIconCode> codes = null;
            var suc = false;
            if (searchManager != null) {    // Can searchManager be null????
                suc = searchManager.FindIconCodesByName(keyword, out codes);
            }
            if (suc && codes != null && codes.Any()) {
                var list = FontIconListManager.BuildFontIconSymbolList(codes);
                this.searchResultList = list;
                this.IsInSearch = true;
                return true;
            } else {
                // Show Toast?
                return false;
            }
        }
        public void EndSearch()
        {
            this.IsInSearch = false;
            this.searchResultList = null;
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
