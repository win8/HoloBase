﻿using HoloBase.Icons.Core;
using HoloBase.Icons.Util;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.ViewModels
{
    public class FontFamilyListViewModel : IViewModel, INotifyPropertyChanged
    {
        private IList<FontFamilyStruct> fontFamilyList = null;
        public FontFamilyListViewModel()
        {
            var defaultList = FontFamilyRegistry.List;
            UpdateFontFamilyList(defaultList);
        }
        private void UpdateFontFamilyList(IList<FontFamilyStruct> fontFamilyList)
        {
            this.fontFamilyList = fontFamilyList ?? new List<FontFamilyStruct>();   // ????
            RaisePropertyChanged("FontFamilyList");
        }

        public IList<FontFamilyStruct> FontFamilyList
        {
            get
            {
                return fontFamilyList;
            }
            set
            {
                UpdateFontFamilyList(value);
            }
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
