﻿using HoloBase.Data.Core;
using HoloBase.Icons.Common;
using HoloBase.Icons.Data;
using HoloBase.Icons.SymbolIcons;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.ViewModels
{
    public class SymbolIconListViewModel : IViewModel, INotifyPropertyChanged
    {
        private bool isInSearch = false;   // the initial value needs to be always false.
        //private SymbolIconSearchManager favoriteListSearchManager;
        //private SymbolIconSearchManager allListSearchManager;
        private SymbolIconSearchManager searchManager = null;
        private IList<SymbolIconSymbol> searchResultList = null;

        private bool useFavoriteList;
        private IList<SymbolIconSymbol> symbolList = null;
        private SortFieldAndOrder currentSortFieldAndOrder = null;

        public SymbolIconListViewModel()
        {
            // tbd
            currentSortFieldAndOrder = new SortFieldAndOrder("Name", SortOrder.Ascending);
            useFavoriteList = false;
            InitializeSymbolList();
        }
        private void InitializeSymbolList()
        {
            if (symbolList != null && isInSearch) {
                // Keep the current list.
            } else {
                IList<SymbolIconSymbol> list = null;
                if (UseFavoriteList) {
                    list = FavoriteIconsDataManager.Instance.GetFavoriteSymbolIconSymbolList();
                } else {
                    // list = SymbolIconSymbols.SymbolList;
                    list = SymbolIconListManager.Instance.SymbolList;
                }
                UpdateSymbolList(list);
            }
        }
        private void UpdateSymbolList(IList<SymbolIconSymbol> symbolList)
        {
            this.symbolList = symbolList ?? new List<SymbolIconSymbol>();    // ????
            searchManager = new SymbolIconSearchManager(this.symbolList);
            searchResultList = null;
            SortListItems();
        }

        public bool IsInSearch
        {
            get
            {
                return isInSearch;
            }
            internal set
            {
                if (isInSearch != value) {
                    isInSearch = value;
                    // RaisePropertyChanged("SymbolIconSymbolList");
                    SortListItems();   // ????
                }
            }
        }

        public bool UseFavoriteList
        {
            get
            {
                return useFavoriteList;
            }
            set
            {
                if (symbolList == null
                    || (isInSearch == false    // ???
                    && useFavoriteList != value)) {
                    useFavoriteList = value;
                    InitializeSymbolList();
                }
            }
        }

        public void UpdateFavoriteStatus(SymbolIconSymbol iconSymbol)
        {
            // ????
            for (var i = 0; i < symbolList.Count; i++) {
                var s = symbolList[i];
                if (s.Symbol == iconSymbol.Symbol) {
                    // symbolList[i] = iconSymbol;
                    var symbol = s;
                    symbol.IsFavorite = iconSymbol.IsFavorite;
                    symbolList[i] = symbol;
                    break;
                }
            }
            // No need to re-sort the list. ???? Just use RaisePropertyChanged().
            // SortListItems();
            // ?????
            // RaisePropertyChanged() does not work for some reason.
            // We need to refresh symbolList in some way (heck)...
            // to make the list/grid view screen udpated....
            symbolList = new List<SymbolIconSymbol>(symbolList);
            RaisePropertyChanged("SymbolIconSymbolList");
            // ?????
        }


        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }

        private Visibility visibilityForAppBarButtons = Visibility.Visible;
        public string VisibilityForAppBarButtons
        {
            get
            {
                return Enum.GetName(typeof(Visibility), visibilityForAppBarButtons);
            }
            set
            {
                visibilityForAppBarButtons = (Visibility) Enum.Parse(typeof(Visibility), value);
                RaisePropertyChanged("VisibilityForAppBarButtons");
            }
        }
        private bool isCompactForAppBarButtons = false;
        public bool IsCompactForAppBarButtons
        {
            get
            {
                return isCompactForAppBarButtons;
            }
            set
            {
                isCompactForAppBarButtons = value;
                RaisePropertyChanged("IsCompactForAppBarButtons");
            }
        }


        public IList<SymbolIconSymbol> SymbolIconSymbolList
        {
            get
            {
                if (isInSearch) {
                    return searchResultList;
                } else {
                    return symbolList;
                }
            }
            set
            {
                if (isInSearch == false) {
                    UpdateSymbolList(value);
                }
            }
        }

        public void SortByName()
        {
            if (IsCurrentlySortedByName && CurrentSortOrder == SortOrder.Ascending) {
                currentSortFieldAndOrder = new SortFieldAndOrder("Name", SortOrder.Descending);
            } else {
                currentSortFieldAndOrder = new SortFieldAndOrder("Name", SortOrder.Ascending);
            }
            SortListItems();
        }

        public void SortBySymbol()
        {
            if (IsCurrentlySortedBySymbol && CurrentSortOrder == SortOrder.Ascending) {
                currentSortFieldAndOrder = new SortFieldAndOrder("Symbol", SortOrder.Descending);
            } else {
                currentSortFieldAndOrder = new SortFieldAndOrder("Symbol", SortOrder.Ascending);
            }
            SortListItems();
        }


        private bool IsCurrentlySortedByName
        {
            get
            {
                return (currentSortFieldAndOrder != null && "Name".Equals(currentSortFieldAndOrder.SortField));
            }
        }
        private bool IsCurrentlySortedBySymbol
        {
            get
            {
                return (currentSortFieldAndOrder != null && "Symbol".Equals(currentSortFieldAndOrder.SortField));
            }
        }
        private SortOrder CurrentSortOrder
        {
            get
            {
                if (currentSortFieldAndOrder == null) {
                    return SortOrder.None;
                } else {
                    return currentSortFieldAndOrder.SortOrder;
                }
            }
        }

        private void SortListItems()
        {
            // TBD:
            // var list = UniversalBookmarkManager.Instance.GetAllSymbolIconSymbols();
            if (IsCurrentlySortedByName) {
                switch (CurrentSortOrder) {
                    case SortOrder.Descending:
                        symbolList = new List<SymbolIconSymbol>(symbolList.OrderByDescending(o => o.Name));
                        if (searchResultList != null) {
                            searchResultList = new List<SymbolIconSymbol>(searchResultList.OrderByDescending(o => o.Name));
                        }
                        break;
                    case SortOrder.Ascending:
                    default:
                        symbolList = new List<SymbolIconSymbol>(symbolList.OrderBy(o => o.Name));
                        if (searchResultList != null) {
                            searchResultList = new List<SymbolIconSymbol>(searchResultList.OrderBy(o => o.Name));
                        }
                        break;
                }
            } else if (IsCurrentlySortedBySymbol) {
                switch (CurrentSortOrder) {
                    case SortOrder.Descending:
                        symbolList = new List<SymbolIconSymbol>(symbolList.OrderByDescending(o => o.Symbol));
                        if (searchResultList != null) {
                            searchResultList = new List<SymbolIconSymbol>(searchResultList.OrderByDescending(o => o.Symbol));
                        }
                        break;
                    case SortOrder.Ascending:
                    default:
                        symbolList = new List<SymbolIconSymbol>(symbolList.OrderBy(o => o.Symbol));
                        if (searchResultList != null) {
                            searchResultList = new List<SymbolIconSymbol>(searchResultList.OrderBy(o => o.Symbol));
                        }
                        break;
                }
            } else {
                // ignore.
            }
            RaisePropertyChanged("SymbolIconSymbolList");
        }


        // This is NOT considered "search".
        public SymbolIconSymbolWrap GetSymbolWrapForName(string name)
        {
            // Note that we use the global list for this...
            Symbol symbol;
            // var found = searchManager.FindSymbolForName(name, out symbol);
            var found = SymbolIconSearchManager.DefaultInstance.FindSymbolForName(name, out symbol);
            if (found) {
                var isFavorite = FavoriteIconsDataManager.Instance.IsFavorteSymbolIcon(symbol);
                var symbolIconSymbolWrap = new SymbolIconSymbolWrap(new SymbolIconSymbol(symbol, null, isFavorite));
                System.Diagnostics.Debug.WriteLine("GetSymbolWrapForName(). name = {0}; symbolIconSymbolWrap = {1}", name, symbolIconSymbolWrap);
                return symbolIconSymbolWrap;
            } else {
                return null;
            }
        }

        // Note: DoSearchByName() can be called multiple times before EndSearch() is called.
        public bool DoSearchByName(string keyword)
        {
            IList<Symbol> symbols;
            var suc = searchManager.FindSymbolsByName(keyword, out symbols);
            if (suc && symbols.Any()) {
                var list = SymbolIconListManager.BuildSymbolIconSymbolList(symbols);
                this.searchResultList = list;
                this.IsInSearch = true;
                return true;
            } else {
                // Show Toast?
                return false;
            }
        }
        public void EndSearch()
        {
            this.IsInSearch = false;
            this.searchResultList = null;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
