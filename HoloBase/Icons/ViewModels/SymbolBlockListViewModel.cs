﻿using HoloBase.Icons.Common;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.ViewModels
{
    public class SymbolBlockListViewModel : IViewModel, INotifyPropertyChanged
    {
        private IList<SymbolBlockStruct> symbolBlockList = null;
        public SymbolBlockListViewModel()
        {
            var defaultList = BmpSymbolBlocks.List;
            UpdateSymbolBlockList(defaultList);
        }
        private void UpdateSymbolBlockList(IList<SymbolBlockStruct> symbolBlockList)
        {
            this.symbolBlockList = symbolBlockList ?? new List<SymbolBlockStruct>();   // ????
            RaisePropertyChanged("SymbolBlockList");
        }

        public IList<SymbolBlockStruct> SymbolBlockList
        {
            get
            {
                return symbolBlockList;
            }
            set
            {
                UpdateSymbolBlockList(value);
            }
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}