﻿using HoloBase.Icons.Clipboard;
using HoloBase.Icons.Core;
using HoloBase.Icons.Util;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.ViewModels
{
    public class IconClipboardViewModel : IViewModel, INotifyPropertyChanged
    {
        private IList<ISymbol> symbolList;
        public IconClipboardViewModel()
        {
            RefrehSymbolList();
        }
        private void RefrehSymbolList()
        {
            symbolList = IconClipboardManager.Instance.Clipboard.SymbolList;
        }


        public IList<FontFamilyStruct> FontFamilyList
        {
            get
            {
                return FontFamilyRegistry.List;
            }
        }
        public FontFamilyStruct DefaultFontFamily
        {
            get
            {
                return FontFamilyRegistry.FF_SegoeUISymbol;
            }
        }


        public IList<ISymbol> SymbolList
        {
            get
            {
                return symbolList;
            }
            set
            {
                // symbolList = value ?? new List<ISymbol>();
                var newList = value ?? new List<ISymbol>();
                IconClipboardManager.Instance.Clipboard.Clear();
                IconClipboardManager.Instance.Clipboard.PushAll(newList);
                // ...
                RefrehSymbolList();
                RaisePropertyChanged("SymbolList");
                RaisePropertyChanged("Count");
                RaisePropertyChanged("TextString");
                RaisePropertyChanged("IsButtonCopyTextEnabled");
                RaisePropertyChanged("IsButtonPeekSymbolEnabled");
                RaisePropertyChanged("IsButtonUseSymbolEnabled");
                RaisePropertyChanged("IsButtonPopSymbolEnabled");
                RaisePropertyChanged("IsButtonClearListEnabled");
            }
        }

        public int Count
        {
            get
            {
                return IconClipboardManager.Instance.Clipboard.Count;
            }
        }

        public string TextString
        {
            get
            {
                return IconClipboardManager.Instance.Clipboard.ListAsString;
            }
        }


        public void ClearList()
        {
            IconClipboardManager.Instance.Clipboard.Clear();
            RefrehSymbolList();
            RaisePropertyChanged("SymbolList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonPeekSymbolEnabled");
            RaisePropertyChanged("IsButtonUseSymbolEnabled");
            RaisePropertyChanged("IsButtonPopSymbolEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
        }

        public ISymbol LastSymbol
        {
            get
            {
                return IconClipboardManager.Instance.Clipboard.Peek();
            }
        }
        public ISymbol PopSymbol()
        {
            var symbol = IconClipboardManager.Instance.Clipboard.Pop();
            RefrehSymbolList();
            RaisePropertyChanged("SymbolList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonPeekSymbolEnabled");
            RaisePropertyChanged("IsButtonUseSymbolEnabled");
            RaisePropertyChanged("IsButtonPopSymbolEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
            return symbol;
        }
        public void PushSymbol(ISymbol symbol)
        {
            IconClipboardManager.Instance.Clipboard.Push(symbol);
            RefrehSymbolList();
            RaisePropertyChanged("SymbolList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonPeekSymbolEnabled");
            RaisePropertyChanged("IsButtonUseSymbolEnabled");
            RaisePropertyChanged("IsButtonPopSymbolEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
        }


        public bool IsButtonCopyTextEnabled
        {
            get
            {
                return IconClipboardManager.Instance.Clipboard.HasAny;
            }
        }
        public bool IsButtonPeekSymbolEnabled
        {
            get
            {
                return IconClipboardManager.Instance.Clipboard.HasAny;
            }
        }
        public bool IsButtonUseSymbolEnabled
        {
            get
            {
                return IconClipboardManager.Instance.Clipboard.HasAny;
            }
        }
        public bool IsButtonPopSymbolEnabled
        {
            get
            {
                return IconClipboardManager.Instance.Clipboard.HasAny;
            }
        }
        public bool IsButtonClearListEnabled
        {
            get
            {
                return IconClipboardManager.Instance.Clipboard.HasAny;
            }
        }


        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }
        
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
