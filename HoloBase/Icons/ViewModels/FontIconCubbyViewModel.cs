﻿using HoloBase.Collection.Util;
using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.FontIcons;
using HoloBase.Icons.Util;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.ViewModels
{
    public class FontIconCubbyViewModel : IViewModel, INotifyPropertyChanged
    {
        private int maxCount;
        private bool useFavoriteListFirst;
        private FontFamilyStruct fontFamily;
        private SymbolBlockStruct[] symbolBlocks;
        //private IDictionary<int, FontIconSymbol> symbolMap = null;
        //private IList<FontIconSymbol> symbolList = null;
        private IDictionary<int, FontIconSymbolWrap> symbolMap = null;
        private IList<FontIconSymbolWrap> symbolList = null;

        public FontIconCubbyViewModel()
        {
            // temporary
            maxCount = 100;
            useFavoriteListFirst = false;
            fontFamily = FontFamilyRegistry.FF_SegoeUISymbol;
            // symbolBlocks = new SymbolBlockStruct[] { BmpSymbolBlocks.SB_MiscellaneousSymbols };
            // Unfortunately,
            // the random list-generating routines are rather inefficient...
            // the default should use a smaller list....
            symbolBlocks = new SymbolBlockStruct[]{ 
                BmpSymbolBlocks.SB_Arrows,
                BmpSymbolBlocks.SB_BraillePatterns,
                BmpSymbolBlocks.SB_CurrencySymbols,
                BmpSymbolBlocks.SB_Dingbats,
                BmpSymbolBlocks.SB_GeometricShapes,
                BmpSymbolBlocks.SB_MiscellaneousSymbols,
                BmpSymbolBlocks.SB_MiscellaneousTechnical,
                BmpSymbolBlocks.SB_NumberForms
            };

            CreateSymbolList();
        }

        // Note that if UseFavoriteListFirst == true,
        //   then up to the first 50% of the list can be from the user's favorite icons.
        public bool UseFavoriteListFirst
        {
            get
            {
                return useFavoriteListFirst;
            }
            set
            {
                if (useFavoriteListFirst != value) {
                    useFavoriteListFirst = value;
                    CreateSymbolList();
                }
            }
        }

        private void CreateSymbolList()
        {
            // tbd:
            RandomFontIconListBuilder.Instance.MaxCount = maxCount;
            RandomFontIconListBuilder.Instance.FontFamily = fontFamily;
            RandomFontIconListBuilder.Instance.SymbolBlocks = symbolBlocks;
            symbolMap = RandomFontIconListBuilder.Instance.GetRandomFontIconSymbolMap(useFavoriteListFirst);
            //symbolList = new List<FontIconSymbol>(symbolMap.Values);
            symbolList = new List<FontIconSymbolWrap>(symbolMap.Values);
            // --> Do this in Random builder....
            //if (useFavoriteListFirst == true) {
            //    // Now we are randomizing the list.
            //    // Note that we cannot really randomize the map since it's not ordered...
            //    // --> Shuffling may look the list different to the user evey time the listview is loaded (which is undesirable)
            //    symbolList.Shuffle();
            //}
            RaisePropertyChanged("FontIconSymbolMap");
            RaisePropertyChanged("FontIconSymbolList");
            RaisePropertyChanged("FontIconSymbolList1");
            RaisePropertyChanged("FontIconSymbolList2");
            RaisePropertyChanged("FontIconSymbolListA");
            RaisePropertyChanged("FontIconSymbolListB");
            RaisePropertyChanged("FontIconSymbolListC");
            // RaisePropertyChanged("FontIconSymbolPairList");
        }
        private void RebuildSymbolList()
        {
            // symbolList = new List<FontIconSymbol>(symbolMap.Values);
            symbolList = new List<FontIconSymbolWrap>(symbolMap.Values);
            // --> Do this in Random builder....
            //if (useFavoriteListFirst == true) {
            //    // Now we are randomizing the list.
            //    // Note that we cannot really randomize the map since it's not ordered...
            //    // --> Shuffling may look the list different to the user evey time the listview is loaded (which is undesirable)
            //    symbolList.Shuffle();
            //}
            RaisePropertyChanged("FontIconSymbolMap");
            RaisePropertyChanged("FontIconSymbolList");
            RaisePropertyChanged("FontIconSymbolList1");
            RaisePropertyChanged("FontIconSymbolList2");
            RaisePropertyChanged("FontIconSymbolListA");
            RaisePropertyChanged("FontIconSymbolListB");
            RaisePropertyChanged("FontIconSymbolListC");
            // RaisePropertyChanged("FontIconSymbolPairList");
        }

        // public IList<FontIconSymbol> FontIconSymbolList
        public IList<FontIconSymbolWrap> FontIconSymbolList
        {
            get
            {
                // ...
                return symbolList;
                // return new List<FontIconSymbol>(symbolMap.Values);
            }
            //set
            //{
            //    symbolList = value;
            //    // ...
            //}
        }
        // public IDictionary<int, FontIconSymbol> FontIconSymbolMap
        public IDictionary<int, FontIconSymbolWrap> FontIconSymbolMap
        {
            get
            {
                return symbolMap;
            }
            set
            {
                symbolMap = value ?? new Dictionary<int, FontIconSymbolWrap>();   // ???
                RebuildSymbolList();
            }
        }

        // TBD:
        // Dividing odd/even is better for UX...
        public IList<FontIconSymbolWrap> FontIconSymbolList1
        {
            get
            {
                if (symbolList != null) {
                    int mid = (symbolList.Count + 1) / 2;
                    int size = mid;
                    return ((List<FontIconSymbolWrap>) symbolList).GetRange(0, size);
                } else {
                    return null;
                }
            }
        }
        public IList<FontIconSymbolWrap> FontIconSymbolList2
        {
            get
            {
                if (symbolList != null) {
                    int mid = (symbolList.Count + 1) / 2;
                    int size = symbolList.Count - mid;
                    return ((List<FontIconSymbolWrap>) symbolList).GetRange(mid, size);
                } else {
                    return null;
                }
            }
        }

        public IList<FontIconSymbolWrap> FontIconSymbolListA
        {
            get
            {
                if (symbolList != null) {
                    int third1 = (symbolList.Count + 2) / 3;
                    int size = third1;
                    return ((List<FontIconSymbolWrap>) symbolList).GetRange(0, size);
                } else {
                    return null;
                }
            }
        }
        public IList<FontIconSymbolWrap> FontIconSymbolListB
        {
            get
            {
                if (symbolList != null) {
                    int third1 = (symbolList.Count + 2) / 3;
                    int third2 = (symbolList.Count + 1) / 3;
                    int size = third2;
                    return ((List<FontIconSymbolWrap>) symbolList).GetRange(third1, size);
                } else {
                    return null;
                }
            }
        }
        public IList<FontIconSymbolWrap> FontIconSymbolListC
        {
            get
            {
                if (symbolList != null) {
                    int third1 = (symbolList.Count + 2) / 3;
                    int third2 = (symbolList.Count + 1) / 3;
                    int size = symbolList.Count - (third1 + third2);
                    return ((List<FontIconSymbolWrap>) symbolList).GetRange((third1 + third2), size);
                } else {
                    return null;
                }
            }
        }

        // Not being used.
        public IList<Tuple<FontIconSymbolWrap, FontIconSymbolWrap>> FontIconSymbolPairList
        {
            get
            {
                IList<Tuple<FontIconSymbolWrap, FontIconSymbolWrap>> pairList = null;
                if (symbolList != null) {
                    pairList = new List<Tuple<FontIconSymbolWrap, FontIconSymbolWrap>>();
                    // Note that to simplify the logic (not just here, but on UI),
                    //    we truncate the last element if symbolList has an odd number of elements.
                    // This is fine as long as we use this "Cubby" viewmodel for displaying random elements, etc.,
                    //    but, when more precision is needed, we need to use a better algorithm.
                    //    (e.g., how to handle the last null element on the UI, etc.)
                    for (var i = 0; i < symbolList.Count - 1; i += 2) {
                        var pair = new Tuple<FontIconSymbolWrap, FontIconSymbolWrap>(symbolList[i], symbolList[i + 1]);
                        pairList.Add(pair);
                    }
                    // ...
                }
                return pairList;
            }
        }



        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }
        private ColorStruct itemBackgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct ItemBackgroundColor
        {
            get
            {
                return itemBackgroundColor;
            }
            set
            {
                itemBackgroundColor = value;
                // ????
                // tbd: this cannot be efficient....
                if (symbolList != null) {
                    foreach (var s in symbolList) {
                        s.BackgroundColor = itemBackgroundColor;
                    }
                }
                // ????
                RaisePropertyChanged("ItemBackgroundColor");
            }
        }

        public int MaxCount
        {
            get
            {
                return maxCount;
            }
            set
            {
                if (maxCount != value) {
                    maxCount = value;
                    // RandomFontIconListBuilder.Instance.MaxCount = maxCount;
                    CreateSymbolList();
                }
            }
        }

        public FontFamilyStruct FontFamily
        {
            get
            {
                return fontFamily;
            }
            set
            {
                if (value.Name != null && !value.Name.Equals(fontFamily.Name)) {
                    fontFamily = value;
                    // RandomFontIconListBuilder.Instance.FontFamily = fontFamily;
                    CreateSymbolList();
                }
            }
        }
        public SymbolBlockStruct[] SymbolBlocks
        {
            get
            {
                // ...
                return symbolBlocks;
            }
            set
            {
                if (value != null && (symbolBlocks == null || (!RandomFontIconListBuilder.AreTwoSymbolBlockArraysEssentiallyEqual(value, symbolBlocks)))) {
                    symbolBlocks = value;
                    // RandomFontIconListBuilder.Instance.SymbolBlocks = symbolBlocks;
                    CreateSymbolList();
                }
            }
        }



        public void UpdateFavoriteStatus(FontIconSymbol iconSymbol)
        {
            // ????
            var glyph = iconSymbol.Glyph;
            if (symbolMap.ContainsKey(glyph)) {
                var symbol = symbolMap[glyph];
                // symbol = iconSymbol;
                symbol.IsFavorite = iconSymbol.IsFavorite;
                //symbolMap[glyph] = symbol;
                //// symbolList = new List<FontIconSymbol>(symbolMap.Values);
                //symbolList = new List<FontIconSymbolWrap>(symbolMap.Values);

                // ????
                //RaisePropertyChanged("FontIconSymbolMap");
                //RaisePropertyChanged("FontIconSymbolList");
                //RaisePropertyChanged("FontIconSymbolList1");
                //RaisePropertyChanged("FontIconSymbolList2");
                // // RaisePropertyChanged("FontIconSymbolPairList");
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
