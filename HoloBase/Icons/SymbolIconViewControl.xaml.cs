﻿using HoloBase.Icons.Common;
using HoloBase.Icons.UI;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class SymbolIconViewControl : UserControl, IRefreshableElement, IViewModelHolder, IFavoriteStatusUpdaterControl
    {
        // To convey the favorite status change information to the "parent" (list/grid view)
        public event EventHandler<FavoriteStatusChangeEventArgs> FavoriteStatusChanged;

        private SymbolIconSymbolWrap viewModel;

        public SymbolIconViewControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new SymbolIconSymbolWrap();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SymbolIconSymbolWrap viewModel)
        {
            this.viewModel = viewModel;

            // ????
            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SymbolIconSymbolWrap) value;
                // ????

                ResetViewModel(vm);
            }
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Navy);
        public string BackgroundGridColor
        {
            get
            {
                return backgroundColor.ARGB;
            }
            set
            {
                backgroundColor = new ColorStruct(value);
                GridSymbolIconViewControl.Background = new SolidColorBrush(backgroundColor.Color);
            }
        }


        //public Symbol Symbol
        //{
        //    get
        //    {
        //        return viewModel.Symbol;
        //    }
        //    //set
        //    //{
        //    //    viewModel.Symbol = value;
        //    //}
        //}

        public SymbolIconSymbol SymbolIconSymbol
        {
            get
            {
                return ((SymbolIconSymbolWrap) viewModel).SymbolIconSymbol;
            }
            set
            {
                ((SymbolIconSymbolWrap) viewModel).SymbolIconSymbol = value;
            }
        }


        private void GridFavoriteMarker_Tapped(object sender, TappedRoutedEventArgs e)
        {
            // For some reason, this does not work when the view control is used in other control...
            // (viewModel is always invalid.)
            // --> maybe we can use the sender property??
            // In any case, this is currently handled in its parent/container control...

            //var isFavorite = viewModel.IsFavorite;
            //viewModel.IsFavorite = !isFavorite;
            //NotifyFavoriteStatusChange(viewModel.SymbolIconSymbol);
        }

        private void NotifyFavoriteStatusChange(SymbolIconSymbol iconSymbol)
        {
            System.Diagnostics.Debug.WriteLine("NotifyFavoriteStatusChange() iconSymbol = {0}", iconSymbol);

            // temporary
            if (FavoriteStatusChanged != null) {
                var e = new FavoriteStatusChangeEventArgs(this, iconSymbol);
                // FavoriteStatusChanged(this, e);
                FavoriteStatusChanged.Invoke(this, e);
            }
        }

    }
}
