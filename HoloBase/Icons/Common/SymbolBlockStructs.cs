﻿using HoloBase.Icons.Core;
using HoloBase.Icons.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Common
{
    public struct SymbolBlockStruct
    {
        private UnicodeSymbolBlock block;
        //private int minCode;
        //private int maxCode;
        private int[] codeRange;  // size == 2.
        private string name;

        public SymbolBlockStruct(UnicodeSymbolBlock block, int[] codeRange = null, string name = null)
        {
            this.block = block;
            // Validate ???
            this.codeRange = codeRange;
            //if (this.codeRange == null) {
            //    if (SymbolBlockRanges.Map.ContainsKey(this.block)) {
            //        this.codeRange = SymbolBlockRanges.Map[this.block];   // "clone" ???
            //    }
            //}
            this.name = name;
            if (this.name == null) {
                // ???
                this.name = Enum.GetName(typeof(UnicodeSymbolBlock), this.block);
            }
        }

        // TBD:
        // Merge/Union???
        // Create a symbol block from a list of symbol blocks???
        // ....


        public UnicodeSymbolBlock Block
        {
            get
            {
                return block;
            }
            set
            {
                block = value;
            }
        }
        public int[] CodeRange
        {
            get
            {
                return codeRange;
            }
            set
            {
                codeRange = value;
            }
        }
        public int MinCode
        {
            get
            {
                if (codeRange != null && codeRange.Length >= 2) {  // ????
                    return codeRange[0];
                } else {
                    // ???
                    return -1;
                }
            }
        }
        public int MaxCode
        {
            get
            {
                if (codeRange != null && codeRange.Length >= 2) {  // ????
                    return codeRange[1];
                } else {
                    // ???
                    return -1;
                }
            }
        }
        public string Name
        {
            get
            {
                // Can be null.
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string DisplayName
        {
            get
            {
                var sb = new StringBuilder();
                if (!String.IsNullOrEmpty(this.name)) {
                    sb.Append(this.name);
                } else {
                    if (UnicodeSymbolBlocks.IsValidUnicodeSymbolBlock(this.block)) {
                        sb.Append(Enum.GetName(typeof(UnicodeSymbolBlock), this.block));
                    }
                }
                // TBD: Use hex code ???
                if (codeRange != null && codeRange.Length == 2) {
                    sb.Append(String.Format(" ({0} - {1})", codeRange[0], codeRange[1]));
                }
                return sb.ToString();
            }
        }

        // When used as an item in an items control (combobox, etc.)
        public override string ToString()
        {
            if (!String.IsNullOrEmpty(this.Name)) {
                return this.Name;
            } else {
                // ???
                // Could be "invalid"...
                return Enum.GetName(typeof(UnicodeSymbolBlock), this.block);
            }
        }
    }

    public sealed class SymbolBlockWrap
    {
        private SymbolBlockStruct symbolBlockStruct;
        public SymbolBlockWrap(SymbolBlockStruct symbolBlockStruct)
        {
            this.symbolBlockStruct = symbolBlockStruct;
        }

        public SymbolBlockStruct SymbolBlockStruct
        {
            get
            {
                return symbolBlockStruct;
            }
        }

        public UnicodeSymbolBlock Block
        {
            get
            {
                return symbolBlockStruct.Block;
            }
        }
        public int[] CodeRange
        {
            get
            {
                return symbolBlockStruct.CodeRange;
            }
        }
        public int MinCode
        {
            get
            {
                return symbolBlockStruct.MinCode;
            }
        }
        public int MaxCode
        {
            get
            {
                return symbolBlockStruct.MaxCode;
            }
        }
        public string Name
        {
            get
            {
                return symbolBlockStruct.Name;
            }
        }

        public override string ToString()
        {
            return symbolBlockStruct.ToString();
        }
    }


    public static class SymbolBlocks
    {
        public static bool IsCustomRange(SymbolBlockStruct symbolBlock)
        {
            return (symbolBlock.Block == UnicodeSymbolBlock.CustomRange);

        }
        public static bool IsRangeNull(SymbolBlockStruct symbolBlock)
        {
            return (symbolBlock.CodeRange == null 
                || symbolBlock.CodeRange.Length < 2 
                || (symbolBlock.CodeRange[0] == 0 && symbolBlock.CodeRange[1] == 0));
        }

    }

}
