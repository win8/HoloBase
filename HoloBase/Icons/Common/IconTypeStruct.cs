﻿using HoloBase.Icons.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Common
{
    public struct IconTypeStruct
    {
        private IconType type;
        private string name;

        public IconTypeStruct(IconType type, string name = null) 
        {
            this.type = type;
            this.name = name;
        }

	    public IconType Type
	    {
		    get { return type;}
		    set { type = value;}
	    }
	    public string Name
	    {
		    get 
            {
                // if name is null,
                // return Enum.GetName(typeof(IconType), type);  // ???
                return name;
            }
		    set { name = value;}
	    }
	
        public override string ToString()
        {
 	         return this.Name;
        }
    }

    public class IconTypes
    {
        public static readonly IconTypeStruct SYMBOL_ICON = new IconTypeStruct(IconType.SymbolIcon, "Symbol Icon");
        public static readonly IconTypeStruct FONT_ICON = new IconTypeStruct(IconType.FontIcon, "Font Icon");
 
        private static readonly IDictionary<IconType, IconTypeStruct> iconTypes = new Dictionary<IconType, IconTypeStruct>();
        static IconTypes()
        {
            iconTypes.Add(IconType.SymbolIcon, SYMBOL_ICON);
            iconTypes.Add(IconType.FontIcon, FONT_ICON);
        }

        public static bool IsValid(IconType type)
        {
            return (type == IconType.SymbolIcon || type == IconType.FontIcon);
        }

        public static IList<IconType> Types
        {
            get
            {
                return new List<IconType>(iconTypes.Keys);
            }
        }

        public static IList<IconTypeStruct> List
        {
            get
            {
                return new List<IconTypeStruct>(iconTypes.Values);
            }
        }

    }

}
