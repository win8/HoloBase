﻿using HoloBase.Icons.Core;
using HoloBase.Icons.Data;
using HoloBase.Icons.FontIcons;
using HoloBase.Icons.Util;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Common
{
    // Despite the name,
    // this struct can be used for both SymbolIcons and FontIcons.
    public struct FontIconCode : ISymbol
    {
        // temporary
        public static FontIconCode Null = new FontIconCode(null, 0);  // ???

        private string fontFamilyName;
        private int glyph;

        public FontIconCode(string fontFamilyName, int glyph)
        {
            this.fontFamilyName = fontFamilyName;
            this.glyph = glyph;
        }

        public string FontFamily
        {
            get
            {
                return fontFamilyName;
            }
            set
            {
                fontFamilyName = value;
            }
        }
        public int Glyph
        {
            get
            {
                return glyph;
            }
            set
            {
                glyph = value;
            }
        }

        public override string ToString()
        {
            // ???
            return fontFamilyName + " - " + glyph;
        }
    }

    public struct FontIconSymbol : IFontIconSymbol
    {
        // ???
        public static readonly FontIconSymbol Null = new FontIconSymbol(null, -1);

        private string fontFamily;
        // private FontFamilyStruct fontFamily;
        // private char glyph;   // ???
        private int glyph;
        // private int fontSize;
        private string name;   // User provided name ???
        private bool isFavorite;

        public FontIconSymbol(FontIconCode fontIconCode)
            : this(fontIconCode.FontFamily, fontIconCode.Glyph)
        {
        }
        public FontIconSymbol(string fontFamily, int glyph, string name = null, bool isFavorite = false)
        {
            this.fontFamily = fontFamily;
            this.glyph = glyph;
            //if (name != null) {
            //    this.name = name;
            //} else {
            //    // ???
            //    this.name = fontFamily + " - " + glyph;
            //}
            this.name = name;
            this.isFavorite = isFavorite;
        }

        public string FontFamily
        {
            get
            {
                return fontFamily;
            }
            set
            {
                fontFamily = value;
            }
        }
        public int Glyph
        {
            get
            {
                return glyph;
            }
            set
            {
                glyph = value;
            }
        }
        public string GlyphText
        {
            get
            {
                var text = FontIconSymbols.GlyphText(glyph);
                System.Diagnostics.Debug.WriteLine("GlyphText = {0}", text);
                return text;
            }
        }
        public string GlyphCodeHex
        {
            get
            {
                var hexStr = String.Format("0x{0}", Glyph.ToString("X"));
                return hexStr;
            }
        }
        public string GlyphCodeString
        {
            get
            {
                var codeStr = String.Format("&#x{0};", Glyph.ToString("X"));
                return codeStr;
            }
        }

        public string SymbolName
        {
            get
            {
                // ???
                // return fontFamily + " - " + glyph;
                return GlyphCodeHex + " - " + fontFamily;
            }
        }
        public string CustomName
        {
            get
            {
                // ???
                // This could be null.
                return this.name;
            }
        }
        public string Name
        {
            get
            {
                // This has the effect of "unsetting" custom names
                //   by storing null/empty names.
                if (!String.IsNullOrEmpty(name)) {
                    return name;
                } else {
                    // ???
                    return String.Format("({0})", this.SymbolName);
                }
            }
            set
            {
                name = value;
            }
        }

        public bool IsFavorite
        {
            get
            {
                return isFavorite;
            }
            set
            {
                isFavorite = value;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }

    public sealed class FontIconSymbolWrap : IFontIconSymbol, IViewModel, INotifyPropertyChanged
    {
        private FontIconSymbol fontIconSymbol;

        public FontIconSymbolWrap()
            : this(FontIconSymbol.Null)   // ????
        {
        }
        public FontIconSymbolWrap(FontIconCode fontIconCode)
            : this(new FontIconSymbol(fontIconCode))
        {
        }
        public FontIconSymbolWrap(FontIconSymbol fontIconSymbol)
        {
            this.fontIconSymbol = fontIconSymbol;

            // This is done while build a font icon list...
            // Don't do this here again...

            //// tbd:
            //// temporary
            //var isFavorite = FavoriteIconsDataManager.Instance.IsFavorteFontIconGlyph(this.fontIconSymbol.Glyph);
            //if (isFavorite) {
            //    this.fontIconSymbol.IsFavorite = true;
            //}
            //// what about custom name???
            //// temporary
        }

        public IList<string> FontFamilyNameList
        {
            get
            {
                return FontFamilyRegistry.Names;
            }
        }
        public IList<FontFamilyStruct> FontFamilyList
        {
            get
            {
                return FontFamilyRegistry.List;
            }
        }
        //public IList<FontIconSymbol> FontIconSymbolList
        //{
        //    get
        //    {
        //        return FontIconListManager.Instance.BuildFontIconSymbolList(new FontFamilyStruct(this.fontIconSymbol.FontFamily));
        //    }
        //}
        
        
        public FontIconSymbol FontIconSymbol
        {
            get
            {
                return fontIconSymbol;
            }
            set
            {
                fontIconSymbol = value;
                RaisePropertyChanged("FontIconSymbol");
                RaisePropertyChanged("FontFamily");
                RaisePropertyChanged("Glyph");
                RaisePropertyChanged("GlyphText");
                RaisePropertyChanged("GlyphCodeHex");
                RaisePropertyChanged("GlyphCodeString");
                RaisePropertyChanged("SymbolName");
                RaisePropertyChanged("CustomName");
                RaisePropertyChanged("Name");
                RaisePropertyChanged("IsFavorite");
                RaisePropertyChanged("FavoriteMarkerSymbol");
                RaisePropertyChanged("XamlSnippetFontIcon");
                RaisePropertyChanged("XamlSnippetAppBarButton");
                RaisePropertyChanged("XamlSnippetButton");
                RaisePropertyChanged("XamlSnippetTextBlock");
            }
        }

        public string FontFamily
        {
            get
            {
                return FontIconSymbol.FontFamily;
            }
        }
        public int Glyph
        {
            get
            {
                return FontIconSymbol.Glyph;
            }
        }
        public string GlyphText
        {
            get
            {
                return FontIconSymbol.GlyphText;
            }
        }
        public string GlyphCodeHex
        {
            get
            {
                return FontIconSymbol.GlyphCodeHex;
            }
        }
        public string GlyphCodeString
        {
            get
            {
                return FontIconSymbol.GlyphCodeString;
            }
        }

        public string SymbolName
        {
            get
            {
                return FontIconSymbol.SymbolName;
            }
        }
        public string CustomName
        {
            get
            {
                return FontIconSymbol.CustomName;
            }
        }
        public string Name
        {
            get
            {
                return FontIconSymbol.Name;
            }
            set
            {
                var name = value;
                // Note that we store empty name as well.
                // Empty name resets the currently set value, if any.
                var glyphName = new GlyphName(Glyph, name);
                FontIconCustomDataManager.Instance.StoreGlyphName(glyphName);
                fontIconSymbol.Name = name;
                System.Diagnostics.Debug.WriteLine("New name saved, {0}, for glyph = {1}.", name, Glyph);
                RaisePropertyChanged("SymbolName");
                RaisePropertyChanged("CustomName");
                RaisePropertyChanged("Name");
                // tbd...
                RaisePropertyChanged("XamlSnippetAppBarButton");
                RaisePropertyChanged("XamlSnippetButtonWithIcon");
                // ...
            }
        }

        public bool IsFavorite
        {
            get
            {
                return FontIconSymbol.IsFavorite;
            }
            set
            {
                if (FontIconSymbol.IsFavorite != value) {
                    fontIconSymbol.IsFavorite = value;

                    // Update the favorite status in the DB.
                    FavoriteIconsDataManager.Instance.UpdateFavorteFontIconGlyphStatus(Glyph, fontIconSymbol.IsFavorite);

                    RaisePropertyChanged("IsFavorite");
                    RaisePropertyChanged("FavoriteMarkerSymbol");
                }
            }
        }
        public string FavoriteMarkerSymbol
        {
            get
            {
                // temporary
                if (IsFavorite) {
                    //return "Favorite";
                    return "SolidStar";
                } else {
                    return "Add";
                }
            }
        }


        public string XamlSnippetFontIcon
        {
            get
            {
                // temporary
                var snippet = String.Format(@"<FontIcon FontFamily=""{0}"" Glyph=""{1}""/>", FontFamily, GlyphCodeString);
                return snippet;
            }
        }
        public string XamlSnippetAppBarButton
        {
            get
            {
                // temporary
                // var label = !String.IsNullOrEmpty(CustomName) ? WebUtility.UrlEncode(CustomName) : "Button";   // ???
                var label = !String.IsNullOrEmpty(CustomName) ? CustomName : "Button";   // ???
                // var snippet = String.Format(@"<AppBarButton FontFamily=""{0}"" Content=""{1}"" Label=""{2}"" IsCompact=""False""/>", FontFamily, GlyphCodeString, label);
                var snippet = String.Format(@"<AppBarButton Label=""{2}"" IsCompact=""False""><AppBarButton.Icon><FontIcon FontFamily=""{0}"" Glyph=""{1}"" FontSize=""28"" Margin=""0,0,0,3""/></AppBarButton.Icon></AppBarButton>", FontFamily, GlyphCodeString, label);
                return snippet;
            }
        }
        public string XamlSnippetButton
        {
            get
            {
                // temporary
                var snippet = String.Format(@"<Button FontFamily=""{0}"" Content=""{1}""/>", FontFamily, GlyphCodeString);
                return snippet;
            }
        }
        public string XamlSnippetButtonWithIcon
        {
            get
            {
                // temporary
                // var label = !String.IsNullOrEmpty(CustomName) ? WebUtility.UrlEncode(CustomName) : "Button";   // ???
                var label = !String.IsNullOrEmpty(CustomName) ? CustomName : "Button";   // ???
                var snippet = String.Format(@"<Button><StackPanel Orientation=""Horizontal""><FontIcon FontFamily=""{0}"" Glyph=""{1}""/><TextBlock Text=""{2}"" VerticalAlignment=""Center""/></StackPanel></Button>", FontFamily, GlyphCodeString, label);
                return snippet;
            }
        }
        public string XamlSnippetTextBlock
        {
            get
            {
                // temporary
                var snippet = String.Format(@"<TextBlock FontFamily=""{0}"" Text=""{1}""/>", FontFamily, GlyphCodeString);
                return snippet;
            }
        }

        public IList<String> XamlSnippetList
        {
            get
            {
                var list = new List<string>() {
                    this.XamlSnippetFontIcon,
                    this.XamlSnippetAppBarButton,
                    this.XamlSnippetButton,
                    this.XamlSnippetButtonWithIcon,
                    this.XamlSnippetTextBlock
                };
                return list;
            }
        }
        public string XamlSnippetsAsString
        {
            get
            {
                var snippets = XamlSnippetList;
                if (snippets != null && snippets.Any()) {
                    var sb = new StringBuilder();
                    foreach (var x in snippets) {
                        sb.Append(x).Append(Environment.NewLine);
                    }
                    var text = sb.ToString();
                    System.Diagnostics.Debug.WriteLine("XamlSnippetsAsString: text = {0}", text);
                    return text;
                } else {
                    return null;   // ???
                }
            }
        }


        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }


        public override string ToString()
        {
            return fontIconSymbol.ToString();
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }


    public static class FontIconSymbols
    {
        public static string HexCodeForGlyph(int glyph)
        {
            return glyph.ToString("X");
        }
        public static int GlyphFromHexCode(string hexCode)
        {
            int glyph = 0;
            try {
                glyph = Convert.ToInt32(hexCode, 16);
            } catch (Exception ex) {
                // Ignore.
                System.Diagnostics.Debug.WriteLine("GlyphFromHexCode() failed. glyph = {0}, error = {1}", hexCode, ex.Message);
            }
            return glyph;
        }
        public static int GlyphFromString(string strGlyph)
        {
            int glyph = 0;
            try {
                glyph = Convert.ToInt32(strGlyph);
            } catch (Exception ex) {
                // Ignore.
                System.Diagnostics.Debug.WriteLine("GlyphFromString() failed. glyph = {0}, error = {1}", strGlyph, ex.Message);
            }
            return glyph;
        }

        public static char GlyphChar(int glyph)
        {
            var c = (char) 0;
            try {
                // ....
                // c = Convert.ToChar(glyph);
                // Note that this seems much more efficient than Convert.ToChar(),
                //     and it does not throw eception.
                c = (char) glyph;
                // ....
            } catch (Exception) {
                // Ignore.
            }
            // System.Diagnostics.Debug.WriteLine("GlyphChar = {0}", c);
            return c;
        }
        public static string GlyphText(int glyph)
        {
            //return "\u03A3";

            //// temporary
            // var text = String.Format("&#x{0};", glyph.ToString("X"));
            //var text = String.Format(@"\u{0}", glyph.ToString("X"));
            //System.Diagnostics.Debug.WriteLine("GlyphText = {0}", text);
            //// System.Diagnostics.Debug.WriteLine("<FontIcon Grid.Column=\"0\" FontFamily=\"{{Binding FontFamily}}\" Glyph=\"{0}\"/>", text);
            // return text;

            var text = "";
            try {
                // ....
                // char c = Convert.ToChar(glyph);
                // Note that this seems much more efficient than Convert.ToChar(),
                //     and it does not throw eception.
                char c = (char) glyph;
                // ....
                text = c.ToString();
            } catch (Exception) {
                // tbd:
                // if it's a overflow exception (e.e., because the glyph was beyond the utf16 BMP)
                // then return the string value????
                // --> This does not really work.
                //     If Convert.ToChar(0 overflows, then ConvertFromUtf32() also throws argument exception.
                //try {
                //    text = Char.ConvertFromUtf32(glyph);
                //} catch (Exception) {
                //    // Ignore...
                //}
            }
            // System.Diagnostics.Debug.WriteLine("GlyphText = {0}", text);
            return text;
        }
        public static string GlyphTextAsUnicode(int glyph)
        {
            //return "\u03A3";

            // temporary
            var text = String.Format(@"\u{0}", glyph.ToString("X"));
            // System.Diagnostics.Debug.WriteLine("GlyphText = {0}", text);
            // System.Diagnostics.Debug.WriteLine("<FontIcon Grid.Column=\"0\" FontFamily=\"{{Binding FontFamily}}\" Glyph=\"{0}\"/>", text);
            return text;
        }
        public static string GlyphTextForXAML(int glyph)
        {
            //return "&#x03A3;";

            // temporary
            var text = String.Format("&#x{0};", glyph.ToString("X"));
            // System.Diagnostics.Debug.WriteLine("GlyphText = {0}", text);
            // System.Diagnostics.Debug.WriteLine("<FontIcon Grid.Column=\"0\" FontFamily=\"{{Binding FontFamily}}\" Glyph=\"{0}\"/>", text);
            return text;
        }

    }

}
