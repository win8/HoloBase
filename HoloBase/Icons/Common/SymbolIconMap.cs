﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.Common
{
    // FontIcon symbol list
    public class SymbolIconMap
    {
        private readonly IDictionary<Symbol, SymbolIconSymbolWrap> symbolMap;

        public SymbolIconMap()
        {
            symbolMap = new Dictionary<Symbol, SymbolIconSymbolWrap>();
        }

        public void Add(Symbol symbol)
        {
            symbolMap.Add(symbol, new SymbolIconSymbolWrap(SymbolIconSymbols.SymbolMap[symbol]));
        }
        public void Remove(Symbol symbol)
        {
            symbolMap.Remove(symbol);
        }

        public IDictionary<Symbol, SymbolIconSymbolWrap> Map
        {
            get
            {
                return symbolMap;
            }
        }
        public IList<SymbolIconSymbolWrap> List
        {
            get
            {
                return new List<SymbolIconSymbolWrap>(symbolMap.Values);
            }
        }

    }
}
