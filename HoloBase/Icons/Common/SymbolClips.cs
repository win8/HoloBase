﻿using HoloBase.Icons.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Common
{
    public struct SymbolClip : ISymbol
    {
        // temporary
        public static SymbolClip Null = new SymbolClip(IconType.Unknown, null, 0);  // ???

        private IconType type;
        private string fontFamilyName;
        private int glyph;

        public SymbolClip(IconType type, string fontFamilyName, int glyph)
        {
            this.type = type;
            this.fontFamilyName = fontFamilyName;
            this.glyph = glyph;
        }

        public IconType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
        public string FontFamily
        {
            get
            {
                return fontFamilyName;
            }
            set
            {
                fontFamilyName = value;
            }
        }
        public int Glyph
        {
            get
            {
                return glyph;
            }
            set
            {
                glyph = value;
            }
        }

        public override string ToString()
        {
            // ???
            return Enum.GetName(typeof(IconType), Type) + ": " + fontFamilyName + " - " + glyph;
        }
    }

    public class SymbolClipWrap : ISymbol
    {
        private SymbolClip symbolClipStruct;

        public SymbolClipWrap(SymbolClip symbolClipStruct)
        {
            this.symbolClipStruct = symbolClipStruct;
        }
        public SymbolClipWrap(IconType type, string fontFamilyName, int glyph)
            : this(new SymbolClip(type, fontFamilyName, glyph))
        {
        }

        public SymbolClip SymbolClipStruct
        {
            get
            {
                return symbolClipStruct;
            }
        }

        public IconType Type
        {
            get
            {
                return symbolClipStruct.Type;
            }
            set
            {
                symbolClipStruct.Type = value;
            }
        }
        public string FontFamily
        {
            get
            {
                return symbolClipStruct.FontFamily;
            }
            set
            {
                symbolClipStruct.FontFamily = value;
            }
        }
        public int Glyph
        {
            get
            {
                return symbolClipStruct.Glyph;
            }
            set
            {
                symbolClipStruct.Glyph = value;
            }
        }


        public override string ToString()
        {
            return symbolClipStruct.ToString();
        }

    }

    public static class SymbolClips
    {

    }
}
