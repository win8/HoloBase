﻿using HoloBase.Icons.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Common
{
    public struct SymbolSizeStruct
    {
        private FontSymbolSize size;
        private ushort symbolFontSize;
        private ushort labelFontSize;    // ???

        // The ctor should really be internal.
        // --> Use factory methods of SymbolSizes....
        // public SymbolSizeStruct(FontSymbolSize size, ushort symbolFontSize = (ushort) 0U, ushort labelFontSize = (ushort) 0U)
        public SymbolSizeStruct(FontSymbolSize size, ushort symbolFontSize, ushort labelFontSize)
        {
            this.size = size;
            this.symbolFontSize = symbolFontSize;
            this.labelFontSize = labelFontSize;
        }

        public FontSymbolSize Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }

        public ushort SymbolFontSize
        {
            get
            {
                return symbolFontSize;
            }
            set
            {
                symbolFontSize = value;
            }
        }

        // Not being used.
        // Could not figure out how to bind an item/text block font size to this....
        public ushort LabelFontSize
        {
            get
            {
                return labelFontSize;
            }
            set
            {
                labelFontSize = value;
            }
        }

        public override string ToString()
        {
            return Enum.GetName(typeof(FontSymbolSize), this.size);
        }

    }

    public static class SymbolSizes
    {
        public static readonly SymbolSizeStruct SymbolSizeSmall = new SymbolSizeStruct(FontSymbolSize.Small, 25, 15);
        public static readonly SymbolSizeStruct SymbolSizeMedium = new SymbolSizeStruct(FontSymbolSize.Medium, 35, 20);
        public static readonly SymbolSizeStruct SymbolSizeLarge = new SymbolSizeStruct(FontSymbolSize.Large, 55, 30);

        private static readonly IDictionary<FontSymbolSize, SymbolSizeStruct> sizeMap = new Dictionary<FontSymbolSize, SymbolSizeStruct>();
        static SymbolSizes()
        {
            sizeMap.Add(FontSymbolSize.Small, SymbolSizeSmall);
            sizeMap.Add(FontSymbolSize.Medium, SymbolSizeMedium);
            sizeMap.Add(FontSymbolSize.Large, SymbolSizeLarge);
        }

        public static IDictionary<FontSymbolSize, SymbolSizeStruct> Map
        {
            get
            {
                return sizeMap;
            }
        }
        //public static IList<FontSymbolSize> FontSizeList
        //{
        //    get
        //    {
        //        return new List<FontSymbolSize>(sizeMap.Keys);
        //    }
        //}
        public static IList<SymbolSizeStruct> List
        {
            get
            {
                return new List<SymbolSizeStruct>(sizeMap.Values);
            }
        }
    
    }
}
