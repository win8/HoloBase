﻿using HoloBase.Icons.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.Common
{
    // FontIcon code list
    public class FontIconMap
    {
        private readonly IDictionary<FontIconCode, FontIconSymbolWrap> codeMap;

        public FontIconMap()
        {
            codeMap = new Dictionary<FontIconCode, FontIconSymbolWrap>();
        }

        public void Add(FontIconCode code)
        {
            codeMap.Add(code, new FontIconSymbolWrap(new FontIconSymbol(FontFamilyRegistry.Map[code.FontFamily].FontFamilyStruct.Name, code.Glyph)));
        }
        public void Remove(FontIconCode code)
        {
            codeMap.Remove(code);
        }

        public IDictionary<FontIconCode, FontIconSymbolWrap> Map
        {
            get
            {
                return codeMap;
            }
        }
        public IList<FontIconSymbolWrap> List
        {
            get
            {
                return new List<FontIconSymbolWrap>(codeMap.Values);
            }
        }

    }
}
