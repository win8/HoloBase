﻿using HoloBase.Icons.Core;
using HoloBase.Icons.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Common
{
    public static class BmpSymbolBlocks
    {
        public static readonly SymbolBlockStruct SB_GeneralPunctuation = new SymbolBlockStruct(UnicodeSymbolBlock.GeneralPunctuation, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.GeneralPunctuation), "General Punctuation");
        public static readonly SymbolBlockStruct SB_SuperscriptsAndSubscripts = new SymbolBlockStruct(UnicodeSymbolBlock.SuperscriptsAndSubscripts, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.SuperscriptsAndSubscripts), "Superscripts and Subscripts");
        public static readonly SymbolBlockStruct SB_CurrencySymbols = new SymbolBlockStruct(UnicodeSymbolBlock.CurrencySymbols, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.CurrencySymbols), "CurrencySymbols");
        public static readonly SymbolBlockStruct SB_CombiningDiacriticalMarks = new SymbolBlockStruct(UnicodeSymbolBlock.CombiningDiacriticalMarks, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.CombiningDiacriticalMarks), "Combining Diacritical Marks");
        public static readonly SymbolBlockStruct SB_LetterlikeSymbols = new SymbolBlockStruct(UnicodeSymbolBlock.LetterlikeSymbols, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.LetterlikeSymbols), "Letterlike Symbols");
        public static readonly SymbolBlockStruct SB_NumberForms = new SymbolBlockStruct(UnicodeSymbolBlock.NumberForms, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.NumberForms), "Number Forms");
        public static readonly SymbolBlockStruct SB_Arrows = new SymbolBlockStruct(UnicodeSymbolBlock.Arrows, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.Arrows), "Arrows");
        public static readonly SymbolBlockStruct SB_MathematicalOperators = new SymbolBlockStruct(UnicodeSymbolBlock.MathematicalOperators, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.MathematicalOperators), "Mathematical Operators");
        public static readonly SymbolBlockStruct SB_MiscellaneousTechnical = new SymbolBlockStruct(UnicodeSymbolBlock.MiscellaneousTechnical, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.MiscellaneousTechnical), "Miscellaneous Technical");
        public static readonly SymbolBlockStruct SB_ControlPictures = new SymbolBlockStruct(UnicodeSymbolBlock.ControlPictures, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.ControlPictures), "Control Pictures");
        public static readonly SymbolBlockStruct SB_OpticalCharacterRecognition = new SymbolBlockStruct(UnicodeSymbolBlock.OpticalCharacterRecognition, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.OpticalCharacterRecognition), "Optical Character Recognition");
        public static readonly SymbolBlockStruct SB_EnclosedAlphanumerics = new SymbolBlockStruct(UnicodeSymbolBlock.EnclosedAlphanumerics, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.EnclosedAlphanumerics), "Enclosed Alphanumerics");
        public static readonly SymbolBlockStruct SB_BoxDrawing = new SymbolBlockStruct(UnicodeSymbolBlock.BoxDrawing, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.BoxDrawing), "Box Drawing");
        public static readonly SymbolBlockStruct SB_BlockElements = new SymbolBlockStruct(UnicodeSymbolBlock.BlockElements, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.BlockElements), "Block Elements");
        public static readonly SymbolBlockStruct SB_GeometricShapes = new SymbolBlockStruct(UnicodeSymbolBlock.GeometricShapes, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.GeometricShapes), "Geometric Shapes");
        public static readonly SymbolBlockStruct SB_MiscellaneousSymbols = new SymbolBlockStruct(UnicodeSymbolBlock.MiscellaneousSymbols, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.MiscellaneousSymbols), "Miscellaneous Symbols");
        public static readonly SymbolBlockStruct SB_Dingbats = new SymbolBlockStruct(UnicodeSymbolBlock.Dingbats, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.Dingbats), "Dingbats");
        public static readonly SymbolBlockStruct SB_MiscellaneousMathematicalSymbolsA = new SymbolBlockStruct(UnicodeSymbolBlock.MiscellaneousMathematicalSymbolsA, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.MiscellaneousMathematicalSymbolsA), "Miscellaneous Mathematical Symbols-A");
        public static readonly SymbolBlockStruct SB_SupplementalArrowsA = new SymbolBlockStruct(UnicodeSymbolBlock.SupplementalArrowsA, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.SupplementalArrowsA), "Supplemental Arrows-A");
        public static readonly SymbolBlockStruct SB_BraillePatterns = new SymbolBlockStruct(UnicodeSymbolBlock.BraillePatterns, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.BraillePatterns), "Braille Patterns");
        public static readonly SymbolBlockStruct SB_SupplementalArrowsB = new SymbolBlockStruct(UnicodeSymbolBlock.SupplementalArrowsB, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.SupplementalArrowsB), "Supplemental Arrows-B");
        public static readonly SymbolBlockStruct SB_MiscellaneousMathematicalSymbolsB = new SymbolBlockStruct(UnicodeSymbolBlock.MiscellaneousMathematicalSymbolsB, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.MiscellaneousMathematicalSymbolsB), "Miscellaneous Mathematical Symbols-B");
        public static readonly SymbolBlockStruct SB_SupplementalMathematicalOperators = new SymbolBlockStruct(UnicodeSymbolBlock.SupplementalMathematicalOperators, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.SupplementalMathematicalOperators), "Supplemental Mathematical Operators");
        public static readonly SymbolBlockStruct SB_MiscellaneousSymbolsAndArrows = new SymbolBlockStruct(UnicodeSymbolBlock.MiscellaneousSymbolsAndArrows, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.MiscellaneousSymbolsAndArrows), "Miscellaneous Symbols and Arrows");
        // ...
        public static readonly SymbolBlockStruct SB_AllSymbols = new SymbolBlockStruct(UnicodeSymbolBlock.AllSymbols, SymbolBlockRanges.CodeRange(UnicodeSymbolBlock.AllSymbols), "All Symbols (0x2000 - 0x2BFF)");
        public static readonly SymbolBlockStruct SB_DefaultCustomRange = new SymbolBlockStruct(UnicodeSymbolBlock.CustomRange, new int[] { 97, 122 }, "(Custom Range)");   // 'a' through 'z'. Temporary
        public static readonly SymbolBlockStruct SB_NullCustomRange = new SymbolBlockStruct(UnicodeSymbolBlock.CustomRange, null, "(Custom Range)"); 
        // ...

        private static readonly IDictionary<UnicodeSymbolBlock, SymbolBlockStruct> blockMap = new Dictionary<UnicodeSymbolBlock, SymbolBlockStruct>();
        static BmpSymbolBlocks()
        {
            blockMap.Add(UnicodeSymbolBlock.GeneralPunctuation, SB_GeneralPunctuation);
            blockMap.Add(UnicodeSymbolBlock.SuperscriptsAndSubscripts, SB_SuperscriptsAndSubscripts);
            blockMap.Add(UnicodeSymbolBlock.CurrencySymbols, SB_CurrencySymbols);
            blockMap.Add(UnicodeSymbolBlock.CombiningDiacriticalMarks, SB_CombiningDiacriticalMarks);
            blockMap.Add(UnicodeSymbolBlock.LetterlikeSymbols, SB_LetterlikeSymbols);
            blockMap.Add(UnicodeSymbolBlock.NumberForms, SB_NumberForms);
            blockMap.Add(UnicodeSymbolBlock.Arrows, SB_Arrows);
            blockMap.Add(UnicodeSymbolBlock.MathematicalOperators, SB_MathematicalOperators);
            blockMap.Add(UnicodeSymbolBlock.MiscellaneousTechnical, SB_MiscellaneousTechnical);
            blockMap.Add(UnicodeSymbolBlock.ControlPictures, SB_ControlPictures);
            blockMap.Add(UnicodeSymbolBlock.OpticalCharacterRecognition, SB_OpticalCharacterRecognition);
            blockMap.Add(UnicodeSymbolBlock.EnclosedAlphanumerics, SB_EnclosedAlphanumerics);
            blockMap.Add(UnicodeSymbolBlock.BoxDrawing, SB_BoxDrawing);
            blockMap.Add(UnicodeSymbolBlock.BlockElements, SB_BlockElements);
            blockMap.Add(UnicodeSymbolBlock.GeometricShapes, SB_GeometricShapes);
            blockMap.Add(UnicodeSymbolBlock.MiscellaneousSymbols, SB_MiscellaneousSymbols);
            blockMap.Add(UnicodeSymbolBlock.Dingbats, SB_Dingbats);
            blockMap.Add(UnicodeSymbolBlock.MiscellaneousMathematicalSymbolsA, SB_MiscellaneousMathematicalSymbolsA);
            blockMap.Add(UnicodeSymbolBlock.SupplementalArrowsA, SB_SupplementalArrowsA);
            blockMap.Add(UnicodeSymbolBlock.BraillePatterns, SB_BraillePatterns);
            blockMap.Add(UnicodeSymbolBlock.SupplementalArrowsB, SB_SupplementalArrowsB);
            blockMap.Add(UnicodeSymbolBlock.MiscellaneousMathematicalSymbolsB, SB_MiscellaneousMathematicalSymbolsB);
            blockMap.Add(UnicodeSymbolBlock.SupplementalMathematicalOperators, SB_SupplementalMathematicalOperators);
            blockMap.Add(UnicodeSymbolBlock.MiscellaneousSymbolsAndArrows, SB_MiscellaneousSymbolsAndArrows);
            // ...
            blockMap.Add(UnicodeSymbolBlock.AllSymbols, SB_AllSymbols);
            // blockMap.Add(UnicodeSymbolBlock.CustomRange, SB_DefaultCustomRange);
            blockMap.Add(UnicodeSymbolBlock.CustomRange, SB_NullCustomRange);
            // ...
        }

        public static IDictionary<UnicodeSymbolBlock, SymbolBlockStruct> Map
        {
            get
            {
                return blockMap;
            }
        }

        public static IList<SymbolBlockStruct> List
        {
            get
            {
                return new List<SymbolBlockStruct>(blockMap.Values);
            }
        }

    }
}
