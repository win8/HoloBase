﻿using HoloBase.Icons.Core;
using HoloBase.Icons.Data;
using HoloBase.Icons.SymbolIcons;
using HoloBase.Icons.Util;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.Common
{
    public struct SymbolIconSymbol : ISymbolIconSymbol
    {
        // ????
        public static readonly SymbolIconSymbol Clear = new SymbolIconSymbol(Symbol.Clear);   // ???

        private Symbol symbol;
        // Just use symbol.GetName() ????
        // (Potentially, in the future, the name can be overwritten by the user)....
        // currently, Name == SymbolName
        private string name;
        private bool isFavorite;

        public SymbolIconSymbol(Symbol symbol, string name = null, bool isFavorite = false)
        {
            this.symbol = symbol;
            if (name != null) {
                this.name = name;
            } else {
                this.name = Enum.GetName(typeof(Symbol), this.symbol);  // default value.
            }
            this.isFavorite = isFavorite;
        }

        public Symbol Symbol
        {
            get
            {
                return symbol;
            }
            //set
            //{
            //    symbol = value;
            //    // ???
            //}
        }
        public int SymbolCode
        {
            get
            {
                // ???
                return (int) symbol;
            }
        }
        public string SymbolCodeHex
        {
            get
            {
                var hexStr = String.Format("0x{0}", SymbolCode.ToString("X"));
                return hexStr;
            }
        }
        public string SymbolGlyphText
        {
            get
            {
                var text = SymbolIconSymbols.SymbolGlyphText(symbol);
                System.Diagnostics.Debug.WriteLine("SymbolGlyphText = {0}", text);
                return text;
            }
        }

        public string SymbolName
        {
            get
            {
                return Enum.GetName(typeof(Symbol), this.symbol);
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set  // ????
            {
                name = value;
            }
        }

        public string FontFamily
        {
            get
            {
                return FontFamilyRegistry.FF_SegoeUISymbol.Name;
            }
        }
        public int Glyph
        {
            get
            {
                // ????
                return SymbolCode;
            }
        }
        public bool IsFavorite
        {
            get
            {
                return isFavorite;
            }
            set
            {
                isFavorite = value;
            }
        }
        

        public override string ToString()
        {
            return Name;
        }
        
    }

    public sealed class SymbolIconSymbolWrap : ISymbolIconSymbol, IViewModel, INotifyPropertyChanged
    {
        private SymbolIconSymbol symbolIconSymbol;

        public SymbolIconSymbolWrap()
            : this(SymbolIconSymbol.Clear)    // ???
        {
        }
        public SymbolIconSymbolWrap(Symbol symbol)
            : this(new SymbolIconSymbol(symbol))
        {
        }
        public SymbolIconSymbolWrap(SymbolIconSymbol symbolIconSymbol)
        {
            this.symbolIconSymbol = symbolIconSymbol;

            // This is done while building a list.
            // Don't do it here...

            //// temporary
            //var isFavorite = FavoriteIconsDataManager.Instance.IsFavorteSymbolIcon(this.symbolIconSymbol.Symbol);
            //if (isFavorite) {
            //    this.symbolIconSymbol.IsFavorite = true;
            //}
            //// temporary
        }

        public IList<Symbol> SymbolList
        {
            get
            {
                return new List<Symbol>((Symbol[]) Enum.GetValues(typeof(Symbol)));
            }
        }
        public IList<string> SymbolNameList
        {
            get
            {
                return new List<string>((string[]) Enum.GetNames(typeof(Symbol)));
            }
        }
        //public IList<SymbolIconSymbol> SymbolIconSymbolList
        //{
        //    get
        //    {
        //        return SymbolIconListManager.Instance.SymbolList;
        //    }
        //}


        public SymbolIconSymbol SymbolIconSymbol
        {
            get
            {
                return symbolIconSymbol;
            }
            set
            {
                symbolIconSymbol = value;
                RaisePropertyChanged("SymbolIconSymbol");
                RaisePropertyChanged("Symbol");
                RaisePropertyChanged("SymbolCode");
                RaisePropertyChanged("SymbolCodeHex");
                RaisePropertyChanged("SymbolName");
                RaisePropertyChanged("Name");
                RaisePropertyChanged("IsFavorite");
                RaisePropertyChanged("FavoriteMarkerSymbol");
                RaisePropertyChanged("XamlSnippetForSymbolIcon");
                RaisePropertyChanged("XamlSnippetForAppBarButton");
                RaisePropertyChanged("XamlSnippetForButton");
            }
        }

        public Symbol Symbol
        {
            get
            {
                return SymbolIconSymbol.Symbol;
            }
            //set
            //{
            //    // ?????
            //    symbolIconSymbol.Symbol = value;
            //    RaisePropertyChanged("SymbolIconSymbol");
            //    RaisePropertyChanged("Symbol");
            //    RaisePropertyChanged("SymbolCode");
            //    RaisePropertyChanged("SymbolCodeHex");
            //    RaisePropertyChanged("SymbolName");
            //    RaisePropertyChanged("Name");
            //    RaisePropertyChanged("IsFavorite");
            //    RaisePropertyChanged("FavoriteMarkerSymbol");
            //    RaisePropertyChanged("XamlSnippetForSymbolIcon");
            //    RaisePropertyChanged("XamlSnippetForAppBarButton");
            //}
        }
        public int SymbolCode
        {
            get
            {
                return SymbolIconSymbol.SymbolCode;
            }
        }
        public string SymbolCodeHex
        {
            get
            {
                return SymbolIconSymbol.SymbolCodeHex;
            }
        }
        public string SymbolGlyphText
        {
            get
            {
                return SymbolIconSymbol.SymbolGlyphText;
            }
        }

        public string SymbolName
        {
            get
            {
                return SymbolIconSymbol.SymbolName;
            }
        }
        public string Name
        {
            get
            {
                return SymbolIconSymbol.Name;
            }
        }

        public string FontFamily
        {
            get
            {
                return SymbolIconSymbol.FontFamily;
            }
        }
        public int Glyph
        {
            get
            {
                return SymbolIconSymbol.Glyph;
            }
        }
        public bool IsFavorite
        {
            get
            {
                return SymbolIconSymbol.IsFavorite;
            }
            set
            {
                if (SymbolIconSymbol.IsFavorite != value) {
                    symbolIconSymbol.IsFavorite = value;

                    // Update the favorite status in the DB.
                    FavoriteIconsDataManager.Instance.UpdateFavorteSymbolIconStatus(Symbol, symbolIconSymbol.IsFavorite);

                    RaisePropertyChanged("IsFavorite");
                    RaisePropertyChanged("FavoriteMarkerSymbol");
                }
            }
        }
        public string FavoriteMarkerSymbol
        {
            get
            {
                // temporary
                if (IsFavorite) {
                    //return "Favorite";
                    return "SolidStar";
                } else {
                    return "Add";
                }
            }
        }


        public string XamlSnippetForSymbolIcon
        {
            get
            {
                // temporary
                var snippet = String.Format(@"<SymbolIcon Symbol=""{0}""/>", SymbolName);
                return snippet;
            }
        }
        public string XamlSnippetForAppBarButton
        {
            get
            {
                // temporary
                var snippet = String.Format(@"<AppBarButton Icon=""{0}"" Label=""{0}"" IsCompact=""False""/>", SymbolName);
                return snippet;
            }
        }
        public string XamlSnippetForButton
        {
            get
            {
                // temporary
                var snippet = String.Format(@"<Button MinWidth=""55""><Button.Content><SymbolIcon Symbol=""{0}""/></Button.Content></Button>", SymbolName);
                return snippet;
            }
        }

        public IList<String> XamlSnippetList
        {
            get
            {
                var list = new List<string>() {
                    this.XamlSnippetForSymbolIcon,
                    this.XamlSnippetForAppBarButton,
                    this.XamlSnippetForButton
                };
                return list;
            }
        }
        public string XamlSnippetsAsString
        {
            get
            {
                var snippets = XamlSnippetList;
                if (snippets != null && snippets.Any()) {
                    var sb = new StringBuilder();
                    foreach (var x in snippets) {
                        sb.Append(x).Append(Environment.NewLine);
                    }
                    var text = sb.ToString();
                    System.Diagnostics.Debug.WriteLine("XamlSnippetsAsString: text = {0}", text);
                    return text;
                } else {
                    return null;   // ???
                }
            }
        }


        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }


        public override string ToString()
        {
            return symbolIconSymbol.ToString();
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }


    public static class SymbolIconSymbols
    {
        private static readonly IDictionary<Symbol, SymbolIconSymbol> symbolMap = new Dictionary<Symbol, SymbolIconSymbol>();

        static SymbolIconSymbols()
        {
            foreach(Symbol s in Enum.GetValues(typeof(Symbol))) {
                symbolMap.Add(s, new SymbolIconSymbol(s));
            }
        }

        public static IDictionary<Symbol, SymbolIconSymbol> SymbolMap
        {
            get
            {
                return symbolMap;
            }
        }
        public static IList<SymbolIconSymbol> SymbolList
        {
            get
            {
                return new List<SymbolIconSymbol>(symbolMap.Values);
            }
        }
        public static IList<Symbol> AllSymbols
        {
            get
            {
                return new List<Symbol>(symbolMap.Keys);
            }
        }


        public static char SymbolGlyphChar(Symbol symbol)
        {
            var c = (char) 0;
            try {
                // ....
                // c = Convert.ToChar((int) symbol);
                // Note that this seems much more efficient than Convert.ToChar(),
                //     and it does not throw eception.
                // c = (char) ((int) symbol);  // ????
                c = (char) symbol;
                // ....
            } catch (Exception) {
                // Ignore.
            }
            // System.Diagnostics.Debug.WriteLine("SymbolGlyphChar = {0}", c);
            return c;
        }
        public static string SymbolGlyphText(Symbol symbol)
        {
            var text = "";
            try {
                // ....
                // char c = Convert.ToChar((int) symbol);
                // Note that this seems much more efficient than Convert.ToChar(),
                //     and it does not throw eception.
                // char c = (char) ((int) symbol);  // ????
                char c = (char) symbol;
                // ....
                text = c.ToString();
            } catch (Exception) {
                // Ignore.
            }
            // System.Diagnostics.Debug.WriteLine("SymbolGlyphText = {0}", text);
            return text;
        }
        public static string SymbolGlyphTextAsUnicode(Symbol symbol)
        {
            //return "\u03A3";

            // temporary
            var text = String.Format(@"\u{0}", ((int) symbol).ToString("X"));
            // System.Diagnostics.Debug.WriteLine("GlyphText = {0}", text);
            return text;
        }
        public static string SymbolGlyphTextForXAML(Symbol symbol)
        {
            //return "&#x03A3;";

            // temporary
            var text = String.Format("&#x{0};", ((int) symbol).ToString("X"));
            // System.Diagnostics.Debug.WriteLine("GlyphText = {0}", text);
            return text;
        }


    }

}
