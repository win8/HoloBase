﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Common
{
    public class GlyphName
    {
        // FontFamily indendepnt????
        // private string fontFamily;
        private int glyph;
        private string name;
        private string comment;   // note to self? description?
        private long created;
        private long updated;

        public GlyphName(int glyph, string name = null, string comment = null)
        {
            this.glyph = glyph;
            this.name = name;
            this.comment = comment;
            this.created = 0L;
            this.updated = 0L;
        }

        public int Glyph
        {
            get
            {
                return glyph;
            }
            set
            {
                glyph = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                comment = value;
            }
        }
        public long Created
        {
            get
            {
                return created;
            }
            set
            {
                created = value;
            }
        }
        public long Updated
        {
            get
            {
                return updated;
            }
            set
            {
                updated = value;
            }
        }

        //public override string ToString()
        //{
        //    // tbd.
        //    return "{" + this.glyph + "," + this.Name + "}";
        //}
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Glyph:").Append(Glyph).Append("; ");
            sb.Append("Name:").Append(Name).Append("; ");
            sb.Append("Comment:").Append(Comment).Append("; ");
            sb.Append("Created: ").Append(Created).Append("; ");
            sb.Append("Updated: ").Append(Updated);
            return sb.ToString();
        }

    }

    public static class GlyphNames
    {
    }

}

