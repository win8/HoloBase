﻿using HoloBase.Icons.Common;
using HoloBase.Icons.ViewModels;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class SymbolIconViewGridControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private SymbolIconCubbyViewModel viewModel;

        public SymbolIconViewGridControl()
        {
            this.InitializeComponent();

            ResetViewModel();
            this.SizeChanged += SymbolIconViewGridControl_SizeChanged;
        }
        void SymbolIconViewGridControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var newSize = e.NewSize;
            System.Diagnostics.Debug.WriteLine("SymbolIconViewGridControl_SizeChanged() newSize = {0}", newSize);

            var visualStateName = "VisualStateNormal";   // landscape
            //if (newSize.Height > 640 || newSize.Height > newSize.Width - 60) {   // 640/60 arbitrary
            //    visualStateName = "VisualStateTallerThanWide";   // portrait
            //}
            // 640/960 based on 300x280 icon view size.
            if (newSize.Height > 960) {
                visualStateName = "VisualStateVeryTall";
            } else if (newSize.Height > 640) {
                visualStateName = "VisualStateTallerThanWide";
            }
            // System.Diagnostics.Debug.WriteLine("visualStateName = {0}", visualStateName);

            //// testing...
            //visualStateName = "VisualStateTallerThanWide";
            //// testing...

            var suc = VisualStateManager.GoToState(this, visualStateName, false);
            System.Diagnostics.Debug.WriteLine("GoToState(): {0}. Succeeded = {1}", visualStateName, suc);
        }

        private void ResetViewModel()
        {
            var vm = new SymbolIconCubbyViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SymbolIconCubbyViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SymbolIconCubbyViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }


        public bool UseFavoriteListFirst
        {
            get
            {
                return viewModel.UseFavoriteListFirst;
            }
            set
            {
                viewModel.UseFavoriteListFirst = value;
            }
        }
    
        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }
        public string ItemBackgroundColor
        {
            get
            {
                return viewModel.ItemBackgroundColor.ARGB;
            }
            set
            {
                viewModel.ItemBackgroundColor = new ColorStruct(value);
            }
        }


        public int MaxSymbolCount
        {
            get
            {
                return viewModel.MaxCount;
            }
            set
            {
                viewModel.MaxCount = value;
            }
        }


        private void GridViewSymbolList_ItemClick(object sender, ItemClickEventArgs e)
        {
            // var clickedItem = (SymbolIconSymbol) e.ClickedItem;
            var clickedItem = (SymbolIconSymbolWrap) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("Symbol selected: {0}", clickedItem);
            // ...

            var isFavorite = clickedItem.IsFavorite;
            clickedItem.IsFavorite = !isFavorite;
            System.Diagnostics.Debug.WriteLine("Favorite status updated: symbol = {0}, isFavorite = {1}", clickedItem.Symbol, clickedItem.IsFavorite);

            viewModel.UpdateFavoriteStatus(clickedItem.SymbolIconSymbol);

        }

    }
}
