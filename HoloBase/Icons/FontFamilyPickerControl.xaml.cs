﻿using HoloBase.Icons.Core;
using HoloBase.Icons.UI;
using HoloBase.Icons.ViewModels;
using HoloBase.UI.Colors;
using HoloBase.UI.Util;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class FontFamilyPickerControl : UserControl, IRefreshableElement, IViewModelHolder, IFontFamilyPickerControl
    {
        public event EventHandler<FontFamilyPickerEventArgs> FontFamilySelectionChanged;

        private FontFamilyListViewModel viewModel;

        public FontFamilyPickerControl()
        {
            this.InitializeComponent();
            ResetViewModel();

            isCancelButtonVisible = true;
            RefreshElementVisibility();
        }
        private void ResetViewModel()
        {
            var vm = new FontFamilyListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(FontFamilyListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (FontFamilyListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }

        private bool isCancelButtonVisible;
        public bool IsCancelButtonVisible
        {
            get
            {
                return isCancelButtonVisible;
            }
            set
            {
                isCancelButtonVisible = value;
                RefreshElementVisibility();
            }
        }
        private void RefreshElementVisibility()
        {
            if (isCancelButtonVisible) {
                StackPanelBottomRow.Visibility = Visibility.Visible;
            } else {
                StackPanelBottomRow.Visibility = Visibility.Collapsed;
            }
        }


        private void NotifyFontFamilySelectionChange(FontFamilyStruct fontFamily)
        {
            // temporary
            if (FontFamilySelectionChanged != null) {
                var e = new FontFamilyPickerEventArgs(this, fontFamily);
                // FontFamilySelectionChanged(this, e);
                FontFamilySelectionChanged.Invoke(this, e);
            }
        }


        private void DismissPopup()
        {
            // tbd:
            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DismissPopup();
        }

        private void ListViewFontFamilyList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = (FontFamilyStruct) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("FontFamily selected: {0}", clickedItem);
            NotifyFontFamilySelectionChange(clickedItem);

            DismissPopup();
        }

    }
}
