﻿using HoloBase.Icons.UI;
using HoloCore.Core;
using HoloBase.UI.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HoloBase.Icons.ViewModels;
using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.UI.Colors;
using HoloBase.Toast;
using HoloBase.Controls.Core;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class FontIconListControl : UserControl, IRefreshableElement, IViewModelHolder, IFontIconPickerControl, IFontIconListingControl  // , ITextClipSourceControl
    {
        // Public event for the symbol picker clients.
        public event EventHandler<FontIconPickerEventArgs> FontIconSymbolSelectionChanged;
        // for relaying xaml snippet to its container/parent.
        public event EventHandler<TextClipEventArgs> TextClipAvailable;

        // temporary
        private Size currentControlSize = Size.Empty;

        private FontIconListViewModel viewModel;
        private bool isSymbolInfoPopupEnabed;
        private Popup symbolInfoPopup;
        private string visualStateName;    // tbd: use enum???

        // For visibility of optional top header elements/controls.
        private bool isTopHeaderBoxVisible;
        private bool isGridFindForGlyphVisible;
        private bool isGridFindByNameVisible;
        private bool isGridFilterByRangeVisible;
        private bool isListSelectionHeaderVisible;
        private bool isSymbolSizeComboBoxVisible;
        private bool isSortButtonsVisible;

        public FontIconListControl()
        {
            this.InitializeComponent();

            ResetViewModel();

            isSymbolInfoPopupEnabed = false;
            symbolInfoPopup = BuildSymbolInfoPopup();

            isTopHeaderBoxVisible = true;   // this should always be true.
            isGridFindForGlyphVisible = false;
            isGridFindByNameVisible = false;
            isGridFilterByRangeVisible = false;
            isListSelectionHeaderVisible = false;
            isSymbolSizeComboBoxVisible = false;   // false by default.
            isSortButtonsVisible = true;
            // No need for this when in singleInput mode...
            AppBarButtonHideAll.Visibility = Visibility.Collapsed;
            // ...
            RefreshElementVisibility();

            visualStateName = "VisualStateNormal";
            this.SizeChanged += FontIconListControl_SizeChanged;
        }
        void FontIconListControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            currentControlSize = e.NewSize;
            System.Diagnostics.Debug.WriteLine("FontIconGridControl_SizeChanged() currentControlSize = {0}", currentControlSize);

            if (currentControlSize.Width > 560) {
                IsCompactForAppBarButtons = false;
            } else {
                IsCompactForAppBarButtons = true;
            }

            if (currentControlSize.Width > 480) {               // 480 Arbitrary
                visualStateName = "VisualStateMediumAndWide";   // the rest.
            } else {
                visualStateName = "VisualStateNormal";          // Phone/Portrait
            }
            // System.Diagnostics.Debug.WriteLine("visualStateName = {0}", visualStateName);

            var suc = VisualStateManager.GoToState(this, visualStateName, false);
            System.Diagnostics.Debug.WriteLine("GoToState(): {0}. Succeeded = {1}", visualStateName, suc);
        }
        private void ResetViewModel()
        {
            var vm = new FontIconListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(FontIconListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;


            //// tbd:
            //ListViewSymbolList.Items.Add(new FontIconSymbol(viewModel.FontFamily, 0x3a3));
            //ListViewSymbolList.Items.Add(new FontIconSymbol(viewModel.FontFamily, 0x3a4));
            //ListViewSymbolList.Items.Add(new FontIconSymbol(viewModel.FontFamily, 0x3a5));


        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        private Popup BuildSymbolInfoPopup()
        {
            // tbd...
            var popup = new Popup();
            var infoControl = new FontIconSymbolInfoControl();
            infoControl.GlyphNameChanged += fontIconSymbolInfoControl_GlyphNameChanged;
            infoControl.FavoriteStatusChanged += infoControl_FavoriteStatusChanged;
            infoControl.TextClipAvailable += infoControl_TextClipAvailable;
            popup.Child = infoControl;
            return popup;
        }
        private void fontIconSymbolInfoControl_GlyphNameChanged(object sender, GlyphNameUpdateEventArgs e)
        {
            var updatedGlyphName = e.UpdatedGlyphName;
            // viewModel.ResetFontIconSymbolList();
            viewModel.UpdateGlyphName(updatedGlyphName);
        }
        private void infoControl_FavoriteStatusChanged(object sender, FavoriteStatusChangeEventArgs e)
        {
            var iconSymbol = e.IconSymbol;
            viewModel.UpdateFavoriteStatus((FontIconSymbol) iconSymbol);
        }
        void infoControl_TextClipAvailable(object sender, TextClipEventArgs e)
        {
            // Just "relay"
            var text = e.TextClip;
            NotifyTextClipAvailable(text);
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (FontIconListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }


        public bool IsSymbolInfoPopupEnabed
        {
            get
            {
                return isSymbolInfoPopupEnabed;
            }
            set
            {
                isSymbolInfoPopupEnabed = value;
            }
        }
        public bool IsSymbolInfoPopupOpen
        {
            get
            {
                return (symbolInfoPopup != null && symbolInfoPopup.IsOpen);
            }
            set
            {
                if (symbolInfoPopup != null) {
                    symbolInfoPopup.IsOpen = value;
                }
            }
        }
        public FontIconSymbolWrap CurrentSymbolInfo
        {
            get
            {
                if (symbolInfoPopup != null && symbolInfoPopup.IsOpen) {
                    var infoControl = symbolInfoPopup.Child as FontIconSymbolInfoControl;
                    if (infoControl != null) {
                        return infoControl.FontIconSymbolInfo;
                    }
                }
                return null;
            }
        }


        public bool UseFavoriteList
        {
            get
            {
                return viewModel.UseFavoriteList;
            }
            set
            {
                viewModel.UseFavoriteList = value;
            }
        }

        public SymbolSizeStruct SymbolFontSize
        {
            get
            {
                return viewModel.SymbolSize;
            }
            set
            {
                viewModel.SymbolSize = value;
            }
        }

        public bool IsAppBarToggleButtonFilterEnabled
        {
            get
            {
                return AppBarToggleButtonFilter.IsEnabled;
            }
            set
            {
                AppBarToggleButtonFilter.IsEnabled = value;
            }
        }

        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }
        public string PopupBackgroundColor
        {
            get
            {
                var infoControl = symbolInfoPopup.Child as FontIconSymbolInfoControl;
                if (infoControl != null) {
                    return infoControl.BackgroundGridColor;
                } else {
                    // ????
                    return "Navy";
                }
            }
            set
            {
                var infoControl = symbolInfoPopup.Child as FontIconSymbolInfoControl;
                if (infoControl != null) {
                    infoControl.BackgroundGridColor = value;
                } else {
                    // ???
                }
            }
        }

        public string VisibilityForAppBarButtons
        {
            get
            {
                return viewModel.VisibilityForAppBarButtons;
            }
            set
            {
                viewModel.VisibilityForAppBarButtons = value;
            }
        }
        public bool IsCompactForAppBarButtons
        {
            get
            {
                return (viewModel != null) ? viewModel.IsCompactForAppBarButtons : false;
            }
            set
            {
                if (viewModel != null) {
                    viewModel.IsCompactForAppBarButtons = value;
                }
            }
        }


        public bool IsTopHeaderBoxVisible
        {
            get
            {
                return isTopHeaderBoxVisible;
            }
            private set
            {
                isTopHeaderBoxVisible = value;
                RefreshElementVisibility();
            }
        }
        public bool IsGridFindForGlyphVisible
        {
            get
            {
                return isGridFindForGlyphVisible;
            }
            set
            {
                isGridFindForGlyphVisible = value;
                RefreshElementVisibility();
            }
        }
        public bool IsGridFindByNameVisible
        {
            get
            {
                return isGridFindByNameVisible;
            }
            set
            {
                isGridFindByNameVisible = value;
                RefreshElementVisibility();
            }
        }
        public bool IsGridFilterByRangeVisible
        {
            get
            {
                return isGridFilterByRangeVisible;
            }
            set
            {
                isGridFilterByRangeVisible = value;
                RefreshElementVisibility();
            }
        }
        public bool IsListSelectionHeaderVisible
        {
            get
            {
                return isListSelectionHeaderVisible;
            }
            set
            {
                isListSelectionHeaderVisible = value;
                RefreshElementVisibility();
            }
        }
        public bool IsSymbolSizeComboBoxVisible
        {
            get
            {
                return isSymbolSizeComboBoxVisible;
            }
            set
            {
                isSymbolSizeComboBoxVisible = value;
                RefreshElementVisibility();
            }
        }
        public bool IsSortButtonsVisible
        {
            get
            {
                return isSortButtonsVisible;
            }
            set
            {
                isSortButtonsVisible = value;
                RefreshElementVisibility();
            }
        }

        private void RefreshElementVisibility()
        {
            if (isTopHeaderBoxVisible) {
                StackPanelTopRow.Visibility = Visibility.Visible;
                if (isGridFindForGlyphVisible) {
                    GridFindForGlyph.Visibility = Visibility.Visible;
                } else {
                    GridFindForGlyph.Visibility = Visibility.Collapsed;
                }
                AppBarToggleButtonView.IsChecked = isGridFindForGlyphVisible;

                if (isGridFindByNameVisible) {
                    GridFindByName.Visibility = Visibility.Visible;
                } else {
                    GridFindByName.Visibility = Visibility.Collapsed;
                }
                AppBarToggleButtonFind.IsChecked = isGridFindByNameVisible;

                if (isGridFilterByRangeVisible) {
                    GridFilterByRange.Visibility = Visibility.Visible;
                } else {
                    GridFilterByRange.Visibility = Visibility.Collapsed;
                }
                AppBarToggleButtonRange.IsChecked = isGridFilterByRangeVisible;

                if (isListSelectionHeaderVisible) {
                    StackPanelListSelectionHeader.Visibility = Visibility.Visible;
                } else {
                    StackPanelListSelectionHeader.Visibility = Visibility.Collapsed;
                }
                AppBarToggleButtonFilter.IsChecked = isListSelectionHeaderVisible;

                if (isSymbolSizeComboBoxVisible) {
                    GridSymbolSizeComboBox.Visibility = Visibility.Visible;
                } else {
                    GridSymbolSizeComboBox.Visibility = Visibility.Collapsed;
                }

                if (isSortButtonsVisible) {
                    GridSortButtons.Visibility = Visibility.Visible;
                } else {
                    GridSortButtons.Visibility = Visibility.Collapsed;
                }
            } else {
                StackPanelTopRow.Visibility = Visibility.Collapsed;
                // Visibility of the child elemetns doesn't matter.
                // App bar toggle buttons???
            }
        }



        private void HideAllInputGrids()
        {
            isGridFindForGlyphVisible = false;
            isGridFindByNameVisible = false;
            isGridFilterByRangeVisible = false;
            isListSelectionHeaderVisible = false;
            RefreshElementVisibility();
        }
        private void ShowGridFindForGlyphOnly()
        {
            isGridFindForGlyphVisible = true;
            isGridFindByNameVisible = false;
            isGridFilterByRangeVisible = false;
            isListSelectionHeaderVisible = false;
            RefreshElementVisibility();
        }
        private void ShowGridFindByNameOnly()
        {
            isGridFindForGlyphVisible = false;
            isGridFindByNameVisible = true;
            isGridFilterByRangeVisible = false;
            isListSelectionHeaderVisible = false;
            RefreshElementVisibility();
        }
        private void ShowGridFilterByRangeOnly()
        {
            isGridFindForGlyphVisible = false;
            isGridFindByNameVisible = false;
            isGridFilterByRangeVisible = true;
            isListSelectionHeaderVisible = false;
            RefreshElementVisibility();
        }
        private void ShowListSelectionHeaderOnly()
        {
            isGridFindForGlyphVisible = false;
            isGridFindByNameVisible = false;
            isGridFilterByRangeVisible = false;
            isListSelectionHeaderVisible = true;
            RefreshElementVisibility();
        }





        private void NotifySymbolSelectionChange(FontIconSymbol symbol)
        {
            // temporary
            if (FontIconSymbolSelectionChanged != null) {
                var e = new FontIconPickerEventArgs(this, symbol);
                // FontIconSymbolSelectionChanged(this, e);
                FontIconSymbolSelectionChanged.Invoke(this, e);
            }
        }



        private void ButtonSortByName_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByName();
        }

        private void ButtonSortByGlyph_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByGlyph();
        }


        private void ListViewSymbolList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = (FontIconSymbol) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("Symbol selected: {0}", clickedItem);
            NotifySymbolSelectionChange(clickedItem);

            if (isSymbolInfoPopupEnabed) {
                var infoControl = symbolInfoPopup.Child as FontIconSymbolInfoControl;
                if (infoControl != null) {
                    infoControl.FontIconSymbol = clickedItem;
                    if (!symbolInfoPopup.IsOpen) {
                        if (currentControlSize != Size.Empty) {
                            symbolInfoPopup.HorizontalOffset = (currentControlSize.Width - 340) < 0 ? 0 : (currentControlSize.Width - 340) / 2.0;
                            symbolInfoPopup.VerticalOffset = (currentControlSize.Height - 380) > 400 ? 200 : ((currentControlSize.Height - 380) < 0 ? 0 : (currentControlSize.Height - 380) / 2.0);
                        }
                        symbolInfoPopup.IsOpen = true;
                    }
                }
            }

        }

        private void ComboboxFontFamily_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items != null && items.Any()) {
                var selectedItem = (FontFamilyStruct) items[0];
                System.Diagnostics.Debug.WriteLine("FontFamily selected: {0}", selectedItem);
                if (visualStateName != null) {
                    if (visualStateName.Equals("VisualStateMediumAndWide")) {
                        if (ComboboxFontFamily.Items != null && ComboboxFontFamily.Items.Count > 0) {
                            if (((FontFamilyStruct) ComboboxFontFamily.SelectedItem).Name != selectedItem.Name) {
                                ComboboxFontFamily.SelectedItem = selectedItem;
                            }
                        }
                    } else {
                        if (ComboboxFontFamily2.Items != null && ComboboxFontFamily2.Items.Count > 0) {
                            if (((FontFamilyStruct) ComboboxFontFamily2.SelectedItem).Name != selectedItem.Name) {
                                ComboboxFontFamily2.SelectedItem = selectedItem;
                            }
                        }
                    }
                }
                viewModel.FontFamily = selectedItem;
            }
        }

        private void ComboboxSymbolBlock_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items != null && items.Any()) {
                var selectedItem = (SymbolBlockStruct) items[0];
                System.Diagnostics.Debug.WriteLine("SymbolBlock selected: {0}", selectedItem);
                if (visualStateName != null) {
                    if (visualStateName.Equals("VisualStateMediumAndWide")) {
                        if (ComboboxSymbolBlock.Items != null && ComboboxSymbolBlock.Items.Count > 0) {
                            if (((SymbolBlockStruct) ComboboxSymbolBlock.SelectedItem).Block != selectedItem.Block) {
                                ComboboxSymbolBlock.SelectedItem = selectedItem;
                            }
                        }
                    } else {
                        if (ComboboxSymbolBlock2.Items != null && ComboboxSymbolBlock2.Items.Count > 0) {
                            if (((SymbolBlockStruct) ComboboxSymbolBlock2.SelectedItem).Block != selectedItem.Block) {
                                ComboboxSymbolBlock2.SelectedItem = selectedItem;
                            }
                        }
                    }
                }
                viewModel.SymbolBlock = selectedItem;
            }
        }

        private void ComboboxSymbolSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items != null && items.Any()) {
                var selectedItem = (SymbolSizeStruct) items[0];
                System.Diagnostics.Debug.WriteLine("SymbolSize selected: {0}", selectedItem);
                if (visualStateName != null) {
                    if (visualStateName.Equals("VisualStateMediumAndWide")) {
                        if (ComboboxSymbolSize.Items != null && ComboboxSymbolSize.Items.Count > 0) {
                            if (((SymbolSizeStruct) ComboboxSymbolSize.SelectedItem).Size != selectedItem.Size) {
                                ComboboxSymbolSize.SelectedItem = selectedItem;
                            }
                        }
                    } else {
                        if (ComboboxSymbolSize2.Items != null && ComboboxSymbolSize2.Items.Count > 0) {
                            if (((SymbolSizeStruct) ComboboxSymbolSize2.SelectedItem).Size != selectedItem.Size) {
                                ComboboxSymbolSize2.SelectedItem = selectedItem;
                            }
                        }
                    }
                }
                viewModel.SymbolSize = selectedItem;
            }
        }


        // Global flag
        private bool isSingleInputModeVisible = true;
        public bool IsSingleInputModeVisible
        {
            get
            {
                return isSingleInputModeVisible;
            }
            set
            {
                // The effect of changing this flag will not be manifested 
                //   until visibility of any of the input grids changes.
                isSingleInputModeVisible = value;
            }
        }

        private void AppBarToggleButtonView_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as AppBarToggleButton;
            if (button != null && button.IsChecked != null) {
                if (isSingleInputModeVisible) {
                    if (button.IsChecked.Value == true) {
                        ShowGridFindForGlyphOnly();
                        //AppBarToggleButtonFind.IsChecked = false;
                        //AppBarToggleButtonFilter.IsChecked = false;
                    } else {
                        HideAllInputGrids();
                    }
                } else {
                    isGridFindForGlyphVisible = button.IsChecked.Value;
                    RefreshElementVisibility();
                }
                if (button.IsChecked.Value == false) {
                    TextBoxFontGlyphForSearch.Text = "";
                    TextBoxFontHexCodeForSearch.Text = "";
                    TextBoxFontGlyphForSearch2.Text = "";
                    TextBoxFontHexCodeForSearch2.Text = "";
                } else {
                    viewModel.EndSearch();
                }
            }
        }

        private void AppBarToggleButtonFind_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as AppBarToggleButton;
            if (button != null && button.IsChecked != null) {
                if (isSingleInputModeVisible) {
                    if (button.IsChecked.Value == true) {
                        ShowGridFindByNameOnly();
                        //AppBarToggleButtonView.IsChecked = false;
                        //AppBarToggleButtonFilter.IsChecked = false;
                    } else {
                        HideAllInputGrids();
                    }
                } else {
                    isGridFindByNameVisible = button.IsChecked.Value;
                    RefreshElementVisibility();
                }
                if (button.IsChecked.Value == false) {
                    TextBoxIconNameForSearch.Text = "";
                    TextBoxIconNameForSearch2.Text = "";
                    viewModel.EndSearch();
                }
            }
        }

        private void AppBarToggleButtonRange_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as AppBarToggleButton;
            if (button != null && button.IsChecked != null) {
                if (isSingleInputModeVisible) {
                    if (button.IsChecked.Value == true) {
                        ShowGridFilterByRangeOnly();
                    } else {
                        HideAllInputGrids();
                    }
                } else {
                    isGridFilterByRangeVisible = button.IsChecked.Value;
                    RefreshElementVisibility();
                }
                if (button.IsChecked.Value == false) {
                    TextBoxGlyphRangeStart.Text = "";
                    TextBoxGlyphRangeEnd.Text = "";
                    TextBoxGlyphRangeStart2.Text = "";
                    TextBoxGlyphRangeEnd2.Text = "";
                } else {
                    viewModel.EndSearch();
                }
            }
        }

        private void AppBarToggleButtonFilter_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as AppBarToggleButton;
            if (button != null && button.IsChecked != null) {
                if (isSingleInputModeVisible) {
                    if (button.IsChecked.Value == true) {
                        ShowListSelectionHeaderOnly();
                        //AppBarToggleButtonView.IsChecked = false;
                        //AppBarToggleButtonFind.IsChecked = false;
                    } else {
                        HideAllInputGrids();
                    }
                } else {
                    isListSelectionHeaderVisible = button.IsChecked.Value;
                    RefreshElementVisibility();
                }
                if (button.IsChecked.Value == true) {
                    viewModel.EndSearch();
                }
            }
        }

        private void AppBarButtonHideAll_Click(object sender, RoutedEventArgs e)
        {
            HideAllInputGrids();
            //AppBarToggleButtonView.IsChecked = false;
            //AppBarToggleButtonFind.IsChecked = false;
            //AppBarToggleButtonFilter.IsChecked = false;
            TextBoxFontGlyphForSearch.Text = "";
            TextBoxFontHexCodeForSearch.Text = "";
            TextBoxIconNameForSearch.Text = "";
            TextBoxGlyphRangeStart.Text = "";
            TextBoxGlyphRangeEnd.Text = "";
            TextBoxFontGlyphForSearch2.Text = "";
            TextBoxFontHexCodeForSearch2.Text = "";
            TextBoxIconNameForSearch2.Text = "";
            TextBoxGlyphRangeStart2.Text = "";
            TextBoxGlyphRangeEnd2.Text = "";
            viewModel.EndSearch();
        }


        private void ButtonFindForFontGlyph_Click(object sender, RoutedEventArgs e)
        {
            string strGlyph = null;
            string hexCode = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                strGlyph = TextBoxFontGlyphForSearch2.Text;
                hexCode = TextBoxFontHexCodeForSearch2.Text;
            } else {
                strGlyph = TextBoxFontGlyphForSearch.Text;
                hexCode = TextBoxFontHexCodeForSearch.Text;
            }
            var glyph = 0;
            string strInput = null;
            if (!String.IsNullOrEmpty(hexCode)) {  // hexcode has the priority.
                strInput = hexCode;
                glyph = FontIconSymbols.GlyphFromHexCode(strInput);
            } else {
                strInput = strGlyph;
                glyph = FontIconSymbols.GlyphFromString(strInput);
            }
            System.Diagnostics.Debug.WriteLine("glyph = {0}", glyph);

            FontIconSymbolWrap wrap = null;
            if(glyph > 0) {
                wrap = viewModel.GetSymbolWrapForGlyph(glyph);
                System.Diagnostics.Debug.WriteLine("GetSymbolWrapForName(): hexCode = {0}; strGlyph = {1}; wrap = {2}", hexCode, strGlyph, wrap);
            }
            if (wrap != null) {
                // Note that we do this even if isSymbolInfoPopupEnabed == false.
                var infoControl = symbolInfoPopup.Child as FontIconSymbolInfoControl;
                if (infoControl != null) {
                    infoControl.FontIconSymbol = wrap.FontIconSymbol;
                    if (!symbolInfoPopup.IsOpen) {
                        if (currentControlSize != Size.Empty) {
                            symbolInfoPopup.HorizontalOffset = (currentControlSize.Width - 340) < 0 ? 0 : (currentControlSize.Width - 340) / 2.0;
                            symbolInfoPopup.VerticalOffset = (currentControlSize.Height - 440) > 400 ? 200 : (currentControlSize.Height - 440) / 2.0;
                        }
                        symbolInfoPopup.IsOpen = true;
                    }
                }
            } else {
                TextBoxFontGlyphForSearch.Text = "";   // ???
                TextBoxFontHexCodeForSearch.Text = "";   // ???
                TextBoxFontGlyphForSearch2.Text = "";   // ???
                TextBoxFontHexCodeForSearch2.Text = "";   // ???
                InstantToastHelper.Instance.ShowToast("No symbols found for strInput, " + strInput);
            }
        }

        private void ButtonFindByIconName_Click(object sender, RoutedEventArgs e)
        {
            string strName = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                strName = TextBoxIconNameForSearch2.Text;
            } else {
                strName = TextBoxIconNameForSearch.Text;
            }
            if (!String.IsNullOrEmpty(strName)) {
                var suc = viewModel.DoSearchByName(strName);
                System.Diagnostics.Debug.WriteLine("DoSearchByName(): strName = {0}; suc = {1}", strName, suc);
                if (suc) {
                    // ...
                } else {
                    TextBoxIconNameForSearch.Text = "";   // ???
                    TextBoxIconNameForSearch2.Text = "";   // ???
                    InstantToastHelper.Instance.ShowToast("No font icons found for the string, " + strName);
                }
            }
        }

        private void TextBoxIconNameForSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strName = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                strName = TextBoxIconNameForSearch2.Text;
                TextBoxIconNameForSearch.Text = strName;
            } else {
                strName = TextBoxIconNameForSearch.Text;
                TextBoxIconNameForSearch2.Text = strName;
            }
            if (String.IsNullOrEmpty(strName)) {
                viewModel.EndSearch();
            }
        }

        private void TextBoxFontGlyphForSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strGlyph = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                strGlyph = TextBoxFontGlyphForSearch2.Text;
                TextBoxFontGlyphForSearch.Text = strGlyph;
            } else {
                strGlyph = TextBoxFontGlyphForSearch.Text;
                TextBoxFontGlyphForSearch2.Text = strGlyph;
            }
            if (!String.IsNullOrEmpty(strGlyph)) {
                TextBoxFontHexCodeForSearch.Text = "";
                TextBoxFontHexCodeForSearch2.Text = "";
            }
        }

        private void TextBoxFontHexCodeForSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string hexCode = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                hexCode = TextBoxFontHexCodeForSearch2.Text;
                TextBoxFontHexCodeForSearch.Text = hexCode;
            } else {
                hexCode = TextBoxFontHexCodeForSearch.Text;
                TextBoxFontHexCodeForSearch2.Text = hexCode;
            }
            if (!String.IsNullOrEmpty(hexCode)) {
                TextBoxFontGlyphForSearch.Text = "";
                TextBoxFontGlyphForSearch2.Text = "";
            }
        }


        private void TextBoxGlyphRangeStart_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                TextBoxGlyphRangeStart.Text = TextBoxGlyphRangeStart2.Text;
            } else {
                TextBoxGlyphRangeStart2.Text = TextBoxGlyphRangeStart.Text;
            }
        }
        private void TextBoxGlyphRangeEnd_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                TextBoxGlyphRangeEnd.Text = TextBoxGlyphRangeEnd2.Text;
            } else {
                TextBoxGlyphRangeEnd2.Text = TextBoxGlyphRangeEnd.Text;
            }
        }

        private void ButtonFilterByGlyphRange_Click(object sender, RoutedEventArgs e)
        {
            string strRangeStart = null;
            string strRangeEnd = null;
            if (visualStateName.Equals("VisualStateMediumAndWide")) {
                strRangeStart = TextBoxGlyphRangeStart2.Text;
                strRangeEnd = TextBoxGlyphRangeEnd2.Text;
            } else {
                strRangeStart = TextBoxGlyphRangeStart.Text;
                strRangeEnd = TextBoxGlyphRangeEnd.Text;
            }

            if (!String.IsNullOrEmpty(strRangeStart) && !String.IsNullOrEmpty(strRangeEnd)) {
                var isStartHex = false;
                if (strRangeStart.StartsWith("0x") || strRangeStart.StartsWith("x")) {
                    var idx = strRangeStart.IndexOf("x");
                    strRangeStart = strRangeStart.Substring(idx + 1, strRangeStart.Length - idx - 1);
                    isStartHex = true;
                }
                var glyphStart = 0;
                if (isStartHex) {
                    glyphStart = FontIconSymbols.GlyphFromHexCode(strRangeStart);
                } else {
                    glyphStart = FontIconSymbols.GlyphFromString(strRangeStart);
                }
                System.Diagnostics.Debug.WriteLine(">>>> glyphStart = {0}", glyphStart);

                var isEndHex = false;
                if (strRangeEnd.StartsWith("0x") || strRangeEnd.StartsWith("x")) {
                    var idx = strRangeEnd.IndexOf("x");
                    strRangeEnd = strRangeEnd.Substring(idx + 1, strRangeEnd.Length - idx - 1);
                    isEndHex = true;
                }
                var glyphEnd = 0;
                if (isEndHex) {
                    glyphEnd = FontIconSymbols.GlyphFromHexCode(strRangeEnd);
                } else {
                    glyphEnd = FontIconSymbols.GlyphFromString(strRangeEnd);
                }
                System.Diagnostics.Debug.WriteLine(">>>> glyphEnd = {0}", glyphEnd);

                if (glyphStart > 0 && glyphEnd >= glyphStart) {

                    var symbolBlock = new SymbolBlockStruct(UnicodeSymbolBlock.CustomRange, new int[] { glyphStart, glyphEnd });

                    // ???
                    //if (!ComboboxSymbolBlock.Items.Contains(symbolBlock)) {
                    //    ComboboxSymbolBlock.Items.Add(symbolBlock);
                    //}
                    ComboboxSymbolBlock.SelectedItem = BmpSymbolBlocks.SB_NullCustomRange;
                    ComboboxSymbolBlock2.SelectedItem = BmpSymbolBlocks.SB_NullCustomRange;
                    // ????

                    viewModel.SymbolBlock = symbolBlock;

                } else {
                    InstantToastHelper.Instance.ShowToast("Invalid glyph range.");
                }
            }

        }


        private void NotifyTextClipAvailable(string textClip)
        {
            // temporary
            if (TextClipAvailable != null) {
                var e = new TextClipEventArgs(this, textClip);
                // TextClipAvailable(this, e);
                TextClipAvailable.Invoke(this, e);
            }
        }

    }
}