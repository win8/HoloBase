﻿using HoloBase.Icons.Common;
using HoloBase.Icons.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.SymbolIcons
{
    public sealed class SymbolIconListManager
    {        
        private static SymbolIconListManager instance = null;
        public static SymbolIconListManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new SymbolIconListManager();
                }
                return instance;
            }
        }
        private SymbolIconListManager()
        {
        }

        public static IList<SymbolIconSymbol> BuildSymbolIconSymbolList(IEnumerable<Symbol> symbols)
        {
            var list = new List<SymbolIconSymbol>();
            foreach (var s in symbols) {
                // TBD: Get custom names here.
                var isFavorite = FavoriteIconsDataManager.Instance.IsFavorteSymbolIcon(s);
                var symbol = new SymbolIconSymbol(s, null, isFavorite);
                list.Add(symbol);
            }
            return list;
        }


        // This should really be static.
        public IList<SymbolIconSymbol> SymbolList
        {
            get
            {
                var list = new List<SymbolIconSymbol>();
                foreach (var s in SymbolIconSymbols.SymbolMap.Values) {
                    var isFavorite = FavoriteIconsDataManager.Instance.IsFavorteSymbolIcon(s.Symbol);
                    var symbol = new SymbolIconSymbol(s.Symbol, s.Name, isFavorite);
                    list.Add(symbol);
                }
                return list;
            }
        }


    }
}
