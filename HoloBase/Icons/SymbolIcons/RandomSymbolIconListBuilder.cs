﻿using HoloBase.Collection.Util;
using HoloBase.Icons.Common;
using HoloBase.Icons.Data;
using HoloJson.Mini.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.SymbolIcons
{
    public sealed class RandomSymbolIconListBuilder
    {
        // temporary
        private const long TTL = 24 * 3600 * 1000L;   // 1 day
        private const int DEFAULT_MAX_COUNT = 100;    // arbitrary.

        private static readonly Random Rand = new Random((int) (DateTimeUtil.CurrentUnixEpochMillis() / 1000L));

        private static RandomSymbolIconListBuilder instance = null;
        public static RandomSymbolIconListBuilder Instance
        {
            get
            {
                if (instance == null) {
                    instance = new RandomSymbolIconListBuilder();
                }
                return instance;
            }
        }

        // In order to be able to cache the list,
        // we need to fix the max count.
        // TBd: maxCount cannot be bigger than the allowed value for the user....
        private int maxCount = DEFAULT_MAX_COUNT;

        // "cache"
        //// private IList<SymbolIconSymbol> symbolList = null;
        //private IList<SymbolIconSymbolWrap> symbolList = null;
        //private long lastBuiltTime = 0L;
        //private bool lastUsedUseFavoriteListFirst = false;

        // New cache
        // (count, useFavoriteListFirst) => (timestamp, symbolList)
        private readonly IDictionary<Tuple<int, bool>, Tuple<long, IList<SymbolIconSymbolWrap>>> masterCache = new Dictionary<Tuple<int, bool>, Tuple<long, IList<SymbolIconSymbolWrap>>>();



        private RandomSymbolIconListBuilder()
        {
        }


        public int MaxCount
        {
            get
            {
                return maxCount;
            }
            set
            {
                if (maxCount != value) {
                    maxCount = value;
                    //symbolList = null;
                    //lastBuiltTime = 0L;
                    //// lastUsedUseFavoriteListFirst ????

                    // No need for this.
                    // masterCache[Tuple.Create(maxCount, lastUsedUseFavoriteListFirst)] = null;
                }
            }
        }


        // public IList<SymbolIconSymbol> GetRandomSymbols()
        public IList<SymbolIconSymbolWrap> GetRandomSymbols(bool useFavoriteListFirst = false)
        {
            return GetRandomSymbols(MaxCount, useFavoriteListFirst);
        }
        // private IList<SymbolIconSymbol> GetRandomSymbols(int count)
        private IList<SymbolIconSymbolWrap> GetRandomSymbols(int count, bool useFavoriteListFirst = false)
        {
            //// Note that the cache is ignored if tbe last used useFavoriteListFirst is different
            //var now = DateTimeUtil.CurrentUnixEpochMillis();
            //if (symbolList != null && lastUsedUseFavoriteListFirst == useFavoriteListFirst && lastBuiltTime + TTL > now) {
            //    return symbolList;
            //}

            var key = Tuple.Create(maxCount, useFavoriteListFirst);
            // Note that the cache is ignored if tbe last used useFavoriteListFirst is different
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            if (masterCache.ContainsKey(key) && masterCache[key] != null && masterCache[key].Item1 + TTL > now) {
                var symbolList = masterCache[key].Item2;
                if (symbolList != null) {   // this cannot be null, obviously.
                    System.Diagnostics.Debug.WriteLine("Cached SymbolIconSymbolWrap list is returned for maxCount = {0}, useFavoriteListFirst = {1}", maxCount, useFavoriteListFirst);
                    return symbolList;
                }
            }
            System.Diagnostics.Debug.WriteLine("No cache hit for SymbolIconSymbolWrap list: maxCount = {0}, useFavoriteListFirst = {1}", maxCount, useFavoriteListFirst);

            var allSymbols = SymbolIconListManager.Instance.SymbolList;
            var symbolCount = allSymbols.Count;

            // Note:
            // Instead of insisting on adding exactly the count number of symbols,
            // we just try to add up to count.
            // var randomSet = new HashSet<SymbolIconSymbol>();
            var randomSet = new Dictionary<Symbol, SymbolIconSymbolWrap>();
            if (useFavoriteListFirst) {
                var favoriteList = FavoriteIconsDataManager.Instance.GetFavoriteSymbolIconSymbolList();
                if (favoriteList != null && favoriteList.Any()) {
                    var favCount = favoriteList.Count;
                    var maxFavs = (int) (count * 0.5);   // 50% max favs.
                    for (var i = 0; i < maxFavs; i++) {
                        var r = Rand.Next(favCount);
                        randomSet[favoriteList[r].Symbol] = new SymbolIconSymbolWrap(favoriteList[r]);
                    }
                }
            } else {
                for (var i = 0; i < count; i++) {
                    var r = Rand.Next(symbolCount);
                    // randomSet.Add(allSymbols[r]);
                    randomSet[allSymbols[r].Symbol] = new SymbolIconSymbolWrap(allSymbols[r]);
                }
            }
            // Try one more time,
            // but from allSymbols list regardless of useFavoriteListFirst flag.
            var randomSetCount = randomSet.Count;
            if (randomSetCount < count) {
                var delta = count - randomSetCount;
                for (var i = 0; i < delta; i++) {
                    var r = Rand.Next(symbolCount);
                    // randomSet.Add(allSymbols[r]);
                    randomSet[allSymbols[r].Symbol] = new SymbolIconSymbolWrap(allSymbols[r]);
                }
            }
            // We may still be a bit short at this point, but that's ok.
            // We do not need to return the exact count in our use cases.

            //// Update the "cache".
            //// symbolList = new List<SymbolIconSymbol>(randomSet);
            //symbolList = new List<SymbolIconSymbolWrap>(randomSet.Values);
            //// Now shuffling to mix the favorites....
            //if (useFavoriteListFirst == true) {
            //    symbolList = symbolList.Shuffle();
            //}
            //lastBuiltTime = now;
            //lastUsedUseFavoriteListFirst = useFavoriteListFirst;

            IList<SymbolIconSymbolWrap> symbolWrapList = new List<SymbolIconSymbolWrap>(randomSet.Values);
            // Now shuffling to mix the favorites....
            if (useFavoriteListFirst == true) {
                symbolWrapList = symbolWrapList.Shuffle();
            }

            // Cache it.
            var newValue = Tuple.Create(now, symbolWrapList);
            masterCache[key] = newValue;

            // debuggings
            foreach (var s in symbolWrapList) {
                System.Diagnostics.Debug.WriteLine("Random symbol: {0}", s);
            }
            // debuggings

            return symbolWrapList;
        }


    }
}
