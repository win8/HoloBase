﻿using HoloBase.Icons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.SymbolIcons
{
    public sealed class SymbolIconSearchManager
    {
        //private static SymbolIconSearchManager instance = null;
        //public static SymbolIconSearchManager Instance
        //{
        //    get
        //    {
        //        if (instance == null) {
        //            instance = new SymbolIconSearchManager();
        //        }
        //        return instance;
        //    }
        //}
        //private SymbolIconSearchManager()
        //{
        //}

        // Default instance.
        private static SymbolIconSearchManager defaultInstance = null;
        public static SymbolIconSearchManager DefaultInstance
        {
            get
            {
                if (defaultInstance == null) {
                    defaultInstance = new SymbolIconSearchManager();
                }
                return defaultInstance;
            }
        }

        // targetList cannot be null.
        private IList<SymbolIconSymbol> targetList;
        public SymbolIconSearchManager()
            : this(null)
        {
        }
        public SymbolIconSearchManager(IList<SymbolIconSymbol> targetList)
        {
            if(targetList != null) {
                this.targetList = targetList;
            } else {
                // Note that defaultInstance uses this "default list".
                this.targetList = new List<SymbolIconSymbol>(SymbolIconSymbols.SymbolMap.Values);
            }
        }
        private IList<SymbolIconSymbol> TargetList
        {
            get
            {
                return this.targetList;
            }
            set
            {
                if (value != null) {
                    this.targetList = value;
                    this.symbolNameMap = null;
                } else {
                    // ????
                }
            }
        }


        //////////////////////////////////
        // Search "Index"

        private Object lockObject = new Object();
        private IDictionary<string, Symbol> symbolNameMap = null;
        private IDictionary<string, Symbol> SymbolNameMap
        {
            get
            {
                if (symbolNameMap == null) {
                    lock (lockObject) {
                        if (symbolNameMap == null) {
                            symbolNameMap = new Dictionary<string, Symbol>();
                            foreach (var s in TargetList) {
                                if (!String.IsNullOrEmpty(s.Name)) {
                                    symbolNameMap[s.Name.ToLower()] = s.Symbol;
                                }
                            }
                        }
                    }
                }
                return symbolNameMap;
            }
        }

        //// This method does not really make sense.
        //private Symbol FindSymbolByName(string symbolName)
        //{
        //    if (symbolName != null && SymbolNameMap.ContainsKey(symbolName.ToLower())) {
        //        return SymbolNameMap[symbolName.ToLower()];
        //    } else {
        //        // ????
        //        throw new ArgumentException("Invalid symbol name: " + symbolName);
        //    }
        //}


        public bool FindSymbolForName(string name, out Symbol symbol)
        {
            IList<Symbol> symbols;
            var suc = FindSymbolsByName(name, out symbols, true);
            if (suc && symbols.Any()) {
                symbol = symbols[0];
                return true;
            } else {
                // Just use an arbitrary symbol. Not to be used anyway since found == false.
                symbol = Symbol.Clear;
                return false;
            }
        }

        // exactMatch == true: word match case-folded.
        public bool FindSymbolsByName(string name, out IList<Symbol> symbols, bool exactMatch = false)
        {
            try {
                if (exactMatch) {
                    var symbol = SymbolNameMap[name.ToLower()];
                    symbols = new List<Symbol>( new Symbol[]{ symbol } );
                    return true;
                } else {
                    var list = ((List<SymbolIconSymbol>) targetList).FindAll(o => (o.Name != null && o.Name.ToLower().Contains(name.ToLower())));
                    if (list != null) {
                        symbols = new List<Symbol>();
                        foreach (var sis in list) {
                            symbols.Add(sis.Symbol);
                        }
                        return true;
                    }
                }
            } catch (Exception) {
                // Ignore..
            }
            symbols = null;
            return false;
        }


    }
}
