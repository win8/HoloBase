﻿using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.UI;
using HoloBase.Icons.ViewModels;
using HoloBase.UI.Colors;
using HoloBase.UI.Util;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class FontIconPickerControl : UserControl, IRefreshableElement, IViewModelHolder, IFontIconPickerControl
    {
        // Public event for the symbol picker clients.
        public event EventHandler<FontIconPickerEventArgs> FontIconSymbolSelectionChanged;

        private FontIconListViewModel viewModel;

        public FontIconPickerControl()
        {
            this.InitializeComponent();

            ResetViewModel();
            //SortListItems();

            isCancelButtonVisible = true;
            isSortButtonsVisible = true;
            RefreshElementVisibility();
        }
        private void ResetViewModel()
        {
            var vm = new FontIconListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(FontIconListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;


            //// tbd:
            //ListViewSymbolList.Items.Add(new FontIconSymbol(viewModel.FontFamily, 0x3a3));
            //ListViewSymbolList.Items.Add(new FontIconSymbol(viewModel.FontFamily, 0x3a4));
            //ListViewSymbolList.Items.Add(new FontIconSymbol(viewModel.FontFamily, 0x3a5));


        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (FontIconListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }


        // tbd:
        private bool isSortButtonsVisible;
        public bool IsSortButtonsVisible
        {
            get
            {
                return isSortButtonsVisible;
            }
            set
            {
                isSortButtonsVisible = value;
                RefreshElementVisibility();
            }
        }

        private bool isCancelButtonVisible;
        public bool IsCancelButtonVisible
        {
            get
            {
                return isCancelButtonVisible;
            }
            set
            {
                isCancelButtonVisible = value;
                RefreshElementVisibility();
            }
        }
        private void RefreshElementVisibility()
        {
            if (isSortButtonsVisible) {
                StackPanelTopRow.Visibility = Visibility.Visible;
            } else {
                StackPanelTopRow.Visibility = Visibility.Collapsed;
            }
            if (isCancelButtonVisible) {
                StackPanelBottomRow.Visibility = Visibility.Visible;
            } else {
                StackPanelBottomRow.Visibility = Visibility.Collapsed;
            }
        }



        private void NotifySymbolSelectionChange(FontIconSymbol symbol)
        {
            // temporary
            if (FontIconSymbolSelectionChanged != null) {
                var e = new FontIconPickerEventArgs(this, symbol);
                // FontIconSymbolSelectionChanged(this, e);
                FontIconSymbolSelectionChanged.Invoke(this, e);
            }
        }



        private void ButtonSortByName_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByName();
        }

        private void ButtonSortByGlyph_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByGlyph();
        }


        private void DismissPopup()
        {
            // tbd:
            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DismissPopup();
        }

        private void ListViewSymbolList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = (FontIconSymbol) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("Symbol selected: {0}", clickedItem);
            NotifySymbolSelectionChange(clickedItem);

            DismissPopup();
        }

        private void ComboboxFontFamily_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items != null && items.Any()) {
                var selectedItem = (FontFamilyStruct) items[0];
                System.Diagnostics.Debug.WriteLine("FontFamily selected: {0}", selectedItem);
                viewModel.FontFamily = selectedItem;
            }
        }

        private void ComboboxSymbolBlock_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items != null && items.Any()) {
                var selectedItem = (SymbolBlockStruct) items[0];
                System.Diagnostics.Debug.WriteLine("SymbolBlock selected: {0}", selectedItem);
                viewModel.SymbolBlock = selectedItem;
            }
        }
  
    
    }
}
