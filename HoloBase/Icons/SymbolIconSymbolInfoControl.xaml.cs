﻿using HoloBase.Icons.Common;
using HoloCore.Core;
using HoloBase.UI.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HoloBase.UI.Colors;
using HoloBase.Icons.UI;
using HoloBase.Icons.Clipboard;
using HoloBase.Controls.Core;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class SymbolIconSymbolInfoControl : UserControl, IRefreshableElement, IViewModelHolder, IFavoriteStatusUpdaterControl, ITextClipSourceControl
    {
        // To convey the favorite status change information to the "parent" (list/grid view)
        public event EventHandler<FavoriteStatusChangeEventArgs> FavoriteStatusChanged;
        // for copying xaml snippets...
        public event EventHandler<TextClipEventArgs> TextClipAvailable;

        private SymbolIconSymbolWrap viewModel;

        public SymbolIconSymbolInfoControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new SymbolIconSymbolWrap();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SymbolIconSymbolWrap viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
            // ???
            SymbolIconAddToClipboard.Visibility = Visibility.Visible;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SymbolIconSymbolWrap) value;
                // ????

                ResetViewModel(vm);
            }
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Navy);
        public string BackgroundGridColor
        {
            get
            {
                return backgroundColor.ARGB;
            }
            set
            {
                backgroundColor = new ColorStruct(value);
                GridSymbolIconSymbolInfoControl.Background = new SolidColorBrush(backgroundColor.Color);
            }
        }


        public SymbolIconSymbolWrap SymbolIconSymbolInfo
        {
            get
            {
                return (SymbolIconSymbolWrap) viewModel;
            }
        }
        public SymbolIconSymbol SymbolIconSymbol
        {
            get
            {
                return ((SymbolIconSymbolWrap) viewModel).SymbolIconSymbol;
            }
            set
            {
                ((SymbolIconSymbolWrap) viewModel).SymbolIconSymbol = value;
                // ???
                SymbolIconAddToClipboard.Visibility = Visibility.Visible;
            }
        }


        private void DismissPopup()
        {
            // tbd:
            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }
        }
        private void ButtonDone_Click(object sender, RoutedEventArgs e)
        {
            SymbolIconAddToClipboard.Visibility = Visibility.Visible;
            DismissPopup();
        }

        private void ButtonCopyXaml_Click(object sender, RoutedEventArgs e)
        {
            var xamlSnippets = viewModel.XamlSnippetsAsString;
            if (!String.IsNullOrEmpty(xamlSnippets)) {
                NotifyTextClipAvailable(xamlSnippets);
            }
        }


        private void GridFavoriteMarker_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var isFavorite = viewModel.IsFavorite;
            viewModel.IsFavorite = !isFavorite;
            NotifyFavoriteStatusChange(viewModel.SymbolIconSymbol);
        }

        private void SymbolIconAddToClipboard_Tapped(object sender, TappedRoutedEventArgs e)
        {
            IconClipboardManager.Instance.Clipboard.Push(viewModel.SymbolIconSymbol);
            // Temporarily hide it (as a feedback)...
            // (TBD: We need to make it visible again if a different symbol is selected without closing the popup....)
            SymbolIconAddToClipboard.Visibility = Visibility.Collapsed;
        }


        private void NotifyFavoriteStatusChange(SymbolIconSymbol iconSymbol)
        {
            // temporary
            if (FavoriteStatusChanged != null) {
                var e = new FavoriteStatusChangeEventArgs(this, iconSymbol);
                // FavoriteStatusChanged(this, e);
                FavoriteStatusChanged.Invoke(this, e);
            }
        }

        private void NotifyTextClipAvailable(string textClip)
        {
            // temporary
            if (TextClipAvailable != null) {
                var e = new TextClipEventArgs(this, textClip);
                // TextClipAvailable(this, e);
                TextClipAvailable.Invoke(this, e);
            }
        }
    }
}
