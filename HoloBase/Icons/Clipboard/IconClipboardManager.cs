﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Clipboard
{
    public sealed class IconClipboardManager
    {
        private static IconClipboardManager instance = null;
        public static IconClipboardManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new IconClipboardManager();
                }
                return instance;
            }
        }

        // TBD:
        //      Multiple clipboards????
        private readonly IconClipboard iconClipboard;
        private IconClipboardManager()
        {
            iconClipboard = new IconClipboard();
        }

        public IconClipboard Clipboard
        {
            get
            {
                return iconClipboard;
            }
        }



    }
}
