﻿using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.Clipboard
{
    public sealed class IconClipboard
    {
        // ????
        // Note: We need to use nullable objects even if ISymbol can be a struct...
        //      --> Just use reference objects.....
        // ????

        // private readonly IList<ISymbol> symbolList;
        private readonly Stack<ISymbol> symbolStack;
        public IconClipboard()
        {
            // symbolList = new List<ISymbol>();
            symbolStack = new Stack<ISymbol>();


            //// testing
            //symbolStack.Push(new SymbolClipWrap(IconType.FontIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, 9000));
            //symbolStack.Push(new SymbolClipWrap(IconType.FontIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, 9001));
            //symbolStack.Push(new SymbolClipWrap(IconType.SymbolIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, (int) Symbol.Accept));
            //symbolStack.Push(new SymbolClipWrap(IconType.SymbolIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, (int) Symbol.Account));
            //symbolStack.Push(new SymbolClipWrap(IconType.FontIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, 9002));
            //symbolStack.Push(new SymbolClipWrap(IconType.FontIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, 9003));
            //symbolStack.Push(new SymbolClipWrap(IconType.SymbolIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, (int) Symbol.Add));
            //symbolStack.Push(new SymbolClipWrap(IconType.SymbolIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, (int) Symbol.AddFriend));
            //symbolStack.Push(new SymbolClipWrap(IconType.FontIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, 9004));
            //symbolStack.Push(new SymbolClipWrap(IconType.FontIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, 9005));
            //symbolStack.Push(new SymbolClipWrap(IconType.SymbolIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, (int) Symbol.Admin));
            //symbolStack.Push(new SymbolClipWrap(IconType.SymbolIcon, FontFamilyRegistry.FF_SegoeUISymbol.Name, (int) Symbol.AlignCenter));
            //// testing

        }

        public void Clear()
        {
            // symbolList.Clear();
            symbolStack.Clear();
        }

        public IList<ISymbol> SymbolList
        {
            get
            {
                // return symbolList;
                // return symbolStack.ToList();
                var list = symbolStack.ToList();
                list.Reverse();   // Put last one at the end.
                return list;
            }
        }

        public string ListAsString
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var s in SymbolList) {   // Note the chars are ordered so that first/oldest char comes first.
                    sb.Append(FontIconSymbols.GlyphChar(s.Glyph));
                }
                return sb.ToString();
            }
        }

        public bool HasAny
        {
            get
            {
                //var count = symbolStack.Count;
                //System.Diagnostics.Debug.WriteLine("symbolStack.Count = {0}", count);

                return symbolStack.Any();
                // return symbolStack.ToList().Any(); 
            }
        }
        public int Count
        {
            get
            {
                return symbolStack.Count;
            }
        }


        // Note that, in our implementation,
        //   clips need not be pop'ed().
        // We will mostly use Peek and Push,
        // (and, item-based access, e.g., from ListView).
        public ISymbol Peek()
        {
            if (symbolStack.Any()) {
                return symbolStack.Peek();
            } else {
                // ???
                return null;
            }
        }
        public ISymbol Pop()
        {
            if (symbolStack.Any()) {
                return symbolStack.Pop();
            } else {
                // ???
                return null;
            }
        }

        public void Push(ISymbol symbol)
        {
            symbolStack.Push(symbol);
        }
        public void Push(IconType type, string fontFamilyName, int glyph)
        {
            // ????
            var symbol = new SymbolClipWrap(type, fontFamilyName, glyph);
            Push(symbol);
        }
        
        public void PushAll(IEnumerable<ISymbol> symbols)
        {
            if (symbols != null) {
                foreach (var s in symbols) {
                    symbolStack.Push(s);
                }
            }
        }


        public override string ToString()
        {
            return ListAsString;
        }

    }
}
