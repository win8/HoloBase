﻿using HoloBase.Icons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public class SymbolBlockPickerEventArgs : EventArgs
    {
        private SymbolBlockStruct pickedSymbolBlock;
        public SymbolBlockPickerEventArgs(object sender, SymbolBlockStruct pickedSymbolBlock)
        {
            this.pickedSymbolBlock = pickedSymbolBlock;
        }

        public SymbolBlockStruct SymbolBlock
        {
            get
            {
                return pickedSymbolBlock;
            }
            set
            {
                pickedSymbolBlock = value;
            }
        }

    }
}
