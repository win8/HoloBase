﻿using HoloBase.Icons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public class SymbolIconPickerEventArgs : EventArgs
    {
        // private string pickedSymbolLabel;
        private SymbolIconSymbol pickedSymbol;
        public SymbolIconPickerEventArgs(object sender, SymbolIconSymbol pickedSymbol)
        {
            this.pickedSymbol = pickedSymbol;
        }

        public SymbolIconSymbol PickedSymbol
        {
            get
            {
                return pickedSymbol;
            }
            set
            {
                pickedSymbol = value;
            }
        }

    }
}
