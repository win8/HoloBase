﻿using HoloBase.Controls.Core;
using HoloBase.Icons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public interface IIconListingControl : ITextClipSourceControl
    {
        bool IsSymbolInfoPopupEnabed
        {
            get;
            set;
        }
        bool IsSymbolInfoPopupOpen
        {
            get;
            set;
        }
        bool UseFavoriteList
        {
            get;
            set;
        }
        string BackgroundGridColor
        {
            get;
            set;
        }
        string PopupBackgroundColor
        {
            get;
            set;
        }
        string VisibilityForAppBarButtons
        {
            get;
            set;
        }
        bool IsCompactForAppBarButtons
        {
            get;
            set;
        }
        bool IsGridFindByNameVisible
        {
            get;
            set;
        }
        bool IsSortButtonsVisible
        {
            get;
            set;
        }
        bool IsSingleInputModeVisible
        {
            get;
            set;
        }

    }

    public interface ISymbolIconListingControl : IIconListingControl
    {
        SymbolIconSymbolWrap CurrentSymbolInfo
        {
            get;
        }
        bool IsGridFindForSymbolVisible
        {
            get;
            set;
        }

    }

    public interface IFontIconListingControl : IIconListingControl
    {
        FontIconSymbolWrap CurrentSymbolInfo
        {
            get;
        }
        SymbolSizeStruct SymbolFontSize
        {
            get;
            set;
        }
        bool IsAppBarToggleButtonFilterEnabled
        {
            get;
            set;
        }
        bool IsTopHeaderBoxVisible
        {
            get;
        }
        bool IsGridFindForGlyphVisible
        {
            get;
            set;
        }
        bool IsListSelectionHeaderVisible
        {
            get;
            set;
        }
        bool IsSymbolSizeComboBoxVisible
        {
            get;
            set;
        }

    }
}
