﻿using HoloBase.Icons.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public class FontFamilyPickerEventArgs : EventArgs
    {
        private FontFamilyStruct pickedFontFamily;
        public FontFamilyPickerEventArgs(object sender, FontFamilyStruct pickedFontFamily)
        {
            this.pickedFontFamily = pickedFontFamily;
        }

        public FontFamilyStruct FontFamily
        {
            get
            {
                return pickedFontFamily;
            }
            set
            {
                pickedFontFamily = value;
            }
        }

    }
}
