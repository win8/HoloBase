﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public interface ISymbolIconPickerControl
    {
        event EventHandler<SymbolIconPickerEventArgs> SymbolIconSymbolSelectionChanged;
    }
    public interface IFontIconPickerControl
    {
        event EventHandler<FontIconPickerEventArgs> FontIconSymbolSelectionChanged;
    }

    public interface IFontFamilyPickerControl
    {
        event EventHandler<FontFamilyPickerEventArgs> FontFamilySelectionChanged;
    }
    public interface ISymbolBlockPickerControl
    {
        event EventHandler<SymbolBlockPickerEventArgs> SymbolBlockSelectionChanged;
    }

    public interface IFontSymbolSizePickerControl
    {
        event EventHandler<FontSymbolSizeEventArgs> FontSymbolSizeChanged;
    }

}
