﻿using HoloBase.Icons.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public class FavoriteStatusChangeEventArgs : EventArgs
    {
        // private string updatedNameLabel;
        private IIconSymbol iconSymbol;
        // private bool isFavorite;
        public FavoriteStatusChangeEventArgs(object sender, IIconSymbol iconSymbol)
        {
            this.iconSymbol = iconSymbol;
            // this.isFavorite = isFavorite;
        }

        public IIconSymbol IconSymbol
        {
            get
            {
                return iconSymbol;
            }
            set
            {
                iconSymbol = value;
            }
        }
        //public bool IsFavorite
        //{
        //    get
        //    {
        //        return isFavorite;
        //    }
        //    set
        //    {
        //        isFavorite = value;
        //    }
        //}

    }
}
