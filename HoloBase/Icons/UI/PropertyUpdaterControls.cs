﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public interface IGlyphNameUpdaterControl
    {
        event EventHandler<GlyphNameUpdateEventArgs> GlyphNameChanged;
    }
    public interface IFavoriteStatusUpdaterControl
    {
        event EventHandler<FavoriteStatusChangeEventArgs> FavoriteStatusChanged;
    }
}
