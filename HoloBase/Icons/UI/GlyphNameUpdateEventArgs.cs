﻿using HoloBase.Icons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public class GlyphNameUpdateEventArgs : EventArgs
    {
        // private string updatedNameLabel;
        private GlyphName updatedGlyphName;
        public GlyphNameUpdateEventArgs(object sender, GlyphName updatedGlyphName)
        {
            this.updatedGlyphName = updatedGlyphName;
        }

        public GlyphName UpdatedGlyphName
        {
            get
            {
                return updatedGlyphName;
            }
            set
            {
                updatedGlyphName = value;
            }
        }

    }
}
