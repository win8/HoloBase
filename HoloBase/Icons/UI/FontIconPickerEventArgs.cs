﻿using HoloBase.Icons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public class FontIconPickerEventArgs : EventArgs
    {
        // private string pickedSymbolLabel;
        private FontIconSymbol pickedSymbol;
        public FontIconPickerEventArgs(object sender, FontIconSymbol pickedSymbol)
        {
            this.pickedSymbol = pickedSymbol;
        }

        public FontIconSymbol PickedSymbol
        {
            get
            {
                return pickedSymbol;
            }
            set
            {
                pickedSymbol = value;
            }
        }

    }
}
