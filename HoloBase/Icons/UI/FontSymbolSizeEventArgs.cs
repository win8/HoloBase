﻿using HoloBase.Icons.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.UI
{
    public class FontSymbolSizeEventArgs : EventArgs
    {
        // private string newFontSymbolSizeLabel;
        private FontSymbolSize newFontSymbolSize;
        public FontSymbolSizeEventArgs(object sender, FontSymbolSize newFontSymbolSize)
        {
            this.newFontSymbolSize = newFontSymbolSize;
        }

        public FontSymbolSize NewFontSymbolSize
        {
            get
            {
                return newFontSymbolSize;
            }
            set
            {
                newFontSymbolSize = value;
            }
        }

    }
}
