﻿using HoloBase.Icons.Common;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class FontIconViewerControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private FontIconSymbolWrap viewModel;

        public FontIconViewerControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new FontIconSymbolWrap();

            ResetViewModel(vm);
        }
        private void ResetViewModel(FontIconSymbolWrap viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
            // ????
            UserControlFontIconView.ViewModel = this.viewModel;
            // ????
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (FontIconSymbolWrap) value;
                // ????

                ResetViewModel(vm);
            }
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public string BackgroundGridColor
        {
            get
            {
                return backgroundColor.ARGB;
            }
            set
            {
                backgroundColor = new ColorStruct(value);
                GridFontIconViewerControl.Background = new SolidColorBrush(backgroundColor.Color);
            }
        }

        private ColorStruct iconViewBackgroundColor = new ColorStruct(NamedColor.Navy);
        public string IconViewBackgroundColor
        {
            get
            {
                return iconViewBackgroundColor.ARGB;
            }
            set
            {
                iconViewBackgroundColor = new ColorStruct(value);
                // UserControlFontIconView.Background = new SolidColorBrush(iconViewBackgroundColor.Color);
                UserControlFontIconView.BackgroundGridColor = iconViewBackgroundColor.Text;
            }
        }

        public FontIconSymbol FontIconSymbol
        {
            get
            {
                return viewModel.FontIconSymbol;
            }
            set
            {
                viewModel.FontIconSymbol = value;
            }
        }



        private void ComboBoxFontFamily_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items != null && items.Any()) {
                var selectedItem = (string) items[0];
                System.Diagnostics.Debug.WriteLine("ComboBoxFontFamily_SelectionChanged(): selectedItem = {0}", selectedItem);
                try {
                    // Note the weird expression.
                    // Creating FontIconSymbolWrap updates the isFavorite field....
                    var newIconSymbol = new FontIconSymbolWrap(new FontIconSymbol(selectedItem, viewModel.Glyph)).FontIconSymbol;
                    this.FontIconSymbol = newIconSymbol;
                } catch (Exception) {
                    // ignore
                }
            }
        }

        private void ButtonUpdateGlyph_Click(object sender, RoutedEventArgs e)
        {
            var strGlyph = TextBoxFontGlyph.Text;
            try {
                var glyph = Convert.ToInt32(strGlyph);
                System.Diagnostics.Debug.WriteLine("ButtonUpdateGlyph_Click(): new glyph = {0}", glyph);
                // Note the weird expression.
                // Creating FontIconSymbolWrap updates the isFavorite field....
                var newIconSymbol = new FontIconSymbolWrap(new FontIconSymbol(viewModel.FontFamily, glyph)).FontIconSymbol;
                this.FontIconSymbol = newIconSymbol;
            } catch (Exception) {
                // ignore
            }
        }

        private void UserControlFontIconView_Tapped(object sender, TappedRoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("UserControlFontIconView_Tapped()");

        }
    }
}
