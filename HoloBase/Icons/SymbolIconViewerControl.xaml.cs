﻿using HoloBase.Icons.Common;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class SymbolIconViewerControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private SymbolIconSymbolWrap viewModel;

        public SymbolIconViewerControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new SymbolIconSymbolWrap();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SymbolIconSymbolWrap viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
            // ????
            UserControlSymbolIconView.ViewModel = this.viewModel;
            // ????
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SymbolIconSymbolWrap) value;
                // ????

                ResetViewModel(vm);
            }
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public string BackgroundGridColor
        {
            get
            {
                return backgroundColor.ARGB;
            }
            set
            {
                backgroundColor = new ColorStruct(value);
                GridSymbolIconViewerControl.Background = new SolidColorBrush(backgroundColor.Color);
            }
        }

        private ColorStruct iconViewBackgroundColor = new ColorStruct(NamedColor.Navy);
        public string IconViewBackgroundColor
        {
            get
            {
                return iconViewBackgroundColor.ARGB;
            }
            set
            {
                iconViewBackgroundColor = new ColorStruct(value);
                // UserControlSymbolIconView.Background = new SolidColorBrush(iconViewBackgroundColor.Color);
                UserControlSymbolIconView.BackgroundGridColor = iconViewBackgroundColor.Text;
            }
        }


        public SymbolIconSymbol SymbolIconSymbol
        {
            get
            {
                return viewModel.SymbolIconSymbol;
            }
            set
            {
                viewModel.SymbolIconSymbol = value;
            }
        }



        private void ComboBoxSymbolName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var items = e.AddedItems;
            if (items != null && items.Any()) {
                var selectedItem = (string) items[0];
                System.Diagnostics.Debug.WriteLine("ComboBoxSymbolName_SelectionChanged(): selectedItem = {0}", selectedItem);
                try {
                    var symbol = (Symbol) Enum.Parse(typeof(Symbol), selectedItem);
                    // Note the weird expression.
                    // Creating SymbolIconSymbolWrap updates the isFavorite field....
                    var newIconSymbol = new SymbolIconSymbolWrap(new SymbolIconSymbol(symbol)).SymbolIconSymbol;
                    this.SymbolIconSymbol = newIconSymbol;
                } catch (Exception) {
                    // ignore
                }
            }
        }

        private void UserControlSymbolIconView_Tapped(object sender, TappedRoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("UserControlSymbolIconView_Tapped()");

        }
    }
}
