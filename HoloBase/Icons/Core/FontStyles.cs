﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Core
{
    // ???
    public enum FontStyle
    {
        Unknown = 0,
        Regular, 
        Italic, 
        Bold, 
        LightItalic, 
        BoldItalic,
        Semilight,
        Semibold,
        Light,
        Medium,
        Black,
    }

    public static class FontStyles
    {
    }
}
