﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Core
{
    public enum IconType : short
    {
        Unknown = 0,
        SymbolIcon,
        FontIcon,
    }
}
