﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Core
{
    // http://en.wikipedia.org/wiki/Plane_%28Unicode%29

    public enum UnicodeSymbolBlock
    {
        Unknown = 0,
        CustomRange = 1,
        AllSymbols = 2,     // Union of all symbol blocks below.... (Not being used...)
        // "Symbol" blocks
        GeneralPunctuation = 11,
        SuperscriptsAndSubscripts,
        CurrencySymbols,
        CombiningDiacriticalMarks,
        LetterlikeSymbols,
        NumberForms,
        Arrows,
        MathematicalOperators,
        MiscellaneousTechnical,
        ControlPictures,
        OpticalCharacterRecognition,
        EnclosedAlphanumerics,
        BoxDrawing,
        BlockElements,
        GeometricShapes,
        MiscellaneousSymbols,
        Dingbats,
        MiscellaneousMathematicalSymbolsA,
        SupplementalArrowsA,
        BraillePatterns,
        SupplementalArrowsB,
        MiscellaneousMathematicalSymbolsB,
        SupplementalMathematicalOperators,
        MiscellaneousSymbolsAndArrows,

    }

    public static class UnicodeSymbolBlocks
    {
        // temporary
        public static bool IsValidUnicodeSymbolBlock(UnicodeSymbolBlock usb)
        {
            //return (usb != UnicodeSymbolBlock.Unknown
            //    && usb != UnicodeSymbolBlock.CustomRange
            //    && usb != UnicodeSymbolBlock.AllSymbols);
            return (usb != UnicodeSymbolBlock.Unknown
                && usb != UnicodeSymbolBlock.CustomRange);
        }
    }

}
