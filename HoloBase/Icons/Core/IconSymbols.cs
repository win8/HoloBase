﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace HoloBase.Icons.Core
{
    // TBD:
    public interface ISymbol
    {
        string FontFamily
        {
            get;
        }
        int Glyph
        {
            get;
        }
    }
    public interface IIconSymbol : ISymbol
    {
        string Name
        {
            get;
        }
        bool IsFavorite
        {
            get;
        }
    }
    public interface ISymbolIconSymbol : IIconSymbol
    {
        Symbol Symbol
        {
            get;
        }
    }
    public interface IFontIconSymbol : IIconSymbol
    {
    }
}
