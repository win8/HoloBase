﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Core
{
    public enum FontSymbolSize
    {
        Unknown = 0,
        // Tiny = 1,
        Small = 2,
        Medium,
        Large,
        // ExtraLarge,
    }

    public static class FontSymbolSizes
    {
        private static readonly ISet<FontSymbolSize> sizeSet = new HashSet<FontSymbolSize>();
        private static readonly IList<FontSymbolSize> sizeList = new List<FontSymbolSize>();
        static FontSymbolSizes()
        {
            sizeSet.Add(FontSymbolSize.Small);
            sizeSet.Add(FontSymbolSize.Medium);
            sizeSet.Add(FontSymbolSize.Large);

            sizeList.Add(FontSymbolSize.Small);
            sizeList.Add(FontSymbolSize.Medium);
            sizeList.Add(FontSymbolSize.Large);
        }

        public static ISet<FontSymbolSize> Set
        {
            get
            {
                return sizeSet;
            }
        }
        public static IList<FontSymbolSize> List
        {
            get
            {
                //return new List<FontSymbolSize>(sizeSet);
                return sizeList;
            }
        }

        public static bool IsValid(FontSymbolSize size)
        {
             return sizeSet.Contains(size);
        }
    }
}
