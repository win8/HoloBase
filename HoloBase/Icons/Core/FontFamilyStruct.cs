﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Icons.Core
{
    public struct FontFamilyStruct
    {
        private string name;
        private FontStyle[] styles;  // Available styles.

        public FontFamilyStruct(string name, FontStyle[] styles = null)
        {
            this.name = name;
            this.styles = styles;   // null means it's not set.
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public FontStyle[] Styles
        {
            get
            {
                return styles;
            }
            set
            {
                styles = value;
            }
        }
        
        public bool IsStyleAvailable(FontStyle style)
        {
            return (this.Styles != null && this.Styles.Contains(style));
        }

        // To be used in a combobox/list, etc.
        public override string ToString()
        {
            return this.Name;
        }

    }

    public sealed class FontFamilyWrap
    {
        private FontFamilyStruct fontFamilyStruct;

        public FontFamilyWrap(FontFamilyStruct fontFamilyStruct)
        {
            this.fontFamilyStruct = fontFamilyStruct;
        }

        public FontFamilyStruct FontFamilyStruct
        {
            get
            {
                return fontFamilyStruct;
            }
        }

        public string Name
        {
            get
            {
                return fontFamilyStruct.Name;
            }
        }
        public FontStyle[] Styles
        {
            get
            {
                return fontFamilyStruct.Styles;
            }
        }

        public bool IsStyleAvailable(FontStyle style)
        {
            return fontFamilyStruct.IsStyleAvailable(style);
        }

        public override string ToString()
        {
            return fontFamilyStruct.ToString();
        }

    }

}
