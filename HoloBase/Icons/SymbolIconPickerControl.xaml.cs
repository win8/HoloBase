﻿using HoloBase.Icons.Common;
using HoloBase.Icons.UI;
using HoloBase.Icons.ViewModels;
using HoloCore.Core;
using HoloBase.UI.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HoloBase.UI.Colors;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Icons
{
    public sealed partial class SymbolIconPickerControl : UserControl, IRefreshableElement, IViewModelHolder, ISymbolIconPickerControl
    {
        // Public event for the symbol picker clients.
        public event EventHandler<SymbolIconPickerEventArgs> SymbolIconSymbolSelectionChanged;

        private SymbolIconListViewModel viewModel;

        public SymbolIconPickerControl()
        {
            this.InitializeComponent();

            ResetViewModel();
            //SortListItems();

            isCancelButtonVisible = true;
            isSortButtonsVisible = true;
            RefreshElementVisibility();
        }
        private void ResetViewModel()
        {
            var vm = new SymbolIconListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(SymbolIconListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (SymbolIconListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }


        // tbd:
        private bool isSortButtonsVisible;
        public bool IsSortButtonsVisible
        {
            get
            {
                return isSortButtonsVisible;
            }
            set
            {
                isSortButtonsVisible = value;
                RefreshElementVisibility();
            }
        }

        private bool isCancelButtonVisible;
        public bool IsCancelButtonVisible
        {
            get
            {
                return isCancelButtonVisible;
            }
            set
            {
                isCancelButtonVisible = value;
                RefreshElementVisibility();
            }
        }
        private void RefreshElementVisibility()
        {
            if (isSortButtonsVisible) {
                StackPanelTopRow.Visibility = Visibility.Visible;
            } else {
                StackPanelTopRow.Visibility = Visibility.Collapsed;
            }
            if (isCancelButtonVisible) {
                StackPanelBottomRow.Visibility = Visibility.Visible;
            } else {
                StackPanelBottomRow.Visibility = Visibility.Collapsed;
            }
        }



        private void NotifySymbolSelectionChange(SymbolIconSymbol symbol)
        {
            // temporary
            if (SymbolIconSymbolSelectionChanged != null) {
                var e = new SymbolIconPickerEventArgs(this, symbol);
                // SymbolIconSymbolSelectionChanged(this, e);
                SymbolIconSymbolSelectionChanged.Invoke(this, e);
            }
        }



        private void ButtonSortByName_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByName();
        }

        private void ButtonSortBySymbol_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortBySymbol();
        }


        private void DismissPopup()
        {
            // tbd:
            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DismissPopup();
        }

        private void ListViewSymbolList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = (SymbolIconSymbol) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("Symbol selected: {0}", clickedItem);
            NotifySymbolSelectionChange(clickedItem);

            DismissPopup();
        }
  
    
    }
}
