﻿using HoloBase.Controls.Core;
using HoloBase.Icons.Common;
using HoloBase.Icons.Core;
using HoloBase.Icons.ViewModels;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HoloBase.Icons
{
    public sealed partial class IconClipboardControl : UserControl, IRefreshableElement, IViewModelHolder, ITextClipSourceControl, ICharClipSourceControl
    {
        public event EventHandler<TextClipEventArgs> TextClipAvailable;
        public event EventHandler<CharClipEventArgs> CharClipAvailable;

        // temporary
        private Size currentControlSize = Size.Empty;

        private IconClipboardViewModel viewModel;
        private string visualStateName;    // tbd: use enum???

        public IconClipboardControl()
        {
            this.InitializeComponent();
            ResetViewModel();

            isCompactForAppBarButtons = false;
            AppBarButtonAddSymbol.Visibility = Visibility.Visible;

            visualStateName = "VisualStateNormal";
            this.SizeChanged += IconClipboardControl_SizeChanged;
        }
        void IconClipboardControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            currentControlSize = e.NewSize;
            System.Diagnostics.Debug.WriteLine("IconClipboardControl_SizeChanged() currentControlSize = {0}", currentControlSize);

            // TBD:
            // It is hard to tweak this layouts...
            // Currently, it's kind of weired...
            // Between 560 and 620, a few buttons disappear (and, below 300)....
            // ....

            if (currentControlSize.Width > 560) {   // 560 shoudl be the same across all "listing" controls, including symbol icon and font icon list, etc.
                IsCompactForAppBarButtons = false;
            } else {
                IsCompactForAppBarButtons = true;
            }

            var threshold = 620;
            if (isCompactForAppBarButtons) {   // Heck. We assume isCompactForAppBarButtons == true means it's phone. (In our current deployment, it's true.)
                // threshold = 480;
                threshold = 365;
            }
            if (currentControlSize.Width > threshold) {         // threshold Arbitrary
                visualStateName = "VisualStateMediumAndWide";   // the rest.
            } else {
                visualStateName = "VisualStateNormal";          // Phone/Portrait
            }
            // System.Diagnostics.Debug.WriteLine("visualStateName = {0}", visualStateName);

            var suc = VisualStateManager.GoToState(this, visualStateName, false);
            System.Diagnostics.Debug.WriteLine("GoToState(): {0}. Succeeded = {1}", visualStateName, suc);
        }

        private void ResetViewModel()
        {
            var vm = new IconClipboardViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(IconClipboardViewModel viewModel)
        {
            this.viewModel = viewModel;
            // this.viewModel.UseFavoriteList = useFavoriteList;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (IconClipboardViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }


        // --> SelectAllAsString()
        //public string TextString
        //{
        //    get
        //    {
        //        return viewModel.TextString;
        //    }
        //}

        private bool isCompactForAppBarButtons = false;
        public bool IsCompactForAppBarButtons
        {
            get
            {
                return isCompactForAppBarButtons;
            }
            set
            {
                if (isCompactForAppBarButtons != value) {
                    isCompactForAppBarButtons = value;
                    AppBarButtonAddSymbol.IsCompact = isCompactForAppBarButtons;
                    AppBarButtonCopyText.IsCompact = isCompactForAppBarButtons;
                    AppBarButtonPeekSymbol.IsCompact = isCompactForAppBarButtons;
                    // AppBarButtonUseSymbol.IsCompact = isCompactForAppBarButtons;
                    AppBarButtonPopSymbol.IsCompact = isCompactForAppBarButtons;
                    AppBarButtonClearList.IsCompact = isCompactForAppBarButtons;
                }
            }
        }

        public bool IsButtonAddSymbolDisplayed
        {
            get
            {
                return (AppBarButtonAddSymbol.Visibility == Visibility.Visible);
            }
            set
            {
                if(value == true) {
                    AppBarButtonAddSymbol.Visibility = Visibility.Visible;
                } else {
                    AppBarButtonAddSymbol.Visibility = Visibility.Collapsed;
                }
            }
        }

        public string BackgroundColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }


        public string SelectAllAsString()
        {
            return viewModel.TextString;
        }

        private void AppBarButtonCopyText_Click(object sender, RoutedEventArgs e)
        {
            var text = viewModel.TextString;
            NotifyTextClipAvailable(text);
        }

        private void AppBarButtonPeekSymbol_Click(object sender, RoutedEventArgs e)
        {
            //var count = viewModel.Count;
            //System.Diagnostics.Debug.WriteLine("symbolStack.Count = {0}", count);

            var symbol = viewModel.LastSymbol;
            ProcessSymbolSelected(symbol);
        }

        private void AppBarButtonUseSymbol_Click(object sender, RoutedEventArgs e)
        {
            // Use and discard.
            var symbol = viewModel.PopSymbol();
            ProcessSymbolSelected(symbol);
        }

        private void AppBarButtonPopSymbol_Click(object sender, RoutedEventArgs e)
        {
            var symbol = viewModel.PopSymbol();
            System.Diagnostics.Debug.WriteLine("Symbol discarded: {0}", symbol);
        }

        private void AppBarButtonClearList_Click(object sender, RoutedEventArgs e)
        {
            viewModel.ClearList();
        }

        private void GridViewSymbolClipList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = (ISymbol) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("Symbol clip selected: {0}", clickedItem);
            ProcessSymbolSelected(clickedItem);
        }
        private void ProcessSymbolSelected(ISymbol symbol)
        {
            if (symbol != null) {
                System.Diagnostics.Debug.WriteLine("ProcessSymbolSelected(): symbol = {0}", symbol);
                var glyph = symbol.Glyph;
                // ???
                var c = FontIconSymbols.GlyphChar(glyph);
                NotifyCharClipAvailable(c);
                //// var text = FontIconSymbols.GlyphText(glyph);
                //var text = viewModel.TextString;
                //NotifyTextClipAvailable(text);
            }
        }



        private void NotifyCharClipAvailable(char charClip)
        {
            // temporary
            if (CharClipAvailable != null) {
                var e = new CharClipEventArgs(this, charClip);
                // CharClipAvailable(this, e);
                CharClipAvailable.Invoke(this, e);
            }
        }
        private void NotifyTextClipAvailable(string textClip)
        {
            // temporary
            if (TextClipAvailable != null) {
                var e = new TextClipEventArgs(this, textClip);
                // TextClipAvailable(this, e);
                TextClipAvailable.Invoke(this, e);
            }
        }

        private void ButtonFlyoutAccept_Click(object sender, RoutedEventArgs e)
        {
            var text = TextBoxGlyph.Text;
            if (!String.IsNullOrEmpty(text)) {
                var isHex = false;
                if (text.StartsWith("0x") || text.StartsWith("x")) {
                    var idx = text.IndexOf("x");
                    text = text.Substring(idx + 1, text.Length - idx - 1);
                    isHex = true;
                }
                var glyph = 0;
                if (isHex) {
                    glyph = FontIconSymbols.GlyphFromHexCode(text);
                } else {
                    glyph = FontIconSymbols.GlyphFromString(text);
                }
                System.Diagnostics.Debug.WriteLine(">>>> glyph = {0}", glyph);

                if (glyph > 0) {
                    var ff = (FontFamilyStruct) ComboBoxFontFamily.SelectedItem;
                    var ffName = ff.Name;
                    var symbolClip = new SymbolClipWrap(IconType.FontIcon, ffName, glyph);
                    viewModel.PushSymbol(symbolClip);
                }
            }
            AppBarButtonAddSymbol.Flyout.Hide();
        }

        private void ButtonFlyoutCancel_Click(object sender, RoutedEventArgs e)
        {
            AppBarButtonAddSymbol.Flyout.Hide();
        }

    }
}
