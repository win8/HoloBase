﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Auth.Core
{
    // Note:
    // We distinguish the following two states:
    // Authenticated: the user is identified (e.g., we know the user's name or email address, etc.)
    //                (note: user might have multiple identities on a device)
    // Authorized:  User has logged on (e.g., with password, or via third party auth service).
    // ...
    // Authorized may expire after a certin preset time duration (e.g. to Authenticated state)
    // but, Authenticated may not normally expire (unless the user explicitly unchecks "remember me").
    // ..
    public enum AuthState : short
    {
        Unknown = 0,
        Unidentified = 1,
        // IdentifiedOnly,   // ???
        Authenticated = 2,
        // LoggedOn,
        Authorized = 4,
        // ...
    }

    public static class AuthStates
    {



        public static bool IsUserIdentified(AuthState authState)
        {
            return (authState == AuthState.Authenticated || authState == AuthState.Authorized);
        }
    }

}
