﻿using HoloBase.Controls.Core;
using HoloBase.Controls.ViewModels;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HoloBase.Controls
{
    // http://blog.jerrynixon.com/2013/07/solved-two-way-binding-inside-user.html
    // Walkthrough: Two-way binding inside a XAML User Control 
    // http://blogs.msdn.com/b/mim/archive/2013/03/19/create-a-custom-user-control-using-xaml-and-c-for-windows-8.aspx
    // Create a custom user control using Xaml and C# for Windows 8

    public sealed partial class DoubleSliderControl : UserControl, IDoubleSliderControl   // , IViewModelHolder
    {
        // private DoubleSliderViewModel viewModel;

        public event EventHandler<ValueRangeEventArgs> ValueRangeChanged;

        //private double upperAllowedMinimum;
        //private double lowerAllowedMaximum;
        
        public DoubleSliderControl()
        {
            this.InitializeComponent();
            //ResetViewModel();
        }

        //private void ResetViewModel()
        //{
        //    var vm = new DoubleSliderViewModel();
        //    ResetViewModel(vm);
        //}
        //private void ResetViewModel(DoubleSliderViewModel viewModel)
        //{
        //    this.viewModel = viewModel;
        //    // this.DataContext = this.viewModel;
        //    ((FrameworkElement) this.Content).DataContext = this.viewModel;
        //}
        //public void RefreshDataAndUI()
        //{
        //}
        //public IViewModel ViewModel
        //{
        //    get
        //    {
        //        return viewModel;
        //    }
        //    set
        //    {
        //        var vm = (DoubleSliderViewModel) value;
        //        ResetViewModel(vm);
        //    }
        //}



        ///////////////////////////////////////////////////////////////////////
        // Generic control properties...

        public Brush BackgroundColor
        {
            get
            {
                return GridLayoutRoot.Background;
            }
            set
            {
                GridLayoutRoot.Background = value;
            }
        }


        ///////////////////////////////////////////////////////////////////////
        // Note that "lower"/"upper" refer to the lower/uppers values of the range,
        //    not lower/upper position of the slider placements.

        public string LowerLabelText
        {
            get
            {
                return TextBlockLabelA.Text;
            }
            set
            {
                TextBlockLabelA.Text = value;
            }
        }
        public string UpperLabelText
        {
            get
            {
                return TextBlockLabelB.Text;
            }
            set
            {
                TextBlockLabelB.Text = value;
            }
        }

        //public double LowerLabelFontSize
        //{
        //    get
        //    {
        //        return TextBlockLabelA.FontSize;
        //    }
        //    set
        //    {
        //        TextBlockLabelA.FontSize = value;
        //    }
        //}
        //public double UpperLabelFontSize
        //{
        //    get
        //    {
        //        return TextBlockLabelB.FontSize;
        //    }
        //    set
        //    {
        //        TextBlockLabelB.FontSize = value;
        //    }
        //}

        //public FontWeight LowerLabelFontWeight
        //{
        //    get
        //    {
        //        return TextBlockLabelA.FontWeight;
        //    }
        //    set
        //    {
        //        TextBlockLabelA.FontWeight = value;
        //    }
        //}
        //public FontWeight UpperLabelFontWeight
        //{
        //    get
        //    {
        //        return TextBlockLabelB.FontWeight;
        //    }
        //    set
        //    {
        //        TextBlockLabelB.FontWeight = value;
        //    }
        //}

        public double LabelFontSize
        {
            get
            {
                // ???
                return Math.Min(TextBlockLabelA.FontSize, TextBlockLabelB.FontSize);
            }
            set
            {
                TextBlockLabelA.FontSize = value;
                TextBlockLabelB.FontSize = value;
            }
        }

        public FontWeight LabelFontWeight
        {
            get
            {
                // ???
                return TextBlockLabelA.FontWeight;
            }
            set
            {
                TextBlockLabelA.FontWeight = value;
                TextBlockLabelB.FontWeight = value;
            }
        }


        ///////////////////////////////////////////////////////////////////////
        // Note that SliderA and SliderB should have the same Minimum/Maximum.


        public double Minimum
        {
            get
            {
                return Math.Min(SliderA.Minimum, SliderB.Minimum);
            }
            set
            {
                SliderA.Minimum = value;
                SliderB.Minimum = value;
            }
        }
        public double Maximum
        {
            get
            {
                return Math.Max(SliderA.Maximum, SliderB.Maximum);
            }
            set
            {
                SliderA.Maximum = value;
                SliderB.Maximum = value;
            }
        }

        //public double LowerValue
        //{
        //    get
        //    {
        //        return SliderA.Value;
        //    }
        //    set
        //    {
        //        if (SliderB.Value < value) {
        //            value = SliderB.Value;
        //        }
        //        SliderA.Value = value;
        //        NotifyValueRangeChange(value, UpperValue);
        //    }
        //}
        public double LowerValue
        {
            get
            {
                return (double) GetValue(LowerValueProperty);
            }
            set
            {
                SetValue(LowerValueProperty, value);
                SliderA.Value = value;
                NotifyValueRangeChange(value, UpperValue);
            }
        }
        public static readonly DependencyProperty LowerValueProperty =
            DependencyProperty.Register("LowerValue", typeof(double), typeof(DoubleSliderControl), null);

        //public double UpperValue
        //{
        //    get
        //    {
        //        return SliderB.Value;
        //    }
        //    set
        //    {
        //        if (SliderA.Value > value) {
        //            value = SliderA.Value;
        //        }
        //        SliderB.Value = value;
        //        NotifyValueRangeChange(LowerValue, value);
        //    }
        //}
        public double UpperValue
        {
            get
            {
                return (double) GetValue(UpperValueProperty);
            }
            set
            {
                SetValue(UpperValueProperty, value);
                SliderB.Value = value;
                NotifyValueRangeChange(LowerValue, value);
            }
        }
        public static readonly DependencyProperty UpperValueProperty =
            DependencyProperty.Register("UpperValue", typeof(double), typeof(DoubleSliderControl), null);

        private void SliderA_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            var newValue = e.NewValue;
            if (SliderB.Value < newValue) {
                newValue = SliderB.Value;
            }
            LowerValue = newValue;
            // SliderA.Value = newValue;
        }

        private void SliderB_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            var newValue = e.NewValue;
            if (SliderA.Value > newValue) {
                newValue = SliderA.Value;
            }
            UpperValue = newValue;
            // SliderB.Value = newValue;
        }


        public double TickFrequency
        {
            get
            {
                return Math.Min(SliderA.TickFrequency, SliderB.TickFrequency);
            }
            set
            {
                SliderA.TickFrequency = value;
                SliderB.TickFrequency = value;
            }
        }


        // tbd.

        // TickPlacement
        // etc...




        private void NotifyValueRangeChange()
        {
            NotifyValueRangeChange(LowerValue, UpperValue);
        }
        private void NotifyValueRangeChange(double lowerValue, double upperValue)
        {
            ValueRangeStruct valueRange = new ValueRangeStruct(lowerValue, upperValue);
            NotifyValueRangeChange(valueRange);
        }
        private void NotifyValueRangeChange(ValueRangeStruct valueRange)
        {
            // temporary
            if (ValueRangeChanged != null) {
                var e = new ValueRangeEventArgs(this, valueRange);
                // ValueRangeChanged(this, e);
                ValueRangeChanged.Invoke(this, e);
            }
        }
    }
}
