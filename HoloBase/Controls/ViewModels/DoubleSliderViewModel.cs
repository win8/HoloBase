﻿using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;


namespace HoloBase.Controls.ViewModels
{
    // Not being used...
    public class DoubleSliderViewModel : IViewModel, INotifyPropertyChanged
    {
        public DoubleSliderViewModel()
        {
        }


        // Testing...

        // http://blog.jerrynixon.com/2013/07/solved-two-way-binding-inside-user.html
        // Walkthrough: Two-way binding inside a XAML User Control 
        // http://blogs.msdn.com/b/mim/archive/2013/03/19/create-a-custom-user-control-using-xaml-and-c-for-windows-8.aspx
        // Create a custom user control using Xaml and C# for Windows 8
        public static readonly DependencyProperty LowerValueProperty =
            DependencyProperty.Register("LowerValue", typeof(double), typeof(DoubleSliderControl), null);


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
