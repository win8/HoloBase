﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Controls.Core
{
    public class ValueRangeEventArgs : EventArgs
    {
        private ValueRangeStruct valueRange;
        public ValueRangeEventArgs(object sender, ValueRangeStruct valueRange)
        {
            this.valueRange = valueRange;
        }

        public ValueRangeStruct ValueRange
        {
            get
            {
                return valueRange;
            }
            set
            {
                valueRange = value;
            }
        }

    }
}
