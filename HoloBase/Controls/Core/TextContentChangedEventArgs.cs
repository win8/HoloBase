﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Controls.Core
{
    public class TextContentChangedEventArgs : EventArgs
    {
        private string textContent;
        public TextContentChangedEventArgs(object sender, string textContent)
        {
            this.textContent = textContent;
        }

        public string NewTextContent
        {
            get
            {
                return textContent;
            }
            set
            {
                textContent = value;
            }
        }

    }
}
