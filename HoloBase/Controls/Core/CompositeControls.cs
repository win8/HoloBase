﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Controls.Core
{
    public interface IDoubleSliderControl
    {
        event EventHandler<ValueRangeEventArgs> ValueRangeChanged;
    }

}
