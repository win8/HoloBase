﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Controls.Core
{
    public class CharClipEventArgs : EventArgs
    {
        private char charClip;
        public CharClipEventArgs(object sender, char charClip)
        {
            this.charClip = charClip;
        }

        public char CharClip
        {
            get
            {
                return charClip;
            }
            set
            {
                charClip = value;
            }
        }

    }
}
