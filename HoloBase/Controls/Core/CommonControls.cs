﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Controls.Core
{
    public interface ITextClipSourceControl
    {
        event EventHandler<TextClipEventArgs> TextClipAvailable;
    }
    public interface ICharClipSourceControl
    {
        event EventHandler<CharClipEventArgs> CharClipAvailable;
    }

    public interface ITextContentContainerControl
    {
        event EventHandler<TextContentChangedEventArgs> TextContentChanged;
    }
}
