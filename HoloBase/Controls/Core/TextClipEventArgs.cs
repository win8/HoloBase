﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Controls.Core
{
    public class TextClipEventArgs : EventArgs
    {
        private string textClip;
        public TextClipEventArgs(object sender, string textClip)
        {
            this.textClip = textClip;
        }

        public string TextClip
        {
            get
            {
                return textClip;
            }
            set
            {
                textClip = value;
            }
        }

    }
}
