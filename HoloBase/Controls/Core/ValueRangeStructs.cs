﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Controls.Core
{
    public struct ValueRangeStruct
    {
        private double lowerValue;
        private double upperValue;

        public ValueRangeStruct(double lowerValue, double upperValue)
        {
            this.lowerValue = lowerValue;
            this.upperValue = upperValue;
        }

        public double LowerValue
        {
            get
            {
                return lowerValue;
            }
            set
            {
                lowerValue = value;
            }
        }
        public double UpperValue
        {
            get
            {
                return upperValue;
            }
            set
            {
                upperValue = value;
            }
        }

        public override string ToString()
        {
            var str = "(" + LowerValue + ":" + UpperValue + ")";
            return str;
        }
    }
}
