﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.DataProtection;
using Windows.Storage;
using Windows.Storage.Streams;


namespace HoloBase.Crypto
{
    // Note:
    // Based on the sample code in
    // http://msdn.microsoft.com/en-us/library/windows/apps/windows.security.cryptography.dataprotection.dataprotectionprovider.aspx
    // ..
    // Also, note
    // File access and permissions (Windows Runtime apps):
    // http://msdn.microsoft.com/en-us/library/windows/apps/Hh967755.aspx
    // ...
    public sealed class DataProtectHelper
    {
        private static DataProtectHelper instance = null;
        public static DataProtectHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new DataProtectHelper();
                }
                return instance;
            }
        }
        private DataProtectHelper()
        {
        }

        public async Task<IBuffer> EncryptDataAsync(
            String plainText,
            String descriptor,
            BinaryStringEncoding encoding)
        {
            // Create a DataProtectionProvider object for the specified descriptor.
            DataProtectionProvider Provider = new DataProtectionProvider(descriptor);

            // Encode the plaintext input message to a buffer.
            encoding = BinaryStringEncoding.Utf8;
            IBuffer buffPlain = CryptographicBuffer.ConvertStringToBinary(plainText, encoding);

            // Encrypt the message.
            IBuffer buffProtected = await Provider.ProtectAsync(buffPlain);

            // Execution of the SampleProtectAsync function resumes here
            // after the awaited task (Provider.ProtectAsync) completes.
            return buffProtected;
        }
        public async Task<string> ProtectDataAsync(
            String plainText,
            String descriptor,
            BinaryStringEncoding encoding)
        {
            var buffEncrypted = await EncryptDataAsync(plainText, descriptor, encoding);
            var textProtected = CryptographicBuffer.EncodeToBase64String(buffEncrypted);
            return textProtected;
        }


        public async Task<string> DecryptDataAsync(
            IBuffer buffEncrypted,
            BinaryStringEncoding encoding)
        {
            // Create a DataProtectionProvider object.
            DataProtectionProvider Provider = new DataProtectionProvider();

            // Decrypt the protected message specified on input.
            IBuffer buffDecrypted = await Provider.UnprotectAsync(buffEncrypted);

            // Execution of the SampleUnprotectData method resumes here
            // after the awaited task (Provider.UnprotectAsync) completes
            // Convert the unprotected message from an IBuffer object to a string.
            string clearText = CryptographicBuffer.ConvertBinaryToString(encoding, buffDecrypted);

            // Return the plaintext string.
            return clearText;
        }
        public async Task<string> UnprotectDataAsync(
            string textProtected,
            BinaryStringEncoding encoding)
        {
            IBuffer buffEncrypted = CryptographicBuffer.DecodeFromBase64String(textProtected);
            string clearText = await DecryptDataAsync(buffEncrypted, encoding);
            return clearText;
        }




        public async Task<IBuffer> EncryptDataStreamAsync(
            String clearText,
            String descriptor,
            BinaryStringEncoding encoding)
        {
            // Create a DataProtectionProvider object for the specified descriptor.
            DataProtectionProvider Provider = new DataProtectionProvider(descriptor);

            // Convert the input string to a buffer.
            IBuffer buffMsg = CryptographicBuffer.ConvertStringToBinary(clearText, encoding);

            // Create a random access stream to contain the plaintext message.
            InMemoryRandomAccessStream inputData = new InMemoryRandomAccessStream();

            // Create a random access stream to contain the encrypted message.
            InMemoryRandomAccessStream protectedData = new InMemoryRandomAccessStream();

            // Retrieve an IOutputStream object and fill it with the input (plaintext) data.
            IOutputStream outputStream = inputData.GetOutputStreamAt(0);
            DataWriter writer = new DataWriter(outputStream);
            writer.WriteBuffer(buffMsg);
            await writer.StoreAsync();
            await outputStream.FlushAsync();

            // Retrieve an IInputStream object from which you can read the input data.
            IInputStream source = inputData.GetInputStreamAt(0);

            // Retrieve an IOutputStream object and fill it with encrypted data.
            IOutputStream dest = protectedData.GetOutputStreamAt(0);
            await Provider.ProtectStreamAsync(source, dest);
            await dest.FlushAsync();

            //Verify that the protected data does not match the original
            DataReader reader1 = new DataReader(inputData.GetInputStreamAt(0));
            DataReader reader2 = new DataReader(protectedData.GetInputStreamAt(0));
            await reader1.LoadAsync((uint) inputData.Size);
            await reader2.LoadAsync((uint) protectedData.Size);
            IBuffer buffOriginalData = reader1.ReadBuffer((uint) inputData.Size);
            IBuffer buffProtectedData = reader2.ReadBuffer((uint) protectedData.Size);

            if (CryptographicBuffer.Compare(buffOriginalData, buffProtectedData)) {
                throw new Exception("EncryptDataStreamAsync returned unprotected data");
            }

            // Return the encrypted data.
            return buffProtectedData;
        }

        public async Task<String> DecryptDataStreamAsync(
            IBuffer buffProtected,
            BinaryStringEncoding encoding)
        {
            // Create a DataProtectionProvider object.
            DataProtectionProvider Provider = new DataProtectionProvider();

            // Create a random access stream to contain the encrypted message.
            InMemoryRandomAccessStream inputData = new InMemoryRandomAccessStream();

            // Create a random access stream to contain the decrypted data.
            InMemoryRandomAccessStream unprotectedData = new InMemoryRandomAccessStream();

            // Retrieve an IOutputStream object and fill it with the input (encrypted) data.
            IOutputStream outputStream = inputData.GetOutputStreamAt(0);
            DataWriter writer = new DataWriter(outputStream);
            writer.WriteBuffer(buffProtected);
            await writer.StoreAsync();
            await outputStream.FlushAsync();

            // Retrieve an IInputStream object from which you can read the input (encrypted) data.
            IInputStream source = inputData.GetInputStreamAt(0);

            // Retrieve an IOutputStream object and fill it with decrypted data.
            IOutputStream dest = unprotectedData.GetOutputStreamAt(0);
            await Provider.UnprotectStreamAsync(source, dest);
            await dest.FlushAsync();

            // Write the decrypted data to an IBuffer object.
            DataReader reader2 = new DataReader(unprotectedData.GetInputStreamAt(0));
            await reader2.LoadAsync((uint) unprotectedData.Size);
            IBuffer buffUnprotectedData = reader2.ReadBuffer((uint) unprotectedData.Size);

            // Convert the IBuffer object to a string using the same encoding that was
            // used previously to conver the plaintext string (before encryption) to an
            // IBuffer object.
            String strUnprotected = CryptographicBuffer.ConvertBinaryToString(encoding, buffUnprotectedData);

            // Return the decrypted data.
            return strUnprotected;
        }


        [Obsolete]
        public async Task EncryptDataAsync(
            string inputFilePath,      // plain text
            string outputFilePath,     // cypher text
            string descriptor,
            BinaryStringEncoding encoding)
        {
            System.Diagnostics.Debug.WriteLine("EncryptDataAsync() called.");

            var inputFile = await StorageFile.GetFileFromPathAsync(inputFilePath);
            if (inputFile == null) {
                System.Diagnostics.Debug.WriteLine("Failed to open input file: " + inputFilePath);
            }

            // var outputFile = await StorageFile.GetFileFromPathAsync(outputFilePath);   // Create ???
            // var outputFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(outputFileName, CreationCollisionOption.FailIfExists);
            var outputFile = await StorageFile.CreateStreamedFileAsync(outputFilePath, null, null);
            if (outputFile == null) {
                System.Diagnostics.Debug.WriteLine("Failed to create output file: " + outputFilePath);
            }

            await EncryptDataAsync(inputFile, outputFile, descriptor, encoding);
        }
        public async Task EncryptDataAsync(
            IStorageFile inputFile,      // plain text
            IStorageFile outputFile,     // cypher text
            string descriptor,
            BinaryStringEncoding encoding)
        {
            var clearText = await FileIO.ReadTextAsync(inputFile);
            IBuffer buffProtectedData = await EncryptDataStreamAsync(clearText, descriptor, encoding);
            await FileIO.WriteBufferAsync(outputFile, buffProtectedData);
        }

        [Obsolete]
        public async Task ProtectDataAsync(
            string inputFilePath,      // plain text
            string outputFilePath,     // cypher text
            string descriptor,
            BinaryStringEncoding encoding)
        {
            System.Diagnostics.Debug.WriteLine("ProtectDataAsync() called.");

            // Note: This requires inputFilePath is in the UNC path format.
            var inputFile = await StorageFile.GetFileFromPathAsync(inputFilePath);
            if (inputFile == null) {
                System.Diagnostics.Debug.WriteLine("Failed to open input file: " + inputFilePath);
            }

            // var outputFile = await StorageFile.GetFileFromPathAsync(outputFilePath);   // Create ???
            // var outputFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(outputFileName, CreationCollisionOption.FailIfExists);
            var outputFile = await StorageFile.CreateStreamedFileAsync(outputFilePath, null, null);
            if (outputFile == null) {
                System.Diagnostics.Debug.WriteLine("Failed to create output file: " + outputFilePath);
            }

            await ProtectDataAsync(inputFile, outputFile, descriptor, encoding);
        }
        public async Task ProtectDataAsync(
            IStorageFile inputFile,      // plain text
            IStorageFile outputFile,     // cypher text
            string descriptor,
            BinaryStringEncoding encoding)
        {
            var clearText = await FileIO.ReadTextAsync(inputFile);
            IBuffer buffProtectedData = await EncryptDataStreamAsync(clearText, descriptor, encoding);
            var cypherText = CryptographicBuffer.EncodeToBase64String(buffProtectedData);
            System.Diagnostics.Debug.WriteLine("cypherText = " + cypherText);
            await FileIO.WriteTextAsync(outputFile, cypherText);
        }


        [Obsolete]
        public async Task DecryptDataAsync(
            string inputFilePath,      // cypher text
            string outputFilePath,     // plain text
            BinaryStringEncoding encoding)
        {
            System.Diagnostics.Debug.WriteLine("DecryptDataAsync() called.");

            var inputFile = await StorageFile.GetFileFromPathAsync(inputFilePath);
            if (inputFile == null) {
                System.Diagnostics.Debug.WriteLine("Failed to open input file: " + inputFilePath);
            }

            // var outputFile = await StorageFile.GetFileFromPathAsync(outputFilePath);   // Create ???
            // var outputFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(outputFileName, CreationCollisionOption.FailIfExists);
            var outputFile = await StorageFile.CreateStreamedFileAsync(outputFilePath, null, null);
            if (outputFile == null) {
                System.Diagnostics.Debug.WriteLine("Failed to create output file: " + outputFilePath);
            }

            await DecryptDataAsync(inputFile, outputFile, encoding);
        }
        public async Task DecryptDataAsync(
            IStorageFile inputFile,      // plain text
            IStorageFile outputFile,     // cypher text
            BinaryStringEncoding encoding)
        {
            IBuffer buffProtected = await FileIO.ReadBufferAsync(inputFile);
            String strUnprotected = await DecryptDataStreamAsync(buffProtected, encoding);
            await FileIO.WriteTextAsync(outputFile, strUnprotected);
        }

        [Obsolete]
        public async Task UnprotectDataAsync(
            string inputFilePath,      // cypher text
            string outputFilePath,     // plain text
            BinaryStringEncoding encoding)
        {
            System.Diagnostics.Debug.WriteLine("UnprotectDataAsync() called.");

            var inputFile = await StorageFile.GetFileFromPathAsync(inputFilePath);
            if (inputFile == null) {
                System.Diagnostics.Debug.WriteLine("Failed to open input file: " + inputFilePath);
            }

            // var outputFile = await StorageFile.GetFileFromPathAsync(outputFilePath);   // Create ???
            // var outputFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(outputFileName, CreationCollisionOption.FailIfExists);
            var outputFile = await StorageFile.CreateStreamedFileAsync(outputFilePath, null, null);
            if (outputFile == null) {
                System.Diagnostics.Debug.WriteLine("Failed to create output file: " + outputFilePath);
            }

            await UnprotectDataAsync(inputFile, outputFile, encoding);
        }
        public async Task UnprotectDataAsync(
            IStorageFile inputFile,      // plain text
            IStorageFile outputFile,     // cypher text
            BinaryStringEncoding encoding)
        {
            var cypherText = await FileIO.ReadTextAsync(inputFile);
            IBuffer buffProtected = CryptographicBuffer.DecodeFromBase64String(cypherText);
            String plainText = await DecryptDataStreamAsync(buffProtected, encoding);
            System.Diagnostics.Debug.WriteLine("plainText = " + plainText);
            await FileIO.WriteTextAsync(outputFile, plainText);
        }

    }
}
