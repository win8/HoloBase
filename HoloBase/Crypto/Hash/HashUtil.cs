﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;


namespace HoloBase.Crypto.Hash
{
    public static class HashUtil
    {
        //// temporary
        //private static readonly string DEFAULT_HASH_ALGORITHM_NAME = HashAlgorithmNames.Sha1;

        // "Cache"
        private static readonly IDictionary<string, HashAlgorithmProvider> algoProviders = new Dictionary<string, HashAlgorithmProvider>();

        private static HashAlgorithmProvider GetProvider(string algoName)
        {
            if (! algoProviders.ContainsKey(algoName) || algoProviders[algoName] == null) {
                var algo = HashAlgorithmProvider.OpenAlgorithm(algoName);
                algoProviders[algoName] = algo;
            }
            return algoProviders[algoName];    // This can still be null ???
        }

        //private static string defaultHashAlgorithmName = DEFAULT_HASH_ALGORITHM_NAME;
        //public static string DefaultHashAlgorithmName
        //{
        //    get
        //    {
        //        return defaultHashAlgorithmName;
        //    }
        //    set
        //    {
        //        // Validate????
        //        defaultHashAlgorithmName = value;
        //    }
        //}

        // This API can be confusing...
        // Let the client deal with "default" hash algo.
        //public static string ComputeHash(string text)
        //{
        //    return ComputeHash(text, DefaultHashAlgorithmName);
        //}
        public static string ComputeHash(string text, string algoName)
        {
            var algo = GetProvider(algoName);
            if (algo != null) {
                IBuffer buff = CryptographicBuffer.ConvertStringToBinary(text, BinaryStringEncoding.Utf8);
                var hashed = algo.HashData(buff);
                var strHash = CryptographicBuffer.EncodeToHexString(hashed);
                return strHash;
            } else {
                // ???
                System.Diagnostics.Debug.WriteLine("Specified hash algorithm not found: {0}.", algoName);
                return null;
            }
        }
        public static string ComputeMd5Hash(string text)
        {
            return ComputeHash(text, HashAlgorithmNames.Md5);
        }
        public static string ComputeSha1Hash(string text)
        {
            return ComputeHash(text, HashAlgorithmNames.Sha1);
        }
        public static string ComputeSha256Hash(string text)
        {
            return ComputeHash(text, HashAlgorithmNames.Sha256);
        }
        public static string ComputeSha384Hash(string text)
        {
            return ComputeHash(text, HashAlgorithmNames.Sha384);
        }
        public static string ComputeSha512Hash(string text)
        {
            return ComputeHash(text, HashAlgorithmNames.Sha512);
        }

    }
}
