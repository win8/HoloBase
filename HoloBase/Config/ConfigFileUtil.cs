﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;


namespace HoloBase.Config
{
    public static class ConfigFileUtil
    {
        // temporary
        private static readonly string DEFAULT_CONF_FOLDER = "conf";

        public static async Task<StorageFile> GetConfigFileAsync(string confFile)
        {
            return await GetConfigFileAsync(confFile, null);
        }
        public static async Task<StorageFile> GetConfigFileAsync(string confFile, string confFolder)
        {
            if (String.IsNullOrEmpty(confFolder)) {   // TBD: Validate????
                confFolder = DEFAULT_CONF_FOLDER;
            }

            // Configuration file
            // e.g. (ms-appx:///<conf folder>/<conf file>)

            StorageFile _file = null;
            var _folder = Package.Current.InstalledLocation;
            _folder = await _folder.GetFolderAsync(confFolder);

            if (_folder != null) {
                _file = await _folder.GetFileAsync(confFile);
                if (_file != null) {
                    // success..
                }
            }
            return _file;
        }

    }
}
