﻿using HoloJson.Mini.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;


namespace HoloBase.Config
{
    // For now, we only support one config file.
    // But, it can be easily changed if we want to support multiple config files
    //   (even using a singleton, e.g., by maintaining a dictionary (file -> parsed key-value map), etc.)
    public sealed class PropertyConfigHelper : AbstractConfigHelper
    {
        // TBD: Make this settable???
        public const string KEY_VALUE_SEPARATOR = ":";
        public const string COMMENT_PREFIX = "#"; 

        // temporary
        private const string DEFAULT_CONF_FOLDER = "conf";
        private const string DEFAULT_CONF_FILE = "appconfig.properties";

        //private static PropertyConfigHelper instance = null;
        //public static PropertyConfigHelper Instance
        //{
        //    get
        //    {
        //        if (instance == null) {
        //            var task = BuildPropertyConfigHelperAsync();
        //            task.RunSynchronously();
        //            instance = task.Result;
        //        }
        //        return instance;
        //    }
        //}
        //private PropertyConfigHelper()
        //{
        //}

        //private static async Task<PropertyConfigHelper> BuildPropertyConfigHelperAsync()
        //{
        //    var obj = new PropertyConfigHelper();
        //    await obj.InitConfigAsync();
        //    return obj;
        //}

        private static PropertyConfigHelper instance = null;
        //public static PropertyConfigHelper Instance
        //{
        //    get
        //    {
        //        if (instance == null) {
        //            instance = new PropertyConfigHelper();
        //        }
        //        return instance;
        //    }
        //}
        public static async Task<PropertyConfigHelper> GetInstanceAsync()
        {
            if (instance == null) {
                instance = new PropertyConfigHelper();
                await instance.InitConfigAsync();
            }
            return instance;
        }

        private PropertyConfigHelper()
        {
            //var task = InitConfigAsync();
        }

        private async Task InitConfigAsync()
        {
            // Primary conf file.
            await ReadConfigAsync(DEFAULT_CONF_FILE);
        }

        private async Task ReadConfigAsync(string confFile)
        {
            await ReadConfigAsync(confFile, null);
        }
        private async Task ReadConfigAsync(string confFile, string confFolder)
        {
            // Null or empty folder name means Use the default config folder
            // Not, use the top folder. (Use -> "/" for this?)
            if (String.IsNullOrEmpty(confFolder)) {   // TBD: Validate????
                confFolder = DEFAULT_CONF_FOLDER;
            }

            var _file = await ConfigFileUtil.GetConfigFileAsync(confFile, confFolder);
            if (_file != null) {
                // read lines.
                var _lines = await FileIO.ReadLinesAsync(_file);
                if (_lines != null && _lines.Count > 0) {
                    foreach (var line in _lines) {
                        var trimmedLine = line.TrimStart();  // Key cannot start with space, but value might end with space. ??????  --> Well, parsline actually trims both key and value.
                        if (!String.IsNullOrEmpty(trimmedLine)) {
                            // We only allow comment line that starts with "#" (except for space).
                            // That is, we do not allow comment appended at the end of key-value line, for now.
                            if (trimmedLine.StartsWith(COMMENT_PREFIX)) {
                                // skip
                                continue;
                            }
                            var pair = ParsePropertyLine(trimmedLine);
                            this.Dictionary.Add(pair.Key, pair.Value);
                        }
                    }
                }
                // ...
            }
        }


        public static KeyValuePair<string, object> ParsePropertyLine(string line)
        {
            // Key cannot contain KEY_VALUE_SEPARATOR.
            if (String.IsNullOrEmpty(line)) {
                throw new ArgumentNullException("Input line cannot be null/empty.");
            }

            var splitLine = line.Split(new string[] { KEY_VALUE_SEPARATOR }, 2, StringSplitOptions.None);

            KeyValuePair<string, object> pair;
            if (splitLine.Length > 0) {
                var key = splitLine[0];
                // tbd: trim key or not????
                key = key.Trim();
                // ...
                if (splitLine.Length > 1) {
                    var strVal = splitLine[1];
                    // tbd: trim value or not????
                    strVal = strVal.Trim();
                    // ...

                    // tbd:
                    // try converting it to int/decimal, etc.
                    // try converting it to list, etc. ????
                    // ...

                    
                    object val = null;

                    // ???
                    //double x;
                    //// if (Number.IsNumber(strVal)) {   // this is always false.
                    //if (Double.TryParse(strVal, out x)) {
                    //    // var num = new Number(strVal);
                    //    // var num = new Number((object) x);
                    //    var num = strVal.ToNumber();
                    //    val = num.Value;
                    if(strVal.IsNumber()) {
                        var num = strVal.ToNumber();
                        val = num.Value;
                    } else {
                        //if (strVal.StartsWith("[")) {
                        //    // list
                        //    // tbd: parse list
                        //    // ...
                        //} else if (strVal.StartsWith("{")) {
                        //    // dictionary
                        //    // tbd: parse diectionary
                        //    // ....
                        //} else {
                        //    val = strVal;
                        //}

                        // ????
                        val = strVal;
                    }


                    // ....

                    pair = new KeyValuePair<string, object>(key, val);
                } else {
                    // ????
                    pair = new KeyValuePair<string, object>(key, null);             
                }
            }

            return pair;
        }

    }
}
