﻿using HoloJson.Mini;
using HoloJson.Mini.Builder;
using HoloJson.Mini.Parser;
using HoloJson.Mini.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Config
{
    public sealed class JsonConfigHelper : AbstractConfigHelper
    {
        // temporary
        private const string DEFAULT_CONF_FOLDER = "conf";
        private const string DEFAULT_CONF_FILE = "appconfig.json";

        private static JsonConfigHelper instance = null;
        //public static JsonConfigHelper Instance
        //{
        //    get
        //    {
        //        if (instance == null) {
        //            instance = new JsonConfigHelper();
        //        }
        //        return instance;
        //    }
        //}
        public static async Task<JsonConfigHelper> GetInstanceAsync()
        {
            if (instance == null) {
                instance = new JsonConfigHelper();
                await instance.InitConfigAsync();
            }
            return instance;
        }

        private JsonConfigHelper()
        {
            // var task = InitConfigAsync();
        }


        private async Task InitConfigAsync()
        {
            // Primary conf file.
            await ReadConfigAsync(DEFAULT_CONF_FILE);
        }

        private async Task ReadConfigAsync(string confFile)
        {
            await ReadConfigAsync(confFile, null);
        }
        private async Task ReadConfigAsync(string confFile, string confFolder)
        {
            // Null or empty folder name means Use the default config folder
            // Not, use the top folder. (Use -> "/" for this?)
            if (String.IsNullOrEmpty(confFolder)) {   // TBD: Validate????
                confFolder = DEFAULT_CONF_FOLDER;
            }

            var _file = await ConfigFileUtil.GetConfigFileAsync(confFile, confFolder);
            if (_file != null) {
                // read json content
                var _json = await FileIO.ReadTextAsync(_file);
                if (!String.IsNullOrEmpty(_json)) {
                    var dict = await ParseJsonConfigAsync(_json);
                    if (dict != null && dict.Count > 0) {
                        foreach (var k in dict.Keys) {
                            this.Dictionary.Add(k, dict[k]);
                        }
                    }
                }
                // ...
            }
        }


        // Note that the json config file should be a JsonObjecct (dictionary).
        // The value is, should be, a primitive type including string, not a general object.... 
        // TBD: In the future, we can support a full object (including list, etc.) ....
        public static async Task<IDictionary<string, object>> ParseJsonConfigAsync(string jsonStr)
        {
            IDictionary<string, object> configDictionary = null;

            // TBD:
            // We should really use partial parsing with depth 1.

            LiteJsonParser jsonParser = new HoloJsonMiniParser();
            // LiteJsonBuilder jsonBuilder = new HoloJsonMiniBuilder();

            object jsonObj = null;
            try {
                jsonObj = await jsonParser.ParseAsync(jsonStr);
                System.Diagnostics.Debug.WriteLine("jsonObj = {0}", CollectionUtil.ToDebugString<string,object>(jsonObj));
            } catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("Failed to parse config json string: ", ex.Message);
            }

            if (jsonObj != null) {
                if (GenericUtil.IsDictionary(jsonObj)) {
                    configDictionary = new Dictionary<string, object>();

                    // ????
                    var dict = (IDictionary<string, object>) jsonObj;
                    // configDictionary = (IDictionary<string, object>) jsonObj;
                    // ....

                    foreach (var key in dict.Keys) {
                        var valObj = dict[key];
                        // ???
                        // We need to convert the object back to (json) string
                        //     since we are not using partial parsing.
                        // var valStr = await jsonBuilder.BuildAsync(valObj);
                        // ...
                        // For now, we are assuming the value is a primitive...
                        // --> Is this necessary?
                        // The caller/client should handle whatever type the value is... ????

                        configDictionary.Add(key, valObj);
                    }
                } else {
                    // ????
                    System.Diagnostics.Debug.WriteLine("Invalid jsonObj type: {0}", jsonObj.GetType());
                }
            } else {
                // ????
                System.Diagnostics.Debug.WriteLine("Failed to read jsonObj.");
            }

            return configDictionary;
        }

    }
}
