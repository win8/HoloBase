﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Config
{
    //// Bit mask.
    //public enum ConfigType
    //{
    //    UNDEFINED = 0,
    //    PROPS = (1 << 0),
    //    JSON = (1 << 1)
    //    // ...
    //}

    public static class ConfigTypes
    {
        public const int UNDEFINED = 0;
        public const int PROPS = (1 << 0);
        public const int JSON = (1 << 1);
        // ...

        public static bool IsJsonIncluded(int configType)
        {
            return (configType & JSON) != 0;
        }
        public static bool IsPropsIncluded(int configType)
        {
            return (configType & PROPS) != 0;
        }
    
    }

}
