﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Config
{
    public sealed class ConfigManager : iConfigHelper
    {
        private static ConfigManager instance = null;
        //public static ConfigManager Instance
        //{
        //    get
        //    {
        //        if (instance == null) {
        //            instance = new ConfigManager();
        //        }
        //        return instance;
        //    }
        //}
        public static async Task<ConfigManager> GetInstanceAsync()
        {
            if (instance == null) {
                instance = new ConfigManager();
                await instance.InitializeConfigMapAsync();
            }
            return instance;
        }
        public static async Task<ConfigManager> GetInstanceAsync(int configType)
        {
            if (instance == null) {
                instance = new ConfigManager();
                await instance.SetConfigTypeAsync(configType);  // SetConfigType calls InitializeConfigMapAsync().
            } else {
                int currentType = instance.GetConfigType();
                if (currentType != configType) {
                    await instance.SetConfigTypeAsync(configType); 
                }
            }
            return instance;
        }


        // private ConfigType configType;
        private int configType;

        // Cache
        private IDictionary<string, object> configMap;

        private ConfigManager()
        {
            // Use properties format by default?????
            configType = ConfigTypes.PROPS;
        }

        //public int ConfigType
        //{
        //    get
        //    {
        //        return configType;
        //    }
        //    //set
        //    //{
        //    //    configType = value;
        //    //    // tbd
        //    //    // InitializeConfigMapAsync();
        //    //}
        //}

        public int GetConfigType()
        {
            return configType;
        }
        public async Task SetConfigTypeAsync(int configType)
        {
            // tbd: validate configType???
            this.configType = configType;
            await InitializeConfigMapAsync();
        }

        public IDictionary<string, object> ConfigMap
        {
            get
            {
                return configMap;
            }
        }

        public bool IsJsonIncluded
        {
            get
            {
                return ConfigTypes.IsJsonIncluded(configType);
            }
        }
        public bool IsPropsIncluded
        {
            get
            {
                return ConfigTypes.IsPropsIncluded(configType);
            }
        }

        // TBD:
        // What happens if a config with the same key exists in more than one config files????
        // ....
        private async Task InitializeConfigMapAsync()
        {
            configMap = new Dictionary<string, object>();

            // Note: json first --> props can overwrite json config.
            if (this.IsJsonIncluded) {
                // var dict = JsonConfigHelper.Instance.Dictionary;
                var configHelper = await JsonConfigHelper.GetInstanceAsync();
                var dict = configHelper.Dictionary;
                if (dict != null && dict.Count > 0) {
                    foreach (var k in dict.Keys) {
                        // ???
                        // configMap.Add(k, dict[k]);
                        configMap[k] = dict[k];
                    }
                }
            }
            if (this.IsPropsIncluded) {
                // var dict = PropertyConfigHelper.Instance.Dictionary;
                var configHelper = await PropertyConfigHelper.GetInstanceAsync();
                var dict = configHelper.Dictionary;
                if (dict != null && dict.Count > 0) {
                    foreach (var k in dict.Keys) {
                        // ???
                        // configMap.Add(k, dict[k]);
                        configMap[k] = dict[k];
                    }
                }
            }
            // ...
        }


        public bool IsKeyPresent(string key)
        {
            return this.ConfigMap.ContainsKey(key);
        }
        public bool IsValuePresentForKey(string key)
        {
            if (this.ConfigMap.ContainsKey(key)) {
                return (this.ConfigMap[key] != null);
            } else {
                return false;
            }
        }
        public object GetConfigValue(string key)
        {
            //// ?? does it throw exception?
            //return this.Dictionary[key];
            if (this.ConfigMap.ContainsKey(key)) {
                return this.ConfigMap[key];
            } else {
                return null;
            }
        }

        public string GetStringConfigValue(string key, string defaultVal = null)
        {
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    var stringVal = val.ToString();
                    return stringVal;
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid string config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }

        public bool GetBooleanConfigValue(string key, bool defaultVal = false)
        {
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    var boolVal = Convert.ToBoolean(val);
                    return boolVal;
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid boolean config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }

        public object GetIntegerConfigValue(string key, object defaultVal)
        {
            if (defaultVal == null) {
                return null;  // ???
            }
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    if (defaultVal is long) {
                        var longVal = Convert.ToInt64(val);
                        return longVal;
                    } else if (defaultVal is ulong) {
                        var ulongVal = Convert.ToUInt64(val);
                        return ulongVal;
                    } else if (defaultVal is int) {
                        var intVal = Convert.ToInt64(val);
                        return intVal;
                    } else if (defaultVal is uint) {
                        var uintVal = Convert.ToUInt64(val);
                        return uintVal;
                    } else if (defaultVal is short) {
                        var shortVal = Convert.ToInt64(val);
                        return shortVal;
                    } else if (defaultVal is ushort) {
                        var ushortVal = Convert.ToUInt64(val);
                        return ushortVal;
                    } else {
                        // ????
                    }
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid integer config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }

        public long GetLongConfigValue(string key, long defaultVal = 0L)
        {
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    var longVal = Convert.ToInt64(val);
                    return longVal;
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid long config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }
        public ulong GetULongConfigValue(string key, ulong defaultVal = 0UL)
        {
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    var ulongVal = Convert.ToUInt64(val);
                    return ulongVal;
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid ulong config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }

        public int GetIntConfigValue(string key, int defaultVal = 0)
        {
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    var intVal = Convert.ToInt32(val);
                    return intVal;
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid int config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }
        public uint GetUIntConfigValue(string key, uint defaultVal = 0U)
        {
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    var uintVal = Convert.ToUInt32(val);
                    return uintVal;
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid uint config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }

        public short GetShortConfigValue(string key, short defaultVal = 0)
        {
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    var shortVal = Convert.ToInt16(val);
                    return shortVal;
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid short config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }
        public ushort GetUShortConfigValue(string key, ushort defaultVal = (ushort) 0U)
        {
            var val = GetConfigValue(key);
            if (val != null) {
                try {
                    var ushortVal = Convert.ToUInt16(val);
                    return ushortVal;
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid ushort config: key = " + key + "; value = " + val + "; error = " + ex.Message);
                    // ignore
                }
            }
            return defaultVal;
        }



    }
}
