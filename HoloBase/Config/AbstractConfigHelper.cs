﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Config
{
    public interface iConfigHelper
    {
        bool IsKeyPresent(string key);
        bool IsValuePresentForKey(string key);
        object GetConfigValue(string key);
    }

    public abstract class AbstractConfigHelper : iConfigHelper
    {
        // TBD: We will need one dictionary per config file (if we decide to support multiple config files)???
        //     Or, just read all configs into one dictionary???
        private readonly IDictionary<string, object> mDictionary;

        protected AbstractConfigHelper()
        {
            mDictionary = new Dictionary<string, object>();
        }

        public IDictionary<string, object> Dictionary
        {
            get
            {
                return mDictionary;
            }
        }

        public bool IsKeyPresent(string key)
        {
            return this.Dictionary.ContainsKey(key);
        }
        public bool IsValuePresentForKey(string key)
        {
            if (this.Dictionary.ContainsKey(key)) {
                return (this.Dictionary[key] != null);
            } else {
                return false;
            }
        }
        public object GetConfigValue(string key)
        {
            //// ?? does it throw exception?
            //return this.Dictionary[key];
            if (this.Dictionary.ContainsKey(key)) {
                return this.Dictionary[key];
            } else {
                return null;
            }
        }


        // tbd
        // GetBooleanValue
        // GetIntValue
        // ....



    }
}
