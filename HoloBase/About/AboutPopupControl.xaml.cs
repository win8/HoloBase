﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HoloBase.UI.Util;


namespace HoloBase.About
{
    public sealed partial class AboutPopupControl : UserControl
    {
        public AboutPopupControl()
        {
            this.InitializeComponent();
        }

        private void ButtonAboutPopupOK_Click(object sender, RoutedEventArgs e)
        {
            // tbd:

            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }

        }
    }
}
