﻿using HoloBase.Settings.Common;
using HoloBase.Settings.Core;
using HoloCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Settings.Data
{
    // Note:
    // Regardless of the global app settings,
    // this data should be stored in the LOCAL settings, not in the roaming.
    // This data/settings are specific to this device only.
    public sealed class GlobalAppDataOptionHelper
    {
        private static readonly string GAD_GLOBAL_APP_DATA_OPTION = "GAD-AppDataOption";
        private static readonly string GAD_USE_DEVICE_SPECIFIC_DATA = "GAD-UseDeviceData";
        private static readonly string GAD_LAST_UPDATED_TIME = "GAD-LastUpdated";


        private static GlobalAppDataOptionHelper instance = null;
        public static GlobalAppDataOptionHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new GlobalAppDataOptionHelper();
                }
                return instance;
            }
        }
        private GlobalAppDataOptionHelper()
        {
        }


        public void StoreAppGlobalDataOption(bool useDeviceSpecificData)
        {
            var holoAppGlobalDataOption = new HoloAppGlobalDataOption(useDeviceSpecificData, 0L);
            StoreAppGlobalDataOption(holoAppGlobalDataOption);
        }
        public void StoreAppGlobalDataOption(HoloAppGlobalDataOption holoAppGlobalDataOption)
        {
            var applicationDataContainer = ApplicationData.Current.LocalSettings;
            applicationDataContainer.Values[GAD_USE_DEVICE_SPECIFIC_DATA] = holoAppGlobalDataOption.UseDeviceSpecificData;
            var lastUpdated = holoAppGlobalDataOption.LastUpdated;
            if (lastUpdated == 0L) {
                lastUpdated = DateTimeUtil.CurrentUnixEpochMillis();
            }
            applicationDataContainer.Values[GAD_LAST_UPDATED_TIME] = lastUpdated;
        }
        public HoloAppGlobalDataOption FetchAppGlobalDataOption()
        {
            var useDeviceSpecificData = false;
            var allowEntrySpecificOverride = false;
            var lastUpdated = 0L;
            var applicationDataContainer = ApplicationData.Current.LocalSettings;
            if (applicationDataContainer.Values.ContainsKey(GAD_USE_DEVICE_SPECIFIC_DATA)) {
                // ????
                // useDeviceSpecificData = (bool) applicationDataContainer.Values[GAD_USE_DEVICE_SPECIFIC_DATA];
                try {
                    var strBool = applicationDataContainer.Values[GAD_USE_DEVICE_SPECIFIC_DATA].ToString();
                    useDeviceSpecificData = Convert.ToBoolean(strBool);
                } catch (Exception) {
                    // Ignore.
                }
            }
            if (applicationDataContainer.Values.ContainsKey(GAD_LAST_UPDATED_TIME)) {
                // ????
                // lastUpdated = (long) applicationDataContainer.Values[GAD_LAST_UPDATED_TIME];
                try {
                    var strLong = applicationDataContainer.Values[GAD_LAST_UPDATED_TIME].ToString();
                    lastUpdated = Convert.ToInt64(strLong);
                } catch (Exception) {
                    // Ignore.
                }
            }
            var holoAppGlobalDataOption = new HoloAppGlobalDataOption(useDeviceSpecificData, lastUpdated);
            return holoAppGlobalDataOption;
        }



    }
}
