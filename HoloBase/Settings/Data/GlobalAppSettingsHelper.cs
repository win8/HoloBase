﻿using HoloBase.Settings.Common;
using HoloBase.Settings.Core;
using HoloCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Settings.Data
{
    // Note:
    // Regardless of the global app settings,
    // this data should be stored in the LOCAL settings, not in the roaming.
    // This data/settings are specific to this device only.
    public sealed class GlobalAppSettingsHelper
    {
        private static readonly string GAS_GLOBAL_APP_SETTINGS = "GAS-AppSettings";
        private static readonly string GAS_USE_DEVICE_SPECIFIC_SETTINGS = "GAS-UseDeviceSettings";
        private static readonly string GAS_ALLOW_ENTRY_SPECIFIC_OVERRIDE = "GAS-AllowEntryOverride";
        private static readonly string GAS_LAST_UPDATED_TIME = "GAS-LastUpdated";

        
        private static GlobalAppSettingsHelper instance = null;
        public static GlobalAppSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new GlobalAppSettingsHelper();
                }
                return instance;
            }
        }
        private GlobalAppSettingsHelper()
        {
        }


        // Not being used.
        // Use the "boolean version" below...
        private void SaveGlobalAppSettings(GlobalAppSettingsOption globalAppSettings)
        {
            var applicationDataContainer = ApplicationData.Current.LocalSettings;
            applicationDataContainer.Values[GAS_GLOBAL_APP_SETTINGS] = globalAppSettings;
        }
        private GlobalAppSettingsOption FetchGlobalAppSettings()
        {
            var globalAppSettings = GlobalAppSettingsOption.Unset;
            var applicationDataContainer = ApplicationData.Current.LocalSettings;
            if (applicationDataContainer.Values.ContainsKey(GAS_GLOBAL_APP_SETTINGS)) {
                // ????
                globalAppSettings = (GlobalAppSettingsOption) applicationDataContainer.Values[GAS_GLOBAL_APP_SETTINGS];
            }
            return globalAppSettings;
        }


        public void StoreAppGlobalSettings(bool useDeviceSpecificSettings)
        {
            StoreAppGlobalSettings(useDeviceSpecificSettings, false);
        }
        public void StoreAppGlobalSettings(bool useDeviceSpecificSettings, bool allowEntrySpecificOverride)
        {
            // ???
            // var holoAppGlobalSettings = new HoloAppGlobalSettings(useDeviceSpecificSettings, allowEntrySpecificOverride);
            var holoAppGlobalSettings = new HoloAppGlobalSettings(useDeviceSpecificSettings, allowEntrySpecificOverride, 0L);
            StoreAppGlobalSettings(holoAppGlobalSettings);
        }
        public void StoreAppGlobalSettings(HoloAppGlobalSettings holoAppGlobalSettings)
        {
            var applicationDataContainer = ApplicationData.Current.LocalSettings;
            applicationDataContainer.Values[GAS_USE_DEVICE_SPECIFIC_SETTINGS] = holoAppGlobalSettings.UseDeviceSpecificSettings;
            applicationDataContainer.Values[GAS_ALLOW_ENTRY_SPECIFIC_OVERRIDE] = holoAppGlobalSettings.AllowEntrySpecificOverride;
            var lastUpdated = holoAppGlobalSettings.LastUpdated;
            if (lastUpdated == 0L) {
                lastUpdated = DateTimeUtil.CurrentUnixEpochMillis();
            }
            applicationDataContainer.Values[GAS_LAST_UPDATED_TIME] = lastUpdated;
        }
        public HoloAppGlobalSettings FetchAppGlobalSettings()
        {
            var useDeviceSpecificSettings = false;
            var allowEntrySpecificOverride = false;
            var lastUpdated = 0L;
            var applicationDataContainer = ApplicationData.Current.LocalSettings;
            if (applicationDataContainer.Values.ContainsKey(GAS_USE_DEVICE_SPECIFIC_SETTINGS)) {
                // ????
                // useDeviceSpecificSettings = (bool) applicationDataContainer.Values[GAS_USE_DEVICE_SPECIFIC_SETTINGS];
                try {
                    var strBool = applicationDataContainer.Values[GAS_USE_DEVICE_SPECIFIC_SETTINGS].ToString();
                    useDeviceSpecificSettings = Convert.ToBoolean(strBool);
                } catch (Exception) {
                    // Ignore.
                }
            }
            if (applicationDataContainer.Values.ContainsKey(GAS_ALLOW_ENTRY_SPECIFIC_OVERRIDE)) {
                // ????
                // allowEntrySpecificOverride = (bool) applicationDataContainer.Values[GAS_ALLOW_ENTRY_SPECIFIC_OVERRIDE];
                try {
                    var strBool = applicationDataContainer.Values[GAS_ALLOW_ENTRY_SPECIFIC_OVERRIDE].ToString();
                    allowEntrySpecificOverride = Convert.ToBoolean(strBool);
                } catch (Exception) {
                    // Ignore.
                }
            }
            if (applicationDataContainer.Values.ContainsKey(GAS_LAST_UPDATED_TIME)) {
                // ????
                // lastUpdated = (long) applicationDataContainer.Values[GAS_LAST_UPDATED_TIME];
                try {
                    var strLong = applicationDataContainer.Values[GAS_LAST_UPDATED_TIME].ToString();
                    lastUpdated = Convert.ToInt64(strLong);
                } catch (Exception) {
                    // Ignore.
                }
            }
            var holoAppGlobalSettings = new HoloAppGlobalSettings(useDeviceSpecificSettings, allowEntrySpecificOverride, lastUpdated);
            return holoAppGlobalSettings;
        }




    }
}
