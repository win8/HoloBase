﻿using HoloBase.Settings.ViewModels;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HoloBase.Settings.UI
{
    public sealed partial class GlobalAppSettingsControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private SettingsFlyout parentSettingsFlyout = null;   // For Windows
        private Page parentSettingsPage = null;               // For Windows Phone.

        private string title;
        private string message;

        private string helpLabelOnAppSettings;
        private Uri helpLinkOnAppSettings;

        private GlobalAppSettingsViewModel viewModel;
        public GlobalAppSettingsControl()
        {
            this.InitializeComponent();
            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new GlobalAppSettingsViewModel();

            // this.viewModel. ...

            ResetViewModel(vm);
        }
        private void ResetViewModel(GlobalAppSettingsViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;

            RefreshDataAndUI();
        }
        public void RefreshDataAndUI()
        {
            // ???
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (GlobalAppSettingsViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }




        public SettingsFlyout ParentSettingsFlyout
        {
            get
            {
                return parentSettingsFlyout;
            }
            set
            {
                parentSettingsFlyout = value;
            }
        }
        public Page ParentSettingsPage
        {
            get
            {
                return parentSettingsPage;
            }
            set
            {
                parentSettingsPage = value;
            }
        }


        //public string Title
        //{
        //    get
        //    {
        //        return title;
        //    }
        //    set
        //    {
        //        title = value;
        //        TextBlockSettingsControlTitle.Text = title;
        //    }
        //}

        //public string Message
        //{
        //    get
        //    {
        //        return message;
        //    }
        //    set
        //    {
        //        message = value;
        //        TextBlockSettingsControlMessage.Text = message;
        //    }
        //}


        //public string HelpLabelOnAppSettings
        //{
        //    get
        //    {
        //        return helpLabelOnAppSettings;
        //    }
        //    set
        //    {
        //        helpLabelOnAppSettings = value;
        //        HyperlinkButtonHelpOnAppSettings.Content = helpLabelOnAppSettings;
        //    }
        //}

        //public Uri HelpLinkOnAppSettings
        //{
        //    get
        //    {
        //        return helpLinkOnAppSettings;
        //    }
        //    set
        //    {
        //        helpLinkOnAppSettings = value;
        //        HyperlinkButtonHelpOnAppSettings.NavigateUri = helpLinkOnAppSettings;
        //    }
        //}


        // tbd: who's calling this?
        private void DismissSettingsFlyout()
        {
            if (parentSettingsFlyout != null) {
                parentSettingsFlyout.Hide();
            } else if (parentSettingsPage != null) {
                // ????
                // Go "back" ???
            }
        }


        // TBD:
        // Use a toggle button instead of a radio button
        // (Will there be  more than two options???)

        private void RadioButtonUseDeviceSettings_Checked(object sender, RoutedEventArgs e)
        {
            GlobalAppSettingsOptionManager.Instance.UseDeviceSpecificSettings = true;
        }

        private void RadioButtonUseCommonSettings_Checked(object sender, RoutedEventArgs e)
        {
            GlobalAppSettingsOptionManager.Instance.UseDeviceSpecificSettings = false;
        }

    }
}
