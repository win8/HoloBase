﻿using HoloBase.Settings.Common;
using HoloBase.Settings.Core;
using HoloBase.Settings.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Settings
{
    public sealed class GlobalAppDataOptionManager
    {
        private static readonly bool DEFAULT_USE_DEVICE_SPECIFIC_DATA = false;


        private static GlobalAppDataOptionManager instance = null;
        public static GlobalAppDataOptionManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new GlobalAppDataOptionManager();
                }
                return instance;
            }
        }
        private GlobalAppDataOptionManager()
        {
        }


        public HoloAppGlobalDataOption AppGlobalSettingsForThisDevice
        {
            get
            {
                // TBD
                return GlobalAppDataOptionHelper.Instance.FetchAppGlobalDataOption();
            }
        }

        public bool UseDeviceSpecificData
        {
            get
            {
                return GlobalAppDataOptionHelper.Instance.FetchAppGlobalDataOption().UseDeviceSpecificData;
            }
            set
            {
                // TBD: Is there a better way?
                var holoAppGlobalSettings = GlobalAppDataOptionHelper.Instance.FetchAppGlobalDataOption();
                var currentVal = holoAppGlobalSettings.UseDeviceSpecificData;
                if (currentVal != value) {
                    // TBD:
                    // Schedule "settings update/migration" background task...
                    // ...
                    // --> Does it make sense to allow to change this global value 
                    //     after the app has been installed 
                    //     and user data has been stored.
                    // ...
                    // TBD:
                    // When do we save?
                    //    after the task is done (and, only if the migration was successful)???
                    //    Or, do it immediately, and if the task fails, roll back??? --> probably a better option.
                    // ....
                    GlobalAppDataOptionHelper.Instance.StoreAppGlobalDataOption(value);
                } else {
                    // ignore
                }
            }
        }


    }
}
