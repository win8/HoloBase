﻿using HoloBase.Settings.Common;
using HoloBase.Settings.Core;
using HoloBase.Settings.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Settings
{
    public sealed class GlobalAppSettingsOptionManager
    {
        private static readonly bool DEFAULT_USE_DEVICE_SPECIFIC_SETTINGS = false;
        private static readonly bool DEFAULT_ALLOW_ENTRY_SPECIFIC_OVERRIDE = false;


        private static GlobalAppSettingsOptionManager instance = null;
        public static GlobalAppSettingsOptionManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new GlobalAppSettingsOptionManager();
                }
                return instance;
            }
        }
        private GlobalAppSettingsOptionManager()
        {
        }


        // TBD:
        //public GlobalAppSettings GlobalAppSettingsForThisDevice
        //{
        //    get
        //    {
        //        // TBD
        //        return GlobalAppSettings.Unknown;
        //    }
        //}

        public HoloAppGlobalSettings AppGlobalSettingsForThisDevice
        {
            get
            {
                // TBD
                return GlobalAppSettingsHelper.Instance.FetchAppGlobalSettings();
            }
        }

        public bool UseDeviceSpecificSettings
        {
            get
            {
                return GlobalAppSettingsHelper.Instance.FetchAppGlobalSettings().UseDeviceSpecificSettings;
            }
            set
            {
                // TBD: Is there a better way?
                var holoAppGlobalSettings = GlobalAppSettingsHelper.Instance.FetchAppGlobalSettings();
                var currentVal = holoAppGlobalSettings.UseDeviceSpecificSettings;
                if (currentVal != value) {
                    // TBD:
                    // Schedule "settings update/migration" background task...
                    // ...
                    // TBD:
                    // When do we save?
                    //    after the task is done (and, only if the migration was successful)???
                    //    Or, do it immediately, and if the task fails, roll back??? --> probably a better option.
                    // ....
                    //holoAppGlobalSettings.UseDeviceSpecificSettings = value;
                    //holoAppGlobalSettings.LastUpdated = DateTimeUtil.CurrentUnixEpochMillis();
                    //GlobalAppSettingsHelper.Instance.StoreAppGlobalSettings(holoAppGlobalSettings);
                    GlobalAppSettingsHelper.Instance.StoreAppGlobalSettings(value);
                } else {
                    // ignore
                }
            }
        }





    }

}
