﻿using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Settings.ViewModels
{

    public class GlobalAppSettingsViewModel : IViewModel, INotifyPropertyChanged
    {
        // cache?
        // private bool useDeviceSpecificSettings;

        private string title;
        private string message;

        private string helpLabelOnAppSettings;
        private Uri helpLinkOnAppSettings;

        private string labelUseDeviceSettings;
        private string labelUseCommonSettings;


        public GlobalAppSettingsViewModel()
        {
            // TBD:
            // Initial values.
            // This should be really populated by some external helper.
            // TBD:
            // Strings shoud be localized.
            // ...

            title = "Sharing App Settings";

            message = "App settings can be shared across multiple devices you own. If you enable this sharing feature on this device, then setting/changing a setting/option value of the app on this device will affect the value on other devices on which settings sharing is enabled, and vice versa.";


            helpLabelOnAppSettings = "Help on App Settings";

            helpLinkOnAppSettings = new Uri("http://www.holosoftware.net/help/common/appsettings");

            labelUseDeviceSettings = "Use app settings specific to this device only.";

            labelUseCommonSettings = "Use common app settings on this device as well.";

        }


        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
                // TextBlockSettingsControlTitle.Text = title;
                RaisePropertyChanged("Title");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                // TextBlockSettingsControlMessage.Text = message;
                RaisePropertyChanged("Message");
            }
        }

        public string HelpLabelOnAppSettings
        {
            get
            {
                return helpLabelOnAppSettings;
            }
            set
            {
                helpLabelOnAppSettings = value;
                // HyperlinkButtonHelpOnAppSettings.Content = helpLabelOnAppSettings;
                RaisePropertyChanged("HelpLabelOnAppSettings");
            }
        }

        public Uri HelpLinkOnAppSettings
        {
            get
            {
                return helpLinkOnAppSettings;
            }
            set
            {
                helpLinkOnAppSettings = value;
                // HyperlinkButtonHelpOnAppSettings.NavigateUri = helpLinkOnAppSettings;
                RaisePropertyChanged("HelpLinkOnAppSettings");
            }
        }


        public string LabelUseDeviceSettings
        {
            get
            {
                return labelUseDeviceSettings;
            }
            set
            {
                labelUseDeviceSettings = value;
                RaisePropertyChanged("LabelUseDeviceSettings");
            }
        }
        public string LabelUseCommonSettings
        {
            get
            {
                return labelUseCommonSettings;
            }
            set
            {
                labelUseCommonSettings = value;
                RaisePropertyChanged("LabelUseCommonSettings");
            }
        }


        public bool IsUseDeviceSettingsChecked
        {
            get
            {
                return this.IsCheckedForUseDeviceSpecificSettings;
            }
            set
            {
                this.IsCheckedForUseDeviceSpecificSettings = value;
                RaisePropertyChanged("IsUseDeviceSettingsChecked");
                RaisePropertyChanged("IsUseCommonSettingsChecked");
            }
        }
        public bool IsUseCommonSettingsChecked
        {
            get
            {
                return ! this.IsCheckedForUseDeviceSpecificSettings;
            }
            set
            {
                this.IsCheckedForUseDeviceSpecificSettings = ! value;
                RaisePropertyChanged("IsUseDeviceSettingsChecked");
                RaisePropertyChanged("IsUseCommonSettingsChecked");
            }
        }


        private bool IsCheckedForUseDeviceSpecificSettings
        {
            get
            {
                return GlobalAppSettingsOptionManager.Instance.UseDeviceSpecificSettings;
            }
            set
            {
                GlobalAppSettingsOptionManager.Instance.UseDeviceSpecificSettings = value;
            }
        }

        // Not being used.
        private bool IsCheckedForAllowEntrySpecificOverride
        {
            get
            {
                // tbd.
                return false;
            }
            set
            {
                // tbd.
            }
        }


        
        
        
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }





}
