﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Settings.Core
{
    // TBD:
    // AppSettingsType vs GlobalAppSettings
    // ...

    // TBD:
    // Do we really need this?

    public enum AppSettingsType
    {
        // Temporary,
        Local,
        Roaming
    }


    // Deprecated.
    // Just use booleans.

    public enum GlobalAppSettingsOption
    {
        // Unknown vs Unset ????
        Unknown = 0,
        Unset,        // --> Use default value.
        //UseSettingsForThisDeviceOnly,
        //UseCommonSettingsForThisDeviceAsWell
        UseDeviceSpecificSettings,
        UseCommonSettings

        // global vs.  global with entry-specific override???
        // ....
    }

}
