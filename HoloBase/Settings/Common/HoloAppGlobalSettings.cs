﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Settings.Common
{
    // This class (and, a settings container) is not really needed
    // since each app will be its own.
    // We just use it as a convenience wrapper.

    // tbd...
    // For this device only.
    // This should be stored in the LOCAL settings, not roaming.
    public struct HoloAppGlobalSettings
    {
        // See the comment above.
        // These fields are not really needed.
        // private string appName;
        // private string appGuid;

        private bool useDeviceSpecificSettings;
        private bool allowEntrySpecificOverride;

        private long lastUpdated;

        // tbd:
        // per settings entry/key?
        // ....


        public HoloAppGlobalSettings(bool useDeviceSpecificSettings, bool allowEntrySpecificOverride)
            : this(useDeviceSpecificSettings, allowEntrySpecificOverride, 0L)
        {
        }
        public HoloAppGlobalSettings(bool useDeviceSpecificSettings, bool allowEntrySpecificOverride, long lastUpdated)
        {
            this.useDeviceSpecificSettings = useDeviceSpecificSettings;
            this.allowEntrySpecificOverride = allowEntrySpecificOverride;
            this.lastUpdated = lastUpdated;
        }



        public bool UseDeviceSpecificSettings
        {
            get
            {
                return useDeviceSpecificSettings;
            }
            internal set
            {
                useDeviceSpecificSettings = value;
            }
        }

        public bool AllowEntrySpecificOverride
        {
            get
            {
                return allowEntrySpecificOverride;
            }
            internal set
            {
                allowEntrySpecificOverride = value;
            }
        }


        public long LastUpdated
        {
            get
            {
                return lastUpdated;
            }
            internal set
            {
                lastUpdated = value;
            }
        }


    }
}
