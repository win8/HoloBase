﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Settings.Common
{
    // tbd...
    // For this device only.
    // This should be stored in the LOCAL settings, not roaming.
    public struct HoloAppGlobalDataOption
    {
        // These fields are not really needed.
        // private string appName;
        // private string appGuid;

        // TBD:
        // Is boolean enough?
        private bool useDeviceSpecificData;
        // local data can be local settings, app data folder, and other local storages like local folders, etc.
        // "common data" can be romaing settings/data folder, and cloud-based storages,e tc...
        // ...

        private long lastUpdated;


        public HoloAppGlobalDataOption(bool useDeviceSpecificData)
            : this(useDeviceSpecificData, 0L)
        {
        }
        public HoloAppGlobalDataOption(bool useDeviceSpecificData, long lastUpdated)
        {
            this.useDeviceSpecificData = useDeviceSpecificData;
            this.lastUpdated = lastUpdated;
        }


        public bool UseDeviceSpecificData
        {
            get
            {
                return useDeviceSpecificData;
            }
            internal set
            {
                useDeviceSpecificData = value;
            }
        }


        public long LastUpdated
        {
            get
            {
                return lastUpdated;
            }
            internal set
            {
                lastUpdated = value;
            }
        }


    }
}
