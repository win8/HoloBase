﻿using HoloBase.Settings.Core;
using HoloBase.Settings.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Settings.App
{

    // TBD:

    // Currently
    // not being used.
    // We need to implement
    //    what we mean by local and what we mean by roaming...
    // ...
    public sealed class AppDataOptionHelper
    {
        private static readonly AppDataType DEFAULT_SETTINGS_TYPE = AppDataType.Roaming;


        private static AppDataOptionHelper instance = null;
        public static AppDataOptionHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new AppDataOptionHelper();
                }
                return instance;
            }
        }
        private AppDataOptionHelper()
        {
        }


        public void StoreAppDataType(AppDataType appDataType)
        {
            // TBD: Check RoamingSettings quota first???
            var useDeviceSpecificData = false;
            switch (appDataType) {
                case AppDataType.Local:
                    useDeviceSpecificData = true;
                    break;
                case AppDataType.Roaming:
                default:
                    useDeviceSpecificData = false;
                    break;
            }
            GlobalAppDataOptionHelper.Instance.StoreAppGlobalDataOption(useDeviceSpecificData);
        }

        public AppDataType GetAppDataType()
        {
            var AppDataType = DEFAULT_SETTINGS_TYPE;
            var appGlobalDataOption = GlobalAppDataOptionHelper.Instance.FetchAppGlobalDataOption();
            if (appGlobalDataOption.UseDeviceSpecificData) {
                // tbd
                // local data storage???
                AppDataType = AppDataType.Local;
            } else {
                // tbd
                // onedrive, etc. ???
                AppDataType = AppDataType.Roaming;
            }
            return AppDataType;
        }


        public ApplicationDataContainer GetLocalAppSettings()
        {
            return ApplicationData.Current.LocalSettings;
        }
        public ApplicationDataContainer GetRoamingAppSettings()
        {
            return ApplicationData.Current.RoamingSettings;
        }

        public ApplicationDataContainer GetAppSettings()
        {
            //// TBD:
            //// If we are over the quota, or close to the limit (so that the real "settings"/prefs can use the roaming),
            //// then we should just return local container!!!
            //var quota = ApplicationData.Current.RoamingStorageQuota;
            //System.Diagnostics.Debug.WriteLine("RoamingStorageQuota = {0}", quota);
            //// TBD:
            //// How to check the size of the current storage???
            //// ...

            ApplicationDataContainer applicationDataContainer;
            AppDataType appDataType = GetAppDataType();
            switch (appDataType) {
                case AppDataType.Local:
                    // tbd.
                    applicationDataContainer = ApplicationData.Current.LocalSettings;
                    break;
                case AppDataType.Roaming:
                default:
                    // tbd.
                    applicationDataContainer = ApplicationData.Current.RoamingSettings;
                    break;
            }
            return applicationDataContainer;
        }

        public IStorageFolder GetLocalAppDataFolder()
        {
            return Windows.Storage.ApplicationData.Current.LocalFolder;
        }
        public IStorageFolder GetRoamingAppDataFolder()
        {
            return Windows.Storage.ApplicationData.Current.RoamingFolder;
        }

        public IStorageFolder GetAppDataFolder()
        {
            //// TBD:
            //// If we are over the quota, or close to the limit (so that the real "settings"/prefs can use the roaming),
            //// then we should just return local container!!!
            //var quota = ApplicationData.Current.RoamingStorageQuota;
            //System.Diagnostics.Debug.WriteLine("RoamingStorageQuota = {0}", quota);
            //// TBD:
            //// How to check the size of the current storage???
            //// ...

            IStorageFolder StorageFolder;
            AppDataType appDataType = GetAppDataType();
            switch (appDataType) {
                case AppDataType.Local:
                    // tbd.
                    StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                    break;
                case AppDataType.Roaming:
                default:
                    // tbd.
                    StorageFolder = Windows.Storage.ApplicationData.Current.RoamingFolder;
                    break;
            }
            return StorageFolder;
        }



        // TBD:

        // When the user changes the option,
        // Copy local to roaming
        // Copy roaming to local
        // ?????
        // ...


    }

}
