﻿using HoloBase.Settings.Core;
using HoloBase.Settings.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Settings.App
{

    public sealed class AppSettingsHelper
    {
        private static readonly AppSettingsType DEFAULT_SETTINGS_TYPE = AppSettingsType.Roaming;

        private static AppSettingsHelper instance = null;
        public static AppSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new AppSettingsHelper();
                }
                return instance;
            }
        }
        private AppSettingsHelper()
        {
        }



        public void StoreSettingsType(AppSettingsType settingsType)
        {
            // TBD: Check RoamingSettings quota first???

            var useDeviceSpecificSettings = false;
            switch (settingsType) {
                case AppSettingsType.Local:
                    useDeviceSpecificSettings = true;
                    break;
                case AppSettingsType.Roaming:
                default:
                    useDeviceSpecificSettings = false;
                    break;
            }
            GlobalAppSettingsHelper.Instance.StoreAppGlobalSettings(useDeviceSpecificSettings);
        }

        public AppSettingsType GetSettingsType()
        {
            var settingsType = DEFAULT_SETTINGS_TYPE;
            var appGlobalSettings = GlobalAppSettingsHelper.Instance.FetchAppGlobalSettings();
            if (appGlobalSettings.UseDeviceSpecificSettings) {
                settingsType = AppSettingsType.Local;
            } else {
                settingsType = AppSettingsType.Roaming;
            }
            return settingsType;
        }


        public ApplicationDataContainer GetLocalAppSettings()
        {
            return ApplicationData.Current.LocalSettings;
        }
        public ApplicationDataContainer GetRoamingAppSettings()
        {
            return ApplicationData.Current.RoamingSettings;
        }

        public ApplicationDataContainer GetAppSettings()
        {
            ApplicationDataContainer applicationDataContainer;
            AppSettingsType settingsType = GetSettingsType();
            switch (settingsType) {
                case AppSettingsType.Local:
                    applicationDataContainer = ApplicationData.Current.LocalSettings;
                    break;
                case AppSettingsType.Roaming:
                default:
                    applicationDataContainer = ApplicationData.Current.RoamingSettings;
                    break;
            }
            return applicationDataContainer;
        }



        // TBD:

        // Copy local to roaming
        // Copy roaming to local
        // ...


    }

}
