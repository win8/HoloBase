﻿using HoloBase.Version;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Background.Common
{
    public struct BackgroundTaskAccessGrant
    {
        private bool accessGranted;
        private long requestedTime;
        // private string appVersion;   // ????
        // cf. HoloBase.Version.VersionUtil
        private ulong appVersion;


        //public BackgroundTaskAccessGrant(bool accessGranted, long requestedTime)
        //    : this(accessGranted, requestedTime, VersionUtil.GetCurrentVersionCode())
        //{
        //}
        //public BackgroundTaskAccessGrant(bool accessGranted, long requestedTime, ulong appVersion)
        //{
        //    this.accessGranted = accessGranted;
        //    this.requestedTime = requestedTime;
        //    this.appVersion = appVersion;
        //}

        public BackgroundTaskAccessGrant(ulong appVersion)
            : this(appVersion, false, 0L)
        {
        }
        public BackgroundTaskAccessGrant(ulong appVersion, bool accessGranted, long requestedTime)
        {
            this.appVersion = appVersion;
            this.accessGranted = accessGranted;
            this.requestedTime = requestedTime;
        }

        public ulong AppVersion
        {
            get
            {
                return appVersion;
            }
            set
            {
                appVersion = value;
            }
        }

        public bool AccessGranted
        {
            get
            {
                return accessGranted;
            }
            set
            {
                accessGranted = value;
            }
        }
        public long RequestedTime
        {
            get
            {
                return requestedTime;
            }
            set
            {
                requestedTime = value;
            }
        }


    }
}
