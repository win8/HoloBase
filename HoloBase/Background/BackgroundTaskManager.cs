﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;


namespace HoloBase.Background
{
    public sealed class BackgroundTaskManager
    {
        private static BackgroundTaskManager instance = null;
        public static BackgroundTaskManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new BackgroundTaskManager();
                }
                return instance;
            }
        }
        private BackgroundTaskManager()
        {
        }


        // ????
        // ????

        //public bool IsAccessRequestGranted()
        //{
        //    // ???
        //    var task = IsAccessRequestGrantedAsync(false);
        //    return task.Result;
        //}
        public async Task<bool> IsAccessRequestGrantedAsync()
        {
            return await IsAccessRequestGrantedAsync(false);
        }
        public async Task<bool> IsAccessRequestGrantedAsync(bool showRequest)
        {
            var granted = false;

            // [1] check the settings.
            // ...
            granted = GeneralTaskDataManager.Instance.IsBackgroundTaskAccessGranted();

            // [2] if granted == false,
            if (granted == false) {
                if (showRequest) {

                    granted = await RequestAccessAsync();
                }
            }

            return granted;
        }


        // TBD:
        // This call from the MainPage crashes once in a while.
        // Why????
        public async Task<bool> RequestAccessAsync()
        {
            var granted = false;

            var status = await BackgroundExecutionManager.RequestAccessAsync();
            if (status == BackgroundAccessStatus.Unspecified) {
                // ????
                // what to do???
            } else {
                if (status == BackgroundAccessStatus.Denied) {

                    // ???

                } else {
                    // Allowed...



                    granted = true;
                }
            
                // TBD:
                // Store this on the settings db...
                // ....
                GeneralTaskDataManager.Instance.SaveBackgroundTaskAccessGrantStatus(granted);
            
            }

            return granted;
        }


        public bool IsTaskAlreadyRegistered(string taskName)
        {
            var taskRegistered = false;
            foreach (var task in BackgroundTaskRegistration.AllTasks) {
                if (task.Value.Name.Equals(taskName)) {
                    taskRegistered = true;
                    break;
                }
            }
            return taskRegistered;
        }


        public IBackgroundTaskRegistration FindTaskRegistration(string taskName)
        {
            IBackgroundTaskRegistration reg = null;
            foreach (var task in BackgroundTaskRegistration.AllTasks) {
                if (task.Value.Name.Equals(taskName)) {
                    reg = task.Value;
                    break;
                }
            }
            return reg;
        }


        public void UnregisterTask(string taskName)
        {
            var reg = FindTaskRegistration(taskName);
            if (reg != null) {
                reg.Unregister(false);
                // TBD:
                // how to cancel Progress/Completed event handlers???
                // ...
            }
        }
    
    }
}
