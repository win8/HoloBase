﻿using HoloBase.Background.Common;
using HoloBase.Settings.App;
using HoloBase.Version;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Background
{
    public sealed class GeneralTaskSettingsHelper
    {
        private const string KEY_ACCESSGRANT_CONTAINER_PREFIX = "BackgroundTask_AccessGrant_Container_";
        private const string ITEM_FIELD_APP_VERSION = "AppVersion";
        private const string ITEM_FIELD_ACCESS_GRANTED = "AccessGranted";
        private const string ITEM_FIELD_REQUESTED_TIME = "RequestedTime";


        private static GeneralTaskSettingsHelper instance = null;
        public static GeneralTaskSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new GeneralTaskSettingsHelper();
                }
                return instance;
            }
        }
        private GeneralTaskSettingsHelper()
        {
        }


        private static string GenerateAccessGrantContainerKey(ulong appVersion)
        {
            var key = KEY_ACCESSGRANT_CONTAINER_PREFIX + appVersion;
            return key;
        }
        private static ulong ParseAppVersionFromAccessGrantContainerKey(string key)
        {
            var appVersion = 0UL;
            try {
                var strAppVersion = key.Substring(KEY_ACCESSGRANT_CONTAINER_PREFIX.Length);
                appVersion = Convert.ToUInt64(strAppVersion);
            } catch (Exception) {
                // Ignore.
            }
            //if (appVersion == 0UL) {
            //    appVersion = VersionUtil.GetCurrentVersionCode();   // ????
            //}
            return appVersion;
        }



        // TBD:
        public List<BackgroundTaskAccessGrant> GetAllAccessGrants()
        {
            var items = new List<BackgroundTaskAccessGrant>();
            var containers = AppDataOptionHelper.Instance.GetAppSettings().Containers;
            foreach (var key in containers.Keys) {
                // var dm = containers[key];

                var id = ParseAppVersionFromAccessGrantContainerKey(key);
                var dm = FetchAccessGrant(id);
                if (dm != null) {
                    items.Add(dm.Value);
                }
            }

            // ????
            // List<BackgroundTaskAccessGrant> sortedList = clips.OrderByDescending(o => o.RequestedTime).ToList();
            List<BackgroundTaskAccessGrant> sortedList = items.OrderByDescending(o => o.AppVersion).ToList();
            // ...

            return sortedList;
        }



        public void StoreAccessGrant(BackgroundTaskAccessGrant item)
        {
            var id = item.AppVersion;
            if (id == 0UL) {
                id = VersionUtil.GetCurrentVersionCode();   // ????
                item.AppVersion = id;
            }
            var key = GenerateAccessGrantContainerKey(id);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();

            var container = appSettings.CreateContainer(key, ApplicationDataCreateDisposition.Always);
            appSettings.Containers[key].Values[ITEM_FIELD_APP_VERSION] = item.AppVersion;
            appSettings.Containers[key].Values[ITEM_FIELD_ACCESS_GRANTED] = item.AccessGranted;
            appSettings.Containers[key].Values[ITEM_FIELD_REQUESTED_TIME] = item.RequestedTime;
        }

        public bool ContainsAccessGrant(ulong appVersion)
        {
            var key = GenerateAccessGrantContainerKey(appVersion);
            return AppDataOptionHelper.Instance.GetAppSettings().Containers.ContainsKey(key);
        }

        public BackgroundTaskAccessGrant? FetchAccessGrant(ulong appVersion)
        {
            BackgroundTaskAccessGrant? item = null;
            var key = GenerateAccessGrantContainerKey(appVersion);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
            if (appSettings.Containers.ContainsKey(key)) {
                // var strId = appSettings.Containers[key].Values[ITEM_FIELD_APP_VERSION].ToString();

                BackgroundTaskAccessGrant dm = new BackgroundTaskAccessGrant(appVersion);

                var granted = false;
                if (appSettings.Containers[key].Values.ContainsKey(ITEM_FIELD_ACCESS_GRANTED) && appSettings.Containers[key].Values[ITEM_FIELD_ACCESS_GRANTED] != null) {
                    try {
                        var strGranted = appSettings.Containers[key].Values[ITEM_FIELD_ACCESS_GRANTED].ToString();
                        granted = Convert.ToBoolean(strGranted);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                dm.AccessGranted = granted;

                var updated = 0L;
                if (appSettings.Containers[key].Values.ContainsKey(ITEM_FIELD_REQUESTED_TIME) && appSettings.Containers[key].Values[ITEM_FIELD_REQUESTED_TIME] != null) {
                    try {
                        var strRequestedTime = appSettings.Containers[key].Values[ITEM_FIELD_REQUESTED_TIME].ToString();
                        updated = Convert.ToInt64(strRequestedTime);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                dm.RequestedTime = updated;

                item = dm;
            }
            return item;
        }

        public bool DeleteAccessGrant(ulong appVersion)
        {
            var suc = false;
            var key = GenerateAccessGrantContainerKey(appVersion);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
            if (appSettings.Containers.ContainsKey(key)) {
                appSettings.DeleteContainer(key);
                suc = true;
            }
            return suc;
        }


    }

}
