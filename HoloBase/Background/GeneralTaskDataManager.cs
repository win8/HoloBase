﻿using HoloBase.Background.Common;
using HoloBase.Version;
using HoloCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Background
{
    public sealed class GeneralTaskDataManager
    {
        private static GeneralTaskDataManager instance = null;
        public static GeneralTaskDataManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new GeneralTaskDataManager();
                }
                return instance;
            }
        }
        private GeneralTaskDataManager()
        {
        }

    
        public bool IsBackgroundTaskAccessGranted()
        {
            var granted = false;
            var currentVersion = VersionUtil.GetCurrentVersionCode();
            var item = GeneralTaskSettingsHelper.Instance.FetchAccessGrant(currentVersion);
            if (item != null) {
                var accessGrant = item.Value;
                granted = accessGrant.AccessGranted;
            }
            return granted;
        }

        public void SaveBackgroundTaskAccessGrantStatus(bool granted)
        {
            var currentVersion = VersionUtil.GetCurrentVersionCode();
            var backgroundTaskAccessGrant = new BackgroundTaskAccessGrant(currentVersion);
            backgroundTaskAccessGrant.AccessGranted = granted;
            backgroundTaskAccessGrant.RequestedTime = DateTimeUtil.CurrentUnixEpochMillis();
            GeneralTaskSettingsHelper.Instance.StoreAccessGrant(backgroundTaskAccessGrant);
        }
    

    }
}
