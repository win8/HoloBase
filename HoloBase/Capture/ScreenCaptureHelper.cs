﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Capture
{
    // TBD:
    // what is screen capture?
    // Can we use it to record screens???

    // ScreenCapture class
    // https://msdn.microsoft.com/en-us/library/windows/apps/windows.media.capture.screencapture.aspx

    // MediaCaptureInitializationSettings class
    // https://msdn.microsoft.com/en-us/library/windows/apps/xaml/windows.media.capture.mediacaptureinitializationsettings.aspx

    // RenderTargetBitmap Class
    // https://msdn.microsoft.com/en-us/library/system.windows.media.imaging.rendertargetbitmap%28v=vs.110%29.aspx


    public sealed class ScreenCaptureHelper
    {
        private static ScreenCaptureHelper instance = null;
        public static ScreenCaptureHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new ScreenCaptureHelper();
                }
                return instance;
            }
        }
        private ScreenCaptureHelper()
        {
        }

 
        // tbd
        public void CaptureScreen()
        {
            // ....
        }

        // tbd
        public void StartRecording()
        {
            // ....
        }

        // tbd
        public void PauseRecording()
        {
            // ....
        }

        // tbd
        public void ResumeRecording()
        {
            // ....
        }

        // tbd
        public void StopRecording()
        {
            // ....
        }

    }
}
