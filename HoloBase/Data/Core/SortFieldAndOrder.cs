﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Data.Core
{
    public class SortFieldAndOrder
    {
        // ??? No sorting ???
        public static readonly SortFieldAndOrder Null = new SortFieldAndOrder(null, SortOrder.None);

        // Does it make sense to have here a field name???
        private string sortField;   // Cannot be null/empty...
        private SortOrder sortOrder;

        public SortFieldAndOrder(string sortField)
            : this(sortField, SortOrder.Ascending)   // ascending by default.
        {
        }
        public SortFieldAndOrder(string sortField, SortOrder sortOrder)
        {
            this.sortField = sortField;
            this.sortOrder = sortOrder;
        }

        public string SortField
        {
            get
            {
                return sortField;
            }
        }
        public SortOrder SortOrder
        {
            get
            {
                return sortOrder;
            }
            internal set
            {
                sortOrder = value;
            }
        }


        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("SortField:").Append(SortField).Append(";");
            sb.Append("SortOrder:").Append(Enum.GetName(typeof(SortOrder), SortOrder));
            return sb.ToString();
        }
    }
}
