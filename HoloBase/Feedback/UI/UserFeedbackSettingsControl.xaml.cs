﻿using HoloBase.Share;
using HoloBase.Version;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HoloBase.Feedback.UI
{
    public sealed partial class UserFeedbackSettingsControl : UserControl
    {
        private SettingsFlyout parentSettingsFlyout = null;   // For Windows
        private Page parentSettingsPage = null;               // For Windows Phone.

        private string defaultMessageSubject;
        private string defaultMessageBody;

        public UserFeedbackSettingsControl()
        {
            this.InitializeComponent();

            defaultMessageSubject = "Feedback on Holo App";  // TBD: how to get the app name?
            CheckBoxReplyRequested.IsChecked = false;
            //TextBoxReplyEmailAddress.IsEnabled = false;
            ButtonSend.IsEnabled = false;

            var versionString = VersionUtil.GetCurrentVersionString();
            // TextBlockVerionString.Text = "(Current app version: " + versionString + ")";
            TextBlockVerionString.Text = "- Current app version: " + versionString;
        }

        public SettingsFlyout ParentSettingsFlyout
        {
            get
            {
                return parentSettingsFlyout;
            }
            set
            {
                parentSettingsFlyout = value;
            }
        }
        public Page ParentSettingsPage
        {
            get
            {
                return parentSettingsPage;
            }
            set
            {
                parentSettingsPage = value;
            }
        }


        public string DefaultMessageSubject
        {
            get
            {
                return defaultMessageSubject;
            }
            set
            {
                defaultMessageSubject = value;
            }
        }

        public string DefaultMessageBody
        {
            get
            {
                return defaultMessageBody;
            }
            set
            {
                defaultMessageBody = value;
                TextBoxMessageBody.Text = defaultMessageBody;  // ???
            }
        }

        // temporary
        public void ShowCancelButton()
        {
            ButtonSend.Width = 160;
            ButtonCancel.Width = 90;
            ButtonSend.Visibility = Visibility.Visible;
            ButtonCancel.Visibility = Visibility.Visible;
        }
        public void HideCancelButton()
        {
            ButtonSend.Width = 255;
            ButtonSend.Visibility = Visibility.Visible;
            ButtonCancel.Visibility = Visibility.Collapsed;
        }
        // temporary


        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            // Note: Client may have set the default subject.
            // We append the reply requested email....
            if (CheckBoxReplyRequested.IsChecked == true) {
                //var emailAddr = TextBoxReplyEmailAddress.Text;   // TBD: Validate email address?
                //if (!String.IsNullOrEmpty(emailAddr)) {
                //    defaultMessageSubject += " (Reply requested: " + emailAddr + ")";
                //}
                defaultMessageSubject += " (Reply requested)";
            }

            var subject = defaultMessageSubject;
            var msgBody = TextBoxMessageBody.Text;
            if (!String.IsNullOrEmpty(msgBody)) {
                var emailShareHandler = new EmailShareHandler(subject, msgBody);
                emailShareHandler.InitiateShare(EmailShareContentFormat.Text);

                // Explicit
                Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
            } else {
                // ????
            }

            // ???
            // DismissSettingsFlyout();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DismissSettingsFlyout();
        }

        private void DismissSettingsFlyout()
        {
            if (parentSettingsFlyout != null) {
                parentSettingsFlyout.Hide();
            } else if (parentSettingsPage != null) {
                // ????
                // Go "back" ???
            }
        }

        private void CheckBoxReplyRequested_Checked(object sender, RoutedEventArgs e)
        {
            //TextBoxReplyEmailAddress.IsEnabled = true;
            //// TextBoxReplyEmailAddress.Text = "";
        }

        private void CheckBoxReplyRequested_Unchecked(object sender, RoutedEventArgs e)
        {
            //TextBoxReplyEmailAddress.IsEnabled = false;
            //TextBoxReplyEmailAddress.Text = "";
        }

        private void TextBoxMessageBody_TextChanged(object sender, TextChangedEventArgs e)
        {
            var text = TextBoxMessageBody.Text;
            if (String.IsNullOrEmpty(text)) {
                ButtonSend.IsEnabled = false;
            } else {
                ButtonSend.IsEnabled = true;
            }
        }
    }
}
