﻿using HoloBase.Documents.Core;
using HoloBase.Texts.Clips;
using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Core;
using HoloBase.Texts.Edit;
using HoloBase.Texts.Edit.Core;
using HoloBase.Texts.Store;
using HoloBase.Texts.Urr;
using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Documents
{
    public class SimpleDocumentManager : IContentContainer, ITextClipboardHolder, IUndoRedoManagerHolder, ICoreEditManagerHolder, IDocumentManager, IClipboardEditManager, IBasicTextEditManager, IEditUndoRedoManager, IContentUndoRedoManager
    {
        // Pair, storageFile and contentManager, represents a "document".

        // temporary buffer.
        // This is really IContentContainer.
        // private IContentEditUndoRedoManager contentManager;
        private IBasicTextEditManager contentEditManager;
        // Somewhat strange... but "Cast cache" 
        private ICoreEditManager coreEditManager = null;
        private IBasicTextEditManager basicTextEditManager = null;
        private ITextEditManager textEditManager = null;
        private IAdvancedTextEditManager advancedTextEditManager = null;
        private IUndoRedoManager undoRedoManager = null;
        private IEditUndoRedoManager editUndoRedoManager = null;
        private IContentUndoRedoManager contentUndoRedoManager = null;
        private ITextClipboard textClipboard = null;
        private ITextLinearClipboard textLinearClipboard = null;

        // tbd: 
        // metadata ???

        // persistent storage.
        //private IStorageFile storageFile;
        //// What about data stored in non-fs storages????
        private IContentStoreManager contentStorageManager;

        //// Clipboard
        //// private ITextClipboard clipboard;
        //private ITextStackClipboard clipboard;

        // Options:
        private bool isAutoSaveEnabled;
        private uint autoSaveInterval;    // in seconds

        // Inverse-Dirty flag
        // tbd: When do we set this to false????
        private bool isSynchronized;

        // public SimpleDocumentManager(ICoreEditManager contentEditManager = null, IContentStoreManager contentStorageManager = null)
        public SimpleDocumentManager(IBasicTextEditManager contentEditManager = null, IContentStoreManager contentStorageManager = null)
        {
            this.contentEditManager = contentEditManager ?? new BasicTextEditManager(new UndoRedoManager());
            if (this.contentEditManager is IUndoRedoManagerHolder) {
                this.undoRedoManager = ((IUndoRedoManagerHolder) this.contentEditManager).UndoRedoManager;
                this.editUndoRedoManager = this.undoRedoManager as IEditUndoRedoManager;
                this.contentUndoRedoManager = this.undoRedoManager as IContentUndoRedoManager;
            }
            if (this.contentEditManager is ITextClipboardHolder) {
                this.textClipboard = ((ITextClipboardHolder) this.contentEditManager).TextClipboard;
                this.textLinearClipboard = this.textClipboard as ITextLinearClipboard;
            }
            //if (this.contentEditManager is ICoreEditManagerHolder) {
            //    this.coreEditManager = ((ICoreEditManagerHolder) this.contentEditManager).CoreEditManager;
            //    this.basicTextEditManager = this.coreEditManager as IBasicTextEditManager;
            //    this.textEditManager = this.coreEditManager as ITextEditManager;
            //    this.advancedTextEditManager = this.coreEditManager as IAdvancedTextEditManager;
            //}
            if (this.contentEditManager is ICoreEditManager) {
                this.coreEditManager = (ICoreEditManager) this.contentEditManager;
                this.basicTextEditManager = this.coreEditManager as IBasicTextEditManager;
                this.textEditManager = this.coreEditManager as ITextEditManager;
                this.advancedTextEditManager = this.coreEditManager as IAdvancedTextEditManager;
            }


            // TBD:

            // storageFile = null;
            // this.contentStorageManager = contentStorageManager ?? new DefaultStorageFileContentStoreManager(storageFile);

            var itemName = "abc";
            this.contentStorageManager = contentStorageManager ?? new DefaultAppDataContentStoreManager(itemName);


            isAutoSaveEnabled = false;
            autoSaveInterval = 0U;
            isSynchronized = false;
        }


        public string CurrentContent
        {
            get
            {
                return this.contentEditManager.CurrentContent;
            }
            set
            {
                this.contentEditManager.CurrentContent = value;
            }
        }

        public IUndoRedoManager UndoRedoManager
        {
            get
            {
                return this.undoRedoManager;
            }
        }
        protected IEditUndoRedoManager EditUndoRedoManager
        {
            get
            {
                return this.editUndoRedoManager;
            }
        }
        protected IContentUndoRedoManager ContentUndoRedoManager
        {
            get
            {
                return this.contentUndoRedoManager;
            }
        }

        public ITextClipboard TextClipboard
        {
            get
            {
                return this.textClipboard;
            }
        }
        protected ITextLinearClipboard TextLinearClipboard
        {
            get
            {
                return this.textLinearClipboard;
            }
        }

        public ICoreEditManager CoreEditManager
        {
            get
            {
                return this.coreEditManager;
            }
        }
        protected IBasicTextEditManager BasicTextEditManager
        {
            get
            {
                return this.basicTextEditManager;
            }
        }
        protected ITextEditManager TextEditManager
        {
            get
            {
                return this.textEditManager;
            }
        }
        protected IAdvancedTextEditManager AdvancedTextEditManager
        {
            get
            {
                return this.advancedTextEditManager;
            }
        }



        public bool IsAutoSaveEnabled
        {
            get
            {
                return isAutoSaveEnabled;
            }
            set
            {
                isAutoSaveEnabled = value;
            }
        }
        public uint AutoSaveInterval
        {
            get
            {
                return autoSaveInterval;
            }
            set
            {
                autoSaveInterval = value;
            }
        }
        public bool IsSynchronized
        {
            get
            {
                return isSynchronized;
            }
            set
            {
                isSynchronized = value;
            }
        }


        public async Task<bool> SyncDownAsync()
        {
            var suc = await contentStorageManager.SyncDownAsync(CurrentContent);
            if (suc) {
                isSynchronized = true;
            }
            return suc;
        }

        public async Task<bool> SyncUpAsync()
        {
            var storedContent = await contentStorageManager.SyncUpAsync();
            if (storedContent != null) {
                CurrentContent = storedContent;
                isSynchronized = true;
                return true;
            }
            return false;
        }



        // //////////////////////////////////////////////////////////////////////
        // IClipboardEditManager

        public string CutText(int position, string selectedText)
        {
            if (BasicTextEditManager != null) {
                return BasicTextEditManager.CutText(position, selectedText);
            } else {
                // tbd:
                return null;
            }
        }

        public string CopyText(int position, string selectedText)
        {
            if (BasicTextEditManager != null) {
                return BasicTextEditManager.CopyText(position, selectedText);
            } else {
                // tbd:
                return null;
            }
        }

        public string PasteText(int position)
        {
            if (BasicTextEditManager != null) {
                return BasicTextEditManager.PasteText(position);
            } else {
                // tbd:
                return null;
            }
        }

        public string PasteText(int position, string replacedText)
        {
            if (BasicTextEditManager != null) {
                return BasicTextEditManager.PasteText(position, replacedText);
            } else {
                // tbd:
                return null;
            }
        }



        // ////////////////////////////////////////////////////////////////////////////////////
        // IBasicTextEditManager

        public string InsertText(int position, string text)
        {
            if (BasicTextEditManager != null) {
                return BasicTextEditManager.InsertText(position, text);
            } else {
                // tbd:
                return null;
            }
        }

        public string DeleteText(int position, string text)
        {
            if (BasicTextEditManager != null) {
                return BasicTextEditManager.DeleteText(position, text);
            } else {
                // tbd:
                return null;
            }
        }

        public string ReplaceText(int position, string newText, string oldText)
        {
            if (BasicTextEditManager != null) {
                return BasicTextEditManager.ReplaceText(position, newText, oldText);
            } else {
                // tbd:
                return null;
            }
        }



        // ///////////////////////////////////////////////////////////////////////////
        // IEditUndoRedoManager

        public bool AutoCombine
        {
            get
            {
                if (EditUndoRedoManager != null) {
                    return EditUndoRedoManager.AutoCombine;
                } else {
                    // tbd:
                    return false;
                    // throw new NotImplementedException();
                }
            }
        }

        public bool CanUndo
        {
            get
            {
                if (EditUndoRedoManager != null) {
                    return EditUndoRedoManager.CanUndo;
                } else {
                    // tbd:
                    return false;
                    // throw new NotImplementedException();
                }
            }
        }

        public string Undo()
        {
            if (EditUndoRedoManager != null) {
                return EditUndoRedoManager.Undo();
            } else {
                // tbd:
                return null;
            }
        }

        public bool CanRedo
        {
            get
            {
                if (EditUndoRedoManager != null) {
                    return EditUndoRedoManager.CanRedo;
                } else {
                    // tbd:
                    return false;
                    // throw new NotImplementedException();
                }
            }
        }

        public string Redo()
        {
            if (EditUndoRedoManager != null) {
                return EditUndoRedoManager.Redo();
            } else {
                // tbd:
                return null;
            }
        }



        // /////////////////////////////////////////////////////////////////////////////
        // IContentUndoRedoManager

        public bool UpdateContent(string postOpContent)
        {
            if (ContentUndoRedoManager != null) {
                return ContentUndoRedoManager.UpdateContent(postOpContent);
            } else {
                // tbd:
                return false;
            }
        }

        public bool UpdateContent(string postOpContent, string preOpContent)
        {
            if (ContentUndoRedoManager != null) {
                return ContentUndoRedoManager.UpdateContent(postOpContent, preOpContent);
            } else {
                // tbd:
                return false;
            }
        }

    }
}
