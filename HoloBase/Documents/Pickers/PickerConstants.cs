﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Documents.Pickers
{
    // These are used only for WP81
    // but, they may be used in the common method signatures, etc...
    // ...

    public enum FolderPickerContext : short
    {
        Unspecified = 0,
        DocumentInputFolder = 1,
        DocumentOutputFolder,
    }
    public enum FileOpenPickerContext : short
    {
        Unspecified = 0,
        DocumentInputFile = 1,
    }
    public enum FileSavePickerContext : short
    {
        Unspecified = 0,
        DocumentOutputFile = 1,
    }

    public static class PickerConstants
    {
        // Better names???
        public const string KEY_CONTEXT_FOR_FOLDER_PICKER = "folderContext";
        public const string KEY_CONTEXT_FOR_FILE_OPEN_PICKER = "fileOpenContext";
        public const string KEY_CONTEXT_FOR_FILE_SAVE_PICKER = "fileSaveContext";

    }
}
