﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;


namespace HoloBase.Documents.Pickers
{
    public interface IOutputDocumentPicker
    {
        Task<IStorageFolder> ChooseOutputFolderAsync(string settingsIdentifier, FolderPickerContext pickerContext);
        Task<IStorageFile> ChooseOutputFileAsync(string settingsIdentifier, string suggestedFileName, FileSavePickerContext pickerContext);

    }

    public interface IInputDocumentPicker
    {
        Task<IStorageFile> ChooseInputFileAsync(string settingsIdentifier, PickerLocationId suggestedStartLocation);
        Task<IStorageFile> ChooseInputFileAsync(string settingsIdentifier, PickerLocationId suggestedStartLocation, FileOpenPickerContext pickerContext);

    }
}
