﻿using HoloJson.Mini.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Documents.Pickers
{
    public static class DefaultDocumentFileNameUtil
    {
        public static string GenerateSuggestedOutputFileName(string content = null)
        {
            string suggestedOutputFileName = "Document-";
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            suggestedOutputFileName += now;
            suggestedOutputFileName += ".txt";   // ???

            return suggestedOutputFileName;
        }


    }
}
