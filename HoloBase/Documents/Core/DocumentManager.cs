﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Documents.Core
{
    public interface IDocumentManager : IContentContainer  // , IContentStoreManager
    {
        //IContentStoreManager ContentStoreManager
        //{
        //    get;
        //}

        // if true, we call SyncDown() on a regular interval.
        //     Sync down is not instantaneous.
        bool IsAutoSaveEnabled
        {
            get;
        }
        uint AutoSaveInterval
        {
            get;
        }

        bool IsSynchronized
        {
            get;
        }

        // Save buffer to file
        // This is normal "save"
        Task<bool> SyncDownAsync();

        // Update buffer from file.
        // This is an unutal operation. 
        // Needed when "resotring" the buffer from the persistent storage.
        Task<bool> SyncUpAsync();


    }
}
