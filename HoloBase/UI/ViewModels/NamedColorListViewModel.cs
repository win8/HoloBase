﻿using HoloBase.Data.Core;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.ViewModels
{
    public class NamedColorListViewModel : IViewModel, INotifyPropertyChanged
    {
        // consts.
        private static readonly SortFieldAndOrder SortByNameAscending = new SortFieldAndOrder("Name", SortOrder.Ascending);
        private static readonly SortFieldAndOrder SortByNameDescending = new SortFieldAndOrder("Name", SortOrder.Descending);
        private static readonly SortFieldAndOrder SortByColorAscending = new SortFieldAndOrder("Color", SortOrder.Ascending);
        private static readonly SortFieldAndOrder SortByColorDescending = new SortFieldAndOrder("Color", SortOrder.Descending);

        private IList<ColorWrap> colorList;
        private SortFieldAndOrder currentSortFieldAndOrder = null;

        public NamedColorListViewModel()
        {
            // tbd
            colorList = NamedColors.ColorWrapList;
            currentSortFieldAndOrder = SortByNameAscending;
            SortListItems();
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }

        public IList<string> ColorNameList
        {
            get
            {
                // ...
                return NamedColors.ColorNameList;
            }
        }
        public IList<ColorWrap> NamedColorList
        {
            get
            {
                // ...
                return colorList;
            }
            private set
            {
                colorList = value;
            }
        }

        public void SortByName()
        {
            if (IsCurrentlySortedByName && CurrentSortOrder == SortOrder.Ascending) {
                currentSortFieldAndOrder = SortByNameDescending;
            } else {
                currentSortFieldAndOrder = SortByNameAscending;
            }
            SortListItems();
        }

        public void SortByColor()
        {
            if (IsCurrentlySortedByColor && CurrentSortOrder == SortOrder.Ascending) {
                currentSortFieldAndOrder = SortByColorDescending;
            } else {
                currentSortFieldAndOrder = SortByColorAscending;
            }
            SortListItems();
        }

        private bool IsCurrentlySortedByName
        {
            get
            {
                return (currentSortFieldAndOrder != null && "Name".Equals(currentSortFieldAndOrder.SortField));
            }
        }
        private bool IsCurrentlySortedByColor
        {
            get
            {
                return (currentSortFieldAndOrder != null && "Color".Equals(currentSortFieldAndOrder.SortField));
            }
        }
        private SortOrder CurrentSortOrder
        {
            get
            {
                if (currentSortFieldAndOrder == null) {
                    return SortOrder.None;
                } else {
                    return currentSortFieldAndOrder.SortOrder;
                }
            }
        }

        private void SortListItems()
        {
            // var list = NamedColors.ColorWrapList;
            if (IsCurrentlySortedByName) {
                switch (CurrentSortOrder) {
                    case SortOrder.Descending:
                        NamedColorList = new List<ColorWrap>(NamedColorList.OrderByDescending(o => o.ColorName));
                        break;
                    case SortOrder.Ascending:
                    default:
                        NamedColorList = new List<ColorWrap>(NamedColorList.OrderBy(o => o.ColorName));
                        break;
                }
            } else if (IsCurrentlySortedByColor) {
                switch (CurrentSortOrder) {
                    case SortOrder.Descending:
                        NamedColorList = new List<ColorWrap>(NamedColorList.OrderByDescending(o => (o.Saturation > 0) ? o.Hue : 360.1 + (1.0 - o.Lightness)).ThenByDescending(o => (1.0 - o.Lightness)));
                        break;
                    case SortOrder.Ascending:
                    default:
                        NamedColorList = new List<ColorWrap>(NamedColorList.OrderBy(o => (o.Saturation > 0) ? o.Hue : 360.1 + (1.0 - o.Lightness)).ThenBy(o => (1.0 - o.Lightness)));
                        break;
                }
            } else {
                // ignore.
            }
            RaisePropertyChanged("NamedColorList");
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
