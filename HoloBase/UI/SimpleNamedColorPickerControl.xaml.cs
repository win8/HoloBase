﻿using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HoloBase.UI.Util;
using HoloBase.UI.Core;
using HoloCore.Core;
using HoloBase.UI.ViewModels;


namespace HoloBase.UI
{
    public sealed partial class SimpleNamedColorPickerControl : UserControl, IRefreshableElement, IViewModelHolder, INamedColorPickerControl
    {
        public event EventHandler<ColorSelectionChangedEventArgs> ColorSelectionChanged;

        private NamedColorListViewModel viewModel;

        public SimpleNamedColorPickerControl()
        {
            this.InitializeComponent();

            ResetViewModel();
            ListViewSimpleNamedColorList.ItemsSource = NamedColors.ColorNameList;

            isCancelButtonVisible = false;
            RefreshElementVisibility();
        }
        private void ResetViewModel()
        {
            var vm = new NamedColorListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(NamedColorListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (NamedColorListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }


        //public NamedColor CurrentSelectedColor
        //{
        //    get
        //    {
        //        return currentSelectedColor;
        //    }
        //    set
        //    {
        //        currentSelectedColor = value;
        //    }
        //}
        

        private bool isCancelButtonVisible;
        public bool IsCancelButtonVisible
        {
            get
            {
                return isCancelButtonVisible;
            }
            set
            {
                isCancelButtonVisible = value;
                RefreshElementVisibility();
            }
        }
        private void RefreshElementVisibility()
        {
            if (isCancelButtonVisible) {
                StackPanelBottomRow.Visibility = Visibility.Visible;
            } else {
                StackPanelBottomRow.Visibility = Visibility.Collapsed;
            }
        }


        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = ListViewSimpleNamedColorList.SelectedItem;
            var selectedColorName = selectedItem as string;
            if (selectedColorName != null) {   // tbd: validate?
                System.Diagnostics.Debug.WriteLine("Color selected: {0}", selectedColorName);
                //currentSelectedColor = NamedColors.GetNamedColor(selectedColorName);
                NotifyColorSelectionChange(selectedColorName);
            }
            DismissPopup();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DismissPopup();
        }

        private void DismissPopup()
        {
            // tbd:
            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }
        }

        private void ListViewSimpleNamedColorList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var selectedColorName = e.ClickedItem as string;
            if (selectedColorName != null) {   // tbd: validate?
                System.Diagnostics.Debug.WriteLine("Color selected: {0}", selectedColorName);
                //currentSelectedColor = NamedColors.GetNamedColor(selectedColorName);
                NotifyColorSelectionChange(selectedColorName);
            }
            DismissPopup();
        }

        private void NotifyColorSelectionChange(string newColorName)
        {
            // temporary
            if (ColorSelectionChanged != null) {
                ColorSelectionChangedEventArgs e = new ColorSelectionChangedEventArgs(this, new ColorStruct(newColorName));
                // ColorSelectionChanged(this, e);
                ColorSelectionChanged.Invoke(this, e);
            }
        }

    }
}
