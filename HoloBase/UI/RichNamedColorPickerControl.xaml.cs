﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HoloBase.UI.Util;
using HoloBase.UI.Colors;
using HoloBase.UI.Core;
using HoloBase.Data.Core;
using HoloBase.UI.ViewModels;
using HoloCore.Core;


namespace HoloBase.UI
{
    public sealed partial class RichNamedColorPickerControl : UserControl, IRefreshableElement, IViewModelHolder, INamedColorPickerControl
    {
        // Public event for the color picker clients.
        public event EventHandler<ColorSelectionChangedEventArgs> ColorSelectionChanged;

        private NamedColorListViewModel viewModel;

        public RichNamedColorPickerControl()
        {
            this.InitializeComponent();

            ResetViewModel();

            isCancelButtonVisible = false;
            isSortButtonsVisible = false;
            RefreshElementVisibility();
        }
        private void ResetViewModel()
        {
            var vm = new NamedColorListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(NamedColorListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (NamedColorListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }


        // tbd:
        private bool isSortButtonsVisible;
        public bool IsSortButtonsVisible
        {
            get
            {
                return isSortButtonsVisible;
            }
            set
            {
                isSortButtonsVisible = value;
                RefreshElementVisibility();
            }
        }

        private bool isCancelButtonVisible;
        public bool IsCancelButtonVisible
        {
            get
            {
                return isCancelButtonVisible;
            }
            set
            {
                isCancelButtonVisible = value;
                RefreshElementVisibility();
            }
        }
        private void RefreshElementVisibility()
        {
            if (isSortButtonsVisible) {
                StackPanelTopRow.Visibility = Visibility.Visible;
            } else {
                StackPanelTopRow.Visibility = Visibility.Collapsed;
            }
            if (isCancelButtonVisible) {
                StackPanelBottomRow.Visibility = Visibility.Visible;
            } else {
                StackPanelBottomRow.Visibility = Visibility.Collapsed;
            }
        }

        private void ButtonSortByName_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByName();
        }

        private void ButtonSortByColor_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByColor();
        }


        //private void ButtonOK_Click(object sender, RoutedEventArgs e)
        //{
        //    var selectedItem = (ColorStruct) ListViewRichNamedColorList.SelectedItem;
        //    System.Diagnostics.Debug.WriteLine("Color selected: {0}", selectedItem);
        //    NotifyColorSelectionChange(selectedItem);

        //    DismissPopup();
        //}

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DismissPopup();
        }

        private void DismissPopup()
        {
            // tbd:
            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }
        }

        private void ListViewRichNamedColorList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = e.ClickedItem as ColorWrap;
            if (clickedItem != null) {
                var newColor = clickedItem.ColorStruct;
                System.Diagnostics.Debug.WriteLine("Color selected: {0}", newColor);
                NotifyColorSelectionChange(newColor);
            }
            DismissPopup();
        }


        private void NotifyColorSelectionChange(ColorStruct newColor)
        {
            // temporary
            if (ColorSelectionChanged != null) {
                ColorSelectionChangedEventArgs e = new ColorSelectionChangedEventArgs(this, newColor);
                // ColorSelectionChanged(this, e);
                ColorSelectionChanged.Invoke(this, e);
            }
        }

 
    }
}
