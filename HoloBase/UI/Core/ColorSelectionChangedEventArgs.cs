﻿using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Core
{
    public class ColorSelectionChangedEventArgs : EventArgs
    {
        private ColorStruct selectedColor;
        public ColorSelectionChangedEventArgs(object sender, ColorStruct selectedColor)
        {
            this.selectedColor = selectedColor;
        }

        public ColorStruct SelectedColor
        {
            get
            {
                return selectedColor;
            }
            set
            {
                selectedColor = value;
            }
        }
        
    }
}
