﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Core
{
    public interface INamedColorPickerControl
    {
        event EventHandler<ColorSelectionChangedEventArgs> ColorSelectionChanged;
    }
}
