﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Core
{
    public enum CollectionViewType : short
    {
        Unknown = 0,
        List,
        Grid
    }

    public struct CollectionViewTypeStruct
    {
        private CollectionViewType type;
        private string name;

        public CollectionViewTypeStruct(CollectionViewType type, string name = null)
        {
            this.type = type;
            this.name = name;
        }

        public CollectionViewType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
        public string Name
        {
            get
            {
                return name ?? Enum.GetName(typeof(CollectionViewType), this.type);
            }
            set
            {
                name = value;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public static class CollectionViewTypes
    {
        private static IList<CollectionViewType> types = new List<CollectionViewType>();
        static CollectionViewTypes()
        {
            types.Add(CollectionViewType.List);
            types.Add(CollectionViewType.Grid);
        }

        public static IList<CollectionViewType> List
        {
            get
            {
                return types;
            }
        }

        public static bool IsValid(CollectionViewType type)
        {
            return (type == CollectionViewType.List || type == CollectionViewType.Grid);
        }

    }

}
