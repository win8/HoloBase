﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Core
{
    public class CollectionViewTypeEventArgs : EventArgs
    {
        private CollectionViewType newType;
        public CollectionViewTypeEventArgs(object sender, CollectionViewType newType)
        {
            this.newType = newType;
        }

        public CollectionViewType NewType
        {
            get
            {
                return newType;
            }
            set
            {
                newType = value;
            }
        }
        
    }
}
