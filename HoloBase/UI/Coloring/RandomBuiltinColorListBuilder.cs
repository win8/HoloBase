﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Coloring
{
    public sealed class RandomBuiltinColorListBuilder
    {
        private static RandomBuiltinColorListBuilder instance = null;
        public static RandomBuiltinColorListBuilder Instance
        {
            get
            {
                if (instance == null) {
                    instance = new RandomBuiltinColorListBuilder();
                }
                return instance;
            }
        }
        private RandomBuiltinColorListBuilder()
        {
        }



    }
}
