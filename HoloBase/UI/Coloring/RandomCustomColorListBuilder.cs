﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Coloring
{
    public sealed class RandomCustomColorListBuilder
    {
        private static RandomCustomColorListBuilder instance = null;
        public static RandomCustomColorListBuilder Instance
        {
            get
            {
                if (instance == null) {
                    instance = new RandomCustomColorListBuilder();
                }
                return instance;
            }
        }
        private RandomCustomColorListBuilder()
        {
        }



    }
}
