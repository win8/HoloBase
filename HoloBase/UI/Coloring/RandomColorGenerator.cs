﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Coloring
{
    public sealed class RandomColorGenerator
    {
        private static RandomColorGenerator instance = null;
        public static RandomColorGenerator Instance
        {
            get
            {
                if (instance == null) {
                    instance = new RandomColorGenerator();
                }
                return instance;
            }
        }
        private RandomColorGenerator()
        {
        }



    }
}
