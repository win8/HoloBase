﻿using HoloBase.UI.Colors;
using HoloBase.UI.Core;
using HoloBase.UI.ViewModels;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HoloBase.UI
{
    public sealed partial class NamedColorListControl : UserControl, IRefreshableElement, IViewModelHolder, INamedColorPickerControl
    {
        // Public event for the color picker clients.
        public event EventHandler<ColorSelectionChangedEventArgs> ColorSelectionChanged;

        // temporary
        private Size currentControlSize = Size.Empty;

        private NamedColorListViewModel viewModel;
        private bool isColorInfoPopupEnabed;
        private Popup colorInfoPopup;

        public NamedColorListControl()
        {
            this.InitializeComponent();

            ResetViewModel();

            isColorInfoPopupEnabed = false;
            colorInfoPopup = BuildColorInfoPopup();

            isSortButtonsVisible = false;
            RefreshElementVisibility();
            this.SizeChanged += NamedColorListControl_SizeChanged;
        }
        void NamedColorListControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            currentControlSize = e.NewSize;
        }
        private void ResetViewModel()
        {
            var vm = new NamedColorListViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(NamedColorListViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }


        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (NamedColorListViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

        public string BackgroundGridColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }
        public string PopupBackgroundColor
        {
            get
            {
                var infoControl = colorInfoPopup.Child as NamedColorInfoControl;
                if (infoControl != null) {
                    return infoControl.BackgroundGridColor;
                } else {
                    // ????
                    return "Navy";
                }
            }
            set
            {
                var infoControl = colorInfoPopup.Child as NamedColorInfoControl;
                if (infoControl != null) {
                    infoControl.BackgroundGridColor = value;
                } else {
                    // ???
                }
            }
        }


        private static Popup BuildColorInfoPopup()
        {
            // tbd...
            var popup = new Popup();
            popup.Child = new NamedColorInfoControl();
            return popup;
        }

        public bool IsColorInfoPopupEnabed
        {
            get
            {
                return isColorInfoPopupEnabed;
            }
            set
            {
                isColorInfoPopupEnabed = value;
            }
        }
        

        // tbd:
        private bool isSortButtonsVisible;
        public bool IsSortButtonsVisible
        {
            get
            {
                return isSortButtonsVisible;
            }
            set
            {
                isSortButtonsVisible = value;
                RefreshElementVisibility();
            }
        }

        private void RefreshElementVisibility()
        {
            if (isSortButtonsVisible) {
                StackPanelTopRow.Visibility = Visibility.Visible;
            } else {
                StackPanelTopRow.Visibility = Visibility.Collapsed;
            }
        }

        private void ButtonSortByName_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByName();
        }

        private void ButtonSortByColor_Click(object sender, RoutedEventArgs e)
        {
            viewModel.SortByColor();
        }

        private void ListViewNamedColorList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = e.ClickedItem as ColorWrap;
            if (clickedItem != null) {
                var newColor = clickedItem.ColorStruct;
                System.Diagnostics.Debug.WriteLine("Color selected: {0}", newColor);
                NotifyColorSelectionChange(newColor);

                if (isColorInfoPopupEnabed) {
                    var infoControl = colorInfoPopup.Child as NamedColorInfoControl;
                    if (infoControl != null) {
                        infoControl.ColorStruct = newColor;
                        if (!colorInfoPopup.IsOpen) {
                            if (currentControlSize != Size.Empty) {
                                colorInfoPopup.HorizontalOffset = (currentControlSize.Width - 320) < 0 ? 0 : (currentControlSize.Width - 320) / 2.0;
                                colorInfoPopup.VerticalOffset = (currentControlSize.Height - 300) > 400 ? 200 : (currentControlSize.Height - 300) / 2.0;
                            }
                            colorInfoPopup.IsOpen = true;
                        }
                    }
                }
            }
        }


        private void NotifyColorSelectionChange(ColorStruct newColor)
        {
            // temporary
            if (ColorSelectionChanged != null) {
                ColorSelectionChangedEventArgs e = new ColorSelectionChangedEventArgs(this, newColor);
                // ColorSelectionChanged(this, e);
                ColorSelectionChanged.Invoke(this, e);
            }
        }


    }
}
