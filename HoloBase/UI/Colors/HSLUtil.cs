﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;


namespace HoloBase.UI.Colors
{
    public static class HSLUtil
    {
        public static HSL ComputeHSL(ColorStruct colorStruct)
        {
            return ComputeHSL(colorStruct.Color);
        }
        public static HSL ComputeHSL(Color rgbColor)
        {
            var r = rgbColor.R;
            var g = rgbColor.G;
            var b = rgbColor.B;
            return ComputeHSL(r, g, b);
        }
        public static HSL ComputeHSL(byte r, byte g, byte b)
        {
            var hue = 0.0f;
            var saturation = 0.0f;
            var lightness = 0.0f;

            var max = Math.Max(Math.Max(r, g), b);
            var min = Math.Min(Math.Min(r, g), b);

            var sum = max + min;
            var delta = max - min;  // (chroma == delta / 255.0)
            lightness = (float) ((double) sum / (2 * 255.0));

            if (max == min) {
                hue = 0.0f;    // Undefined. Not zero.  (hue==0 is pure red)
                saturation = 0.0f;
            } else {
                if (lightness > 0.5f) {
                    saturation = (float) ((double) delta / ((2 * 255.0 - sum)));
                } else {
                    saturation = (float) ((double) delta / sum);
                }
                var h = 0.0;
                if (max == r) {
                    h = ((double) (g - b)) / delta + (g < b ? 6.0 : 0.0);
                } else if (max == g) {
                    h = ((double) (b - r)) / delta + 2.0;
                } else if (max == b) {
                    h = ((double) (r - g)) / delta + 4.0;
                } else {
                    // this cannot hapen.
                    // What to do ???
                }
                hue = (float) (h * 60.0);
            }

            var hsl = new HSL(hue, saturation, lightness);
            // System.Diagnostics.Debug.WriteLine("HSL computed: {0}", this.hsl);

            return hsl;
        }
    }

}
