﻿using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;


namespace HoloBase.UI.Colors
{
    // Wrapper for ColorStruct (which is essentially a wrapper for Windows.UI.Color).
    // Using many structs, ColorStruct, in a collection can be rather inefficient.
    // Use this class wrapper in a large collection.
    public sealed class ColorWrap : IViewModel, INotifyPropertyChanged
    {
        private ColorStruct colorStruct;
        // Note that we delegate all properties to the embedded colorStruct except for HSL.
        private HSL hsl;
        private bool hslInitialized;
        private object lockObject;   // For HSL computation...

        public ColorWrap()
            : this(ColorStruct.Null)    // ????
        {
        }
        public ColorWrap(NamedColor namedColor)
            : this(new ColorStruct(namedColor))
        {
        }
        public ColorWrap(Color rgbColor)
            : this(new ColorStruct(rgbColor))
        {
        }
        public ColorWrap(string rgbString)
            : this(new ColorStruct(rgbString))
        {
        }
        public ColorWrap(ColorStruct colorStruct)
        {
            lockObject = new object();
            this.colorStruct = colorStruct;
            Init();
        }
        private void Init()
        {
            this.hsl = HSL.Null;
            this.hslInitialized = false;
            // ComputeHSL();
        }

        private void ComputeHSL()
        {
            if (this.hslInitialized == false) {
                this.hsl = HSLUtil.ComputeHSL(colorStruct);
                System.Diagnostics.Debug.WriteLine("HSL computed: {0}", this.hsl);
                this.hslInitialized = true;
            }
        }

        public ColorStruct ColorStruct
        {
            get
            {
                return colorStruct;
            }
            set
            {
                colorStruct = value;
                Init();
                RaisePropertyChanged("ColorStruct");
                RaisePropertyChanged("Color");
                RaisePropertyChanged("Text");
                RaisePropertyChanged("ARGB");
                RaisePropertyChanged("HSL");
            }
        }

        public NamedColor NamedColor
        {
            get
            {
                return ColorStruct.NamedColor;
            }
        }
        public Color Color
        {
            get
            {
                return ColorStruct.Color;
            }
        }

        public string ARGB
        {
            get
            {
                return ColorStruct.ARGB;
            }
        }

        public string ColorName
        {
            get
            {
                return ColorStruct.ColorName;
            }
        }
        public string Text
        {
            get
            {
                return ColorStruct.Text;
            }
        }


        public HSL HSL
        {
            get
            {
                if (this.hslInitialized == false) {
                    lock (lockObject) {    // To avoid multiple computations of HSL.  (--> This is not really necessary...)
                        ComputeHSL();
                    }
                }
                return hsl;
            }
        }

        public float Hue
        {
            get
            {
                return this.HSL.Hue;
            }
        }
        public float Saturation
        {
            get
            {
                return this.HSL.Saturation;
            }
        }
        public float Lightness
        {
            get
            {
                return this.HSL.Lightness;
            }
        }

        public override string ToString()
        {
            return ColorStruct.ToString();
        }

        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
