﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;


namespace HoloBase.UI.Colors
{
    public struct ColorStruct
    {
        // temporary
        public static readonly ColorStruct Null = new ColorStruct(NamedColor.Invalid);
        public static readonly ColorStruct Transparent = new ColorStruct(NamedColor.Transparent);
        public static readonly ColorStruct White = new ColorStruct(NamedColor.White);
        public static readonly ColorStruct Black = new ColorStruct(NamedColor.Black);
        // etc...
        // ...

        // These two fields are like sort of a union.
        // If namedColor is set, rgbColor is used sort of as a cache.
        // If namedColor is not set, or invalid, then rgbColor is the authorative value.
        private NamedColor namedColor;
        private Color rgbColor;
        private string name;
        private HSL hsl;
        private bool hslInitialized;
        private object lockObject;   // For HSL computation...

        public ColorStruct(NamedColor namedColor)
        {
            this.namedColor = namedColor;
            if (NamedColors.IsValid(namedColor)) {
                this.rgbColor = NamedColors.GetColor(namedColor);
            } else {
                // ???
                this.rgbColor = Windows.UI.Colors.Transparent;
            }
            this.name = null;
            this.hsl = HSL.Null;
            this.hslInitialized = false;
            lockObject = new object();
            // ComputeHSL();
        }
        public ColorStruct(Color rgbColor)
        {
            this.namedColor = NamedColor.Invalid;
            this.rgbColor = rgbColor;
            this.name = null;
            this.hsl = HSL.Null;
            this.hslInitialized = false;
            lockObject = new object();
            // ComputeHSL();
        }
        // arg can be a "color name" or an argb string.
        public ColorStruct(string rgbString)
        {
            if (NamedColors.IsNamedColor(rgbString)) {
                this.namedColor = NamedColors.GetNamedColor(rgbString);
                this.rgbColor = NamedColors.GetColor(namedColor);
            } else {
                this.namedColor = NamedColor.Invalid;
                try {
                    this.rgbColor = ColorUtil.GetColorFromArgbString(rgbString);
                } catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Invalid color name or ARGB string. rgbString = {0}, error = {1}", rgbString, ex.Message);
                    // ????
                    this.rgbColor = Windows.UI.Colors.Transparent;
                }
            }
            this.name = null;
            this.hsl = HSL.Null;
            this.hslInitialized = false;
            lockObject = new object();
            // ComputeHSL();
        }

        // Note: Sicne ColorStruct is a struct (not class)
        // this computation will be redone every time .HSL property is accessed. which may not be good for performance.
        // --> Tbd: change ColorStruct to struct?
        // Or, at least use a class version of colorStruct in a list????? 
        private void ComputeHSL()
        {
            if (this.hslInitialized == false) {
                this.hsl = HSLUtil.ComputeHSL(rgbColor);
                // System.Diagnostics.Debug.WriteLine("HSL computed: {0}", this.hsl);
                this.hslInitialized = true;
            }
        }

        public NamedColor NamedColor
        {
            get
            {
                return this.namedColor;
            }
        }
        public Color Color
        {
            get
            {
                return this.rgbColor;
            }
        }
        public string ARGB
        {
            get
            {
                return this.rgbColor.ToString();
            }
        }


        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                // null value "resets" ColorName.
                name = value;
            }
        }
        public string ColorName
        {
            get
            {
                if (this.name != null) {   // empty string is a valid name???
                    // Custom name.
                    return this.name;
                } else {
                    if (NamedColors.IsValid(this.namedColor)) {
                        return Enum.GetName(typeof(NamedColor), this.namedColor);
                    } else {
                        // ????
                        // return null;
                        return this.ARGB;
                    }
                }
            }
        }
        public string Text
        {
            get
            {
                // Note that Text property does not use the custom name.
                if (NamedColors.IsValid(this.namedColor)) {
                    return Enum.GetName(typeof(NamedColor), this.namedColor);
                } else {
                    return this.ARGB;
                }
            }
        }


        public HSL HSL
        {
            get
            {
                if (this.hslInitialized == false) {
                    lock (lockObject) {    // To avoid multiple computations of HSL.  (--> This is not really necessary...)
                        ComputeHSL();
                    }
                }
                return hsl;
            }
        }

        public float Hue
        {
            get
            {
                return this.HSL.Hue;
            }
        }
        public float Saturation
        {
            get
            {
                return this.HSL.Saturation;
            }
        }
        public float Lightness
        {
            get
            {
                return this.HSL.Lightness;
            }
        }


        public override string ToString()
        {
            return this.Text;
        }

    }

}
