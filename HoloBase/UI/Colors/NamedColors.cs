﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;


namespace HoloBase.UI.Colors
{
    // Colors:
    // https://msdn.microsoft.com/en-us/library/windows/apps/windows.ui.colors.aspx
    // ..

    public enum NamedColor
    {
        Invalid = 0,   // Not a valid color. Or, custom color.
        // Custom = 1,  // ???
        AliceBlue,
        AntiqueWhite,
        Aqua,
        Aquamarine,
        Azure,
        Beige,
        Bisque,
        Black,
        BlanchedAlmond,
        Blue,
        BlueViolet,
        Brown,
        BurlyWood,
        CadetBlue,
        Chartreuse,
        Chocolate,
        Coral,
        CornflowerBlue,
        Cornsilk,
        Crimson,
        Cyan,
        DarkBlue,
        DarkCyan,
        DarkGoldenrod,
        DarkGray,
        DarkGreen,
        DarkKhaki,
        DarkMagenta,
        DarkOliveGreen,
        DarkOrange,
        DarkOrchid,
        DarkRed,
        DarkSalmon,
        DarkSeaGreen,
        DarkSlateBlue,
        DarkSlateGray,
        DarkTurquoise,
        DarkViolet,
        DeepPink,
        DeepSkyBlue,
        DimGray,
        DodgerBlue,
        Firebrick,
        FloralWhite,
        ForestGreen,
        Fuchsia,
        Gainsboro,
        GhostWhite,
        Gold,
        Goldenrod,
        Gray,
        Green,
        GreenYellow,
        Honeydew,
        HotPink,
        IndianRed,
        Indigo,
        Ivory,
        Khaki,
        Lavender,
        LavenderBlush,
        LawnGreen,
        LemonChiffon,
        LightBlue,
        LightCoral,
        LightCyan,
        LightGoldenrodYellow,
        LightGray,
        LightGreen,
        LightPink,
        LightSalmon,
        LightSeaGreen,
        LightSkyBlue,
        LightSlateGray,
        LightSteelBlue,
        LightYellow,
        Lime,
        LimeGreen,
        Linen,
        Magenta,
        Maroon,
        MediumAquamarine,
        MediumBlue,
        MediumOrchid,
        MediumPurple,
        MediumSeaGreen,
        MediumSlateBlue,
        MediumSpringGreen,
        MediumTurquoise,
        MediumVioletRed,
        MidnightBlue,
        MintCream,
        MistyRose,
        Moccasin,
        NavajoWhite,
        Navy,
        OldLace,
        Olive,
        OliveDrab,
        Orange,
        OrangeRed,
        Orchid,
        PaleGoldenrod,
        PaleGreen,
        PaleTurquoise,
        PaleVioletRed,
        PapayaWhip,
        PeachPuff,
        Peru,
        Pink,
        Plum,
        PowderBlue,
        Purple,
        Red,
        RosyBrown,
        RoyalBlue,
        SaddleBrown,
        Salmon,
        SandyBrown,
        SeaGreen,
        SeaShell,
        Sienna,
        Silver,
        SkyBlue,
        SlateBlue,
        SlateGray,
        Snow,
        SpringGreen,
        SteelBlue,
        Tan,
        Teal,
        Thistle,
        Tomato,
        Transparent,
        Turquoise,
        Violet,
        Wheat,
        White,
        WhiteSmoke,
        Yellow,
        YellowGreen    
    }

    public static class NamedColors
    {
        private static readonly IDictionary<NamedColor, Color> colorMap = new Dictionary<NamedColor, Color>();
        static NamedColors()
        {
            colorMap.Add(NamedColor.AliceBlue, Windows.UI.Colors.AliceBlue);
            colorMap.Add(NamedColor.AntiqueWhite, Windows.UI.Colors.AntiqueWhite);
            colorMap.Add(NamedColor.Aqua, Windows.UI.Colors.Aqua);
            colorMap.Add(NamedColor.Aquamarine, Windows.UI.Colors.Aquamarine);
            colorMap.Add(NamedColor.Azure, Windows.UI.Colors.Azure);
            colorMap.Add(NamedColor.Beige, Windows.UI.Colors.Beige);
            colorMap.Add(NamedColor.Bisque, Windows.UI.Colors.Bisque);
            colorMap.Add(NamedColor.Black, Windows.UI.Colors.Black);
            colorMap.Add(NamedColor.BlanchedAlmond, Windows.UI.Colors.BlanchedAlmond);
            colorMap.Add(NamedColor.Blue, Windows.UI.Colors.Blue);
            colorMap.Add(NamedColor.BlueViolet, Windows.UI.Colors.BlueViolet);
            colorMap.Add(NamedColor.Brown, Windows.UI.Colors.Brown);
            colorMap.Add(NamedColor.BurlyWood, Windows.UI.Colors.BurlyWood);
            colorMap.Add(NamedColor.CadetBlue, Windows.UI.Colors.CadetBlue);
            colorMap.Add(NamedColor.Chartreuse, Windows.UI.Colors.Chartreuse);
            colorMap.Add(NamedColor.Chocolate, Windows.UI.Colors.Chocolate);
            colorMap.Add(NamedColor.Coral, Windows.UI.Colors.Coral);
            colorMap.Add(NamedColor.CornflowerBlue, Windows.UI.Colors.CornflowerBlue);
            colorMap.Add(NamedColor.Cornsilk, Windows.UI.Colors.Cornsilk);
            colorMap.Add(NamedColor.Crimson, Windows.UI.Colors.Crimson);
            colorMap.Add(NamedColor.Cyan, Windows.UI.Colors.Cyan);
            colorMap.Add(NamedColor.DarkBlue, Windows.UI.Colors.DarkBlue);
            colorMap.Add(NamedColor.DarkCyan, Windows.UI.Colors.DarkCyan);
            colorMap.Add(NamedColor.DarkGoldenrod, Windows.UI.Colors.DarkGoldenrod);
            colorMap.Add(NamedColor.DarkGray, Windows.UI.Colors.DarkGray);
            colorMap.Add(NamedColor.DarkGreen, Windows.UI.Colors.DarkGreen);
            colorMap.Add(NamedColor.DarkKhaki, Windows.UI.Colors.DarkKhaki);
            colorMap.Add(NamedColor.DarkMagenta, Windows.UI.Colors.DarkMagenta);
            colorMap.Add(NamedColor.DarkOliveGreen, Windows.UI.Colors.DarkOliveGreen);
            colorMap.Add(NamedColor.DarkOrange, Windows.UI.Colors.DarkOrange);
            colorMap.Add(NamedColor.DarkOrchid, Windows.UI.Colors.DarkOrchid);
            colorMap.Add(NamedColor.DarkRed, Windows.UI.Colors.DarkRed);
            colorMap.Add(NamedColor.DarkSalmon, Windows.UI.Colors.DarkSalmon);
            colorMap.Add(NamedColor.DarkSeaGreen, Windows.UI.Colors.DarkSeaGreen);
            colorMap.Add(NamedColor.DarkSlateBlue, Windows.UI.Colors.DarkSlateBlue);
            colorMap.Add(NamedColor.DarkSlateGray, Windows.UI.Colors.DarkSlateGray);
            colorMap.Add(NamedColor.DarkTurquoise, Windows.UI.Colors.DarkTurquoise);
            colorMap.Add(NamedColor.DarkViolet, Windows.UI.Colors.DarkViolet);
            colorMap.Add(NamedColor.DeepPink, Windows.UI.Colors.DeepPink);
            colorMap.Add(NamedColor.DeepSkyBlue, Windows.UI.Colors.DeepSkyBlue);
            colorMap.Add(NamedColor.DimGray, Windows.UI.Colors.DimGray);
            colorMap.Add(NamedColor.DodgerBlue, Windows.UI.Colors.DodgerBlue);
            colorMap.Add(NamedColor.Firebrick, Windows.UI.Colors.Firebrick);
            colorMap.Add(NamedColor.FloralWhite, Windows.UI.Colors.FloralWhite);
            colorMap.Add(NamedColor.ForestGreen, Windows.UI.Colors.ForestGreen);
            colorMap.Add(NamedColor.Fuchsia, Windows.UI.Colors.Fuchsia);
            colorMap.Add(NamedColor.Gainsboro, Windows.UI.Colors.Gainsboro);
            colorMap.Add(NamedColor.GhostWhite, Windows.UI.Colors.GhostWhite);
            colorMap.Add(NamedColor.Gold, Windows.UI.Colors.Gold);
            colorMap.Add(NamedColor.Goldenrod, Windows.UI.Colors.Goldenrod);
            colorMap.Add(NamedColor.Gray, Windows.UI.Colors.Gray);
            colorMap.Add(NamedColor.Green, Windows.UI.Colors.Green);
            colorMap.Add(NamedColor.GreenYellow, Windows.UI.Colors.GreenYellow);
            colorMap.Add(NamedColor.Honeydew, Windows.UI.Colors.Honeydew);
            colorMap.Add(NamedColor.HotPink, Windows.UI.Colors.HotPink);
            colorMap.Add(NamedColor.IndianRed, Windows.UI.Colors.IndianRed);
            colorMap.Add(NamedColor.Indigo, Windows.UI.Colors.Indigo);
            colorMap.Add(NamedColor.Ivory, Windows.UI.Colors.Ivory);
            colorMap.Add(NamedColor.Khaki, Windows.UI.Colors.Khaki);
            colorMap.Add(NamedColor.Lavender, Windows.UI.Colors.Lavender);
            colorMap.Add(NamedColor.LavenderBlush, Windows.UI.Colors.LavenderBlush);
            colorMap.Add(NamedColor.LawnGreen, Windows.UI.Colors.LawnGreen);
            colorMap.Add(NamedColor.LemonChiffon, Windows.UI.Colors.LemonChiffon);
            colorMap.Add(NamedColor.LightBlue, Windows.UI.Colors.LightBlue);
            colorMap.Add(NamedColor.LightCoral, Windows.UI.Colors.LightCoral);
            colorMap.Add(NamedColor.LightCyan, Windows.UI.Colors.LightCyan);
            colorMap.Add(NamedColor.LightGoldenrodYellow, Windows.UI.Colors.LightGoldenrodYellow);
            colorMap.Add(NamedColor.LightGray, Windows.UI.Colors.LightGray);
            colorMap.Add(NamedColor.LightGreen, Windows.UI.Colors.LightGreen);
            colorMap.Add(NamedColor.LightPink, Windows.UI.Colors.LightPink);
            colorMap.Add(NamedColor.LightSalmon, Windows.UI.Colors.LightSalmon);
            colorMap.Add(NamedColor.LightSeaGreen, Windows.UI.Colors.LightSeaGreen);
            colorMap.Add(NamedColor.LightSkyBlue, Windows.UI.Colors.LightSkyBlue);
            colorMap.Add(NamedColor.LightSlateGray, Windows.UI.Colors.LightSlateGray);
            colorMap.Add(NamedColor.LightSteelBlue, Windows.UI.Colors.LightSteelBlue);
            colorMap.Add(NamedColor.LightYellow, Windows.UI.Colors.LightYellow);
            colorMap.Add(NamedColor.Lime, Windows.UI.Colors.Lime);
            colorMap.Add(NamedColor.LimeGreen, Windows.UI.Colors.LimeGreen);
            colorMap.Add(NamedColor.Linen, Windows.UI.Colors.Linen);
            colorMap.Add(NamedColor.Magenta, Windows.UI.Colors.Magenta);
            colorMap.Add(NamedColor.Maroon, Windows.UI.Colors.Maroon);
            colorMap.Add(NamedColor.MediumAquamarine, Windows.UI.Colors.MediumAquamarine);
            colorMap.Add(NamedColor.MediumBlue, Windows.UI.Colors.MediumBlue);
            colorMap.Add(NamedColor.MediumOrchid, Windows.UI.Colors.MediumOrchid);
            colorMap.Add(NamedColor.MediumPurple, Windows.UI.Colors.MediumPurple);
            colorMap.Add(NamedColor.MediumSeaGreen, Windows.UI.Colors.MediumSeaGreen);
            colorMap.Add(NamedColor.MediumSlateBlue, Windows.UI.Colors.MediumSlateBlue);
            colorMap.Add(NamedColor.MediumSpringGreen, Windows.UI.Colors.MediumSpringGreen);
            colorMap.Add(NamedColor.MediumTurquoise, Windows.UI.Colors.MediumTurquoise);
            colorMap.Add(NamedColor.MediumVioletRed, Windows.UI.Colors.MediumVioletRed);
            colorMap.Add(NamedColor.MidnightBlue, Windows.UI.Colors.MidnightBlue);
            colorMap.Add(NamedColor.MintCream, Windows.UI.Colors.MintCream);
            colorMap.Add(NamedColor.MistyRose, Windows.UI.Colors.MistyRose);
            colorMap.Add(NamedColor.Moccasin, Windows.UI.Colors.Moccasin);
            colorMap.Add(NamedColor.NavajoWhite, Windows.UI.Colors.NavajoWhite);
            colorMap.Add(NamedColor.Navy, Windows.UI.Colors.Navy);
            colorMap.Add(NamedColor.OldLace, Windows.UI.Colors.OldLace);
            colorMap.Add(NamedColor.Olive, Windows.UI.Colors.Olive);
            colorMap.Add(NamedColor.OliveDrab, Windows.UI.Colors.OliveDrab);
            colorMap.Add(NamedColor.Orange, Windows.UI.Colors.Orange);
            colorMap.Add(NamedColor.OrangeRed, Windows.UI.Colors.OrangeRed);
            colorMap.Add(NamedColor.Orchid, Windows.UI.Colors.Orchid);
            colorMap.Add(NamedColor.PaleGoldenrod, Windows.UI.Colors.PaleGoldenrod);
            colorMap.Add(NamedColor.PaleGreen, Windows.UI.Colors.PaleGreen);
            colorMap.Add(NamedColor.PaleTurquoise, Windows.UI.Colors.PaleTurquoise);
            colorMap.Add(NamedColor.PaleVioletRed, Windows.UI.Colors.PaleVioletRed);
            colorMap.Add(NamedColor.PapayaWhip, Windows.UI.Colors.PapayaWhip);
            colorMap.Add(NamedColor.PeachPuff, Windows.UI.Colors.PeachPuff);
            colorMap.Add(NamedColor.Peru, Windows.UI.Colors.Peru);
            colorMap.Add(NamedColor.Pink, Windows.UI.Colors.Pink);
            colorMap.Add(NamedColor.Plum, Windows.UI.Colors.Plum);
            colorMap.Add(NamedColor.PowderBlue, Windows.UI.Colors.PowderBlue);
            colorMap.Add(NamedColor.Purple, Windows.UI.Colors.Purple);
            colorMap.Add(NamedColor.Red, Windows.UI.Colors.Red);
            colorMap.Add(NamedColor.RosyBrown, Windows.UI.Colors.RosyBrown);
            colorMap.Add(NamedColor.RoyalBlue, Windows.UI.Colors.RoyalBlue);
            colorMap.Add(NamedColor.SaddleBrown, Windows.UI.Colors.SaddleBrown);
            colorMap.Add(NamedColor.Salmon, Windows.UI.Colors.Salmon);
            colorMap.Add(NamedColor.SandyBrown, Windows.UI.Colors.SandyBrown);
            colorMap.Add(NamedColor.SeaGreen, Windows.UI.Colors.SeaGreen);
            colorMap.Add(NamedColor.SeaShell, Windows.UI.Colors.SeaShell);
            colorMap.Add(NamedColor.Sienna, Windows.UI.Colors.Sienna);
            colorMap.Add(NamedColor.Silver, Windows.UI.Colors.Silver);
            colorMap.Add(NamedColor.SkyBlue, Windows.UI.Colors.SkyBlue);
            colorMap.Add(NamedColor.SlateBlue, Windows.UI.Colors.SlateBlue);
            colorMap.Add(NamedColor.SlateGray, Windows.UI.Colors.SlateGray);
            colorMap.Add(NamedColor.Snow, Windows.UI.Colors.Snow);
            colorMap.Add(NamedColor.SpringGreen, Windows.UI.Colors.SpringGreen);
            colorMap.Add(NamedColor.SteelBlue, Windows.UI.Colors.SteelBlue);
            colorMap.Add(NamedColor.Tan, Windows.UI.Colors.Tan);
            colorMap.Add(NamedColor.Teal, Windows.UI.Colors.Teal);
            colorMap.Add(NamedColor.Thistle, Windows.UI.Colors.Thistle);
            colorMap.Add(NamedColor.Tomato, Windows.UI.Colors.Tomato);
            colorMap.Add(NamedColor.Transparent, Windows.UI.Colors.Transparent);
            colorMap.Add(NamedColor.Turquoise, Windows.UI.Colors.Turquoise);
            colorMap.Add(NamedColor.Violet, Windows.UI.Colors.Violet);
            colorMap.Add(NamedColor.Wheat, Windows.UI.Colors.Wheat);
            colorMap.Add(NamedColor.White, Windows.UI.Colors.White);
            colorMap.Add(NamedColor.WhiteSmoke, Windows.UI.Colors.WhiteSmoke);
            colorMap.Add(NamedColor.Yellow, Windows.UI.Colors.Yellow);
            colorMap.Add(NamedColor.YellowGreen, Windows.UI.Colors.YellowGreen);
        }


        // tbd:
        // Does it make sense to expose this as public property?
        public static IDictionary<NamedColor, Color> ColorMap
        {
            get
            {
                return colorMap;
            }
        }
        private static ISet<NamedColor> namedColorSet = null;
        public static ISet<NamedColor> NamedColorSet
        {
            get
            {
                if (namedColorSet == null) {
                    namedColorSet = new HashSet<NamedColor>(colorMap.Keys);
                }
                return namedColorSet;
            }
        }
        private static IList<NamedColor> namedColorList = null;
        public static IList<NamedColor> NamedColorList
        {
            get
            {
                if (namedColorList == null) {
                    namedColorList = new List<NamedColor>(colorMap.Keys);
                }
                return namedColorList;
            }
        }
        private static IList<string> colorNameList = null;
        public static IList<string> ColorNameList
        {
            get
            {
                if (colorNameList == null) {
                    colorNameList = new List<string>();
                    foreach (var c in colorMap.Keys) {
                        colorNameList.Add(Enum.GetName(typeof(NamedColor), c));
                    }
                }
                return colorNameList;
            }
        }

        private static IList<ColorStruct> colorStructList = null;
        public static IList<ColorStruct> ColorStructList
        {
            get
            {
                if (colorStructList == null) {
                    colorStructList = new List<ColorStruct>();
                    foreach (var c in colorMap.Keys) {
                        colorStructList.Add(new ColorStruct(c));
                    }
                }
                return colorStructList;
            }
        }


        // temporary
        private static IList<ColorWrap> colorWrapList = null;
        public static IList<ColorWrap> ColorWrapList
        {
            get
            {
                if (colorWrapList == null) {
                    colorWrapList = new List<ColorWrap>();
                    foreach (var c in colorMap.Keys) {
                        colorWrapList.Add(new ColorWrap(c));
                    }
                }
                return colorWrapList;
            }
        }



        public static bool IsValid(NamedColor namedColor)
        {
            // Note that we check the colorMap, not the enum.
            return (colorMap.ContainsKey(namedColor));
        }

        // Note: colorName is case-sensitive.
        public static bool IsNamedColor(string colorName)
        {
            NamedColor namedColor;
            return Enum.TryParse(colorName, out namedColor);
        }

        public static NamedColor GetNamedColor(string colorName)
        {
            NamedColor namedColor;
            var converted = Enum.TryParse(colorName, out namedColor);
            if (converted) {
                return namedColor;
            } else {
                // ???
                return NamedColor.Invalid;
            }
        }
        public static NamedColor GetNamedColor(Color color)
        {
            return GetNamedColor(color.ToString());
        }

        public static Color GetColor(NamedColor namedColor)
        {
            if (colorMap.ContainsKey(namedColor)) {
                return colorMap[namedColor];
            } else {
                // ????
                return Windows.UI.Colors.Transparent;
            }
        }

        public static string GetColorName(NamedColor namedColor)
        {
            if (colorMap.ContainsKey(namedColor)) {
                return Enum.GetName(typeof(NamedColor), namedColor);
            } else {
                // ????
                return null;
            }
        }

    }

}
