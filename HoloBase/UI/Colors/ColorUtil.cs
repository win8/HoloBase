﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;


namespace HoloBase.UI.Colors
{
    public static class ColorUtil
    {
        // We support the following four formats (with the optional prefix #):
        // #rgb
        // #argb
        // #rrggbb
        // #aarrggbb
        public static Color GetColorFromArgbString(string argb)
        {
            if (String.IsNullOrEmpty(argb)) {
                throw new ArgumentNullException("ARGB value cannot be null/empty.");
            }
            if (argb.StartsWith("#")) {
                argb = argb.Substring(1);
            }
            var len = argb.Length;
            if (len != 3 && len != 4 && len != 6 && len != 8) {
                throw new ArgumentException("Incorrect ARGB string format: " + argb);
            }

            // Note:
            // ToByte() method can throw a number of different types of exceptions...
            // ...

            byte alpha = 0;
            var pos = 0;
            switch(len) {
                case 4:
                    alpha = Convert.ToByte(argb.Substring(0,1), 16);
                    pos = 1;
                    break;
                case 8:
                    alpha = Convert.ToByte(argb.Substring(0,2), 16);
                    pos = 2;
                    break;
                default:
                    break;
            }

            byte red = 0;
            byte green = 0;
            byte blue = 0;
            switch (len) {
                case 3:
                case 4:
                    red = Convert.ToByte(argb.Substring(pos,1), 16);
                    green = Convert.ToByte(argb.Substring(pos+1,1), 16);
                    blue = Convert.ToByte(argb.Substring(pos+2,1), 16);
                    break;
                case 6:
                case 8:
                    red = Convert.ToByte(argb.Substring(pos,2), 16);
                    green = Convert.ToByte(argb.Substring(pos+2,2), 16);
                    blue = Convert.ToByte(argb.Substring(pos+4,2), 16);
                    break;
                default:
                    break;
            }

            var color = Color.FromArgb(alpha, red, green, blue);
            return color;
        }
    }
}
