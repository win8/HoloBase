﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Colors
{
    public struct HSL
    {
        // temporary
        public static readonly HSL Null = new HSL(0.0f, 0.0f, 0.0f);

        private float hue;
        private float saturation;
        private float lightness;

        public HSL(float hue, float saturation, float lightness)
        {
            this.hue = hue;
            this.saturation = saturation;
            this.lightness = lightness;
        }

        public float Hue
        {
            get
            {
                return hue;
            }
            private set
            {
                hue = value;
            }
        }
        public float Saturation
        {
            get
            {
                return saturation;
            }
            private set
            {
                saturation = value;
            }
        }
        public float Lightness
        {
            get
            {
                return lightness;
            }
            private set
            {
                lightness = value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Hue:").Append(Hue).Append("; ");
            sb.Append("Saturation:").Append(Saturation).Append("; ");
            sb.Append("Lightness:").Append(Lightness);
            return sb.ToString();
        }
            
    }

}
