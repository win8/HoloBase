﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Data
{
    public sealed class BuiltinColorSearchManager
    {
        private static BuiltinColorSearchManager instance = null;
        public static BuiltinColorSearchManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new BuiltinColorSearchManager();
                }
                return instance;
            }
        }
        private BuiltinColorSearchManager()
        {
        }

    
    
    }
}
