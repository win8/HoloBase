﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Data
{
    public sealed class CustomColorSearchManager
    {
        private static CustomColorSearchManager instance = null;
        public static CustomColorSearchManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new CustomColorSearchManager();
                }
                return instance;
            }
        }
        private CustomColorSearchManager()
        {
        }

    
    }
}
