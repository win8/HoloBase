﻿using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Data
{
    public sealed class CustomColorSettingsHelper
    {

        
        private static CustomColorSettingsHelper instance = null;
        public static CustomColorSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new CustomColorSettingsHelper();
                }
                return instance;
            }
        }
        private CustomColorSettingsHelper()
        {
        }


        // TBd:
        public void StoreCustomColor(ColorStruct colorStruct)
        {
            // ....
            // Need to store 
            // {name and argb}
            // ....
        }




    }
}
