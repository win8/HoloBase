﻿using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.UI.Data
{
    public sealed class FavoriteColorSettingsHelper
    {


        private static FavoriteColorSettingsHelper instance = null;
        public static FavoriteColorSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new FavoriteColorSettingsHelper();
                }
                return instance;
            }
        }
        private FavoriteColorSettingsHelper()
        {
        }


        // TBd:
        public void StoreFavoriteColor(ColorStruct colorStruct)
        {

            // ....
        }




    }
}
