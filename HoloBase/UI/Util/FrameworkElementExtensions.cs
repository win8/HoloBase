﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;


namespace HoloBase.UI.Util
{
    public static class FrameworkElementExtensions
    {
        // tbd
        private static readonly uint MAX_TRAVERSE_FOR_PARENT_PAGE = 20;
        private static readonly uint MAX_TRAVERSE_FOR_PARENT_POPUP = 5;
        // ...

        // tbd
        // Returns the parent/ancestor Page startign from "me".
        public static Page GetParentPage(this FrameworkElement me)
        {
            return GetParentPage(me, 0U);
        }
        public static Page GetParentPage(this FrameworkElement me, uint maxLevel)
        {
            // temporary
            if (maxLevel == 0U) {
                maxLevel = MAX_TRAVERSE_FOR_PARENT_PAGE;
            }
            // DependencyObject parent = null;
            // FrameworkElement fe = null;
            var parent = me.Parent;
            var count = 0;
            while (!(parent is Page) && count++ < maxLevel) {
                // System.Diagnostics.Debug.WriteLine("count = {0}", count);

                var fe = parent as FrameworkElement;
                if (fe == null) {
                    break;
                }
                parent = fe.Parent;
            }
            return parent as Page;
        }
        // tbd:


        // tbd
        // Returns the parent/ancestor Popup startign from "me".
        public static Popup GetParentPopup(this FrameworkElement me)
        {
            return GetParentPopup(me, 0U);
        }
        public static Popup GetParentPopup(this FrameworkElement me, uint maxLevel)
        {
            // temporary
            if (maxLevel == 0U) {
                maxLevel = MAX_TRAVERSE_FOR_PARENT_POPUP;
            }
            // DependencyObject parent = null;
            // FrameworkElement fe = null;
            var parent = me.Parent;
            var count = 0;
            while (!(parent is Popup) && count++ < maxLevel) {
                // System.Diagnostics.Debug.WriteLine("count = {0}", count);

                var fe = parent as FrameworkElement;
                if (fe == null) {
                    break;
                }
                parent = fe.Parent;
            }
            return parent as Popup;
        }
        // tbd:


    }
}
