﻿using HoloBase.UI.Colors;
using HoloBase.UI.Util;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HoloBase.UI
{
    public sealed partial class NamedColorInfoControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private ColorWrap viewModel;

        public NamedColorInfoControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new ColorWrap();

            ResetViewModel(vm);
        }
        private void ResetViewModel(ColorWrap viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (ColorWrap) value;
                // ????

                ResetViewModel(vm);
            }
        }

        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Navy);
        public string BackgroundGridColor
        {
            get
            {
                return backgroundColor.ARGB;
            }
            set
            {
                backgroundColor = new ColorStruct(value);
                GridNamedColorInfoControl.Background = new SolidColorBrush(backgroundColor.Color);
            }
        }


        public ColorStruct ColorStruct
        {
            get
            {
                return ((ColorWrap) viewModel).ColorStruct;
            }
            set
            {
                ((ColorWrap) viewModel).ColorStruct = value;
            }
        }


        private void DismissPopup()
        {
            // tbd:
            var popup = this.GetParentPopup();
            if (popup != null) {
                popup.IsOpen = false;
            }
        }
        private void ButtonDone_Click(object sender, RoutedEventArgs e)
        {
            DismissPopup();
        }

    
    }
}
