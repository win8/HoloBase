﻿using HoloBase.Startup.Common;
using HoloBase.Version;
using HoloCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Startup.Settings
{
    // Note:
    // Regardless of the user's general preference regarding global app settings option,
    // the methods here always have two versions,
    //     local and roaming.
    public sealed class StartupSettingsHelper
    {
        // Just use the version container with versionCode == 0.
        // private static readonly string APP_LAST_STARTUP_TIME = "LastStartupTime";
        // private static readonly string APP_STARTUP_COUNTER = "StartupCounter";

        private static readonly string KEY_VERSION_STARTUP_INFO_CONTAINER_PREFIX = "VersionStartupInfo_Container_";
        private static readonly string VERSION_STARTUP_INFO_FIELD_VERSION_CODE = "VersionCode";
        private static readonly string VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME = "LastStartupTime";
        private static readonly string VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER = "StartupCounter";

    
        private static StartupSettingsHelper instance = null;
        public static StartupSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new StartupSettingsHelper();
                }
                return instance;
            }
        }

        
        //// "Cache"
        //// private long lastStartupTime = 0L;
        //// versionCode -> counter.
        //private readonly IDictionary<ulong, int> startupCounterMap = new Dictionary<ulong, int>();
        //// taskId -> counter.
        //// private readonly IDictionary<ulong, int> runCounterMap = new Dictionary<ulong, int>();

        private StartupSettingsHelper()
        {
        }


        // TBD:
        // Use versionCode or version string???
        // ...
        // versionCode==0UL means across the app, not specific to a particular version.

        private static string GenerateVersionContainerKey(ulong versionCode)
        {
            var key = KEY_VERSION_STARTUP_INFO_CONTAINER_PREFIX + versionCode;
            return key;
        }
        private static ulong ParseVersionContainerKey(string key)
        {
            var versionCode = 0UL;
            try {
                var strId = key.Substring(KEY_VERSION_STARTUP_INFO_CONTAINER_PREFIX.Length);
                versionCode = Convert.ToUInt64(strId);
            } catch (Exception) {
                // Ignore.
            }
            return versionCode;
        }


        public void StoreAppStartupInfo(VersionStartupInfo startupInfo)
        {
            var versionCode = startupInfo.VersionCode;
            if (versionCode != 0UL) {
                // ???
                // bail out???
                versionCode = 0UL;
            }
            var key = GenerateVersionContainerKey(versionCode);
            var appSettings = ApplicationData.Current.RoamingSettings;

            var container = appSettings.CreateContainer(key, ApplicationDataCreateDisposition.Always);
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_VERSION_CODE] = versionCode;
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME] = startupInfo.LastStartupTime > 0L ? startupInfo.LastStartupTime : DateTimeUtil.CurrentUnixEpochMillis();
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER] = startupInfo.Counter > 0 ? startupInfo.Counter : 1;
        }
        public void StoreAppStartupInfoForDevice(VersionStartupInfo startupInfo)
        {
            var versionCode = startupInfo.VersionCode;
            if (versionCode != 0UL) {
                // ???
                // bail out???
                versionCode = 0UL;
            }
            var key = GenerateVersionContainerKey(versionCode);
            var appSettings = ApplicationData.Current.LocalSettings;

            var container = appSettings.CreateContainer(key, ApplicationDataCreateDisposition.Always);
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_VERSION_CODE] = versionCode;
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME] = startupInfo.LastStartupTime > 0L ? startupInfo.LastStartupTime : DateTimeUtil.CurrentUnixEpochMillis();
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER] = startupInfo.Counter > 0 ? startupInfo.Counter : 1;
        }

        public void StoreVersionStartupInfo(VersionStartupInfo startupInfo)
        {
            var versionCode = startupInfo.VersionCode;
            if (versionCode == 0UL) {
                // ???
                // bail out???
                versionCode = VersionUtil.GetCurrentVersionCode();
            }
            var key = GenerateVersionContainerKey(versionCode);
            var appSettings = ApplicationData.Current.RoamingSettings;

            var container = appSettings.CreateContainer(key, ApplicationDataCreateDisposition.Always);
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_VERSION_CODE] = versionCode;
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME] = startupInfo.LastStartupTime > 0L ? startupInfo.LastStartupTime : DateTimeUtil.CurrentUnixEpochMillis();
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER] = startupInfo.Counter > 0 ? startupInfo.Counter : 1;
        }
        public void StoreVersionStartupInfoForDevice(VersionStartupInfo startupInfo)
        {
            var versionCode = startupInfo.VersionCode;
            if (versionCode == 0UL) {
                // ???
                // bail out???
                versionCode = VersionUtil.GetCurrentVersionCode();
            }
            var key = GenerateVersionContainerKey(versionCode);
            var appSettings = ApplicationData.Current.LocalSettings;

            var container = appSettings.CreateContainer(key, ApplicationDataCreateDisposition.Always);
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_VERSION_CODE] = versionCode;
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME] = startupInfo.LastStartupTime > 0L ? startupInfo.LastStartupTime : DateTimeUtil.CurrentUnixEpochMillis();
            appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER] = startupInfo.Counter > 0 ? startupInfo.Counter : 1;
        }


        public VersionStartupInfo? FetchAppStartupInfo()
        {
            return FetchVersionStartupInfo(0UL);
        }
        public VersionStartupInfo? FetchAppStartupInfoForDevice()
        {
            return FetchVersionStartupInfoForDevice(0UL);
        }

        public VersionStartupInfo? FetchVersionStartupInfo(ulong versionCode)
        {
            VersionStartupInfo? startupInfo = null;
            var key = GenerateVersionContainerKey(versionCode);
            var appSettings = ApplicationData.Current.RoamingSettings;
            if (appSettings.Containers.ContainsKey(key)) {
                var vsi = new VersionStartupInfo(versionCode);

                var lastStartup = 0L;
                if (appSettings.Containers[key].Values.ContainsKey(VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME) && appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME] != null) {
                    try {
                        var strLastStartup = appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME].ToString();
                        lastStartup = Convert.ToInt64(strLastStartup);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                vsi.LastStartupTime = lastStartup;

                var counter = 0;
                if (appSettings.Containers[key].Values.ContainsKey(VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER) && appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER] != null) {
                    try {
                        var strCounter = appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER].ToString();
                        counter = Convert.ToInt32(strCounter);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                vsi.Counter = counter;

                startupInfo = vsi;
            }

            return startupInfo;
        }
        public VersionStartupInfo? FetchVersionStartupInfoForDevice(ulong versionCode)
        {
            VersionStartupInfo? startupInfo = null;
            var key = GenerateVersionContainerKey(versionCode);
            var appSettings = ApplicationData.Current.LocalSettings;
            if (appSettings.Containers.ContainsKey(key)) {
                var vsi = new VersionStartupInfo(versionCode);

                var lastStartup = 0L;
                if (appSettings.Containers[key].Values.ContainsKey(VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME) && appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME] != null) {
                    try {
                        var strLastStartup = appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_LAST_STARTUP_TIME].ToString();
                        lastStartup = Convert.ToInt64(strLastStartup);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                vsi.LastStartupTime = lastStartup;

                var counter = 0;
                if (appSettings.Containers[key].Values.ContainsKey(VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER) && appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER] != null) {
                    try {
                        var strCounter = appSettings.Containers[key].Values[VERSION_STARTUP_INFO_FIELD_STARTUP_COUNTER].ToString();
                        counter = Convert.ToInt32(strCounter);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                vsi.Counter = counter;

                startupInfo = vsi;
            }

            return startupInfo;
        }

    }
}
