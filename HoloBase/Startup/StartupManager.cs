﻿using HoloBase.Startup.Common;
using HoloBase.Startup.Settings;
using HoloBase.Version;
using HoloCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Startup
{
    public sealed class StartupManager
    {
        private static StartupManager instance = null;
        public static StartupManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new StartupManager();
                }
                return instance;
            }
        }
        

        // "Cache"
        // (For "app", we use versionCode == 0UL.)

        // versionCode -> last startup time.
        private readonly IDictionary<ulong, long> lastStartupTimeMap = new Dictionary<ulong, long>();
        private readonly IDictionary<ulong, long> deviceLastStartupTimeMap = new Dictionary<ulong, long>();
        // versionCode -> counter.
        private readonly IDictionary<ulong, int> startupCounterMap = new Dictionary<ulong, int>();
        private readonly IDictionary<ulong, int> deviceStartupCounterMap = new Dictionary<ulong, int>();
        
        private StartupManager()
        {
        }


        public bool IsAppFirstStartup()
        {
            return (GetAppLastStartupTime() == 0L);
            // return (GetAppStartupCounter() == 0);
        }
        public bool IsAppFirstStartupForDevice()
        {
            return (GetAppLastStartupTimeForDevice() == 0L);
            // return (GetAppStartupCounterForDevice() == 0);
        }

        public bool IsCurrentVersionFirstStartup()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            return IsVersionFirstStartup(versionCode);
        }
        public bool IsCurrentVersionFirstStartupForDevice()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            return IsVersionFirstStartupForDevice(versionCode);
        }

        public bool IsVersionFirstStartup(ulong versionCode)
        {
            return (GetVersionLastStartupTime(versionCode) == 0L);
            // return (GetVersionStartupCounter(versionCode) == 0);
        }
        public bool IsVersionFirstStartupForDevice(ulong versionCode)
        {
            return (GetVersionLastStartupTimeForDevice(versionCode) == 0L);
            // return (GetVersionStartupCounterForDevice(versionCode) == 0);
        }


        // 0L means the app has never been started before.
        public long GetAppLastStartupTime()
        {
            if (lastStartupTimeMap.ContainsKey(0UL)) {
                return lastStartupTimeMap[0UL];
            } else {
                var lastStartupTime = 0L;
                var startupInfo = StartupSettingsHelper.Instance.FetchAppStartupInfo();
                if (startupInfo != null) {
                    lastStartupTime = startupInfo.Value.LastStartupTime;
                }
                lastStartupTimeMap.Add(0UL, lastStartupTime);
                return lastStartupTime;
            }
        }
        public long GetAppLastStartupTimeForDevice()
        {
            if (deviceLastStartupTimeMap.ContainsKey(0UL)) {
                return deviceLastStartupTimeMap[0UL];
            } else {
                var lastStartupTime = 0L;
                var startupInfo = StartupSettingsHelper.Instance.FetchAppStartupInfoForDevice();
                if (startupInfo != null) {
                    lastStartupTime = startupInfo.Value.LastStartupTime;
                }
                deviceLastStartupTimeMap.Add(0UL, lastStartupTime);
                return lastStartupTime;
            }
        }


        public void SetAppLastStartupTime()
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            SetAppLastStartupTime(now);
        }
        public void SetAppLastStartupTime(long lastStartupTime)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchAppStartupInfo();
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(0UL);
            }
            startupInfo.LastStartupTime = lastStartupTime;
            StartupSettingsHelper.Instance.StoreVersionStartupInfo(startupInfo);
            lastStartupTimeMap[0UL] = lastStartupTime;

            // Roaming includes local (this device)
            SetAppLastStartupTimeForDevice(lastStartupTime);
        }

        public void SetAppLastStartupTimeForDevice()
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            SetAppLastStartupTimeForDevice(now);
        }
        public void SetAppLastStartupTimeForDevice(long lastStartupTime)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchAppStartupInfoForDevice();
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(0UL);
            }
            startupInfo.LastStartupTime = lastStartupTime;
            StartupSettingsHelper.Instance.StoreVersionStartupInfoForDevice(startupInfo);
            deviceLastStartupTimeMap[0UL] = lastStartupTime;
        }



        public long GetCurrentVersionLastStartupTime()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            return GetVersionLastStartupTime(versionCode);
        }
        public long GetCurrentVersionLastStartupTimeForDevice()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            return GetVersionLastStartupTimeForDevice(versionCode);
        }

        public long GetVersionLastStartupTime(ulong versionCode)
        {
            if (lastStartupTimeMap.ContainsKey(versionCode)) {
                return lastStartupTimeMap[versionCode];
            } else {
                var lastStartupTime = 0L;
                var startupInfo = StartupSettingsHelper.Instance.FetchVersionStartupInfo(versionCode);
                if (startupInfo != null) {
                    lastStartupTime = startupInfo.Value.LastStartupTime;
                }
                lastStartupTimeMap.Add(versionCode, lastStartupTime);
                return lastStartupTime;
            }
        }
        public long GetVersionLastStartupTimeForDevice(ulong versionCode)
        {
            if (deviceLastStartupTimeMap.ContainsKey(versionCode)) {
                return deviceLastStartupTimeMap[versionCode];
            } else {
                var lastStartupTime = 0L;
                var startupInfo = StartupSettingsHelper.Instance.FetchVersionStartupInfoForDevice(versionCode);
                if (startupInfo != null) {
                    lastStartupTime = startupInfo.Value.LastStartupTime;
                }
                deviceLastStartupTimeMap.Add(versionCode, lastStartupTime);
                return lastStartupTime;
            }
        }


        public void SetVersionLastStartupTime(ulong versionCode)
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            SetVersionLastStartupTime(versionCode, now);
        }
        public void SetVersionLastStartupTime(ulong versionCode, long lastStartupTime)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchVersionStartupInfo(versionCode);
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(versionCode);
            }
            startupInfo.LastStartupTime = lastStartupTime;
            StartupSettingsHelper.Instance.StoreVersionStartupInfo(startupInfo);
            lastStartupTimeMap[versionCode] = lastStartupTime;

            // Roaming includes local (this device)
            SetVersionLastStartupTimeForDevice(versionCode, lastStartupTime);
        }

        public void SetVersionLastStartupTimeForDevice(ulong versionCode)
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            SetVersionLastStartupTimeForDevice(versionCode, now);
        }
        public void SetVersionLastStartupTimeForDevice(ulong versionCode, long lastStartupTime)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchVersionStartupInfoForDevice(versionCode);
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(versionCode);
            }
            startupInfo.LastStartupTime = lastStartupTime;
            StartupSettingsHelper.Instance.StoreVersionStartupInfoForDevice(startupInfo);
            deviceLastStartupTimeMap[versionCode] = lastStartupTime;
        }


        public int GetAppStartupCounter()
        {
            if (startupCounterMap.ContainsKey(0UL)) {
                return startupCounterMap[0UL];
            } else {
                var counter = 0;
                var startupInfo = StartupSettingsHelper.Instance.FetchAppStartupInfo();
                if (startupInfo != null) {
                    counter = startupInfo.Value.Counter;
                }
                startupCounterMap.Add(0UL, counter);
                return counter;
            }
        }
        public int GetAppStartupCounterForDevice()
        {
            if (deviceStartupCounterMap.ContainsKey(0UL)) {
                return deviceStartupCounterMap[0UL];
            } else {
                var counter = 0;
                var startupInfo = StartupSettingsHelper.Instance.FetchAppStartupInfoForDevice();
                if (startupInfo != null) {
                    counter = startupInfo.Value.Counter;
                }
                deviceStartupCounterMap.Add(0UL, counter);
                return counter;
            }
        }

        public int IncrementAppStartupCounter()
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchAppStartupInfo();
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(0UL);
            }
            startupInfo.IncrementCounter();
            StartupSettingsHelper.Instance.StoreAppStartupInfo(startupInfo);
            startupCounterMap[0UL] = startupInfo.Counter;

            // Roaming includes local (this device)
            IncrementAppStartupCounterForDevice();

            return startupInfo.Counter;
        }
        public int IncrementAppStartupCounterForDevice()
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchAppStartupInfoForDevice();
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(0UL);
            }
            startupInfo.IncrementCounter();
            StartupSettingsHelper.Instance.StoreAppStartupInfoForDevice(startupInfo);
            deviceStartupCounterMap[0UL] = startupInfo.Counter;
            return startupInfo.Counter;
        }


        public int GetCurrentVersionStartupCounter()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            return GetVersionStartupCounter(versionCode);
        }
        public int GetCurrentVersionStartupCounterForDevice()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            return GetVersionStartupCounterForDevice(versionCode);
        }

        public int IncrementCurrentVersionStartupCounter()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            return IncrementVersionStartupCounter(versionCode);
        }
        public int IncrementCurrentVersionStartupCounterForDevice()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            return IncrementVersionStartupCounterForDevice(versionCode);
        }
        
        
        public int GetVersionStartupCounter(ulong versionCode)
        {
            if (startupCounterMap.ContainsKey(versionCode)) {
                return startupCounterMap[versionCode];
            } else {
                var counter = 0;
                var startupInfo = StartupSettingsHelper.Instance.FetchVersionStartupInfo(versionCode);
                if (startupInfo != null) {
                    counter = startupInfo.Value.Counter;
                }
                startupCounterMap.Add(versionCode, counter);
                return counter;
            }
        }
        public int GetVersionStartupCounterForDevice(ulong versionCode)
        {
            if (deviceStartupCounterMap.ContainsKey(versionCode)) {
                return deviceStartupCounterMap[versionCode];
            } else {
                var counter = 0;
                var startupInfo = StartupSettingsHelper.Instance.FetchVersionStartupInfoForDevice(versionCode);
                if (startupInfo != null) {
                    counter = startupInfo.Value.Counter;
                }
                deviceStartupCounterMap.Add(versionCode, counter);
                return counter;
            }
        }

        public int IncrementVersionStartupCounter(ulong versionCode)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchVersionStartupInfo(versionCode);
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(versionCode);
            }
            startupInfo.IncrementCounter();
            StartupSettingsHelper.Instance.StoreVersionStartupInfo(startupInfo);
            startupCounterMap[versionCode] = startupInfo.Counter;

            // Roaming includes local (this device)
            IncrementVersionStartupCounterForDevice(versionCode);

            return startupInfo.Counter;
        }
        public int IncrementVersionStartupCounterForDevice(ulong versionCode)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchVersionStartupInfoForDevice(versionCode);
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(versionCode);
            }
            startupInfo.IncrementCounter();
            StartupSettingsHelper.Instance.StoreVersionStartupInfoForDevice(startupInfo);
            deviceStartupCounterMap[versionCode] = startupInfo.Counter;
            return startupInfo.Counter;
        }




        public void UpdateAppLastStartupInfo()
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            UpdateAppLastStartupInfo(now);
        }
        public void UpdateAppLastStartupInfo(long lastStartupTime)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchAppStartupInfo();
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(0UL);
            }
            startupInfo.LastStartupTime = lastStartupTime;
            startupInfo.IncrementCounter();
            StartupSettingsHelper.Instance.StoreVersionStartupInfo(startupInfo);
            lastStartupTimeMap[0UL] = lastStartupTime;
            startupCounterMap[0UL] = startupInfo.Counter;

            // Roaming includes local (this device)
            UpdateAppLastStartupInfoForDevice(lastStartupTime);
        }

        public void UpdateAppLastStartupInfoForDevice()
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            UpdateAppLastStartupInfoForDevice(now);
        }
        public void UpdateAppLastStartupInfoForDevice(long lastStartupTime)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchAppStartupInfoForDevice();
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(0UL);
            }
            startupInfo.LastStartupTime = lastStartupTime;
            startupInfo.IncrementCounter();
            StartupSettingsHelper.Instance.StoreVersionStartupInfoForDevice(startupInfo);
            deviceLastStartupTimeMap[0UL] = lastStartupTime;
            deviceStartupCounterMap[0UL] = startupInfo.Counter;
        }


        public void UpdateCurrentVersionLastStartupInfo()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            UpdateVersionLastStartupInfo(versionCode);
        }
        public void UpdateCurrentVersionLastStartupInfo(long lastStartupTime)
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            UpdateVersionLastStartupInfo(versionCode, lastStartupTime);
        }

        public void UpdateCurrentVersionLastStartupInfoForDevice()
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            UpdateVersionLastStartupInfoForDevice(versionCode);
        }
        public void UpdateCurrentVersionLastStartupInfoForDevice(long lastStartupTime)
        {
            var versionCode = VersionUtil.GetCurrentVersionCode();
            UpdateVersionLastStartupInfoForDevice(versionCode, lastStartupTime);
        }


        public void UpdateVersionLastStartupInfo(ulong versionCode)
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            UpdateVersionLastStartupInfo(versionCode, now);
        }
        public void UpdateVersionLastStartupInfo(ulong versionCode, long lastStartupTime)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchVersionStartupInfo(versionCode);
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(versionCode);
            }
            startupInfo.LastStartupTime = lastStartupTime;
            startupInfo.IncrementCounter();
            StartupSettingsHelper.Instance.StoreVersionStartupInfo(startupInfo);
            lastStartupTimeMap[versionCode] = lastStartupTime;
            startupCounterMap[versionCode] = startupInfo.Counter;

            // Roaming includes local (this device)
            UpdateVersionLastStartupInfoForDevice(versionCode, lastStartupTime);
        }

        public void UpdateVersionLastStartupInfoForDevice(ulong versionCode)
        {
            var now = DateTimeUtil.CurrentUnixEpochMillis();
            UpdateVersionLastStartupInfoForDevice(versionCode, now);
        }
        public void UpdateVersionLastStartupInfoForDevice(ulong versionCode, long lastStartupTime)
        {
            VersionStartupInfo startupInfo;
            var vsi = StartupSettingsHelper.Instance.FetchVersionStartupInfoForDevice(versionCode);
            if (vsi != null) {
                startupInfo = vsi.Value;
            } else {
                startupInfo = new VersionStartupInfo(versionCode);
            }
            startupInfo.LastStartupTime = lastStartupTime;
            startupInfo.IncrementCounter();
            StartupSettingsHelper.Instance.StoreVersionStartupInfoForDevice(startupInfo);
            deviceLastStartupTimeMap[versionCode] = lastStartupTime;
            deviceStartupCounterMap[versionCode] = startupInfo.Counter;
        }


    }
}
