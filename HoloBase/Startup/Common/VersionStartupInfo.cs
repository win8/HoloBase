﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Startup.Common
{
    public struct VersionStartupInfo
    {
        // tbd: Just use the version string????
        private ulong versionCode;   // 0UL means across the app, not specific to a particular version.
        private long lastStartupTime;
        private int counter;
        // local vs roaming ????


        //public VersionStartupInfo(long lastStartupTime)
        //    : this(lastStartupTime, 0)  // ????
        //{
        //}
        //public VersionStartupInfo(int counter)
        //    : this(0L, counter)  // ????
        //{
        //}
        public VersionStartupInfo(ulong versioncCode)
            : this(versioncCode, 0L, 0)   // ????
        {
        }
        public VersionStartupInfo(ulong versionCode, long lastStartupTime, int counter)
        {
            this.versionCode = versionCode;
            this.lastStartupTime = lastStartupTime;
            this.counter = counter;
        }


        public ulong VersionCode
        {
            get
            {
                return versionCode;
            }
            //internal set
            //{
            //    versionCode = value;
            //}
        }

        public long LastStartupTime
        {
            get
            {
                return lastStartupTime;
            }
            set
            {
                lastStartupTime = value;
            }
        }
        public int Counter
        {
            get
            {
                return counter;
            }
            set
            {
                counter = value;
            }
        }

        public void IncrementCounter()
        {
            ++counter;
        }


        

    }
}
