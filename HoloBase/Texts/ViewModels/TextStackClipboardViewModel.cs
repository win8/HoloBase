﻿using HoloBase.Texts.Clips;
using HoloBase.Texts.Clips.Core;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.ViewModels
{
    public class TextStackClipboardViewModel : IViewModel, INotifyPropertyChanged
    {
        private IList<ITextClip> clipList;
        public TextStackClipboardViewModel()
        {
            RefrehClipList();
        }
        private void RefrehClipList()
        {
            clipList = TextClipManager.Instance.GlobalStackClipboard.ClipList;
        }

        public IList<ITextClip> ClipList
        {
            get
            {
                return clipList;
            }
            set
            {
                // clipList = value ?? new List<ITextClip>();
                var newList = value ?? new List<ITextClip>();
                TextClipManager.Instance.GlobalStackClipboard.Clear();
                TextClipManager.Instance.GlobalStackClipboard.Push(newList);
                // ...
                RefrehClipList();
                RaisePropertyChanged("ClipList");
                RaisePropertyChanged("Count");
                RaisePropertyChanged("TextString");
                RaisePropertyChanged("IsButtonCopyTextEnabled");
                RaisePropertyChanged("IsButtonPeekTextClipEnabled");
                RaisePropertyChanged("IsButtonUseTextClipEnabled");
                RaisePropertyChanged("IsButtonPopTextClipEnabled");
                RaisePropertyChanged("IsButtonClearListEnabled");
            }
        }

        public int Count
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.Count;
            }
        }

        public int Size
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.Size;
            }
        }

        public string TextString
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.ListAsString;
            }
        }


        public void ClearList()
        {
            TextClipManager.Instance.GlobalStackClipboard.Clear();
            RefrehClipList();
            RaisePropertyChanged("ClipList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonPeekTextClipEnabled");
            RaisePropertyChanged("IsButtonUseTextClipEnabled");
            RaisePropertyChanged("IsButtonPopTextClipEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
        }

        public ITextClip LastTextClip
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.Peek();
            }
        }
        public ITextClip PopTextClip()
        {
            var text = TextClipManager.Instance.GlobalStackClipboard.Pop();
            RefrehClipList();
            RaisePropertyChanged("ClipList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonPeekTextClipEnabled");
            RaisePropertyChanged("IsButtonUseTextClipEnabled");
            RaisePropertyChanged("IsButtonPopTextClipEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
            return text;
        }
        public void PushTextClip(ITextClip text)
        {
            TextClipManager.Instance.GlobalStackClipboard.Push(text);
            RefrehClipList();
            RaisePropertyChanged("ClipList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonPeekTextClipEnabled");
            RaisePropertyChanged("IsButtonUseTextClipEnabled");
            RaisePropertyChanged("IsButtonPopTextClipEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
        }


        public bool IsButtonCopyTextEnabled
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.HasAny;
            }
        }
        public bool IsButtonPeekTextClipEnabled
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.HasAny;
            }
        }
        public bool IsButtonUseTextClipEnabled
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.HasAny;
            }
        }
        public bool IsButtonPopTextClipEnabled
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.HasAny;
            }
        }
        public bool IsButtonClearListEnabled
        {
            get
            {
                return TextClipManager.Instance.GlobalStackClipboard.HasAny;
            }
        }


        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
