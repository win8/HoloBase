﻿using HoloBase.Texts.Clips;
using HoloBase.Texts.Clips.Core;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.ViewModels
{
    public class TextMultiClipboardViewModel : IViewModel, INotifyPropertyChanged
    {
        // private IList<ITextClip> clipList;
        public TextMultiClipboardViewModel()
        {
            //RefrehClipList();
        }
        //private void RefrehClipList()
        //{
        //    clipList = TextClipManager.Instance.GlobalMultiClipboard.ClipList;
        //}

        //public IList<ITextClip> ClipList
        //{
        //    get
        //    {
        //        return clipList;
        //    }
        //}
        public IList<Tuple<string, ITextClip>> CellClipList
        {
            get
            {
                return TextClipManager.Instance.GlobalMultiClipboard.CellClipList;
            }
        }

        public int Count
        {
            get
            {
                return TextClipManager.Instance.GlobalMultiClipboard.Count;
            }
        }

        public int Size
        {
            get
            {
                return TextClipManager.Instance.GlobalMultiClipboard.Size;
            }
        }

        public string TextString
        {
            get
            {
                return TextClipManager.Instance.GlobalMultiClipboard.ListAsString;
            }
        }

        public string FirstEmptyCell
        {
            get
            {
                return TextClipManager.Instance.GlobalMultiClipboard.FirstEmptyCell;
            }
        }
        public bool HasEmptyCell
        {
            get
            {
                return ! String.IsNullOrEmpty(FirstEmptyCell);
            }
        }


        public void ClearList()
        {
            TextClipManager.Instance.GlobalMultiClipboard.Clear();
            //RefrehClipList();
            RaisePropertyChanged("ClipList");
            RaisePropertyChanged("ClipClipList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("FirstEmptyCell");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
        }

        public ITextClip PeekTextClip(string cellId)
        {
            var text = TextClipManager.Instance.GlobalMultiClipboard.PeekAt(cellId);
            ////RefrehClipList();
            //RaisePropertyChanged("ClipList");
            //RaisePropertyChanged("ClipClipList");
            //RaisePropertyChanged("Count");
            //RaisePropertyChanged("FirstEmptyCell");
            //RaisePropertyChanged("TextString");
            //RaisePropertyChanged("IsButtonCopyTextEnabled");
            //RaisePropertyChanged("IsButtonClearListEnabled");
            return text;
        }
        public ITextClip PopTextClip(string cellId)
        {
            var text = TextClipManager.Instance.GlobalMultiClipboard.PopAt(cellId);
            //RefrehClipList();
            RaisePropertyChanged("ClipList");
            RaisePropertyChanged("ClipClipList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("FirstEmptyCell");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
            return text;
        }
        public void PushTextClip(string cellId, ITextClip text)
        {
            TextClipManager.Instance.GlobalMultiClipboard.PushAt(cellId, text);
            //RefrehClipList();
            RaisePropertyChanged("ClipList");
            RaisePropertyChanged("ClipClipList");
            RaisePropertyChanged("Count");
            RaisePropertyChanged("FirstEmptyCell");
            RaisePropertyChanged("TextString");
            RaisePropertyChanged("IsButtonCopyTextEnabled");
            RaisePropertyChanged("IsButtonClearListEnabled");
        }


        public bool IsButtonAddNewEnabled
        {
            get
            {
                // This is not correct since the user can overwrite existing clip...
                // return this.HasEmptyCell;
                return true;
            }
        }
        public bool IsButtonCopyTextEnabled
        {
            get
            {
                return TextClipManager.Instance.GlobalMultiClipboard.HasAny;
            }
        }
        public bool IsButtonClearListEnabled
        {
            get
            {
                return TextClipManager.Instance.GlobalMultiClipboard.HasAny;
            }
        }


        private ColorStruct backgroundColor = new ColorStruct(NamedColor.Transparent);
        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged("BackgroundColor");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
