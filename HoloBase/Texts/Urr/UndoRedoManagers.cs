﻿using HoloBase.Texts.Core;
using HoloBase.Texts.Urr.Common;
using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr
{
    // UndoRedoManager really manages a "document" or content.
    // Or, at least a copy/buffer of a document (e.g., from a file system)
    // Cf. Documents package.
    public class UndoRedoManager : IContentContainer, IEditUndoRedoManager, IContentUndoRedoManager
    {
        //private static UndoRedoManager instance = null;
        //public static UndoRedoManager Instance
        //{
        //    get
        //    {
        //        if (instance == null) {
        //            instance = new UndoRedoManager();
        //        }
        //        return instance;
        //    }
        //}

        // currentContent cannot be null once initialized.
        private string currentContent;
        private readonly UndoRedoBuffer urBuffer;

        public UndoRedoManager()
        {
            urBuffer = new UndoRedoBuffer();
            // Enabling autocombine by default.
            urBuffer.AutoCombine = true;
            // tbd: Other UndoRedoBuffer options ???
            // ...

            // ???
            Initialize("");
        }


        // Initial content does not have to be empty.
        public void Initialize(string content)
        {
            this.currentContent = content ?? "";
            this.urBuffer.Reset();
        }

        public void ResetCurrentContent(string content)
        {
            //if (content == null) {
            //    content = String.Empty;   // ???
            //}
            //this.currentContent = content;
            //// urBuffer ???

            Initialize(content);
        }


        public bool AutoCombine
        {
            get
            {
                return urBuffer.AutoCombine;
            }
            set
            {
                urBuffer.AutoCombine = value;
            }
        }

        public string CurrentContent
        {
            get
            {
                return this.currentContent;
            }
            set
            {
                ResetCurrentContent(value);
            }
        }



        private string DoEditAction(IEditAction editAction, bool excludeFromUndo = false)
        {
            // For now, we only support ISingleEditAction.
            var action = editAction as ISingleEditAction;
            if (action == null) {
                return null;
            }
            var position = action.Position;
            var insertedText = action.InsertedText;
            var deletedText = action.DeletedText;
            switch (action.Type) {
                case EditType.Insert:
                    return InsertText(position, insertedText, excludeFromUndo);
                case EditType.Delete:
                    return DeleteText(position, deletedText, excludeFromUndo);
                case EditType.Replace:
                    return ReplaceText(position, insertedText, deletedText, excludeFromUndo);
                default:
                    return null;
            }
        }


        public string InsertText(int position, string text)
        {
            return InsertText(position, text, false);
        }
        private string InsertText(int position, string text, bool excludeFromUndo)
        {
            return InsertText(position, text, null, excludeFromUndo);
        }
        private string InsertText(int position, string text, string postOpContent, bool excludeFromUndo = false)
        {
            if (!String.IsNullOrEmpty(text)) {

                // temporary
                PreprocessCurrentContent(ref this.currentContent, ref text);
                // temporary

                if (position <= this.currentContent.Length) {
                    if (excludeFromUndo == false) {
                        var editAction = new SingleEditAction(EditType.Insert, position, text);
                        urBuffer.Push(editAction);
                    }
                    if (postOpContent != null) {
                        // tbd: validate?
                        //  --> We have to assume this is valid. Otherwise, what's the point of passing in postOpContent??
                        this.currentContent = postOpContent;
                    } else {
                        if (position < this.currentContent.Length) {
                            this.currentContent = this.currentContent.Insert(position, text);
                        } else {
                            this.currentContent += text;
                        }
                    }

                    // temporary
                    PostprocessCurrentContent(ref this.currentContent);
                    // temporary

                    return this.currentContent;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        public string DeleteText(int position, string text)
        {
            return DeleteText(position, text, false);
        }
        public string DeleteText(int position, string text, bool excludeFromUndo)
        {
            return DeleteText(position, text, null, excludeFromUndo);
        }
        private string DeleteText(int position, string text, string postOpContent, bool excludeFromUndo = false)
        {
            if (!String.IsNullOrEmpty(text)) {

                // temporary
                PreprocessCurrentContent(ref this.currentContent, ref text);
                // temporary

                if (position <= this.currentContent.Length - text.Length) {
                    var target = this.currentContent.Substring(position, text.Length);
                    if (!text.Equals(target)) {
                        return null;
                    }
                    if (excludeFromUndo == false) {
                        var editAction = new SingleEditAction(EditType.Delete, position, null, text);
                        urBuffer.Push(editAction);
                    }
                    if (postOpContent != null) {
                        // tbd: validate?
                        //  --> We have to assume this is valid. Otherwise, what's the point of passing in postOpContent??
                        this.currentContent = postOpContent;
                    } else {
                        if (position == this.currentContent.Length - text.Length) {
                            this.currentContent = this.currentContent.Substring(0, this.currentContent.Length - text.Length);
                        } else {
                            this.currentContent = this.currentContent.Substring(0, position) + this.currentContent.Substring(position + text.Length, this.currentContent.Length - (position + text.Length));
                        }
                    }

                    // temporary
                    PostprocessCurrentContent(ref this.currentContent);
                    // temporary

                    return this.currentContent;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        public string ReplaceText(int position, string newText, string oldText)
        {
            return ReplaceText(position, newText, oldText, false);
        }
        public string ReplaceText(int position, string newText, string oldText, bool excludeFromUndo)
        {
            return ReplaceText(position, newText, oldText, null, excludeFromUndo);
        }
        private string ReplaceText(int position, string newText, string oldText, string postOpContent, bool excludeFromUndo = false)
        {
            if (String.IsNullOrEmpty(newText) && String.IsNullOrEmpty(oldText)) {
                return null;
            }
            if (newText == null) {
                newText = String.Empty;
            }
            if (oldText == null) {
                oldText = String.Empty;
            }

            // temporary
            PreprocessCurrentContent(ref this.currentContent, ref newText, ref oldText);
            // temporary
            
            
            if (position <= this.currentContent.Length - oldText.Length) {
                var target = this.currentContent.Substring(position, oldText.Length);
                if (!oldText.Equals(target)) {
                    return null;
                }
                if (excludeFromUndo == false) {
                    var editAction = new SingleEditAction(EditType.Replace, position, newText, oldText);
                    urBuffer.Push(editAction);
                }
                if (postOpContent != null) {
                    // tbd: validate?
                    //  --> We have to assume this is valid. Otherwise, what's the point of passing in postOpContent??
                    this.currentContent = postOpContent;
                } else {
                    if (position == this.currentContent.Length - oldText.Length) {
                        this.currentContent = this.currentContent.Substring(0, this.currentContent.Length - oldText.Length);
                    } else {
                        this.currentContent = this.currentContent.Substring(0, position) + this.currentContent.Substring(position + oldText.Length, this.currentContent.Length - (position + oldText.Length));
                    }
                    if (position < this.currentContent.Length) {
                        this.currentContent = this.currentContent.Insert(position, newText);
                    } else {
                        this.currentContent += newText;
                    }
                }

                // temporary
                PostprocessCurrentContent(ref this.currentContent);
                // temporary

                return this.currentContent;
            } else {
                return null;
            }
        }


        public bool UpdateContent(string postOpContent)
        {
            return UpdateContent(postOpContent, false);
        }
        private bool UpdateContent(string postOpContent, bool excludeFromUndo)
        {
            return UpdateContent(postOpContent, null, excludeFromUndo);
        }
        public bool UpdateContent(string postOpContent, string preOpContent)
        {
            return UpdateContent(postOpContent, preOpContent, false);
        }
        private bool UpdateContent(string postOpContent, string preOpContent, bool excludeFromUndo)
        {
            if (preOpContent != null) {

                // temporary
                PreprocessCurrentContent(ref preOpContent);
                // temporary

                // This clears the current urBuffer as well...
                ResetCurrentContent(preOpContent);
            }
            if (postOpContent == null) {
                return false;
            }

            // temporary
            PreprocessCurrentContent(ref postOpContent);
            PreprocessCurrentContent(ref this.currentContent);
            // temporary

            if (postOpContent.Equals(this.currentContent)) {  // Note that we return false if the content has not changed....
                return false;
            }
            

            // TBD:
            // We really need to implement a diff algorithm, 
            //     e.g.. using Longest common subsequence problem (at alphabet or word level)
            // https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
            // ...

            // but, for now
            // we will just assume the difference is localized in one place, as cna be represented by ISingleEditAction
            //    (Note: this holds true in general.
            //     Any update can be viewed as a single ISingleEditAction (althout not optimal).
            //     In the worst case, any update can be viewed as a complete replacement of the old content with the new content.)
            //    (Note: the resulting editAction(s) may not be unique depending on what algorithm we use.
            //     A trivial example is "xaaaaay" -> "xaaaaaaay". We have no way to uniquely determine the insertion position.
            //     but, as long as we are consitent, this shouldn't be a problem.
            //    In the end, our goal is to implement undo/redo.)
            // ...
            // Under this assumption,
            // we can simplify the diff.
            // ...

            ISingleEditAction editAction = null;
            if (currentContent.Length == 0) {
                editAction = new SingleEditAction(EditType.Insert, 0, postOpContent);
            } else if (postOpContent.Length == 0) {
                editAction = new SingleEditAction(EditType.Delete, 0, null, currentContent);
            } else {
                if (currentContent.Length > postOpContent.Length && currentContent.StartsWith(postOpContent)) {
                    editAction = new SingleEditAction(EditType.Delete, postOpContent.Length, null, currentContent.Substring(postOpContent.Length, currentContent.Length - postOpContent.Length));
                } else if (currentContent.Length > postOpContent.Length && currentContent.EndsWith(postOpContent)) {
                    editAction = new SingleEditAction(EditType.Delete, 0, null, currentContent.Substring(0, currentContent.Length - postOpContent.Length));
                } else if (currentContent.Length < postOpContent.Length && postOpContent.StartsWith(currentContent)) {
                    editAction = new SingleEditAction(EditType.Insert, currentContent.Length, postOpContent.Substring(currentContent.Length, postOpContent.Length - currentContent.Length));
                } else if (currentContent.Length < postOpContent.Length && postOpContent.EndsWith(currentContent)) {
                    editAction = new SingleEditAction(EditType.Insert, 0, postOpContent.Substring(0, postOpContent.Length - currentContent.Length));
                } else {
                    // index is based on currentContent.
                    int[] diffIdx = IndexOfFirstAndLastDiffs(currentContent, postOpContent);
                    var delta = diffIdx[1] - diffIdx[0];
                    if (delta == 0) {
                        // insert
                        editAction = new SingleEditAction(EditType.Insert, diffIdx[0], postOpContent.Substring(diffIdx[0], postOpContent.Length - currentContent.Length));
                    } else {
                        if ((currentContent.Length - postOpContent.Length) == delta) {   // Note that delta >= 0.
                            // delete
                            editAction = new SingleEditAction(EditType.Delete, diffIdx[0], null, currentContent.Substring(diffIdx[0], delta));
                        } else {
                            // replace
                            var insertedLength = postOpContent.Length - currentContent.Length + delta;
                            System.Diagnostics.Debug.WriteLine("UpdateContent() REPLACE: deletedLength (delta) = {0}", delta);
                            System.Diagnostics.Debug.WriteLine("UpdateContent() REPLACE: insertedLength = {0}", insertedLength);
                            System.Diagnostics.Debug.WriteLine("UpdateContent() REPLACE: currentContent.Length = {0}", currentContent.Length);
                            System.Diagnostics.Debug.WriteLine("UpdateContent() REPLACE: postOpContent.Length = {0}", postOpContent.Length);
                            //if (insertedLength < 0) {
                            //    insertedLength = 0;   // ????
                            //}
                            editAction = new SingleEditAction(EditType.Replace, diffIdx[0], postOpContent.Substring(diffIdx[0], insertedLength), currentContent.Substring(diffIdx[0], delta));
                        }
                    }
                }
            }
            if (editAction != null) {
                if (excludeFromUndo == false) {
                    urBuffer.Push(editAction);
                }
                this.currentContent = postOpContent;

                // temporary
                PostprocessCurrentContent(ref this.currentContent);
                // temporary 
               
                return true;
            } else {
                return false;
            }
        }


        // TBD:
        private void PreprocessCurrentContent(ref string currentContent)
        {
            currentContent = currentContent.Replace("\r\n", "\n").Replace("\r", "\n");
        }
        private void PreprocessCurrentContent(ref string currentContent, ref string newText)
        {
            currentContent = currentContent.Replace("\r\n", "\n").Replace("\r", "\n");
            newText = newText.Replace("\r\n", "\n").Replace("\r", "\n");
        }
        private void PreprocessCurrentContent(ref string currentContent, ref string newText, ref string oldText)
        {
            currentContent = currentContent.Replace("\r\n", "\n").Replace("\r", "\n");
            newText = newText.Replace("\r\n", "\n").Replace("\r", "\n");
            oldText = oldText.Replace("\r\n", "\n").Replace("\r", "\n");
        }
        private void PostprocessCurrentContent(ref string currentContent)
        {
            currentContent = currentContent.Replace("\n", Environment.NewLine);
        }


        // temporary:
        //    this implementation is pretry fragile...
        //    but, it seems to be working....
        // Index is based on the firstString.
        // Note that firstString.Length >= 1 and secondString.Length >= 1.
        //   and firstString != secondString (and, one does not include the other at the beginning or at the end)
        // See the implementation of UpdateContent().
        // This method is valid only within the context of the method, UpdateContent().
        private static int[] IndexOfFirstAndLastDiffs(string firstString, string secondString)
        {
            // Note that depending on whether traverse from the beginning first or end first,
            //      we may end up with different firstDiff/endDiff.
            // Ending up with the diffeferent editAction...
            // (but, as long as we are consistent, this shouldn't be an issue.)

            var firstDiff = -1;
            var endDiff = -1;

            var lengthMax1 = Math.Min(firstString.Length, secondString.Length);
            for (var i = 0; i < lengthMax1; i++) {
                if (firstString[i] != secondString[i]) {
                    firstDiff = i;
                    break;
                }
            }
            // This should not happen.
            if (firstDiff == -1) {
                firstDiff = 0;
            }

            if (firstString[firstString.Length - 1] != secondString[secondString.Length - 1]) {
                endDiff = firstString.Length;
            } else {
                var lengthMax2 = lengthMax1 - firstDiff;  // ????
                for (var j = 1; j <= lengthMax2; j++) {
                    if (firstString[firstString.Length - j] != secondString[secondString.Length - j]) {
                        endDiff = firstString.Length - j + 1;    // Note +1.
                        break;
                    }
                }
                // This should not happen.
                if (endDiff == -1) {
                    System.Diagnostics.Debug.WriteLine("IndexOfFirstAndLastDiffs() endDiff is -1.");
                    endDiff = firstString.Length - lengthMax2;
                }
            }

            System.Diagnostics.Debug.WriteLine("IndexOfFirstAndLastDiffs() firstDiff = {0}; endDiff = {1}", firstDiff, endDiff);

            return new int[] { firstDiff, endDiff };
        }


        public bool CanUndo
        {
            get
            {
                return urBuffer.HasAny;
            }
        }
        public string Undo()
        {
            if (urBuffer.HasAny) {
                var editAction = urBuffer.Peek();
                var inverse = editAction.Inverse;
                var postOp = DoEditAction(inverse, true);
                if (postOp != null) {
                    urBuffer.Pop();
                    return postOp;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        public bool CanRedo
        {
            get
            {
                return (urBuffer.RedoableCount > 0);
            }
        }
        public string Redo()
        {
            if (urBuffer.RedoableCount > 0) {
                var editAction = urBuffer.PeekRedoStack();
                var postOp = DoEditAction(editAction, true);
                if (postOp != null) {
                    urBuffer.PushBack();
                    return postOp;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

    }
}
