﻿using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Common
{
    public class MultiEditAction : EditAction, IMultiEditAction
    {
        // Note:
        // Although MultiEditAction encapsulates (conceptually) simultaneous multiple edit cations,
        // because of the way we implement an edit action (e.g., using "position" var),
        // the order is rather important.
        // For example, the actions need to be ordered from the beginning of the text to the end...
        // ....
        private IList<ISingleEditAction> editActions;
        private IEditAction inverseAction = null;

        public MultiEditAction(EditType editType, IList<ISingleEditAction> editActions = null)
            : base(editType)
        {
            this.editActions = editActions;
        }

        public IList<ISingleEditAction> EditActions
        {
            get
            {
                return editActions;
            }
            set
            {
                editActions = value;
            }
        }

        public override IEditAction Inverse
        {
            get
            {
                if (inverseAction == null) {
                    inverseAction = ComputeInverseAction();
                }
                return inverseAction;
            }
        }
        private IEditAction ComputeInverseAction()
        {
            IEditAction inverse = null;
            if (editActions != null) {
                var inverseActions = new List<ISingleEditAction>();
                // in reverse order...
                foreach (var a in editActions.Reverse()) {
                    inverseActions.Add((ISingleEditAction) a.Inverse);                
                }
                inverse = new MultiEditAction(this.Type, inverseActions);
            }
            return inverse;
        }

    }
}
