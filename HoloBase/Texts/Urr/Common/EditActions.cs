﻿using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Common
{
    public abstract class EditAction
    {
        private EditType editType;
        protected EditAction(EditType editType)
        {
            this.editType = editType;
        }

        public EditType Type
        {
            get
            {
                return editType;
            }
            set
            {
                editType = value;
            }
        }

        public abstract IEditAction Inverse
        {
            get;
        }

        
    }
}
