﻿using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Common
{
    public class UndoRedoBuffer
    {
        // for thread safety...
        private object lockObject = new object();

        // Internal stacks.
        private readonly Stack<IEditAction> undoStack;
        private readonly Stack<IEditAction> redoStack;

        // tbd:
        // maximum size for the undoStack???
        // ...

        // tbd:
        // If true, the newly pushed EditAction is automatically combined with the last added EditAction,
        //     if that operation generates a ISingleEditAction...
        // ...
        // Do autoCombine only if the text is a single character insert or delete??? 
        //    with no whitespace ????
        private bool autoCombine = false;
        // ...

        public UndoRedoBuffer()
        {
            undoStack = new Stack<IEditAction>();
            redoStack = new Stack<IEditAction>();
        }

        public bool AutoCombine
        {
            get
            {
                return autoCombine;
            }
            set
            {
                // tbd:
                // Can we change this value while buffer is being used.. ?????
                // ....
                autoCombine = value;
            }
        }


        // We do not care about redoStack.
        public int Count
        {
            get
            {
                return undoStack.Count;
            }
        }
        public bool HasAny
        {
            get
            {
                return undoStack.Any();
            }
        }

        public int RedoableCount
        {
            get
            {
                return redoStack.Count;
            }
        }


        public void Reset()
        {
            lock (lockObject) {
                undoStack.Clear();
                redoStack.Clear();
            }
        }

        public IEditAction Peek()
        {
            IEditAction editAction = null;
            if (undoStack.Any()) {
                editAction = undoStack.Peek();
            }
            return editAction;
        }
        public IEditAction Pop()
        {
            IEditAction editAction = null;
            if (undoStack.Any()) {
                lock (lockObject) {
                    if (undoStack.Any()) {
                        editAction = undoStack.Pop();
                        redoStack.Push(editAction);
                    }
                }
            }
            return editAction;
        }

        public void Push(IEditAction editAction)
        {
            lock (lockObject) {
                // tbd:
                // autoCombine
                // ...

                IEditAction combinedAction = null;
                if (autoCombine) {
                    var singleAction = editAction as ISingleEditAction;
                    if(singleAction != null) {
                        if (undoStack.Any()) {
                            var peeked = undoStack.Peek();
                            var prev = peeked as ISingleEditAction;
                            if(prev != null) {
                                if(prev.Type == EditType.Insert && editAction.Type == EditType.Insert) {
                                    if (prev.InsertedText.Length == 1 && singleAction.InsertedText.Length == 1) {   // This is not strictly necessary...
                                        if (prev.InsertedText.Length < 30) {   // 30 arbitrary...
                                            if (prev.Position + prev.InsertedText.Length == singleAction.Position           // consecutive insertions.
                                                && !Char.IsWhiteSpace(prev.InsertedText[prev.InsertedText.Length - 1])) {
                                                var insertedText = prev.InsertedText + singleAction.InsertedText;
                                                var combinedInsertAction = new SingleEditAction(EditType.Insert, prev.Position, insertedText);
                                                combinedAction = combinedInsertAction;
                                            }
                                            // tbd: (generalized) right to left writing?
                                            // ....
                                        }
                                    }
                                } else if (prev.Type == EditType.Delete && editAction.Type == EditType.Delete) {
                                    if (prev.DeletedText.Length == 1 && singleAction.DeletedText.Length == 1) {     // This is not strictly necessary...
                                        if (prev.DeletedText.Length < 30) {   // 30 arbitrary
                                            string deletedText = null;
                                            if (prev.Position == singleAction.Position                                             // "Delete"
                                                && !Char.IsWhiteSpace(prev.DeletedText[prev.DeletedText.Length - 1])) {
                                                deletedText = prev.DeletedText + singleAction.DeletedText;
                                            } else if (prev.Position - singleAction.DeletedText.Length == singleAction.Position    // Generalized "Backspace"
                                                && !Char.IsWhiteSpace(singleAction.DeletedText[singleAction.DeletedText.Length - 1])) {
                                                deletedText = singleAction.DeletedText + prev.DeletedText;
                                            }
                                            if (deletedText != null) {
                                                var combinedDeletedAction = new SingleEditAction(EditType.Delete, singleAction.Position, null, deletedText);
                                                combinedAction = combinedDeletedAction;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // ...
                if (combinedAction != null) {
                    undoStack.Pop();
                    undoStack.Push(combinedAction);
                } else {
                    undoStack.Push(editAction);
                }
                redoStack.Clear();
            }
        }

        public void PushBack()
        {
            if (redoStack.Any()) {
                lock (lockObject) {
                    if (redoStack.Any()) {
                        var editAction = redoStack.Pop();
                        undoStack.Push(editAction);
                    }
                }
            }
        }

        public IEditAction PeekRedoStack()
        {
            IEditAction editAction = null;
            if (redoStack.Any()) {
                editAction = redoStack.Peek();
            }
            return editAction;
        }
        // ???
        // Use PeekRedoStack() and PushBack() ???
        private IEditAction PopRedoStack()
        {
            IEditAction editAction = null;
            if (redoStack.Any()) {
                lock (lockObject) {
                    if (redoStack.Any()) {
                        editAction = redoStack.Pop();
                    }
                }
            }
            return editAction;
        }

    }
}
