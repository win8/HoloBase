﻿using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Common
{
    public class SingleEditAction : EditAction, ISingleEditAction
    {
        private int position;
        private string insertedText;
        private string deletedText;
        private IEditAction inverseAction = null;

        // tbd: how to validate editType???
        public SingleEditAction(EditType editType, int position = -1, string insertedText = null, string deletedText = null)
            : base(editType)
        {
            this.position = position;
            this.insertedText = insertedText;
            this.deletedText = deletedText;
        }

        public int Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public string InsertedText
        {
            get
            {
                return insertedText;
            }
            set
            {
                insertedText = value;
            }
        }

        public string DeletedText
        {
            get
            {
                return deletedText;
            }
            set
            {
                deletedText = value;
            }
        }


        public override IEditAction Inverse
        {
            get
            {
                if (inverseAction == null) {
                    inverseAction = ComputeInverseAction();
                }
                return inverseAction;
            }
        }
        private IEditAction ComputeInverseAction()
        {
            if (! EditTypes.IsAtomic(this.Type)) {
                return null;
            }
            IEditAction inverse = null;
            switch(this.Type) {
                case EditType.Insert:
                    inverse = new SingleEditAction(EditType.Delete, Position, null, InsertedText);
                    break;
                case EditType.Delete:
                    inverse = new SingleEditAction(EditType.Insert, Position, DeletedText, null);
                    break;
                case EditType.Replace:
                default:
                    inverse = new SingleEditAction(EditType.Replace, Position, DeletedText, InsertedText);
                    break;
            }
            return inverse;
        }


    }
}
