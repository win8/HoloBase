﻿using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Common
{
    public class EditActionSequence : EditAction, IEditActionSequence
    {
        private IList<IEditAction> editActions;
        private IEditAction combinedEditAction = null;
        private IEditAction inverseAction = null;

        public EditActionSequence(EditType editType, IList<IEditAction> editActions = null, IEditAction combinedEditAction = null)
            : base(editType)
        {
            this.editActions = editActions;
            this.combinedEditAction = combinedEditAction;
        }

        public IList<IEditAction> EditActions
        {
            get
            {
                return editActions;
            }
            set
            {
                editActions = value;
            }
        }
  

        public IEditAction CombinedEditAction
        {
            get
            {
                if (combinedEditAction == null) {
                    combinedEditAction = ComputeCombinedAction();
                }
                return combinedEditAction;
            }
            set
            {
                combinedEditAction = value;
            }
        }
        private IEditAction ComputeCombinedAction()
        {
            IEditAction combined = null;
            if (editActions != null) {
                // tbd
                // ...
            }
            return combined;
        }

        public override IEditAction Inverse
        {
            get
            {
                if (inverseAction == null) {
                    inverseAction = ComputeInverseAction();
                }
                return inverseAction;
            }
        }
        private IEditAction ComputeInverseAction()
        {
            IEditAction inverse = null;
            if (editActions != null) {
                var inverseActions = new List<IEditAction>();
                // in reverse order...
                foreach (var a in editActions.Reverse()) {
                    inverseActions.Add(a.Inverse);
                }
                inverse = new EditActionSequence(this.Type, inverseActions);
            }
            return inverse;
        }

    }
}
