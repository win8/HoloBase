﻿using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Common
{
    public class TextSelection : ITextSelection
    {
        private int position;
        private string text;

        public TextSelection(int position = -1, string text = null)
        {
            this.position = position;
            this.text = text;
        }

        public int Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Position:").Append(Position).Append("; ");
            sb.Append("Text:").Append(Text);
            return sb.ToString();
        }
    }
}
