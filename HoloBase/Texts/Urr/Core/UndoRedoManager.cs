﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Core
{
    public interface IUndoRedoManager : IContentContainer
    {
        bool AutoCombine
        {
            get;
        }

        bool CanUndo
        {
            get;
        }
        string Undo();

        bool CanRedo
        {
            get;
        }
        string Redo();
    }

    // [A] either this
    //     (Use IContentUnderRedoManager instead....)
    public interface IEditUndoRedoManager : IUndoRedoManager, ICoreEditManager
    {
        //string InsertText(int position, string text);
        //string DeleteText(int position, string text);
        //string ReplaceText(int position, string newText, string oldText);
    }

    // [B] or this.
    //     --> API B should be generally more useful....
    public interface IContentUndoRedoManager : IUndoRedoManager
    {
        bool UpdateContent(string postOpContent);
        // this does not really make sense, in general.
        // By resetting preOpContent, we are essentailly invalidating current undoRedoBuffer.
        //    (otherwise, we may be get into an inconsitent state....)
        bool UpdateContent(string postOpContent, string preOpContent);
    }

    //// ??? Just for convenience.
    //public interface IContentEditUndoRedoManager : IContentUndoRedoManager, IEditUndoRedoManager
    //{
    //}

}
