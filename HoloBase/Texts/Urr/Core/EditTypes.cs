﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Core
{
    public enum EditType
    {
        Unknown = 0,
        Insert = 1,
        Delete = 2,
        Replace = 3,   // replace == delete + insert...
        // ...
        Mixed = 8,   // ???  Sequence vs multi ????
        // ????
        // selecting
        // moving cursor
        // ...
    }
    public static class EditTypes
    {
        private readonly static ISet<EditType> atomicEditTypes = new HashSet<EditType>() {
            EditType.Insert,
            EditType.Delete,
            EditType.Replace
        };

        public static bool IsAtomic(EditType editType)
        {
            return atomicEditTypes.Contains(editType);
        }

    }
}
