﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Core
{
    public class ContentSelectionChangeEventArgs : EventArgs
    {
        private ITextSelection selection;
        //public ContentSelectionChangeEventArgs(object sender, int position, string selectedText)
        //{
        //    // ...
        //}
        public ContentSelectionChangeEventArgs(object sender, ITextSelection selection)
        {
            this.selection = selection;
        }

        public ITextSelection TextSelection
        {
            get
            {
                return selection;
            }
            set
            {
                selection = value;
            }
        }

    }
}
