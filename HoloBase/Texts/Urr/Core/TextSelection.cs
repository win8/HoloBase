﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Core
{
    public interface ITextSelection
    {
        int Position
        {
            get;
        }
        string Text
        {
            get;
        }
    }
}
