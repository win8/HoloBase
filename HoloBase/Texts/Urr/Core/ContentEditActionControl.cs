﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Core
{
    public interface IContentEditActionControl
    {
        event EventHandler<ContentEditActionEventArgs> TextContentEdited;
    }
}
