﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Core
{
    public interface ITextContentSelectionControl
    {
        event EventHandler<ContentSelectionChangeEventArgs> TextSelectionChanged;
    }
}
