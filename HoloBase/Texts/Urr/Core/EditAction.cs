﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Core
{
    // To be used for undo/redo.
    public interface IEditAction
    {
        EditType Type
        {
            get;
        }
        IEditAction Inverse
        {
            get;
        }
    }

    public interface ISingleEditAction : IEditAction
    {
        //EditType Type
        //{
        //    get;
        //}

        int Position
        {
            get;
        }
        // --> Just use DeletedText
        //uint Length    // Used only for Replace. Otherwise Length is always 0.
        //{
        //    get;
        //}

        string InsertedText
        {
            get;
        }
        string DeletedText
        {
            get;
        }

    }

    // E.g., global search & replace.
    public interface IMultiEditAction : IEditAction
    {
        // ????
        //EditType Type
        //{
        //    get;
        //}

        IList<ISingleEditAction> EditActions
        {
            get;
        }
        //IList<IEditAction> EditActions
        //{
        //    get;
        //}
    }

    public interface IEditActionSequence : IEditAction   // IMultiEditAction
    {
        IList<IEditAction> EditActions
        {
            get;
        }

        IEditAction CombinedEditAction
        {
            get;
        }
    }

}
