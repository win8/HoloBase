﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Urr.Core
{
    public class ContentEditActionEventArgs : EventArgs
    {
        private IEditAction editAction = null;
        private string postOpContent = null;

        public ContentEditActionEventArgs(object sender, IEditAction editAction)
        {
            this.editAction = editAction;
        }
        public ContentEditActionEventArgs(object sender, string postOpContent)
        {
            this.postOpContent = postOpContent;
        }

        public IEditAction EditAction
        {
            get
            {
                return editAction;
            }
            set
            {
                editAction = value;
            }
        }
        public string PostOpContent
        {
            get
            {
                return postOpContent;
            }
            set
            {
                postOpContent = value;
            }
        }

    }
}
