﻿using HoloBase.Texts.Core;
using HoloBase.Texts.Store.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Store
{
    public class DefaultAppDataContentStoreManager : IContentStoreManager
    {
        private IAppDataContentStore appDataContentStore;

        public DefaultAppDataContentStoreManager(string itemName)
            : this(new AppDataContentStore(itemName))
        {
        }
        public DefaultAppDataContentStoreManager(IAppDataContentStore appDataContentStore)
        {
            this.appDataContentStore = appDataContentStore;
        }



        public async Task<bool> SyncDownAsync(string content)
        {
            var suc = await appDataContentStore.SaveContentAsync(content);

            return suc;
        }

        public async Task<string> SyncUpAsync()
        {
            var content = await appDataContentStore.FetchContentAsync();

            return content;
        }


    }
}
