﻿using HoloJson.Mini.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Store.Util
{
    public static class TemporaryFileNameUtil
    {
        public static string GenerateTemporaryTextFileName(string inputFileName)
        {
            string tempFileName = null;
            try {
                // var inputFileName = Path.GetFileName(inputFilePath);
                var idx2 = inputFileName.LastIndexOf(".");
                var inputFileRoot = (idx2 >= 0) ? inputFileName.Substring(0, idx2) : inputFileName;   // ???
                var inputFileExtension = Path.GetExtension(inputFileName);

                tempFileName = inputFileRoot;
                tempFileName += "_" + DateTimeUtil.CurrentUnixEpochMillis();
                if (!String.IsNullOrEmpty(inputFileExtension) && !inputFileExtension.Equals(".")) {
                    tempFileName += inputFileExtension;
                }
            } catch (Exception) {
                // What to do???
                tempFileName = "temporary_file_" + DateTimeUtil.CurrentUnixEpochMillis() + ".txt";  // ????
            }
            return tempFileName;
        }

    }
}
