﻿using HoloBase.Texts.Core;
using HoloBase.Texts.Store.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Texts.Store
{
    public class DefaultStorageFileContentStoreManager : IContentStoreManager
    {
        //private static DefaultStorageFileContentStoreManager instance = null;
        //public static DefaultStorageFileContentStoreManager Instance
        //{
        //    get
        //    {
        //        if (instance == null) {
        //            instance = new DefaultStorageFileContentStoreManager();
        //        }
        //        return instance;
        //    }
        //}
        //private DefaultStorageFileContentStoreManager()
        //{
        //}


        private IStorageFileContentStore storageFileContentStore;

        public DefaultStorageFileContentStoreManager(IStorageFile storageFile)
            : this(new StorageFileContentStore(storageFile))
        {
        }
        public DefaultStorageFileContentStoreManager(IStorageFileContentStore storageFileContentStore)
        {
            this.storageFileContentStore = storageFileContentStore;
        }



        public async Task<bool> SyncDownAsync(string content)
        {
            var suc = await storageFileContentStore.SaveContentAsync(content);

            return suc;
        }

        public async Task<string> SyncUpAsync()
        {
            var content = await storageFileContentStore.FetchContentAsync();

            return content;
        }


    }
}
