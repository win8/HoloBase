﻿using HoloBase.Settings.App;
using HoloBase.Texts.Core;
using HoloBase.Texts.Store.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Texts.Store.Common
{
    public class AppDataContentStore : AbstractContentStore, IAppDataContentStore
    {
        private string itemName;
        // "cache"
        private IStorageFile contentStorageFile = null;

        public AppDataContentStore(string itemName)
        {
            this.itemName = itemName;
        }

        public async Task<IStorageFile> GetStorageFileAsync()
        {
            if (contentStorageFile == null) {

                // var appDataFolder = AppDataOptionHelper.Instance.GetAppDataFolder();
                var appDataFolder = AppDataOptionHelper.Instance.GetLocalAppDataFolder();

                // tbd: Use an app specific folder
                var contentFileFolderName = "content";  // ....
                var contentFolder = await appDataFolder.CreateFolderAsync(contentFileFolderName, CreationCollisionOption.OpenIfExists);


                // tbd:
                // "translate" itemName to fileName

                var fileName = "abc.txt";  // ....
                contentStorageFile = await contentFolder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);

                
            }
            return contentStorageFile;
        }


        public override async Task<bool> SaveContentAsync(string content)
        {
            var storageFile = await GetStorageFileAsync();
            if (storageFile != null) {
                try {
                    var tempFolder = ApplicationData.Current.TemporaryFolder;
                    var contentFileName = storageFile.Name;
                    var tempFileName = TemporaryFileNameUtil.GenerateTemporaryTextFileName(contentFileName);
                    var tempFile = await tempFolder.CreateFileAsync(tempFileName, CreationCollisionOption.ReplaceExisting);
                    await FileIO.WriteTextAsync(tempFile, content);
                    await tempFile.CopyAndReplaceAsync(storageFile);
                    System.Diagnostics.Debug.WriteLine("storageFile replaced: storageFile.Name = {0}", storageFile.Name);

                    // ???
                    await storageFile.RenameAsync(contentFileName, NameCollisionOption.ReplaceExisting);
                    System.Diagnostics.Debug.WriteLine("storageFile renamed: storageFile.Name = {0}", storageFile.Name);

                    // ???
                    // await tempFile.RenameAsync(contentFileName, NameCollisionOption.ReplaceExisting);
                    // await tempFile.DeleteAsync();
                    // ???
                    return true;
                } catch (Exception ex) {
                    // ignore.
                    System.Diagnostics.Debug.WriteLine("SaveContentAsync() Failed: {0}", ex.Message);
                }
            }
            return false;
        }
        public override async Task<string> FetchContentAsync()
        {
            var storageFile = await GetStorageFileAsync();
            if (storageFile != null) {
                try {
                    var content = await FileIO.ReadTextAsync(storageFile);
                    return content;
                } catch (Exception ex) {
                    // ignore.
                    System.Diagnostics.Debug.WriteLine("FetchContentAsync() Failed: {0}", ex.Message);
                }
            }
            return null;
        }

    }
}
