﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Store.Common
{
    public abstract class AbstractContentStore : IPersistentContentStore
    {
        protected AbstractContentStore()
        {
        }

        public abstract Task<bool> SaveContentAsync(string content);
        public abstract Task<string> FetchContentAsync();

    }
}
