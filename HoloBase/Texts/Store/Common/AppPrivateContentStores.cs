﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Store.Common
{
    public class AppPrivateContentStore : AbstractContentStore, IAppPrivateContentStore
    {
        public AppPrivateContentStore()
        {
        }


        public override async Task<bool> SaveContentAsync(string content)
        {

            return false;
        }
        public override async Task<string> FetchContentAsync()
        {

            return null;
        }



    }
}
