﻿using HoloBase.Controls.Core;
using HoloBase.Texts.Clips.Common;
using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.ViewModels;
using HoloBase.UI.Colors;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HoloBase.Texts.UI
{
    public sealed partial class TextMultiClipboardControl : UserControl, IRefreshableElement, IViewModelHolder, ITextClipSourceControl
    {
        public event EventHandler<TextClipEventArgs> TextClipAvailable;

        // temporary
        private Size currentControlSize = Size.Empty;

        private TextMultiClipboardViewModel viewModel;
        private string visualStateName;    // tbd: use enum???

        public TextMultiClipboardControl()
        {
            this.InitializeComponent();
            ResetViewModel();

            isCompactForAppBarButtons = false;
            AppBarButtonAddTextClip.Visibility = Visibility.Visible;

            visualStateName = "VisualStateNormal";
            this.SizeChanged += TextClipboardControl_SizeChanged;
        }
        void TextClipboardControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            currentControlSize = e.NewSize;
            System.Diagnostics.Debug.WriteLine("TextClipboardControl_SizeChanged() currentControlSize = {0}", currentControlSize);

            // TBD:
            // It is hard to tweak this layouts...
            // Currently, it's kind of weired...
            // Between 560 and 620, a few buttons disappear (and, below 300)....
            // ....

            if (currentControlSize.Width > 560) {   // 560 shoudl be the same across all "listing" controls, including text icon and font icon list, etc.
                IsCompactForAppBarButtons = false;
            } else {
                IsCompactForAppBarButtons = true;
            }

            var threshold = 620;
            if (isCompactForAppBarButtons) {   // Heck. We assume isCompactForAppBarButtons == true means it's phone. (In our current deployment, it's true.)
                // threshold = 480;
                threshold = 365;
            }
            if (currentControlSize.Width > threshold) {         // threshold Arbitrary
                visualStateName = "VisualStateMediumAndWide";   // the rest.
            } else {
                visualStateName = "VisualStateNormal";          // Phone/Portrait
            }
            // System.Diagnostics.Debug.WriteLine("visualStateName = {0}", visualStateName);

            var suc = VisualStateManager.GoToState(this, visualStateName, false);
            System.Diagnostics.Debug.WriteLine("GoToState(): {0}. Succeeded = {1}", visualStateName, suc);
        }

        private void ResetViewModel()
        {
            var vm = new TextMultiClipboardViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(TextMultiClipboardViewModel viewModel)
        {
            this.viewModel = viewModel;
            // this.viewModel.UseFavoriteList = useFavoriteList;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (TextMultiClipboardViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }


        // --> SelectAllAsString()
        //public string TextString
        //{
        //    get
        //    {
        //        return viewModel.TextString;
        //    }
        //}

        private bool isCompactForAppBarButtons = false;
        public bool IsCompactForAppBarButtons
        {
            get
            {
                return isCompactForAppBarButtons;
            }
            set
            {
                if (isCompactForAppBarButtons != value) {
                    isCompactForAppBarButtons = value;
                    AppBarButtonAddTextClip.IsCompact = isCompactForAppBarButtons;
                    AppBarButtonCopyText.IsCompact = isCompactForAppBarButtons;
                    AppBarButtonClearList.IsCompact = isCompactForAppBarButtons;
                }
            }
        }

        public bool IsButtonAddTextClipDisplayed
        {
            get
            {
                return (AppBarButtonAddTextClip.Visibility == Visibility.Visible);
            }
            set
            {
                if (value == true) {
                    AppBarButtonAddTextClip.Visibility = Visibility.Visible;
                } else {
                    AppBarButtonAddTextClip.Visibility = Visibility.Collapsed;
                }
            }
        }

        public string BackgroundColor
        {
            get
            {
                return viewModel.BackgroundColor.ARGB;
            }
            set
            {
                viewModel.BackgroundColor = new ColorStruct(value);
            }
        }


        public string SelectAllAsString()
        {
            return viewModel.TextString;
        }

        private void AppBarButtonCopyText_Click(object sender, RoutedEventArgs e)
        {
            var text = viewModel.TextString;
            NotifyTextClipAvailable(text);
        }

        private void AppBarButtonClearList_Click(object sender, RoutedEventArgs e)
        {
            viewModel.ClearList();
        }

        private void GridViewTextClipClipList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedItem = (ITextClip) e.ClickedItem;
            System.Diagnostics.Debug.WriteLine("TextClip clip selected: {0}", clickedItem);
            ProcessTextSelected(clickedItem);
        }
        private void ProcessTextSelected(ITextClip textClip)
        {
            if (textClip != null) {
                System.Diagnostics.Debug.WriteLine("ProcessTextSelected(): textClip = {0}", textClip);
                var text = textClip.Text;
                NotifyTextClipAvailable(text);
            }
        }


        private void NotifyTextClipAvailable(string text)
        {
            // temporary
            if (TextClipAvailable != null) {
                var e = new TextClipEventArgs(this, text);
                // TextClipAvailable(this, e);
                TextClipAvailable.Invoke(this, e);
            }
        }

        private void ButtonFlyoutAccept_Click(object sender, RoutedEventArgs e)
        {
            var text = TextBoxNewText.Text;
            if (!String.IsNullOrEmpty(text)) {
                var textClip = new TextClipWrap(text);
                var cellId = TextBoxCellId.Text;
                if (String.IsNullOrEmpty(cellId)) {
                    cellId = viewModel.FirstEmptyCell;
                }
                if (! String.IsNullOrEmpty(cellId)) {
                    viewModel.PushTextClip(cellId, textClip);
                } else {
                    // ?????
                }
            }
            AppBarButtonAddTextClip.Flyout.Hide();
        }

        private void ButtonFlyoutCancel_Click(object sender, RoutedEventArgs e)
        {
            AppBarButtonAddTextClip.Flyout.Hide();
        }

    }
}
