﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Base
{
    // This is not very useful (but, not because it's short).
    // In most cases, classes that implement IContentContainer should have a _single_ content
    //    across their containment relationship (not just across inheritance hieararchy).
    // Hence, none of these classes should have their own "current content" except for ONE.
    // That one can possibly inherit from BaseContentContainer or have its own content field.
    // In our implementations, UndoRedoManager will likely be the "base class", 
    //     and since an UndoRedoManager implementation has its own internal content field,
    //     it does not generally inherit from BaseContentContainer.

    // (This can be used as a "string reference wrapper"....)
    public class BaseContentContainer : IContentContainer
    {
        // Content cannot be null.
        private string content;

        public BaseContentContainer()
            : this(null)
        {
        }
        public BaseContentContainer(string content)
        {
            this.content = content ?? "";
        }

        public string CurrentContent
        {
            get
            {
                return content;
            }
            set
            {
                this.content = value ?? "";
            }
        }

    }
}
