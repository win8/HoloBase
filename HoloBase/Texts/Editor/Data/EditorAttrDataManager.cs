﻿using HoloBase.Data.Core;
using HoloBase.Texts.Editor.Core;
using HoloJson.Mini.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Editor.Data
{
    public sealed class EditorAttrDataManager
    {
        private static EditorAttrDataManager instance = null;
        public static EditorAttrDataManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new EditorAttrDataManager();
                }
                return instance;
            }
        }
        private EditorAttrDataManager()
        {
        }


        // TBD:
        public IList<Tuple<uint, ITextEditorAttr>> GetAllEditorAttrs(int size = -1)
        {
            return GetAllEditorAttrs(SortFieldAndOrder.Null, size);
        }
        public IList<Tuple<uint, ITextEditorAttr>> GetAllEditorAttrs(SortFieldAndOrder sfo, int size = -1)
        {
            IList<Tuple<uint, ITextEditorAttr>> attrs = new List<Tuple<uint, ITextEditorAttr>>();
            var profs = EditorAttrSettingsHelper.Instance.GetAllEditorAttrs(sfo);
            if (profs != null) {
                if (size > 0) {
                    var len = profs.Count;
                    var limit = Math.Min(len, size);
                    attrs = ((List<Tuple<uint, ITextEditorAttr>>) profs).GetRange(0, limit);
                } else {
                    attrs = profs;
                }
            }
            return attrs;
        }

        // Create or Update.
        public ITextEditorAttr StoreEditorAttr(uint id, ITextEditorAttr attr)
        {
            // Validate???

            // Store metadata.
            EditorAttrSettingsHelper.Instance.StoreEditorAttr(id, attr);

            return attr;
        }


        public ITextEditorAttr GetEditorAttr(uint id)
        {
            return EditorAttrSettingsHelper.Instance.GetEditorAttr(id);
        }
        //public string GetEditorAttrFontFamilyName(uint id)
        //{
        //    return EditorAttrSettingsHelper.Instance.GetEditorAttrFontFamilyName(id);
        //}

        public bool IsEditorAttrPresent(uint id)
        {
            return EditorAttrSettingsHelper.Instance.IsEditorAttrPresent(id);
        }
        public bool ContainsEditorAttr(uint id)
        {
            return EditorAttrSettingsHelper.Instance.ContainsEditorAttr(id);
        }

        public ITextEditorAttr FetchEditorAttr(uint id)
        {
            var attr = EditorAttrSettingsHelper.Instance.FetchEditorAttr(id);
            System.Diagnostics.Debug.WriteLine("FetchEditorAttr() id = {0}, attr = {1}", id, attr);

            return attr;
        }

        // TBD:
        public bool DeleteEditorAttr(uint id)
        {
            var suc = EditorAttrSettingsHelper.Instance.DeleteEditorAttr(id);
            if (suc) {
                // Delete all bookmarks in this attr ????
                // Cascadingly delete all child attrs of this attr????
                // ...
                // For now, we don't do neither...
                // -->
                // Shouldn't we at least move all bookmarks and subattrs to the top level???
                // ..
            }
            return suc;
        }


    }
}
