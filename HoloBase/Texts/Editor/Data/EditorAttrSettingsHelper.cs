﻿using HoloBase.Data.Core;
using HoloBase.Settings.App;
using HoloBase.Texts.Editor.Common;
using HoloBase.Texts.Editor.Core;
using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Texts.Editor.Data
{
    public sealed class EditorAttrSettingsHelper
    {
        private const string KEY_EDITORATTR_CONTAINER_PREFIX = "HBASE-EditorAttr-";
        private const string ATTR_FIELD_ID = "I";
        private const string ATTR_FIELD_FF_NAME = "FFN";
        private const string ATTR_FIELD_FONT_SIZE = "FSIZE";
        private const string ATTR_FIELD_TEXT_COLOR = "FGC";
        private const string ATTR_FIELD_BACKGROUND_COLOR = "BGC";

        private static EditorAttrSettingsHelper instance = null;
        public static EditorAttrSettingsHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new EditorAttrSettingsHelper();
                }
                return instance;
            }
        }
        private EditorAttrSettingsHelper()
        {
        }

        // "lock"
        private object lockObject = new object();

        // "Cache"
        private IDictionary<uint,ITextEditorAttr> allEditorAttrMap = null;
        private IDictionary<uint,ITextEditorAttr> AllEditorAttrMap
        {
            get
            {
                if (allEditorAttrMap == null) {
                    InitAllEditorAttrMap();
                }
                return allEditorAttrMap;
            }
        }
        private void InitAllEditorAttrMap()
        {
            var containers = AppDataOptionHelper.Instance.GetAppSettings().Containers;
            lock (lockObject) {
                allEditorAttrMap = new Dictionary<uint,ITextEditorAttr>();
                foreach (var key in containers.Keys) {
                    var id = ParseIdFromEditorAttrContainerKey(key);
                    var dm = FetchEditorAttr(id);
                    if (dm != null) {
                        allEditorAttrMap.Add(id, dm);
                    }
                }
            }
        }



        private static string GenerateEditorAttrContainerKey(uint id)
        {
            var key = KEY_EDITORATTR_CONTAINER_PREFIX + id;
            return key;
        }
        private static uint ParseIdFromEditorAttrContainerKey(string key)
        {
            var id = 0U;
            try {
                var strId = key.Substring(KEY_EDITORATTR_CONTAINER_PREFIX.Length);
                id = Convert.ToUInt32(strId);
            } catch (Exception) {
                // Ignore.
            }
            return id;
        }


        // Sorts a given list and returns the sorted list.
        private IList<Tuple<uint, ITextEditorAttr>> SortEditorAttrList(IList<Tuple<uint, ITextEditorAttr>> attrs, SortFieldAndOrder sfo)
        {
            IOrderedEnumerable<Tuple<uint, ITextEditorAttr>> ordered = null;
            if (sfo.SortField != null) {
                if (sfo.SortField.Equals("FontFamilyName")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = attrs.OrderByDescending(o => o.Item2.FontFamilyName);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = attrs.OrderBy(o => o.Item2.FontFamilyName);
                            break;
                    }
                } else if (sfo.SortField.Equals("FontSize")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = attrs.OrderByDescending(o => o.Item2.FontSize);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = attrs.OrderBy(o => o.Item2.FontSize);
                            break;
                    }
                } else if (sfo.SortField.Equals("TextColor")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = attrs.OrderByDescending(o => o.Item2.TextColor.HSL.Hue).ThenByDescending(o => o.Item2.TextColor.HSL.Lightness);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = attrs.OrderBy(o => o.Item2.TextColor.HSL.Hue).ThenBy(o => o.Item2.TextColor.HSL.Lightness);
                            break;
                    }
                } else if (sfo.SortField.Equals("BackgroundColor")) {
                    switch (sfo.SortOrder) {
                        case SortOrder.Descending:
                            ordered = attrs.OrderByDescending(o => o.Item2.BackgroundColor.HSL.Hue).ThenByDescending(o => o.Item2.BackgroundColor.HSL.Lightness);
                            break;
                        case SortOrder.Ascending:
                        default:
                            ordered = attrs.OrderBy(o => o.Item2.BackgroundColor.HSL.Hue).ThenBy(o => o.Item2.BackgroundColor.HSL.Lightness);
                            break;
                    }
                } else {
                    // Default.
                    // ????
                    ordered = attrs.OrderByDescending(o => o.Item1);
                }
            }
            var sortedList = (ordered != null) ? ordered.ToList() : null;   // ???
            return sortedList;
        }


        // Note that we return List not IList, for convenience.
        private List<Tuple<uint, ITextEditorAttr>> GetEditorAttrList()
        {
            //return new List<Tuple<uint, ITextEditorAttr>>(AllEditorAttrMap.Values);

            // tbd: cache this list as well...
            var attrs = new List<Tuple<uint, ITextEditorAttr>>();
            foreach (var entry in AllEditorAttrMap) {
                var row = new Tuple<uint, ITextEditorAttr>(entry.Key, entry.Value);
                attrs.Add(row);
            }
            return attrs;
        }

        public IList<Tuple<uint, ITextEditorAttr>> GetAllEditorAttrs(SortFieldAndOrder sfo)
        {
            var attrs = GetEditorAttrList();
            var sortedList = SortEditorAttrList(attrs, sfo);
            return sortedList;
        }

        public void StoreEditorAttr(uint id, ITextEditorAttr attr)
        {
            var key = GenerateEditorAttrContainerKey(id);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();

            var container = appSettings.CreateContainer(key, ApplicationDataCreateDisposition.Always);
            appSettings.Containers[key].Values[ATTR_FIELD_ID] = id;
            appSettings.Containers[key].Values[ATTR_FIELD_FF_NAME] = attr.FontFamilyName;
            appSettings.Containers[key].Values[ATTR_FIELD_FONT_SIZE] = attr.FontSize;
            appSettings.Containers[key].Values[ATTR_FIELD_TEXT_COLOR] = attr.TextColor.ToString();
            appSettings.Containers[key].Values[ATTR_FIELD_BACKGROUND_COLOR] = attr.BackgroundColor.ToString();

            // Update the cache.
            // (Note: if allEditorAttrMap has not been initialized, 
            //     this statement will try fetching all attrs from the settings DB,
            //     which actually include this just saved attr....
            //  --> is that a problem????)
            AllEditorAttrMap[id] = attr;
        }

        public ITextEditorAttr GetEditorAttr(uint id)
        {
            if (AllEditorAttrMap.ContainsKey(id) && AllEditorAttrMap[id] != null) {
                return AllEditorAttrMap[id];
            } else {
                // tbd: 
                // Fetch ????
                return null;
            }
        }
        //public string GetEditorAttrFontFamilyName(uint id)
        //{
        //    var attr = GetEditorAttr(id);
        //    if (attr != null) {
        //        return attr.FontFamilyName;
        //    } else {
        //        return null;
        //    }
        //}

        public bool IsEditorAttrPresent(uint id)
        {
            var key = GenerateEditorAttrContainerKey(id);
            return AppDataOptionHelper.Instance.GetAppSettings().Containers.ContainsKey(key);
        }
        public bool ContainsEditorAttr(uint id)
        {
            return (AllEditorAttrMap.ContainsKey(id) && AllEditorAttrMap[id] != null);
        }

        // Does not use the allEditorAttrMap cache.
        // It does not update the cache. (Cf. GetEditorAttr())
        public ITextEditorAttr FetchEditorAttr(uint id)
        {
            ITextEditorAttr attr = null;
            var key = GenerateEditorAttrContainerKey(id);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
            if (appSettings.Containers.ContainsKey(key)) {

                TextEditorAttr dm = new TextEditorAttr();

                var ffName = "";
                if (appSettings.Containers[key].Values.ContainsKey(ATTR_FIELD_FF_NAME) && appSettings.Containers[key].Values[ATTR_FIELD_FF_NAME] != null) {
                    ffName = appSettings.Containers[key].Values[ATTR_FIELD_FF_NAME].ToString();
                }
                dm.FontFamilyName = ffName;

                var fontSize = 0.0;
                if (appSettings.Containers[key].Values.ContainsKey(ATTR_FIELD_FONT_SIZE) && appSettings.Containers[key].Values[ATTR_FIELD_FONT_SIZE] != null) {
                    try {
                        var strFontSize = appSettings.Containers[key].Values[ATTR_FIELD_FONT_SIZE].ToString();
                        fontSize = Convert.ToDouble(strFontSize);
                    } catch (Exception) {
                        // Ignore.
                    }
                }
                dm.FontSize = fontSize;

                var textColor = ColorStruct.Null;
                if (appSettings.Containers[key].Values.ContainsKey(ATTR_FIELD_TEXT_COLOR) && appSettings.Containers[key].Values[ATTR_FIELD_TEXT_COLOR] != null) {
                    var strTextColor = appSettings.Containers[key].Values[ATTR_FIELD_TEXT_COLOR].ToString();
                    textColor = new ColorStruct(strTextColor);
                }
                dm.TextColor = textColor;

                var backgroundColor = ColorStruct.Null;
                if (appSettings.Containers[key].Values.ContainsKey(ATTR_FIELD_BACKGROUND_COLOR) && appSettings.Containers[key].Values[ATTR_FIELD_BACKGROUND_COLOR] != null) {
                    var strBackgroundColor = appSettings.Containers[key].Values[ATTR_FIELD_BACKGROUND_COLOR].ToString();
                    backgroundColor = new ColorStruct(strBackgroundColor);
                }
                dm.BackgroundColor = backgroundColor;

                attr = dm;
            }
            return attr;
        }

        public bool DeleteEditorAttr(uint id)
        {
            var suc = false;
            var key = GenerateEditorAttrContainerKey(id);
            var appSettings = AppDataOptionHelper.Instance.GetAppSettings();
            if (appSettings.Containers.ContainsKey(key)) {
                appSettings.DeleteContainer(key);
                suc = true;
            }
            AllEditorAttrMap.Remove(id);  // Note that we call this even if suc == false.
            return suc;
        }


    }
}
