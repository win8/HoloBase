﻿using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Editor.Core
{
    // Despite the name, this could be associated with a "document".
    // (Normally, it might be good enough to save this in a user's global app settings.)
    public interface ITextEditorAttr
    {
        string FontFamilyName
        {
            get;
        }
        double FontSize
        {
            get;
        }
        ColorStruct TextColor
        {
            get;
        }
        ColorStruct BackgroundColor
        {
            get;
        }


    }
}
