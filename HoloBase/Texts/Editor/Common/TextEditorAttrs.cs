﻿using HoloBase.Texts.Editor.Core;
using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Editor.Common
{
    public class TextEditorAttr : ITextEditorAttr
    {
        // id ???
        private string fontFamilyName;
        private double fontSize;
        private ColorStruct textColor;
        private ColorStruct backgroundColor;

        public TextEditorAttr()
        {
        }


        public string FontFamilyName
        {
            get
            {
                return fontFamilyName;
            }
            set
            {
                fontFamilyName = value;
            }
        }

        public double FontSize
        {
            get
            {
                return fontSize;
            }
            set
            {
                fontSize = value;
            }
        }

        public ColorStruct TextColor
        {
            get
            {
                return textColor;
            }
            set
            {
                textColor = value;
            }
        }

        public ColorStruct BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("FontFamilyName").Append(FontFamilyName).Append("; ");
            sb.Append("FontSize").Append(FontSize).Append("; ");
            sb.Append("TextColor").Append(TextColor).Append("; ");
            sb.Append("BackgroundColor").Append(BackgroundColor);
            return sb.ToString();
        }

    }
}
