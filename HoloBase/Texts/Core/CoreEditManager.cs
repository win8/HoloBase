﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Core
{
    public interface ICoreEditManager : IContentContainer
    {
        string InsertText(int position, string text);
        string DeleteText(int position, string text);
        string ReplaceText(int position, string newText, string oldText);
    }
}
