﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;


namespace HoloBase.Texts.Core
{
    // TBD:
    public interface IPersistentContentStore
    {
        // IStorageFile  --> local file, or file on OneDrive, etc.
        // AppSettings...
        // DB (sqlite, etc.)
        // etc..


        Task<bool> SaveContentAsync(string content);
        Task<string> FetchContentAsync();

    }
}
