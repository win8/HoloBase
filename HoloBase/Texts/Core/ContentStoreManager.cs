﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Core
{
    public interface IContentStoreManager
    {
        //IPersistentContentStore PersistentContentStore
        //{
        //    get;
        //}

        // Save buffer to file
        // This is normal "save"
        Task<bool> SyncDownAsync(string content);

        // Update buffer from file.
        // This is an unutal operation. 
        // Needed when "resotring" the buffer from the persistent storage.
        Task<string> SyncUpAsync();

    }
}
