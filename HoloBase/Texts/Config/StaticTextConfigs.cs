﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Config
{
    // "Hard-coded" app configs.
    // These should NOT be changed (even from version to version).
    // App's integrity may depend on using constant values over time.....
    internal static class StaticTextConfigs
    {
        private const int MAX_LENGTH_CLIP_SHORT_TEXT = 12;
        private const int MAX_LENGTH_CLIP_TRUNCATED_TEXT = 36;

        // Currently, because of the way we use "cell index" (a through z),
        //     this value cannot be bigger than 26.
        // (TBD: We need a simple routine to map 27 to 'aa', etc...)
        private const int DEFAULT_SIZE_MULTI_CLIP_CELLS = 26;

        internal static int DefaultMaxLengthShortText
        {
            get
            {
                return MAX_LENGTH_CLIP_SHORT_TEXT;
            }
        }
        internal static int DefaultMaxLengthTruncatedText
        {
            get
            {
                return MAX_LENGTH_CLIP_TRUNCATED_TEXT;
            }
        }

        internal static int DefaultSizeMultiClipboardCells
        {
            get
            {
                return DEFAULT_SIZE_MULTI_CLIP_CELLS;
            }
        }

        // etc...
        // ....

    }
}
