﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Config
{
    public sealed class TextConfigManager
    {
        private static TextConfigManager instance = null;
        public static TextConfigManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new TextConfigManager();
                }
                return instance;
            }
        }

        private int maxLengthShortText;
        private int maxLengthTruncatedText;
        private int multiClipboardSize;

        private TextConfigManager()
        {
            maxLengthShortText = StaticTextConfigs.DefaultMaxLengthShortText;
            maxLengthTruncatedText = StaticTextConfigs.DefaultMaxLengthTruncatedText;
            multiClipboardSize = StaticTextConfigs.DefaultSizeMultiClipboardCells;
        }


        public int MaxLengthShortText
        {
            get
            {
                return maxLengthShortText;
            }
            set
            {
                maxLengthShortText = value;
            }
        }

        public int MaxLengthTruncatedText
        {
            get
            {
                return maxLengthTruncatedText;
            }
            set
            {
                maxLengthTruncatedText = value;
            }
        }

        public int MultiClipboardSize
        {
            get
            {
                return multiClipboardSize;
            }
            //    If we use persistent storage for TextMultiClipboard, then
            //    MultiClipboardSize cannot be changed.
            private set
            {
                multiClipboardSize = value;
            }
        }
           

    }
}
