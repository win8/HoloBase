﻿using HoloBase.Texts.Urr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts
{
    public sealed class UndoRedoManagerFactory
    {
        private static UndoRedoManagerFactory instance = null;
        public static UndoRedoManagerFactory Instance
        {
            get
            {
                if (instance == null) {
                    instance = new UndoRedoManagerFactory();
                }
                return instance;
            }
        }

        // We use a single UndoRedoManager per page or text box, or actually per "document".
        // The "id" should be hard coded into the control/page(s).  --> but, how ???
        //  (Note: since mgr is associated with a document, we can use the same mgr across different controls/pages.)
        // id -> UndoRedoManager.
        private readonly IDictionary<string, UndoRedoManager> undoRedoManagers;

        private UndoRedoManagerFactory()
        {
            undoRedoManagers = new Dictionary<string, UndoRedoManager>();
        }


        public UndoRedoManager GetUndoRedoManager(string docId)
        {
            if (! undoRedoManagers.ContainsKey(docId)) {
                undoRedoManagers[docId] = new UndoRedoManager();
            }
            return undoRedoManagers[docId];
        }




    }
}
