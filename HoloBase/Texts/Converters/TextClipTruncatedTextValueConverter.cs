﻿using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;


namespace HoloBase.Texts.Converters
{
    public class TextClipTruncatedTextValueConverter : IValueConverter
    {
        // value: ITextClip
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var truncatedText = "";
            var textClip = value as ITextClip;
            if (textClip != null) {
                truncatedText = textClip.TruncatedText;
                if (String.IsNullOrEmpty(truncatedText)) {
                    var text = textClip.Text;
                    if (text != null) {
                        var maxLen = TextConfigManager.Instance.MaxLengthTruncatedText;
                        var textLen = text.Length;
                        if (textLen <= maxLen) {
                            truncatedText = text;
                        } else {
                            truncatedText = text.Substring(0, maxLen);  // No ellipsis
                        }
                    }
                }
            } else {
                // truncatedText = value.ToString();   // ???
            }
            return truncatedText;
        }
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
