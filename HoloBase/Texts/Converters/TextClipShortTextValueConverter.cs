﻿using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;


namespace HoloBase.Texts.Converters
{
    public class TextClipShortTextValueConverter : IValueConverter
    {
        // value: ITextClip
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var shortText = "";
            var textClip = value as ITextClip;
            if (textClip != null) {
                shortText = textClip.ShortText;
                if (String.IsNullOrEmpty(shortText)) {
                    var text = textClip.Text;
                    if (text != null) {
                        var maxLen = TextConfigManager.Instance.MaxLengthShortText;
                        var textLen = text.Length;
                        if (textLen <= maxLen) {
                            shortText = text;
                        } else {
                            shortText = text.Substring(0, maxLen);  // No ellipsis
                        }
                    }
                }
            } else {
                // shortText = value.ToString();   // ???
            }
            return shortText;
        }
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
