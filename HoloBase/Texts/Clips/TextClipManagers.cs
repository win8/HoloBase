﻿using HoloBase.Texts.Clips.Common;
using HoloBase.Texts.Clips.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips
{
    public sealed class TextClipManager
    {
        private static TextClipManager instance = null;
        public static TextClipManager Instance
        {
            get
            {
                if (instance == null) {
                    instance = new TextClipManager();
                }
                return instance;
            }
        }

        // TBD:
        private readonly TextStackClipboard globalStackClipboard;
        private readonly TextMultiClipboard globalMultiClipboard;
        // Clipboard per page/document ???
        private readonly IDictionary<string, ITextStackClipboard> documentStackClipboards;
        private readonly IDictionary<string, ITextMultiClipboard> documentMultiClipboards;

        private TextClipManager()
        {
            globalStackClipboard = new TextStackClipboard();
            globalMultiClipboard = new TextMultiClipboard();
            documentStackClipboards = new Dictionary<string, ITextStackClipboard>();
            documentMultiClipboards = new Dictionary<string, ITextMultiClipboard>();
        }

        public ITextStackClipboard GlobalStackClipboard
        {
            get
            {
                return globalStackClipboard;
            }
        }
        public ITextMultiClipboard GlobalMultiClipboard
        {
            get
            {
                return globalMultiClipboard;
            }
        }

        public ITextStackClipboard GetStackClipboard(string docId)
        {
            if (!documentStackClipboards.ContainsKey(docId)) {
                documentStackClipboards[docId] = new TextStackClipboard();
            }
            return documentStackClipboards[docId];
        }
        public ITextMultiClipboard GetMultiClipboard(string docId)
        {
            if (!documentMultiClipboards.ContainsKey(docId)) {
                documentMultiClipboards[docId] = new TextMultiClipboard();
            }
            return documentMultiClipboards[docId];
        }

    }
}
