﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips.Core
{
    public interface ITextRingClipboard : ITextLinearClipboard
    {
        // These Peek's not Pop's.
        ITextClip ShiftForward();
        ITextClip ShiftBackward();
    }
}
