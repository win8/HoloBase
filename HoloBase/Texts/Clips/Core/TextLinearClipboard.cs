﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips.Core
{
    public interface ITextLinearClipboard : ITextClipboard
    {
        ITextClip Peek();
        ITextClip Pop();

        //void Push(string text);
        //void Push(params string[] texts);
        //void Push(IEnumerable<string> texts);

        void Push(ITextClip textClip);
        void Push(params ITextClip[] textClips);
        void Push(IEnumerable<ITextClip> textClips);

    }
}
