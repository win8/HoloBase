﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips.Core
{
    public interface ITextClip
    {
        string Text
        {
            get;
        }
        string ShortText
        {
            get;
        }
        string TruncatedText
        {
            get;
        }
        long Expiration
        {
            get;
        }

    }
}
