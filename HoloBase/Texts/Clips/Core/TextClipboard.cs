﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips.Core
{
    public interface ITextClipboard
    {
        // Returns true if any text clip exists.
        bool HasAny
        {
            get;
        }

        // Returns the current number of text clips.
        // Note that Size == Count in case of StackClipboard.
        int Count
        {
            get;
        }

        // Returns the number of "slots".
        // In case of StackClipboard, this is the same as the number of text clips currently in the clipboard.
        // In case of MultiClipboard, this is the number of "slots", each of which may or may not be empty.
        int Size
        {
            get;
        }

        // Clears the stack.
        // Or, clears all content in the MultiClipboard.
        void Clear();

        // Returns the text clips as a list.
        IList<ITextClip> ClipList
        {
            get;
        }

        // Returns the string representation of ClipList.
        string ListAsString
        {
            get;
        }


    }
}
