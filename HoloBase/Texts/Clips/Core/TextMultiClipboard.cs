﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips.Core
{
    public interface ITextMultiClipboard : ITextClipboard
    {
        // Note that 
        //     we view each "cell" as a single element stack.
        //     --> We have size # of stacks, each of which supports Peek/Pop/Push, etc.

        string FirstEmptyCell
        {
            get;
        }

        IList<Tuple<string, ITextClip>> CellClipList
        {
            get;
        }

        ITextClip PeekAt(string cellId);
        ITextClip PopAt(string cellId);

        // Multi-clip API only makes sense if each of the cell is a stack.
        // Currently, where each cell is a single element Text Clip, these methods are not very useful. 

        //bool PushAt(string cellId, string text);
        ////bool PushAt(string cellId, params string[] texts);
        ////bool PushAt(string cellId, IEnumerable<string> texts);

        bool PushAt(string cellId, ITextClip textClip);
        //bool PushAt(string cellId, params ITextClip[] textClips);
        //bool PushAt(string cellId, IEnumerable<ITextClip> textClips);

    }
}
