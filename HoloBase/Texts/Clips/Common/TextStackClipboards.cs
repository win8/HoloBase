﻿using HoloBase.Texts.Clips.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips.Common
{
    public class TextStackClipboard : ITextStackClipboard
    {
        // private readonly Stack<string> clipStack;
        private readonly Stack<ITextClip> clipStack;

        public TextStackClipboard()
        {
            // clipStack = new Stack<string>();
            clipStack = new Stack<ITextClip>();


            //// testing
            //clipStack.Push("abc");
            //// testing

        }

        protected Stack<ITextClip> ClipStack
        {
            get
            {
                return clipStack;
            }
        }

        public bool HasAny
        {
            get
            {
                //var count = clipStack.Count;
                //System.Diagnostics.Debug.WriteLine("clipStack.Count = {0}", count);

                return clipStack.Any();
                // return clipStack.ToList().Any(); 
            }
        }
        public int Count
        {
            get
            {
                return clipStack.Count;
            }
        }
        public int Size
        {
            get
            {
                return clipStack.Count;
            }
        }

        public void Clear()
        {
            clipStack.Clear();
        }

        //public IList<string> ClipList
        //{
        //    get
        //    {
        //        // return clipStack.ToList();
        //        var list = clipStack.ToList();
        //        list.Reverse();   // Put last one at the end.
        //        return list;
        //    }
        //}
        public IList<ITextClip> ClipList
        {
            get
            {
                // return clipStack.ToList();
                var list = clipStack.ToList();
                list.Reverse();   // Put last one at the end.
                return list;
            }
        }

        public string ListAsString
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var s in ClipList) {   // Note the clips are ordered so that first/oldest clip comes first.
                    sb.Append(s).Append(Environment.NewLine);
                }
                return sb.ToString();
            }
        }


        // Note that, in our implementation,
        //   clips need not be pop'ed().
        // We will mostly use Peek and Push,
        // (and, item-based access, e.g., from ListView).
        public ITextClip Peek()
        {
            if (clipStack.Any()) {
                return clipStack.Peek();
            } else {
                // ???
                return null;
            }
        }
        public ITextClip Pop()
        {
            if (clipStack.Any()) {
                return clipStack.Pop();
            } else {
                // ???
                return null;
            }
        }


        // TBD:
        // TextClipStruct vs TextClipWrap ????

        public void Push(string text, long expiration = 0L)
        {
            clipStack.Push(new TextClipStruct(text, expiration));
        }
        public void Push(params string[] texts)
        {
            foreach (var s in texts) {
                clipStack.Push(new TextClipStruct(s));
            }
        }
        public void Push(IEnumerable<string> texts, long expiration = 0L)
        {
            if (texts != null) {
                foreach (var s in texts) {
                    clipStack.Push(new TextClipStruct(s, expiration));
                }
            }
        }

        public void Push(ITextClip text)
        {
            // tbd:
            // do not put the same text in consecutive spots...
            // ...

            clipStack.Push(text);
        }
        public void Push(params ITextClip[] texts)
        {
            foreach (var s in texts) {
                clipStack.Push(s);
            }
        }
        public void Push(IEnumerable<ITextClip> texts)
        {
            if (texts != null) {
                foreach (var s in texts) {
                    clipStack.Push(s);
                }
            }
        }


        public override string ToString()
        {
            return ListAsString;
        }

    }
}
