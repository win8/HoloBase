﻿using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips.Common
{
    public struct TextClipStruct : ITextClip
    {
        private string text;
        private string shortText;
        private string truncatedText;     // length????
        private long expiration;          // ????

        public TextClipStruct(string text, long expiration = 0L)
        {
            this.text = text;
            this.shortText = null;
            this.truncatedText = null;
            this.expiration = expiration;
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                shortText = null;
                truncatedText = null;
            }
        }
        public string ShortText
        {
            get
            {
                if (shortText == null) {
                    if (text != null) {
                        var maxLen = TextConfigManager.Instance.MaxLengthShortText;
                        var textLen = text.Length;
                        if (textLen <= maxLen) {
                            shortText = text;
                        } else {
                            shortText = text.Substring(0, maxLen);  // No ellipsis
                        }
                    }
                }
                return shortText;
            }
        }
        public string TruncatedText
        {
            get
            {
                if (truncatedText == null) {
                    if (text != null) {
                        var maxLen = TextConfigManager.Instance.MaxLengthTruncatedText;
                        var textLen = text.Length;
                        if (textLen <= maxLen) {
                            truncatedText = text;
                        } else {
                            truncatedText = text.Substring(0, maxLen - 3) + "...";
                        }
                    }
                }
                return truncatedText;
            }
        }
        public long Expiration
        {
            get
            {
                return expiration;
            }
            set
            {
                expiration = value;
            }
        }

        public override string ToString()
        {
            //var sb = new StringBuilder();
            //sb.Append("Text:").Append(Text).Append("; ");
            //sb.Append("Expiration:").Append(Expiration);
            //return sb.ToString();
            return Text;
        }
    }

    public class TextClipWrap : ITextClip
    {
        private TextClipStruct textClipStruct;

        public TextClipWrap(TextClipStruct textClipStruct)
        {
            this.textClipStruct = textClipStruct;
        }
        public TextClipWrap(string text, long expiration = 0L)
            : this(new TextClipStruct(text, expiration))
        {
        }

        public TextClipStruct TextClipStruct
        {
            get
            {
                return textClipStruct;
            }
        }

        public string Text
        {
            get
            {
                return textClipStruct.Text;
            }
            set
            {
                textClipStruct.Text = value;
            }
        }
        public string ShortText
        {
            get
            {
                return textClipStruct.ShortText;
            }
        }
        public string TruncatedText
        {
            get
            {
                return textClipStruct.TruncatedText;
            }
        }
        public long Expiration
        {
            get
            {
                return textClipStruct.Expiration;
            }
            set
            {
                textClipStruct.Expiration = value;
            }
        }


        public override string ToString()
        {
            return textClipStruct.ToString();
        }
    }
}
