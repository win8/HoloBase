﻿using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Clips.Common
{
    public class TextMultiClipboard : ITextMultiClipboard
    {
        // "cell id" -> text clip.
        // TBD: Make each cell a stack ????
        private readonly IDictionary<string, ITextClip> clipMap;

        public TextMultiClipboard()
        {
            clipMap = new Dictionary<string, ITextClip>();
            // Note: the MultiClipboardSize setter should be called, if any, before TextMultiClipboard is constructed.
            //    If we use persistent storage for TextMultiClipboard, then
            //    MultiClipboardSize cannot be changed.
            var size = TextConfigManager.Instance.MultiClipboardSize;
            size = Math.Min(size, 26);     // 26 maximum for now...

            // This makes it a bit convenient since using clipMap[cellIndex] will not throw exception.
            // It also preserves the insertion order, 'a' through 'z'...
            for (var i = 0; i < size; i++) {
                var cellIndex = ((char) ('a' + i)).ToString();
                clipMap[cellIndex] = null;
            }

            //// testing
            //clipMap["a"] = new TextClipStruct("abc");
            //// testing

        }

        protected IDictionary<string, ITextClip> ClipMap
        {
            get
            {
                return clipMap;
            }
        }

        public bool HasAny
        {
            get
            {
                return (this.Count > 0);
            }
        }
        public int Count
        {
            get
            {
                var count = 0;
                foreach (var c in clipMap.Values) {
                    count += (c != null) ? 1 : 0;
                }
                return count;
            }
        }
        public int Size
        {
            get
            {
                return clipMap.Count;
            }
        }

        // Clear() does not clear the map, only its entries....
        public void Clear()
        {
            foreach (var id in clipMap.Keys) {
                clipMap[id] = null;
            }
            cellClipList = null;
        }

        // "Cache"
        private IList<Tuple<string, ITextClip>> cellClipList = null;
        public IList<Tuple<string, ITextClip>> CellClipList
        {
            get
            {
                if (cellClipList == null) {
                    foreach (var e in clipMap) {
                        var cellClip = new Tuple<string, ITextClip>(e.Key, e.Value);  // the value could be null.
                        cellClipList.Add(cellClip);
                    }
                }
                return cellClipList;
            }
        }

        public IList<ITextClip> ClipList
        {
            get
            {
                // Note that the list can contain null elements.
                var list = clipMap.Values.ToList();
                return list;
            }
        }

        public string ListAsString
        {
            get
            {
                var sb = new StringBuilder();
                //foreach (var s in ClipList) {
                //    if (s != null) {
                //        sb.Append(s).Append(Environment.NewLine);
                //    }
                //}
                foreach (var id in clipMap.Keys) {
                    var s = clipMap[id];
                    if (s != null) {
                        sb.Append(id).Append(":").Append(s).Append(Environment.NewLine);
                    }
                }
                return sb.ToString();
            }
        }


        public string FirstEmptyCell
        {
            get
            {
                // tbd: cache this???
                string cellId = null;
                foreach (var id in clipMap.Keys) {
                    var s = clipMap[id];
                    if (s == null) {
                        cellId = id;
                        break;
                    }
                }
                // Note that cellId can be null if the clipboard is currently full.
                return cellId;
            }
        }


        // Note that, in our implementation,
        //   clips need not be pop'ed().
        // We will mostly use Peek and Push/Replace,
        // (and, item-based access, e.g., from ListView).
        public ITextClip PeekAt(string cellId)
        {
            if (clipMap.ContainsKey(cellId)) {
                return clipMap[cellId];
            }
            return null;
        }
        public ITextClip PopAt(string cellId)
        {
            if (clipMap.ContainsKey(cellId)) {
                var clip = clipMap[cellId];
                clipMap[cellId] = null;      // We do not delete the entry. Merely sets the value to null.
                cellClipList = null;
                return clip;
            }
            return null;
        }


        public bool PushAt(string cellId, string text, long expiration = 0L)
        {
            return PushAt(cellId, new TextClipStruct(text, expiration));
        }

        public bool PushAt(string cellId, ITextClip textClip)
        {
            if (clipMap.ContainsKey(cellId)) {
                clipMap[cellId] = textClip;
                cellClipList = null;
                return true;
            } else {
                return false;
            }
        }


        public override string ToString()
        {
            return ListAsString;
        }

    }
}
