﻿using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Core;
using HoloBase.Texts.Edit.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit
{
    public class TextEditManager : BasicTextEditManager, ITextEditManager
    {
        public TextEditManager(IContentContainer contentContainer = null, ITextLinearClipboard clipboard = null)
            : base(contentContainer, clipboard)
        {
        }



        public Tuple<int, string> FindFirst(string targetText)
        {
            throw new NotImplementedException();
        }

        public Tuple<int, string> FindFirst(string targetText, int startPosition)
        {
            throw new NotImplementedException();
        }

        public Tuple<int, string> FindFirst(string targetText, int startPosition, int endPosition)
        {
            throw new NotImplementedException();
        }

        public IDictionary<int, string> FindAll(string targetText)
        {
            throw new NotImplementedException();
        }

        public IDictionary<int, string> FindAll(string targetText, int startPosition)
        {
            throw new NotImplementedException();
        }

        public IDictionary<int, string> FindAll(string targetText, int startPosition, int endPosition)
        {
            throw new NotImplementedException();
        }

        public string ReplaceFirst(string insertedText, string targetText)
        {
            throw new NotImplementedException();
        }

        public string ReplaceFirst(string insertedText, string targetText, int startPosition)
        {
            throw new NotImplementedException();
        }

        public string ReplaceFirst(string insertedText, string targetText, int startPosition, int endPosition)
        {
            throw new NotImplementedException();
        }

        public string ReplaceAll(string insertedText, string targetText)
        {
            throw new NotImplementedException();
        }

        public string ReplaceAll(string insertedText, string targetText, int startPosition)
        {
            throw new NotImplementedException();
        }

        public string ReplaceAll(string insertedText, string targetText, int startPosition, int endPosition)
        {
            throw new NotImplementedException();
        }

    }
}
