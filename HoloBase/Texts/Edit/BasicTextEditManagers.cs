﻿using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Core;
using HoloBase.Texts.Edit.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit
{
    public class BasicTextEditManager : SimpleClipboardEditManager, IBasicTextEditManager
    {
        public BasicTextEditManager(IContentContainer contentContainer = null, ITextLinearClipboard clipboard = null)
            : base(contentContainer, clipboard)
        {
        }



        public string InsertText(int position, string text)
        {
            if (EditUndoRedoManager != null) {
                return EditUndoRedoManager.InsertText(position, text);
            } else {
                // tbd:
                return null;
            }
        }

        public string DeleteText(int position, string text)
        {
            if (EditUndoRedoManager != null) {
                return EditUndoRedoManager.DeleteText(position, text);
            } else {
                // tbd:
                return null;
            }
        }

        public string ReplaceText(int position, string newText, string oldText)
        {
            if (EditUndoRedoManager != null) {
                return EditUndoRedoManager.ReplaceText(position, newText, oldText);
            } else {
                // tbd:
                return null;
            }
        }

    
    }
}
