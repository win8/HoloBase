﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit.Core
{
    public interface ICharWordLineManager
    {
        // Miscellaneous.
        string MakeUppercase(int position, string selectedText);
        string MakeLowercase(int position, string selectedText);

        // Perform the ops on the line containting the given position.
        string DeleteLine(int position);
        string MoveLineUp(int position);
        string MoveLineDown(int position);
        // string CapitalizeLine(int position);

        // Perform the ops on the word containting the given position.
        string DeleteWord(int position);
        string MoveWordBackward(int position);    // Up
        string MoveWordForward(int position);     // Down
        // string CapitalizeWord(int position);

        // TBD: Char-based delete/move, switch(Ctrl+t), etc. ???
        // ...

    }
}
