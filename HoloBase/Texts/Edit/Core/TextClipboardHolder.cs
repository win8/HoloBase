﻿using HoloBase.Texts.Clips.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit.Core
{
    public interface ITextClipboardHolder
    {
        ITextClipboard TextClipboard
        {
            get;
        }
    }
}
