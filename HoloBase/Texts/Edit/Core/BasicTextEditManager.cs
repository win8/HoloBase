﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit.Core
{
    public interface IBasicTextEditManager : ICoreEditManager, IClipboardEditManager
    {
        // TBD:
        // IContentUndoRedoManager ???
        // ...

        // All methods return the current/post-op content if the operation is successful.
        // Otherwise, return null.
        // (Note: position is based on IContentContainer.CurrentContent.)
        // The only exceptions are "Find" methods.

        //// Note: These are the same as those three in IEditUndoRedoManager.
        //string InsertText(int position, string insertedText);
        //string DeleteText(int position, string selectedText);
        //string ReplaceText(int position, string insertedText, string replacedText);


    }
}
