﻿using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit.Core
{
    public interface IUndoRedoManagerHolder
    {
        IUndoRedoManager UndoRedoManager
        {
            get;
        }
    }
    //public interface IEditUndoRedoManagerHolder
    //{
    //    IEditUndoRedoManager EditUndoRedoManager
    //    {
    //        get;
    //    }
    //}
    //public interface IContentUndoRedoManagerHolder
    //{
    //    IContentUndoRedoManager ContentUndoRedoManager
    //    {
    //        get;
    //    }
    //}
    ////public interface IContentEditUndoRedoManagerHolder
    ////{
    ////    IContentEditUndoRedoManager ContentEditUndoRedoManager
    ////    {
    ////        get;
    ////    }
    ////}

}
