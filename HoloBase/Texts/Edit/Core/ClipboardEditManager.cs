﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit.Core
{
    public interface IClipboardEditManager : IContentContainer
    {
        // TBD:
        // TextClipboard ???
        // ...

        // All methods return the current/post-op content if the operation is successful.
        // Otherwise, return null.
        // (Note: position is based on IContentContainer.CurrentContent.)

        string CutText(int position, string selectedText);
        string CopyText(int position, string selectedText);
        string PasteText(int position);
        string PasteText(int position, string replacedText);

    }
}
