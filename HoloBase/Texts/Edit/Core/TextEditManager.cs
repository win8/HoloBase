﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit.Core
{
    public interface ITextEditManager : IBasicTextEditManager, IFindAndReplaceManager
    {
    }
}
