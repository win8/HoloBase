﻿using HoloBase.Texts.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit.Core
{
    public interface IFindAndReplaceManager : IContentContainer
    {
        Tuple<int, string> FindFirst(string targetText);
        Tuple<int, string> FindFirst(string targetText, int startPosition);
        Tuple<int, string> FindFirst(string targetText, int startPosition, int endPosition);
        //IList<Tuple<int, string>> FindAll(string targetText);
        //IList<Tuple<int, string>> FindAll(string targetText, int startPosition);
        //IList<Tuple<int, string>> FindAll(string targetText, int startPosition, int endPosition);
        IDictionary<int, string> FindAll(string targetText);
        IDictionary<int, string> FindAll(string targetText, int startPosition);
        IDictionary<int, string> FindAll(string targetText, int startPosition, int endPosition);

        string ReplaceFirst(string insertedText, string targetText);
        string ReplaceFirst(string insertedText, string targetText, int startPosition);
        string ReplaceFirst(string insertedText, string targetText, int startPosition, int endPosition);
        string ReplaceAll(string insertedText, string targetText);
        string ReplaceAll(string insertedText, string targetText, int startPosition);
        string ReplaceAll(string insertedText, string targetText, int startPosition, int endPosition);

    }
}
