﻿using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Core;
using HoloBase.Texts.Edit.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit
{
    public class AdvancedTextEditManager : TextEditManager, IAdvancedTextEditManager
    {
        public AdvancedTextEditManager(IContentContainer contentContainer = null, ITextLinearClipboard clipboard = null)
            : base(contentContainer, clipboard)
        {
        }



        public string MakeUppercase(int position, string selectedText)
        {
            if (!String.IsNullOrEmpty(selectedText)) {
                if (EditUndoRedoManager != null) {
                    var uppered = selectedText.ToUpper();
                    // if(! selectedText.Equals(uppered))
                    return EditUndoRedoManager.ReplaceText(position, uppered, selectedText);
                } else {
                    // tbd:
                    return null;
                }
            }
            // return null;   // ???
            return CurrentContent;
        }

        public string MakeLowercase(int position, string selectedText)
        {
            if (!String.IsNullOrEmpty(selectedText)) {
                if (EditUndoRedoManager != null) {
                    var lowered = selectedText.ToLower();
                    // if(! selectedText.Equals(lowered))
                    return EditUndoRedoManager.ReplaceText(position, lowered, selectedText);
                } else {
                    // tbd:
                    return null;
                }
            }
            // return null;   // ???
            return CurrentContent;
        }


        public string DeleteLine(int position)
        {
            throw new NotImplementedException();
        }

        public string MoveLineUp(int position)
        {
            throw new NotImplementedException();
        }

        public string MoveLineDown(int position)
        {
            throw new NotImplementedException();
        }


        public string DeleteWord(int position)
        {
            throw new NotImplementedException();
        }

        public string MoveWordBackward(int position)
        {
            throw new NotImplementedException();
        }

        public string MoveWordForward(int position)
        {
            throw new NotImplementedException();
        }

    }
}
