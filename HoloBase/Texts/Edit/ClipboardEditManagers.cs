﻿using HoloBase.Texts.Base;
using HoloBase.Texts.Clips;
using HoloBase.Texts.Clips.Common;
using HoloBase.Texts.Clips.Core;
using HoloBase.Texts.Core;
using HoloBase.Texts.Edit.Core;
using HoloBase.Texts.Urr.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Texts.Edit
{
    public class SimpleClipboardEditManager : IClipboardEditManager, ITextClipboardHolder, IUndoRedoManagerHolder, ICoreEditManagerHolder
    {
        // Note that we use IContentContainer as an internal field, not content:string.
        private IContentContainer contentContainer;
        // Somewhat strange... but "Cast cache" 
        private ICoreEditManager coreEditManager = null;
        private IUndoRedoManager undoRedoManager = null;
        private IEditUndoRedoManager editUndoRedoManager = null;
        private IContentUndoRedoManager contentUndoRedoManager = null;
        // private IContentEditUndoRedoManager contentEditUndoRedoManager = null;

        // Clipboard:
        private ITextClipboard textClipboard = null;
        private ITextLinearClipboard textLinearClipboard = null;

        public SimpleClipboardEditManager(IContentContainer contentContainer = null, ITextLinearClipboard clipboard = null)
        {
            this.contentContainer = contentContainer ?? new BaseContentContainer();
            this.coreEditManager = this.contentContainer as ICoreEditManager;
            this.undoRedoManager = this.contentContainer as IUndoRedoManager;
            this.editUndoRedoManager = this.contentContainer as IEditUndoRedoManager;
            this.contentUndoRedoManager = this.contentContainer as IContentUndoRedoManager;
            // this.contentEditUndoRedoManager = this.contentContainer as IContentEditUndoRedoManager;
            this.textClipboard = clipboard ?? TextClipManager.Instance.GlobalStackClipboard;
            this.textLinearClipboard = this.textClipboard as ITextLinearClipboard;
        }

        public string CurrentContent
        {
            get
            {
                return this.contentContainer.CurrentContent;
            }
            set
            {
                this.contentContainer.CurrentContent = value;
            }
        }

        public IUndoRedoManager UndoRedoManager
        {
            get
            {
                return this.undoRedoManager;
            }
        }
        protected IEditUndoRedoManager EditUndoRedoManager
        {
            get
            {
                return this.editUndoRedoManager;
            }
        }
        protected IContentUndoRedoManager ContentUndoRedoManager
        {
            get
            {
                return this.contentUndoRedoManager;
            }
        }
        //protected IContentEditUndoRedoManager ContentEditUndoRedoManager
        //{
        //    get
        //    {
        //        return this.contentEditUndoRedoManager;
        //    }
        //}

        public ITextClipboard TextClipboard
        {
            get
            {
                return this.textClipboard;
            }
        }
        protected ITextLinearClipboard TextLinearClipboard
        {
            get
            {
                return this.textLinearClipboard;
            }
        }

        public ICoreEditManager CoreEditManager
        {
            get
            {
                return this.coreEditManager;
            }
        }



        public string CutText(int position, string selectedText)
        {
            if (!String.IsNullOrEmpty(selectedText)) {
                var textClip = new TextClipStruct(selectedText);
                textLinearClipboard.Push(textClip);
                if (EditUndoRedoManager != null) {
                    return EditUndoRedoManager.DeleteText(position, selectedText);
                } else {
                    // tbd:
                    return null;
                }
            }
            // return null;   // ???
            return CurrentContent;
        }

        public string CopyText(int position, string selectedText)
        {
            if(!String.IsNullOrEmpty(selectedText)) {
                var textClip = new TextClipStruct(selectedText);
                textLinearClipboard.Push(textClip);
            }
            // return null;   // ???
            return CurrentContent;
        }

        public string PasteText(int position)
        {
            if (textLinearClipboard.HasAny) {
                var textClip = textLinearClipboard.Peek();
                if (textClip != null) {
                    var text = textClip.Text;
                    if (!String.IsNullOrEmpty(text)) {
                        if (EditUndoRedoManager != null) {
                            return EditUndoRedoManager.InsertText(position, text);
                        } else {
                            // tbd:
                            return null;
                        }
                    }
                }
            }
            // return null;   // ???
            return CurrentContent;
        }

        public string PasteText(int position, string replacedText)
        {
            if (textLinearClipboard.HasAny) {
                var textClip = textLinearClipboard.Peek();
                if (textClip != null) {
                    var text = textClip.Text;
                    if (EditUndoRedoManager != null) {
                        if (String.IsNullOrEmpty(text)) {
                            return EditUndoRedoManager.DeleteText(position, replacedText);
                        } else {
                            return EditUndoRedoManager.ReplaceText(position, text, replacedText);
                        }
                    } else {
                        // tbd:
                        return null;
                    }
                }
            }
            // return null;   // ???
            return CurrentContent;
        }


    }
}
