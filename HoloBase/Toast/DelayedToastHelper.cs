﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Notifications;



namespace HoloBase.Toast
{
    public sealed class DelayedToastHelper
    {
        private static DelayedToastHelper instance = null;
        public static DelayedToastHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new DelayedToastHelper();
                }
                return instance;
            }
        }
        private DelayedToastHelper()
        {
        }


        // Default.
        public void ScheduleToast(DateTimeOffset deliveryTime, string content, string arg = null)
        {
            ScheduleToastText01(deliveryTime, content, arg);
        }

        public void ScheduleToastText01(DateTimeOffset deliveryTime, string content, string arg = null)
        {
            // show toast
            var toast = BuildToastText01(deliveryTime, content, arg);
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }

        public void ScheduleToastText02(DateTimeOffset deliveryTime, string title, string content, string arg = null)
        {
            // show toast
            var toast = BuildToastText02(deliveryTime, title, content, arg);
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }

        public void ScheduleToastText03(DateTimeOffset deliveryTime, string title, string content, string arg = null)
        {
            // show toast
            var toast = BuildToastText03(deliveryTime, title, content, arg);
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }

        public void ScheduleToastText04(DateTimeOffset deliveryTime, string title, string content, string content2, string arg = null)
        {
            // show toast
            var toast = BuildToastText04(deliveryTime, title, content, content2, arg);
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }

        public void ScheduleToastImageAndText01(DateTimeOffset deliveryTime, string image, string content, string arg = null)
        {
            // show toast
            var toast = BuildToastImageAndText01(deliveryTime, image, content, arg);
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }

        public void ScheduleToastImageAndText02(DateTimeOffset deliveryTime, string image, string title, string content, string arg = null)
        {
            // show toast
            var toast = BuildToastImageAndText02(deliveryTime, image, title, content, arg);
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }

        public void ScheduleToastImageAndText03(DateTimeOffset deliveryTime, string image, string title, string content, string arg = null)
        {
            // show toast
            var toast = BuildToastImageAndText03(deliveryTime, image, title, content, arg);
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }

        public void ScheduleToastImageAndText04(DateTimeOffset deliveryTime, string image, string title, string content, string content2, string arg = null)
        {
            // show toast
            var toast = BuildToastImageAndText04(deliveryTime, image, title, content, content2, arg);
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }


        public ScheduledToastNotification BuildToastText01(DateTimeOffset deliveryTime, string content, string arg = null)
        {
            // build toast
            var template = ToastTemplateType.ToastText01;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            var elements = xml.GetElementsByTagName("text");

            // content
            var text = xml.CreateTextNode(content);
            elements[0].AppendChild(text);

            // launch arg
            if (arg != null) {
                xml.DocumentElement.SetAttribute("launch", arg);
            }

            // toast
            return new ScheduledToastNotification(xml, deliveryTime);
        }

        public ScheduledToastNotification BuildToastText02(DateTimeOffset deliveryTime, string title, string content, string arg = null)
        {
            // build toast
            var template = ToastTemplateType.ToastText02;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            var elements = xml.GetElementsByTagName("text");

            // title
            var text = xml.CreateTextNode(title);
            elements[0].AppendChild(text);

            // content
            text = xml.CreateTextNode(content);
            elements[1].AppendChild(text);

            // launch arg
            if (arg != null) {
                xml.DocumentElement.SetAttribute("launch", arg);
            }

            // sh toast
            return new ScheduledToastNotification(xml, deliveryTime);
        }

        public ScheduledToastNotification BuildToastText03(DateTimeOffset deliveryTime, string title, string content, string arg = null)
        {
            // build toast
            var template = ToastTemplateType.ToastText03;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            var elements = xml.GetElementsByTagName("text");

            // title
            var text = xml.CreateTextNode(title);
            elements[0].AppendChild(text);

            // content
            text = xml.CreateTextNode(content);
            elements[1].AppendChild(text);

            // launch arg
            if (arg != null) {
                xml.DocumentElement.SetAttribute("launch", arg);
            }

            // show toast
            return new ScheduledToastNotification(xml, deliveryTime);
        }

        public ScheduledToastNotification BuildToastText04(DateTimeOffset deliveryTime, string title, string content, string content2, string arg = null)
        {
            // build toast
            var template = ToastTemplateType.ToastText02;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            var elements = xml.GetElementsByTagName("text");

            // title
            var text = xml.CreateTextNode(title);
            elements[0].AppendChild(text);

            // content
            text = xml.CreateTextNode(content);
            elements[1].AppendChild(text);

            // content2
            text = xml.CreateTextNode(content2);
            elements[2].AppendChild(text);

            // launch arg
            if (arg != null) {
                xml.DocumentElement.SetAttribute("launch", arg);
            }

            // toast
            return new ScheduledToastNotification(xml, deliveryTime);
        }

        public ScheduledToastNotification BuildToastImageAndText01(DateTimeOffset deliveryTime, string image, string content, string arg = null)
        {
            // build toast
            var template = ToastTemplateType.ToastText02;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            var elements = xml.GetElementsByTagName("text");

            // image
            elements[0].Attributes.First(x => x.LocalName.Equals("src")).InnerText = image;

            // content
            var text = xml.CreateTextNode(content);
            elements[1].AppendChild(text);

            // launch arg
            if (arg != null) {
                xml.DocumentElement.SetAttribute("launch", arg);
            }

            // show toast
            return new ScheduledToastNotification(xml, deliveryTime);
        }

        public ScheduledToastNotification BuildToastImageAndText02(DateTimeOffset deliveryTime, string image, string title, string content, string arg = null)
        {
            // build toast
            var template = ToastTemplateType.ToastText02;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            var elements = xml.GetElementsByTagName("text");

            // image
            elements[0].Attributes.First(x => x.LocalName.Equals("src")).InnerText = image;

            // title
            var text = xml.CreateTextNode(title);
            elements[1].AppendChild(text);

            // content
            text = xml.CreateTextNode(content);
            elements[2].AppendChild(text);

            // launch arg
            if (arg != null) {
                xml.DocumentElement.SetAttribute("launch", arg);
            }

            // show toast
            return new ScheduledToastNotification(xml, deliveryTime);
        }

        public ScheduledToastNotification BuildToastImageAndText03(DateTimeOffset deliveryTime, string image, string title, string content, string arg = null)
        {
            // build toast
            var template = ToastTemplateType.ToastText03;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            var elements = xml.GetElementsByTagName("text");

            // image
            elements[0].Attributes.First(x => x.LocalName.Equals("src")).InnerText = image;

            // title
            var text = xml.CreateTextNode(title);
            elements[1].AppendChild(text);

            // content
            text = xml.CreateTextNode(content);
            elements[2].AppendChild(text);

            // launch arg
            if (arg != null) {
                xml.DocumentElement.SetAttribute("launch", arg);
            }

            // show toast
            return new ScheduledToastNotification(xml, deliveryTime);
        }

        public ScheduledToastNotification BuildToastImageAndText04(DateTimeOffset deliveryTime, string image, string title, string content, string content2, string arg = null)
        {
            // build toast
            var template = ToastTemplateType.ToastText02;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            var elements = xml.GetElementsByTagName("text");

            // image
            elements[0].Attributes.First(x => x.LocalName.Equals("src")).InnerText = image;

            // title
            var text = xml.CreateTextNode(title);
            elements[1].AppendChild(text);

            // content
            text = xml.CreateTextNode(content);
            elements[2].AppendChild(text);

            // content2
            text = xml.CreateTextNode(content2);
            elements[3].AppendChild(text);

            // launch arg
            if (arg != null) {
                xml.DocumentElement.SetAttribute("launch", arg);
            }

            // show toast
            return new ScheduledToastNotification(xml, deliveryTime);
        }
    }
}
