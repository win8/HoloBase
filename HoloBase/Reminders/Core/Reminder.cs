﻿using HoloBase.Icons.Common;
using HoloBase.UI.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Reminders.Core
{
    public interface IReminder
    {
        // Note that some of these attributes may not be relevant/used in (many) situations...

        // tbd:
        // is this reminader Roaming?
        // --> Just use app-wide settings...
        // ..

        string name
        {
            get;
        }
        string message
        {
            get;
        }

        string imageUrl
        {
            get;
        }
        string audioUrl
        {
            get;
        }

        ColorStruct Color1
        {
            get;
        }
        ColorStruct Color2
        {
            get;
        }

        FontIconCode IconCode
        {
            get;
        }

        long shceduledTime
        {
            get;
        }
        uint duration
        {
            get;
        }
        // tbd:
        // repetitions??
        // ...

        // Snooze ???
        // ...


    }
}
