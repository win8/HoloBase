﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Reminders.Core
{
    public class ReminderIssuedEventArgs : EventArgs
    {
        private IReminder newReminder;
        public ReminderIssuedEventArgs(IReminder newReminder)
        {
            this.newReminder = newReminder;
        }

        public IReminder NewReminder
        {
            get
            {
                return newReminder;
            }
            set
            {
                newReminder = value;
            }
        }

    }
}
