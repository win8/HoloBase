﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Reminders.Core
{
    // The class does not have to be a control...
    public interface ReminderSourceControl
    {
        event EventHandler<ReminderIssuedEventArgs> ReminderIssued;
    }
}
