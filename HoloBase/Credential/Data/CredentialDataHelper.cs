﻿using HoloBase.Credential.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Credential.Data
{
    public sealed class CredentialDataHelper
    {
        private static CredentialDataHelper instance = null;
        public static CredentialDataHelper Instance
        {
            get
            {
                if (instance == null) {
                    instance = new CredentialDataHelper();
                }
                return instance;
            }
        }
        private CredentialDataHelper()
        {
        }


        public void StoreUserCredential(string resourceName, UserCredential userCredential)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            vault.Add(new Windows.Security.Credentials.PasswordCredential(
                resourceName, userCredential.Username, userCredential.Passwd));
        }

        public IDictionary<string, IList<UserCredential>> GetAllUserCredentials()
        {
            var map = new Dictionary<string, IList<UserCredential>>();
            var vault = new Windows.Security.Credentials.PasswordVault();
            var passwordCreds = vault.RetrieveAll();
            if(passwordCreds != null && passwordCreds.Any()) {
                foreach(var pc in passwordCreds) {
                    var resource = pc.Resource;
                    IList<UserCredential> list = null;
                    if (map.ContainsKey(resource) && map[resource] != null) {
                        list = map[resource];
                    } else {
                        list = new List<UserCredential>();
                        map[resource] = list;
                    }
                    var uc = new UserCredential(pc.UserName, pc.Password);  
                    list.Add(uc);
                }
            }
            return map;
        }
        public IList<UserCredential> GetUserCredentials(string resourceName)
        {
            var list = new List<UserCredential>();
            var vault = new Windows.Security.Credentials.PasswordVault();
            var passwordCreds = vault.FindAllByResource(resourceName);
            if (passwordCreds != null && passwordCreds.Any()) {
                foreach (var pc in passwordCreds) {
                    var uc = new UserCredential(pc.UserName, pc.Password);
                    list.Add(uc);
                }
            }
            return list;
        }
        public UserCredential GetUserCredential(string resourceName, string username)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            var passwordCred = vault.Retrieve(resourceName, username);
            var uc = new UserCredential(passwordCred.UserName, passwordCred.Password);
            return uc;
        }

        public void DeleteUserCredential(string resourceName, UserCredential userCredential)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            vault.Remove(new Windows.Security.Credentials.PasswordCredential(
                resourceName, userCredential.Username, userCredential.Passwd));
        }


    }
}
