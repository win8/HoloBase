﻿using HoloBase.Credential.ViewModels;
using HoloCore.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HoloBase.Credential
{
    public sealed partial class UserCredentialViewControl : UserControl, IRefreshableElement, IViewModelHolder
    {
        private CredentialViewViewModel viewModel;
        public UserCredentialViewControl()
        {
            this.InitializeComponent();

            ResetViewModel();
        }
        private void ResetViewModel()
        {
            var vm = new CredentialViewViewModel();

            ResetViewModel(vm);
        }
        private void ResetViewModel(CredentialViewViewModel viewModel)
        {
            this.viewModel = viewModel;

            this.DataContext = this.viewModel;
        }
        public void RefreshDataAndUI()
        {
            // TBD:
        }

        public IViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                var vm = (CredentialViewViewModel) value;
                // ????

                ResetViewModel(vm);
            }
        }

    
    }
}