﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HoloBase.Credential.Common
{
    public struct UserCredential
    {
        private string username;
        private string passwd;

        public UserCredential(string username, string passwd)
        {
            this.username = username;
            this.passwd = passwd;
        }

        public string Username
        {
            get
            {
                return username;
            }
            private set
            {
                username = value;
            }
        }
        public string Passwd
        {
            get
            {
                return passwd;
            }
            private set
            {
                passwd = value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Username:").Append(Username).Append("; ");
            sb.Append("Passwd:").Append("*");
            return sb.ToString();
        }


    }
}
